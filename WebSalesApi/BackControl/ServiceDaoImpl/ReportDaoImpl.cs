﻿using B1Utility.DataAccess;
using B1Utility.POCO;
using B1Utility.Utils;
using BackControl.Implement;
using BackControl.ServiceDao;
using CommonControl.Constants;
using CommonControl.POCO.Class;
using CommonControl.POCO.Common;
using CommonControl.POCO.Condition;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BackControl.ServiceDaoImpl
{
    public class ReportDaoImpl : IReportDao
    {
        public List<InventoryStatus> getReportInventoryStatus(ReportInventoryStatusCondition condition)
        {
            List<InventoryStatus> response = null;

            try
            {
                response = DBUtils.queryResultList<InventoryStatus>(QueryRegisterImpl.Instance.getQueryInventoryStatus(condition));
            }
            catch (Exception ex)
            {
                ExceptionUtils.handleException(ex);
            }

            return response;
        }

        public List<InventoryStatusDocument> getReportInventoryStatusDocument(ReportInventoryStatusCondition condition)
        {
            List<InventoryStatusDocument> response = null;

            try
            {
                response = DBUtils.queryResultList<InventoryStatusDocument>(QueryRegisterImpl.Instance.getQueryInventoryStatusDocument(condition));
            }
            catch (Exception ex)
            {
                ExceptionUtils.handleException(ex);
            }

            return response;
        }


        public List<BatchNumberTransactionReport> getReportBatchNumberTransaction(ReportBatchNumberTransactionCondition condition)
        {
            List<BatchNumberTransactionReport> response = null;

            try
            {
                response = DBUtils.queryResultList<BatchNumberTransactionReport>(QueryRegisterImpl.Instance.getQueryBatchNumberTransactionReport(condition));
            }
            catch (Exception ex)
            {
                ExceptionUtils.handleException(ex);
            }

            return response;
        }

        public List<BatchNumberTransactionReportDetail> getReportBatchNumberTransactionDetail(ReportBatchNumberTransactionDetailCondition condition)
        {
            List<BatchNumberTransactionReportDetail> response = null;

            try
            {
                response = DBUtils.queryResultList<BatchNumberTransactionReportDetail>(QueryRegisterImpl.Instance.getQueryBatchNumberTransactionReportDetail(condition));
            }
            catch (Exception ex)
            {
                ExceptionUtils.handleException(ex);
            }

            return response;
        }

        public List<CustomerPaymentReport> getReportCustomerPayment(ReportCustomerPaymentCondition condition)
        {
            List<CustomerPaymentReport> response = null;

            try
            {
                response = DBUtils.queryResultList<CustomerPaymentReport>(QueryRegisterImpl.Instance.getQueryCustomerPaymentReport(condition));
            }
            catch (Exception ex)
            {
                ExceptionUtils.handleException(ex);
            }

            return response;
        }

        public List<PurchaseOrderMovement> getReportPurchaseOrderMovement(ReportPurchaseOrderMovementCondition condition)
        {
            List<PurchaseOrderMovement> response = null;

            try
            {
                response = DBUtils.queryResultList<PurchaseOrderMovement>(QueryRegisterImpl.Instance.getQueryPurchaseOrderMovement(condition));
            }
            catch (Exception ex)
            {
                ExceptionUtils.handleException(ex);
            }

            return response;
        }

        public List<SalesAnalysisReport> getReportSalesAnalysis(SalesAnalysisCondition condition)
        {
            List<SalesAnalysisReport> response = null;

            try
            {
                List<SalesAnalysisTemp> resTemp = DBUtils.queryResultList<SalesAnalysisTemp>(QueryRegisterImpl.Instance.getQuerySalesAnalysis(condition));

                if (CollectionUtils.IsAny(resTemp))
                {
                    response = new List<SalesAnalysisReport>();

                    foreach(SalesAnalysisTemp sales in resTemp)
                    {
                        response.Add(sales.cloneMapping());
                    }
                }
            }
            catch (Exception ex)
            {
                ExceptionUtils.handleException(ex);
            }

            return response;
        }
    }
}
