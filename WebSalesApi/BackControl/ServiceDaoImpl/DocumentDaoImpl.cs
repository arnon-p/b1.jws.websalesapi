﻿using B1Utility.Constants;
using B1Utility.DataAccess;
using B1Utility.Enum;
using B1Utility.POCO;
using B1Utility.Utils;
using BackControl.Implement;
using BackControl.ServiceDao;
using CommonControl.Common;
using CommonControl.POCO.Class;
using CommonControl.POCO.Common;
using CommonControl.POCO.Condition;
using CommonControl.POCO.Enum;
using CommonControl.Utils;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace BackControl.ServiceDaoImpl
{
    public class DocumentDaoImpl : IDocumentDao
    {
        //private Object lockThis = new Object();
        private List<string> ignoreCopy;

        public DocumentDaoImpl()
        {
            ignoreCopy = new List<string>();
            ignoreCopy.Add("DocTotal");
        }

        public List<BusinessPartnerForSO> searchBusinessPartnerForSO(ref BusinessPartnerCondition condition)
        {
            List<BusinessPartnerForSO> response = null;

            try
            {
                response = DBUtils.queryResultList<BusinessPartnerForSO>(QueryRegisterImpl.Instance.getQueryBusinessPartnerForSO(condition));
                
                if (CollectionUtils.IsAny(response))
                {
                    // 20230423 Not Use
                    // 20190926 Overdue Debt
                    //Dictionary<string, double> overdueDebt = DBUtils.queryResultDictionary<string, double>(QueryRegisterImpl.Instance.getQueryBusinessPartnerOverdueDebt(condition.Company, response.Select(x => StringUtils.isBlankOrNull(x.FatherCard) ? x.CardCode : x.FatherCard).ToList()));

                    foreach (BusinessPartnerForSO bpSO in response)
                    {
                        BusinessPartnerCondition condAddr = new BusinessPartnerCondition();
                        condAddr.CardCode = bpSO.CardCode;
                        condAddr.Company = condition.Company;
                        List<BusinessPartnerAddress> bpAddrList = DBUtils.queryResultList<BusinessPartnerAddress>(QueryRegisterImpl.Instance.getQuerySearchBusinessPartnerAddressByCardCode(condAddr));

                        if (CollectionUtils.IsAny(bpAddrList))
                        {
                            bpSO.BpBillList = bpAddrList.Where(x => CommonControl.POCO.Enum.AddressType.B.Equals(x.AddressType)).ToList();
                            bpSO.BpShipList = bpAddrList.Where(x => CommonControl.POCO.Enum.AddressType.S.Equals(x.AddressType)).ToList();
                        }

                        bpSO.BpContactList = DBUtils.queryResultList<BusinessPartnerContact>(QueryRegisterImpl.Instance.getQuerySearchBusinessPartnerContactByCardCode(condAddr));

                        // 20230423 Not Use
                        // 20190926 Overdue Debt
                        //string cardCode = StringUtils.isBlankOrNull(bpSO.FatherCard) ? bpSO.CardCode : bpSO.FatherCard;
                        //if (overdueDebt.ContainsKey(cardCode))
                        //{
                        //    bpSO.OverdueDebt = overdueDebt[cardCode];
                        //}
                    }
                }

                // 23/07/2019 Query Row Count Paginable
                if (condition.Paginable != null)
                {
                    Paginable p = condition.Paginable.clone();
                    condition.Paginable = null;
                    p.RowCount = DBUtils.querySingleResult<int>(QueryRegisterImpl.Instance.BaseInstance.getQueryRowCount(QueryRegisterImpl.Instance.getQueryBusinessPartnerForSO(condition)));
                    condition.Paginable = p;
                }
            }
            catch (Exception ex)
            {
                ExceptionUtils.handleException(ex);
            }

            return response;
        }

        public List<ItemMasterForSO> searchItemMasterForSO(ref ItemMasterCondition condition)
        {
            List<ItemMasterForSO> response = null;

            try
            {
                response = DBUtils.queryResultList<ItemMasterForSO>(QueryRegisterImpl.Instance.getQueryItemMasterForSO(condition));

                if (CollectionUtils.IsAny(response))
                {
                    foreach (ItemMasterForSO itemSO in response)
                    {
                        itemSO.BarList = DBUtils.queryResultList<BarCode>(QueryRegisterImpl.Instance.getQueryItemBarCode(condition.Company, itemSO.ItemCode));
                        //itemSO.UomConvertList = DBUtils.queryResultList<UomGroupConverter>(QueryRegisterImpl.Instance.getQueryUomGroupConverter(condition.Company, itemSO.UgpEntry));
                        //List<UomGroupConverter> uomConvertList = DBUtils.queryResultList<UomGroupConverter>(QueryRegisterImpl.Instance.getQueryUomGroupConverter(condition.Company, itemSO.UgpEntry));
                        
                        ItemPriceCondition priceCond = new ItemPriceCondition();
                        priceCond.Company = condition.Company;
                        priceCond.Date = condition.Date;
                        priceCond.CardCode = condition.CardCode;
                        priceCond.ItemCode = itemSO.ItemCode;
                        priceCond.PriceList = condition.PriceListNum;
                        priceCond.Quantity = 1;
                        priceCond.UomEntry = itemSO.UomEntry;
                        ItemPrice itemPrice = getItemPrice(priceCond);

                        if (itemPrice != null)
                        {
                            itemSO.IsSpecialPrice = itemPrice.IsSpecialPrice;
                            itemSO.UnitPrice = itemPrice.UnitPrice;
                            itemSO.DiscountPercent = itemPrice.Discount;
                            itemSO.Price = itemPrice.Price;
                            //if (itemPrice.IsSpecialPrice)
                            //{
                            //    itemSO.UnitPrice = itemPrice.UnitPrice;
                            //    itemSO.DiscountPercent = itemPrice.Discount;
                            //    itemSO.IsSpecialPrice = itemPrice.IsSpecialPrice;
                            //    itemSO.Price = itemPrice.SpecialPrice;
                            //}
                            //else
                            //{
                            //    //itemSO.UnitPrice = (itemSO.UnitPrice * uomConvertList.Where(x => x.UomEntry.CompareTo(itemSO.UomEntry) == 0).FirstOrDefault().BaseQty);
                            //    itemSO.UnitPrice = itemPrice.UnitPrice;
                            //    itemSO.Price = itemPrice.Price;
                            //}
                        }
                    }
                }

                // 23/07/2019 Query Row Count Paginable
                if (condition.Paginable != null)
                {
                    Paginable p = condition.Paginable.clone();
                    condition.Paginable = null;
                    p.RowCount = DBUtils.querySingleResult<int>(QueryRegisterImpl.Instance.BaseInstance.getQueryRowCount(QueryRegisterImpl.Instance.getQueryItemMasterForSO(condition)));
                    condition.Paginable = p;
                }
            }
            catch (Exception ex)
            {
                ExceptionUtils.handleException(ex);
            }

            return response;
        }


        public ItemPrice getItemPrice(ItemPriceCondition condition)
        {
            ServiceDataAccess.Instance.getCompanyB1DataAccess(condition.Company);
            ItemPrice response = new ItemPrice();

            try
            {
                SAPbobsCOM.ItemPriceParams oItemPriceParams = B1DataAccess.Instance.CompanyService.GetDataInterface(SAPbobsCOM.CompanyServiceDataInterfaces.csdiItemPriceParams);
                oItemPriceParams.CardCode = condition.CardCode;
                oItemPriceParams.ItemCode = condition.ItemCode;
                oItemPriceParams.Date = condition.Date;
                oItemPriceParams.PriceList = condition.PriceList;
                oItemPriceParams.UoMEntry = condition.UomEntry;
                SAPbobsCOM.ItemPriceReturnParams oItemPriceReturnParams = B1DataAccess.Instance.CompanyService.GetItemPrice(oItemPriceParams);
                response.IsSpecialPrice = true;
                response.UnitPrice = oItemPriceReturnParams.Price;
                response.Discount = oItemPriceReturnParams.Discount;
                response.Price = response.Discount == 0 ? response.UnitPrice : Math.Round(oItemPriceReturnParams.Price - (oItemPriceReturnParams.Price * oItemPriceReturnParams.Discount / 100), 6);

                //SAPbobsCOM.SBObob oSBObob = B1DataAccess.Instance.Company.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoBridge);
                //SAPbobsCOM.Recordset oRsItemPrice = oSBObob.GetItemPrice(condition.CardCode, condition.ItemCode, condition.Quantity, condition.Date);
                //response.Price = oRsItemPrice.Fields.Item(0).Value;
                //response.UnitPrice = oRsItemPrice.Fields.Item(0).Value;

                //List<ItemSpecialPrice> specialList = DBUtils.queryResultList<ItemSpecialPrice>(QueryRegisterImpl.Instance.getQueryFindSpecialPrice(condition));

                //if (CollectionUtils.IsAny(specialList))
                //{
                //    ItemSpecialPrice itemPrice = null;

                //    if (specialList.Count == 1)
                //    {
                //        itemPrice = specialList.ElementAt(0);
                //    }
                //    else
                //    {
                //        itemPrice = specialList.Where(x => x.CardCode.Equals(condition.CardCode)).FirstOrDefault();
                //    }

                //    if (itemPrice != null)
                //    {
                //        response.IsSpecialPrice = true;
                //        if (itemPrice.SPP2Quantity > 0)
                //        {
                //            response.UnitPrice = itemPrice.UnitPrice;
                //            response.SpecialPrice = itemPrice.SPP2Price;
                //            response.Discount = itemPrice.SPP2Discount;
                //        }
                //        else
                //        {
                //            response.UnitPrice = itemPrice.UnitPrice;
                //            response.SpecialPrice = itemPrice.SPP1Price;
                //            response.Discount = itemPrice.SPP1Discount;
                //        }
                //    }
                //}
                //else
                //{
                //    List<UomGroupConverter> uomConvertList = DBUtils.queryResultList<UomGroupConverter>(QueryRegisterImpl.Instance.getQueryUomGroupConverter(condition.Company, condition.ItemCode));

                //    if (CollectionUtils.IsAny(uomConvertList))
                //    {
                //        UomGroupConverter uomConvert = uomConvertList.Where(x => x.UomEntry.CompareTo(condition.UomEntry) == 0).FirstOrDefault();

                //        if (uomConvert != null)
                //        {
                //            response.UnitPrice = (response.UnitPrice * uomConvert.BaseQty);
                //        }
                //    }
                //}
            }
            catch (Exception ex)
            {
                ExceptionUtils.handleException(ex);
            }

            return response;
        }


        public List<BatchNumberDetail> searchItemBatchNumberSelection(ItemMasterCondition condition)
        {
            List<BatchNumberDetail> response = null;

            try
            {
                response = DBUtils.queryResultList<BatchNumberDetail>(QueryRegisterImpl.Instance.getQueryItemBatchNumberSelection(condition));
            }
            catch (Exception ex)
            {
                ExceptionUtils.handleException(ex);
            }

            return response;
        }

        public List<ItemWhsOnHand> searchItemWhsOnHand(ItemMasterCondition condition)
        {
            List<ItemWhsOnHand> response = null;

            try
            {
                response = DBUtils.queryResultList<ItemWhsOnHand>(QueryRegisterImpl.Instance.getQueryItemWhsOnHand(condition));
            }
            catch (Exception ex)
            {
                ExceptionUtils.handleException(ex);
            }

            return response;
        }

        public List<SalesOrderResponse> createSalesOrderList(List<SalesOrder> objList)
        {
            List<SalesOrderResponse> response = new List<SalesOrderResponse>();

            try
            {
                foreach (SalesOrder so in objList)
                {
                    response.Add(createSalesOrder(so));
                }
            }
            catch (Exception ex)
            {
                ExceptionUtils.handleException(ex);
            }

            return response;
        }

        public SalesOrderResponse createSalesOrder(SalesOrder obj)
        {
            ServiceDataAccess.Instance.getCompanyB1DataAccess(obj.Company);

            //SAPbobsCOM.AdminInfo oAdminInfo = B1DataAccess.Instance.CompanyService.GetAdminInfo();
            //oAdminInfo.EnableApprovalProcedureInDI = SAPbobsCOM.BoYesNoEnum.tYES;
            //oAdminInfo.DocConfirmation = SAPbobsCOM.BoYesNoEnum.tYES;
            //B1DataAccess.Instance.CompanyService.UpdateAdminInfo(oAdminInfo);

            SalesOrderResponse response = new SalesOrderResponse();
            SAPbobsCOM.Documents oDocuments = null;
            SAPbobsCOM.Document_Lines oLines = null;
            SAPbobsCOM.BatchNumbers oBatchs = null;

            try
            {
                if (obj.IsDraft)
                {
                    oDocuments = B1DataAccess.Instance.Company.GetBusinessObject(SAPbobsCOM.BoObjectTypes.oDrafts);
                    oDocuments.DocObjectCode = SAPbobsCOM.BoObjectTypes.oOrders;
                }
                else
                {
                    oDocuments = B1DataAccess.Instance.Company.GetBusinessObject(SAPbobsCOM.BoObjectTypes.oOrders);
                }

                // find series
                //oDocuments.Series = DBUtils.querySingleResult<int>(QueryRegisterImpl.Instance.BaseInstance.getQueryCurrentSeries(oDocuments.DocObjectCodeEx, obj.DocDate.ToString(CommonControl.Constants.ServiceConstants.DATE_FORMAT)));
                
                ObjectUtils.copyPropertiesIncludeUDFToSapObject(obj, oDocuments, ignoreCopy);

                // UDF
                if (!StringUtils.isBlankOrNull(obj.VatBranch))
                {
                    oDocuments.UserFields.Fields.Item("U_ISS_VatBranch").Value = obj.VatBranch;
                }
                if (!StringUtils.isBlankOrNull(obj.BpTaxId))
                {
                    oDocuments.UserFields.Fields.Item("U_ISS_BpTaxId").Value = obj.BpTaxId;
                }
                if (!StringUtils.isBlankOrNull(obj.BpBrnCode))
                {
                    oDocuments.UserFields.Fields.Item("U_ISS_BpBrnCode").Value = obj.BpBrnCode;
                }
                if (!StringUtils.isBlankOrNull(obj.InvRemark))
                {
                    oDocuments.UserFields.Fields.Item("U_WGE_InvRemark").Value = obj.InvRemark;
                }
                // UDF

                oLines = oDocuments.Lines;

                foreach (SalesOrderLine line in obj.Lines)
                {
                    //ObjectUtils.copyPropertiesIncludeUDFToSapObject(line, oLines);
                    //oLines.UnitPrice = 0;
                    //oLines.Price = line.UnitPrice;
                    oLines.ItemCode = line.ItemCode;
                    oLines.Quantity = line.Quantity;
                    oLines.UnitPrice = line.UnitPrice;
                    oLines.DiscountPercent = line.DiscountPercent;
                    oLines.PriceAfterVAT = line.PriceAfterVat;
                    oLines.VatGroup = line.VatGroup;
                    oLines.UoMEntry = line.UomEntry;
                    oLines.WarehouseCode = line.WarehouseCode;

                    if (!StringUtils.isBlankOrNull(line.CostingCode))
                    {
                        oLines.CostingCode = line.CostingCode;
                    }

                    if (!StringUtils.isBlankOrNull(line.CostingCode2))
                    {
                        oLines.CostingCode2 = line.CostingCode2;
                    }

                    if (!StringUtils.isBlankOrNull(line.CostingCode3))
                    {
                        oLines.CostingCode3 = line.CostingCode3;
                    }

                    if (!StringUtils.isBlankOrNull(line.CostingCode4))
                    {
                        oLines.CostingCode4 = line.CostingCode4;
                    }

                    if (!StringUtils.isBlankOrNull(line.CostingCode5))
                    {
                        oLines.CostingCode5 = line.CostingCode5;
                    }

                    oLines.FreeText = line.FreeText;

                    // UDF
                    if (!StringUtils.isBlankOrNull(line.FreeSampling))
                    {
                        oLines.UserFields.Fields.Item("U_WGE_FS").Value = line.FreeSampling;
                    }
                    // UDF

                    oBatchs = oLines.BatchNumbers;

                    if (CollectionUtils.IsAny(line.Batchs))
                    {
                        foreach (BatchNumbers batch in line.Batchs)
                        {
                            oBatchs.BatchNumber = batch.BatchNumber;
                            oBatchs.Quantity = batch.Quantity;

                            oBatchs.Add();
                        }
                    }

                    oLines.Add();
                }

                if (CollectionUtils.IsAny(obj.Attachments))
                {
                    List<AttachInfo> attach = obj.Attachments.Where(x => !StringUtils.isBlankOrNull(x.TempKey)).ToList();

                    if (CollectionUtils.IsAny(attach))
                    {
                        List<AttachmentInfo> attachList = new List<AttachmentInfo>();
                        foreach (AttachInfo att in attach)
                        {
                            if (File.Exists(Path.Combine(att.SrcPath, att.NewFileName)))
                            {
                                attachList.Add(new AttachmentInfo(att.SrcPath, att.NewName, att.Ext));
                            }
                        }

                        if (CollectionUtils.IsAny(attachList))
                        {
                            oDocuments.AttachmentEntry = B1Utility.Implement.AttachmentImpl.Instance.createAttachment(attachList);
                        }
                    }
                }

                // Approval Process
                //if (!obj.IsDraft)
                //{
                //    if (oDocuments.GetApprovalTemplates() == 0 && oDocuments.Document_ApprovalRequests.ApprovalTemplatesID > 0)
                //    {
                //        //Your document will fire an approval procedure
                //        //If you want to add some remarks to your approval you can do this
                //        //for (int i = 0; i < oDocuments.Document_ApprovalRequests.Count; i++)
                //        //{
                //            oDocuments.Document_ApprovalRequests.SetCurrentLine(0);
                //            oDocuments.Document_ApprovalRequests.Remarks = "Sales Order from Web Sales";
                //        //}
                //    }
                //}

                if (TransactionUtils.checkSuccess(oDocuments.Add()))
                {
                    if (oDocuments.GetByKey(ParseUtils.tryParseInt(B1DataAccess.Instance.Company.GetNewObjectKey())))
                    {
                        response.Company = obj.Company;
                        response.DocEntry = oDocuments.DocEntry;
                        response.DocNum = oDocuments.DocNum;
                    }
                }
                else
                {
                    throw new Exception(B1DataAccess.Instance.Company.GetLastErrorDescription());
                }
            }
            catch (Exception ex)
            {
                ExceptionUtils.handleException(ex);
            }
            finally
            {
                ObjectUtils.finalReleaseComObject(oDocuments);
                ObjectUtils.finalReleaseComObject(oLines);
                ObjectUtils.finalReleaseComObject(oBatchs);
            }

            return response;
        }

        public List<SeriesDocument> searchSeriesByCondition(SeriesCondition condition)
        {
            List<SeriesDocument> response = null;

            try
            {
                response = DBUtils.queryResultList<SeriesDocument>(QueryRegisterImpl.Instance.getQuerySearchSeriesByCondition(condition));
            }
            catch (Exception ex)
            {
                ExceptionUtils.handleException(ex);
            }

            return response;
        }

        public List<SalesOrder> searchSalesOrderByCondition(ref SalesOrderCondition condition)
        {
            List<SalesOrder> response = null;

            try
            {
                response = DBUtils.queryResultList<SalesOrder>(QueryRegisterImpl.Instance.getQuerySearchSalesOrderByCondition(condition));

                // 23/07/2019 Query Row Count Paginable
                if (condition.Paginable != null)
                {
                    Paginable p = condition.Paginable.clone();
                    condition.Paginable = null;
                    p.RowCount = DBUtils.querySingleResult<int>(QueryRegisterImpl.Instance.BaseInstance.getQueryRowCount(QueryRegisterImpl.Instance.getQuerySearchSalesOrderByCondition(condition)));
                    condition.Paginable = p;
                }
            }
            catch (Exception ex)
            {
                ExceptionUtils.handleException(ex);
            }

            return response;
        }

        public SalesOrder searchSalesOrderByDocEntry(SalesOrderCondition condition)
        {
            SalesOrder response = null;

            try
            {
                response = DBUtils.querySingleResult<SalesOrder>(QueryRegisterImpl.Instance.getQuerySearchSalesOrderByDocEntry(condition));

                if (response != null)
                {
                    response.Relations = searchSalesOrderDocumentRelation(condition);

                    response.Lines = getSalesOrderLineByDocEntry(condition.Company, condition.IsDraft, condition.DocEntry);

                    if (CollectionUtils.IsAny(response.Lines))
                    {
                        List<BatchNumbers> batchList = getSalesOrderBatchLineByDocEntry(condition.Company, condition.IsDraft, condition.DocEntry);

                        if (CollectionUtils.IsAny(batchList))
                        {
                            foreach (SalesOrderLine soLine in response.Lines)
                            {
                                //soLine.UomConvertList = DBUtils.queryResultList<UomGroupConverter>(QueryRegisterImpl.Instance.getQueryUomGroupConverter(condition.Company, soLine.UgpEntry));
                                soLine.Batchs = batchList.Where(x => x.LineNum.CompareTo(soLine.LineNum) == 0).ToList();
                            }
                        }
                    }

                    BusinessPartnerCondition bpCondition = new BusinessPartnerCondition();
                    bpCondition.CardCode = response.CardCode;
                    bpCondition.Company = condition.Company;

                    List<BusinessPartnerAddress> bpAddrList = DBUtils.queryResultList<BusinessPartnerAddress>(QueryRegisterImpl.Instance.getQuerySearchBusinessPartnerAddressByCardCode(bpCondition));

                    if (CollectionUtils.IsAny(bpAddrList))
                    {
                        response.BpBillList = bpAddrList.Where(x => CommonControl.POCO.Enum.AddressType.B.Equals(x.AddressType)).ToList();
                        response.BpShipList = bpAddrList.Where(x => CommonControl.POCO.Enum.AddressType.S.Equals(x.AddressType)).ToList();
                    }

                    response.BpContactList = DBUtils.queryResultList<BusinessPartnerContact>(QueryRegisterImpl.Instance.getQuerySearchBusinessPartnerContactByCardCode(bpCondition));

                    response.Attachments = DBUtils.queryResultList<AttachInfo>(QueryRegisterImpl.Instance.getQuerySalesOrderAttachment(condition.Company, condition.IsDraft, response.DocEntry));
                }
            }
            catch (Exception ex)
            {
                ExceptionUtils.handleException(ex);
            }

            return response;
        }

        public List<SalesOrderLine> getSalesOrderLineByDocEntry(string company, bool isDraft, int docEntry)
        {
            List<SalesOrderLine> response = null;

            try
            {
                response = DBUtils.queryResultList<SalesOrderLine>(QueryRegisterImpl.Instance.getQuerySalesOrderLineByDocEntry(company, isDraft, docEntry));
            }
            catch (Exception ex)
            {
                ExceptionUtils.handleException(ex);
            }

            return response;
        }

        public List<BatchNumbers> getSalesOrderBatchLineByDocEntry(string company, bool isDraft, int docEntry)
        {
            List<BatchNumbers> response = null;

            try
            {
                if (isDraft)
                {
                    response = DBUtils.queryResultList<BatchNumbers>(QueryRegisterImpl.Instance.getQuerySalesOrderDraftBatchLineByDocEntry(company, docEntry));
                }
                else
                {
                    response = DBUtils.queryResultList<BatchNumbers>(QueryRegisterImpl.Instance.getQuerySalesOrderBatchLineByDocEntry(company, docEntry));
                }
            }
            catch (Exception ex)
            {
                ExceptionUtils.handleException(ex);
            }

            return response;
        }

        public List<DocumentRelation> searchSalesOrderDocumentRelation(SalesOrderCondition condition)
        {
            List<DocumentRelation> response = null;

            try
            {
                if (condition.DocEntry > 0)
                {
                    // Sales Order Draft
                    response = DBUtils.queryResultList<DocumentRelation>(QueryRegisterImpl.Instance.getQueryRelationDraftSalesOrder(condition));

                    // Sales Order
                    //if (CollectionUtils.IsAny(response))
                    //{
                    //    response.AddRange(DBUtils.queryResultList<DocumentRelation>(QueryRegisterImpl.Instance.getQueryRelationSalesOrder(condition)));
                    //}
                    //else
                    //{
                    //    response = DBUtils.queryResultList<DocumentRelation>(QueryRegisterImpl.Instance.getQueryRelationSalesOrder(condition));
                    //}

                    //if (CollectionUtils.IsAny(response))
                    //{
                        // Pick List
                        response.AddRange(DBUtils.queryResultList<DocumentRelation>(QueryRegisterImpl.Instance.getQueryRelationOrderPickList(condition)));

                        // Delivery Base Ref Sales Order
                        response.AddRange(DBUtils.queryResultList<DocumentRelation>(QueryRegisterImpl.Instance.getQueryRelationDelivery(condition)));

                        // Invoice Base Ref Sales Order
                        //response.AddRange(DBUtils.queryResultList<DocumentRelation>(QueryRegisterImpl.Instance.getQueryRelationOrderInvoice(condition)));

                        // Invoice Base Ref Delivery
                        List<DocumentRelation> doList = response.Where(x => ObjectTypeConstant.DELIVERY.Equals(x.ObjType)).ToList();
                        if(CollectionUtils.IsAny(doList))
                        {
                            List<DocumentRelation> temp = new List<DocumentRelation>();
                            foreach (DocumentRelation doc in doList)
                            {
                                temp.AddRange(DBUtils.queryResultList<DocumentRelation>(QueryRegisterImpl.Instance.getQueryRelationDeliveryInvoice(condition.Company, doc.DocEntry)));
                            }

                            for (int i = 0; i < temp.Count; i++)
                            {
                                DocumentRelation chk = response.Where(x => x.DocEntry.CompareTo(temp.ElementAt(i).DocEntry) == 0).Where(x => ObjectTypeConstant.AR_INVOICE.Equals(x.ObjType)).FirstOrDefault();
                                if (chk == null)
                                {
                                    response.Add(temp.ElementAt(i));
                                }
                            }
                        }

                        // Incoming Base Ref Invoice
                        List<DocumentRelation> invList = response.Where(x => ObjectTypeConstant.AR_INVOICE.Equals(x.ObjType)).ToList();
                        if (CollectionUtils.IsAny(invList))
                        {
                            List<DocumentRelation> temp = new List<DocumentRelation>();
                            foreach (DocumentRelation doc in invList)
                            {
                                temp.AddRange(DBUtils.queryResultList<DocumentRelation>(QueryRegisterImpl.Instance.getQueryRelationIncoming(condition.Company, doc.DocEntry)));
                            }

                            for (int i = 0; i < temp.Count; i++)
                            {
                                DocumentRelation chk = response.Where(x => x.DocEntry.CompareTo(temp.ElementAt(i).DocEntry) == 0).Where(x => ObjectTypeConstant.INCOMING_PAYMENT.Equals(x.ObjType)).FirstOrDefault();
                                if (chk == null)
                                {
                                    response.Add(temp.ElementAt(i));
                                }
                            }
                        }
                    //}
                }
            }
            catch (Exception ex)
            {
                ExceptionUtils.handleException(ex);
            }

            return response;
        }

        public List<OrderStatus> searchOrderStatusByCondition(ref ReportOrderStatusCondition condition)
        {
            List<OrderStatus> response = null;

            try
            {
                response = DBUtils.queryResultList<OrderStatus>(QueryRegisterImpl.Instance.searchOrderStatusByCondition(condition));

                // 23/07/2019 Query Row Count Paginable
                if (condition.Paginable != null)
                {
                    Paginable p = condition.Paginable.clone();
                    condition.Paginable = null;
                    p.RowCount = DBUtils.querySingleResult<int>(QueryRegisterImpl.Instance.BaseInstance.getQueryRowCount(QueryRegisterImpl.Instance.searchOrderStatusByCondition(condition)));
                    condition.Paginable = p;
                }
            }
            catch (Exception ex)
            {
                ExceptionUtils.handleException(ex);
            }

            return response;
        }

        public SalesOrderResponse updateSalesOrder(SalesOrder obj)
        {
            ServiceDataAccess.Instance.getCompanyB1DataAccess(obj.Company);

            //SAPbobsCOM.AdminInfo oAdminInfo = B1DataAccess.Instance.CompanyService.GetAdminInfo();
            //oAdminInfo.EnableApprovalProcedureInDI = SAPbobsCOM.BoYesNoEnum.tYES;
            //oAdminInfo.DocConfirmation = SAPbobsCOM.BoYesNoEnum.tYES;
            //B1DataAccess.Instance.CompanyService.UpdateAdminInfo(oAdminInfo);

            SalesOrderResponse response = new SalesOrderResponse();
            SAPbobsCOM.Documents oDocuments = null;
            SAPbobsCOM.Document_Lines oLines = null;
            SAPbobsCOM.BatchNumbers oBatchs = null;

            try
            {
                if (obj.IsDraft)
                {
                    oDocuments = B1DataAccess.Instance.Company.GetBusinessObject(SAPbobsCOM.BoObjectTypes.oDrafts);
                    //oDocuments.DocObjectCode = SAPbobsCOM.BoObjectTypes.oOrders;
                }
                else
                {
                    oDocuments = B1DataAccess.Instance.Company.GetBusinessObject(SAPbobsCOM.BoObjectTypes.oOrders);
                }

                if (oDocuments.GetByKey(obj.DocEntry))
                {
                    ObjectUtils.copyPropertiesIncludeUDFToSapObject(obj, oDocuments);

                    // UDF
                    oDocuments.UserFields.Fields.Item("U_ISS_VatBranch").Value = obj.VatBranch;
                    oDocuments.UserFields.Fields.Item("U_ISS_BpTaxId").Value = obj.BpTaxId;
                    oDocuments.UserFields.Fields.Item("U_ISS_BpBrnCode").Value = obj.BpBrnCode;
                    oDocuments.UserFields.Fields.Item("U_WGE_InvRemark").Value = obj.InvRemark;
                    // UDF

                    oLines = oDocuments.Lines;

                    foreach (SalesOrderLine line in obj.Lines)
                    {
                        if (line.LineNum > -1)
                        {
                            for (int i = 0; i < oLines.Count; i++)
                            {
                                oLines.SetCurrentLine(i);

                                if (line.LineNum.CompareTo(oLines.LineNum) == 0)
                                {
                                    ObjectUtils.copyPropertiesIncludeUDFToSapObject(line, oLines);

                                    oBatchs = oLines.BatchNumbers;

                                    if (CollectionUtils.IsAny(line.Batchs))
                                    {
                                        // clear batch
                                        for (int j = 0; j < oBatchs.Count; j++)
                                        {
                                            oBatchs.SetCurrentLine(j);
                                            oBatchs.BatchNumber = String.Empty;
                                            oBatchs.Quantity = 0;
                                        }

                                        foreach (BatchNumbers batch in line.Batchs)
                                        {
                                            oBatchs.BatchNumber = batch.BatchNumber;
                                            oBatchs.Quantity = batch.Quantity;

                                            oBatchs.Add();
                                        }
                                    }

                                    break;
                                }
                            }
                        }
                        else
                        {
                            ObjectUtils.copyPropertiesIncludeUDFToSapObject(line, oLines);

                            oBatchs = oLines.BatchNumbers;

                            if (CollectionUtils.IsAny(line.Batchs))
                            {
                                foreach (BatchNumbers batch in line.Batchs)
                                {
                                    oBatchs.BatchNumber = batch.BatchNumber;
                                    oBatchs.Quantity = batch.Quantity;

                                    oBatchs.Add();
                                }
                            }

                            oLines.Add();
                        }
                    }

                    if (CollectionUtils.IsAny(obj.Attachments))
                    {
                        List<AttachInfo> attach = obj.Attachments.Where(x => !StringUtils.isBlankOrNull(x.TempKey)).ToList();

                        if (CollectionUtils.IsAny(attach))
                        {
                            List<AttachmentInfo> attachList = new List<AttachmentInfo>();
                            foreach (AttachInfo att in attach)
                            {
                                if (File.Exists(Path.Combine(att.SrcPath, att.NewFileName)))
                                {
                                    attachList.Add(new AttachmentInfo(att.SrcPath, att.NewName, att.Ext));
                                }
                            }

                            if (CollectionUtils.IsAny(attachList))
                            {
                                B1Utility.Implement.AttachmentImpl.Instance.updateAttachment(oDocuments.AttachmentEntry, attachList);
                            }
                        }
                    }

                    // Approval Process
                    //if (!obj.IsDraft)
                    //{
                    //    oDocuments.GetApprovalTemplates();
                    //    if (oDocuments.Document_ApprovalRequests.Count > 0 && oDocuments.Document_ApprovalRequests.ApprovalTemplatesID > 0)
                    //    {
                    //        //Your document will fire an approval procedure
                    //        //If you want to add some remarks to your approval you can do this
                    //        oDocuments.Document_ApprovalRequests.SetCurrentLine(0);
                    //        oDocuments.Document_ApprovalRequests.Remarks = "Approval process from Web Sales.";
                    //    }
                    //}

                    if (TransactionUtils.checkSuccess(oDocuments.Update()))
                    {
                        response.Company = obj.Company;
                        response.DocEntry = oDocuments.DocEntry;
                        response.DocNum = oDocuments.DocNum;
                    }
                    else
                    {
                        throw new Exception(B1DataAccess.Instance.Company.GetLastErrorDescription());
                    }
                }
                else
                {
                    throw new Exception(StringUtils.format("Sales Order Entry {0} not found.", obj.DocEntry));
                }
            }
            catch (Exception ex)
            {
                ExceptionUtils.handleException(ex);
            }
            finally
            {
                ObjectUtils.finalReleaseComObject(oDocuments);
                ObjectUtils.finalReleaseComObject(oLines);
                ObjectUtils.finalReleaseComObject(oBatchs);
            }

            return response;
        }

        public List<SalesOrderResponse> updateSalesOrderList(List<SalesOrder> objList)
        {
            List<SalesOrderResponse> response = new List<SalesOrderResponse>();

            try
            {
                foreach (SalesOrder so in objList)
                {
                    response.Add(updateSalesOrder(so));
                }
            }
            catch (Exception ex)
            {
                ExceptionUtils.handleException(ex);
            }

            return response;
        }

        public ApproveDocumentInfo convertDocumentWithoutApprove(ApproveDocumentInfo obj)
        {
            ServiceDataAccess.Instance.getCompanyB1DataAccess(obj.Company);

            ApproveDocumentInfo response = new ApproveDocumentInfo();
            SAPbobsCOM.Documents oDocuments = null;

            try
            {
                oDocuments = B1DataAccess.Instance.Company.GetBusinessObject(SAPbobsCOM.BoObjectTypes.oDrafts);

                if (oDocuments.GetByKey(obj.DocEntry))
                {
                    if (TransactionUtils.checkSuccess(oDocuments.SaveDraftToDocument()))
                    {
                        oDocuments = B1DataAccess.Instance.Company.GetBusinessObject(SAPbobsCOM.BoObjectTypes.oOrders);
                        if (oDocuments.GetByKey(ParseUtils.tryParseInt(B1DataAccess.Instance.Company.GetNewObjectKey())))
                        {
                            response.Company = obj.Company;
                            response.ObjType = oDocuments.DocObjectCodeEx;
                            response.DocEntry = oDocuments.DocEntry;
                            response.DocNum = oDocuments.DocNum;
                            response.DraftEntry = obj.DocEntry;
                            response.WddStatus = "Y";

                            //DBUtils.executeUpdate(QueryRegisterImpl.Instance.getQueryUpdateOAWS(response), true);
                        }
                    }
                    else
                    {
                        throw new Exception(B1DataAccess.Instance.Company.GetLastErrorDescription());
                    }
                }
                else
                {
                    throw new Exception(StringUtils.format("Sales Order Entry {0} not found.", obj.DocEntry));
                }
            }
            catch (Exception ex)
            {
                ExceptionUtils.handleException(ex);
            }
            finally
            {
                ObjectUtils.finalReleaseComObject(oDocuments);
            }

            return response;
        }

        public List<EmailSenderInfo> getApproverEmail(string company, int docEntry)
        {
            List<EmailSenderInfo> response = null;

            try
            {
                response = DBUtils.queryResultList<EmailSenderInfo>(QueryRegisterImpl.Instance.getQueryApproverEmail(company, docEntry));
            }
            catch (Exception ex)
            {
                ExceptionUtils.handleException(ex);
            }

            return response;
        }

        public List<ApproveDocumentInfo> approveDocument(List<ApproveSalesOrderResult> obj)
        {
            // 20200924 add db transaction
            ServiceDataAccess.Instance.getCompanyB1DataAccess(obj.ElementAt(0).Company);
            List<ApproveDocumentInfo> response = new List<ApproveDocumentInfo>();

            try
            {
                if (!B1DataAccess.Instance.Company.InTransaction)
                {
                    B1DataAccess.Instance.Company.StartTransaction();
                }

                //DBUtils.executeUpdate(QueryRegisterImpl.Instance.getQueryUpdateAWS1(obj), true);

                foreach (ApproveSalesOrderResult so in obj)
                {
                    DBUtils.executeUpdate(QueryRegisterImpl.Instance.getQueryUpdateAWS1(so), true);
                    int chkRowUpdate = DBUtils.querySingleResult<int>(QueryRegisterImpl.Instance.getQueryCheckUpdateAWS1(so), true);
                    
                    //try
                    //{
                    ApproveDocumentInfo apprRes = null;
                    // check approve or reject
                    List<CheckApproveDocumentInfo> apprInfo = DBUtils.queryResultList<CheckApproveDocumentInfo>(QueryRegisterImpl.Instance.getQueryCheckApproveDocumentInfo(obj.ElementAt(0).Company, so.DocEntry), true);

                    //int rejected = apprInfo.Where(x => x.MaxRejReqr <= x.CntRejected).Count();
                    //int approved = apprInfo.Where(x => x.MaxReqr <= x.CntApproved).Count();

                    int approved = apprInfo.Where(x => x.MaxReqr <= x.CntApproved).Count();
                    int partialApproved = apprInfo.Where(x => x.MaxReqr > x.CntApproved && x.MaxRejReqr > x.CntRejected && x.TotalStage > x.CurrentStage && x.Code.CompareTo(so.ParentKey) == 0).Count();
                    int rejected = apprInfo.Where(x => x.MaxRejReqr <= x.CntRejected).Count();

                    if (rejected > 0)
                    {
                        // N = rejected
                        apprRes = new ApproveDocumentInfo();
                        apprRes.Company = so.Company;
                        apprRes.DocEntry = so.DocEntry;
                        apprRes.DraftEntry = so.DocEntry;
                        apprRes.ParentKey = so.ParentKey;
                        apprRes.Code = so.Code;
                        apprRes.WddStatus = CommonControl.Constants.ServiceConstants.STATUS_REJECT;
                        apprRes.IndexApprove = 1;
                        apprRes.UserId = so.UserId;
                        apprRes.EmpId = so.EmpId;
                        apprRes.ExtEmpId = so.ExtEmpId;
                        response.Add(apprRes);

                        DBUtils.executeUpdate(QueryRegisterImpl.Instance.getQueryUpdateOAWS(apprRes), true);

                        // update status W = Pending to C = Close
                        DBUtils.executeUpdate(QueryRegisterImpl.Instance.getQueryUpdateOAWSClose(apprRes), true);
                    }
                    else if (approved.CompareTo(apprInfo.Count) == 0)
                    {
                        // Y = approved
                        int draftConverted = DBUtils.querySingleResult<int>(QueryRegisterImpl.Instance.getQueryCheckDraftConverted(so.Company, so.DocEntry), true);
                        if (draftConverted == 0)
                        {
                            try
                            {
                                apprRes = checkConvertApproveDocument(so);
                            }
                            catch (Exception apprEx)
                            {
                                apprRes = new ApproveDocumentInfo();
                                apprRes.IsError = true;
                                apprRes.ErrorMessage = apprEx.Message;
                                apprRes.DraftEntry = so.DocEntry;
                            }

                            apprRes.Company = so.Company;
                            //apprRes.DocEntry = so.DocEntry;
                            //apprRes.DraftEntry = so.DocEntry;
                            apprRes.ParentKey = so.ParentKey;
                            apprRes.Code = so.Code;
                            apprRes.WddStatus = CommonControl.Constants.ServiceConstants.STATUS_APPROVE;
                            apprRes.IndexApprove = 2;
                            apprRes.UserId = so.UserId;
                            apprRes.EmpId = so.EmpId;
                            apprRes.ExtEmpId = so.ExtEmpId;

                            DBUtils.executeUpdate(QueryRegisterImpl.Instance.getQueryUpdateOAWS(apprRes), true);

                            // update status W = Pending to C = Close
                            DBUtils.executeUpdate(QueryRegisterImpl.Instance.getQueryUpdateOAWSClose(apprRes), true);

                            if (chkRowUpdate == 0)
                            {
                                //apprRes.IsError = true;
                                //apprRes.ErrorMessage = String.Format("Record not found (AWS1). {0}", JsonUtils.toJson(so));
                                apprRes.ErrorMessage = "Record not found (AWS1)";
                                //apprRes.DraftEntry = so.DocEntry;
                            }

                            response.Add(apprRes);
                        }
                    }
                    else if (partialApproved > 0)
                    {
                        // W (Pending) = next stage
                        apprRes = new ApproveDocumentInfo();
                        apprRes.Company = so.Company;
                        apprRes.DocEntry = so.DocEntry;
                        apprRes.DraftEntry = so.DocEntry;
                        apprRes.ParentKey = so.ParentKey;
                        apprRes.Code = so.Code;
                        apprRes.WddStatus = CommonControl.Constants.ServiceConstants.STATUS_PENDING;
                        apprRes.IsNextStage = true;
                        apprRes.IndexApprove = 3;
                        apprRes.UserId = so.UserId;
                        apprRes.EmpId = so.EmpId;
                        apprRes.ExtEmpId = so.ExtEmpId;
                        response.Add(apprRes);

                        DBUtils.executeUpdate(QueryRegisterImpl.Instance.getQueryUpdateOAWSNextStage(apprRes), true);
                    }
                    else
                    {
                        // W (Pending) = not complete
                        apprRes = new ApproveDocumentInfo();
                        apprRes.Company = so.Company;
                        apprRes.DocEntry = so.DocEntry;
                        apprRes.DraftEntry = so.DocEntry;
                        apprRes.ParentKey = so.ParentKey;
                        apprRes.Code = so.Code;
                        apprRes.WddStatus = CommonControl.Constants.ServiceConstants.STATUS_PENDING;
                        apprRes.Approved = approved;
                        apprRes.CntApproved = apprInfo.Count;
                        apprRes.IndexApprove = 4;
                        apprRes.UserId = so.UserId;
                        apprRes.EmpId = so.EmpId;
                        apprRes.ExtEmpId = so.ExtEmpId;
                        response.Add(apprRes);

                        int partialComplete = apprInfo.Where(x => x.Code.CompareTo(so.ParentKey) == 0 && x.TotalStage.CompareTo(x.CurrentStage) == 0 && x.MaxReqr <= x.CntApproved && x.MaxRejReqr > x.CntRejected).Count();
                        if (partialComplete > 0)
                        {
                            apprRes = new ApproveDocumentInfo();
                            apprRes.Company = so.Company;
                            apprRes.DocEntry = so.DocEntry;
                            apprRes.DraftEntry = so.DocEntry;
                            apprRes.ParentKey = so.ParentKey;
                            apprRes.Code = so.Code;
                            apprRes.WddStatus = so.WddStatus;
                            apprRes.UserId = so.UserId;
                            apprRes.EmpId = so.EmpId;
                            apprRes.ExtEmpId = so.ExtEmpId;

                            DBUtils.executeUpdate(QueryRegisterImpl.Instance.getQueryUpdateOAWS(apprRes), true);
                        }
                    }
                    //}
                    //catch(Exception ex)
                    //{
                    //    ExceptionUtils.handleExceptionDoNothingLog(ex);
                    //}
                }

                // 20200924 add db transaction
                B1DataAccess.Instance.Company.EndTransaction(SAPbobsCOM.BoWfTransOpt.wf_Commit);
            }
            catch (Exception ex)
            {
                // 20200924 add db transaction
                if (B1DataAccess.Instance.Company.InTransaction)
                {
                    B1DataAccess.Instance.Company.EndTransaction(SAPbobsCOM.BoWfTransOpt.wf_RollBack);
                }

                ExceptionUtils.handleException(ex);
            }

            return response;
        }

        public ApproveDocumentInfo checkConvertApproveDocument(ApproveSalesOrderResult obj)
        {
            ServiceDataAccess.Instance.getCompanyB1DataAccess(obj.Company);

            ApproveDocumentInfo response = new ApproveDocumentInfo();
            SAPbobsCOM.Documents oDocuments = null;

            try
            {
                oDocuments = B1DataAccess.Instance.Company.GetBusinessObject(SAPbobsCOM.BoObjectTypes.oDrafts);

                if (oDocuments.GetByKey(obj.DocEntry))
                {
                    if (TransactionUtils.checkSuccess(oDocuments.SaveDraftToDocument()))
                    {
                        oDocuments = B1DataAccess.Instance.Company.GetBusinessObject(SAPbobsCOM.BoObjectTypes.oOrders);
                        if (oDocuments.GetByKey(ParseUtils.tryParseInt(B1DataAccess.Instance.Company.GetNewObjectKey())))
                        {
                            response.Company = obj.Company;
                            response.ObjType = oDocuments.DocObjectCodeEx;
                            response.DocEntry = oDocuments.DocEntry;
                            response.DocNum = oDocuments.DocNum;
                            response.DraftEntry = obj.DocEntry;
                        }
                    }
                    else
                    {
                        throw new Exception(B1DataAccess.Instance.Company.GetLastErrorDescription());
                    }
                }
                else
                {
                    throw new Exception(StringUtils.format("Sales Order Entry {0} not found.", obj.DocEntry));
                }
            }
            catch (Exception ex)
            {
                ExceptionUtils.handleException(ex);
            }
            finally
            {
                ObjectUtils.finalReleaseComObject(oDocuments);
            }

            return response;
        }

        public List<ApproveSalesOrder> searchApprovalSalesOrderByCondition(ref ApproveSalesOrderCondition condition)
        {
            List<ApproveSalesOrder> response = null;

            try
            {
                response = DBUtils.queryResultList<ApproveSalesOrder>(QueryRegisterImpl.Instance.getQuerySearchApprovalSalesOrderByCondition(condition));

                // 23/07/2019 Query Row Count Paginable
                if (condition.Paginable != null)
                {
                    Paginable p = condition.Paginable.clone();
                    condition.Paginable = null;
                    p.RowCount = DBUtils.querySingleResult<int>(QueryRegisterImpl.Instance.BaseInstance.getQueryRowCount(QueryRegisterImpl.Instance.getQuerySearchApprovalSalesOrderByCondition(condition)));
                    condition.Paginable = p;
                }
            }
            catch (Exception ex)
            {
                ExceptionUtils.handleException(ex);
            }

            return response;
        }

        public EmailSenderInfo getOriginatorEmail(string company, int docEntry)
        {
            EmailSenderInfo response = null;

            try
            {
                response = DBUtils.querySingleResult<EmailSenderInfo>(QueryRegisterImpl.Instance.getQueryOriginatorEmail(company, docEntry));
            }
            catch (Exception ex)
            {
                ExceptionUtils.handleException(ex);
            }

            return response;
        }

        public List<SalesPriceHistory> searchSalesPriceHistoryByCondition(SalesPriceHistoryCondition condition)
        {
            List<SalesPriceHistory> response = null;

            try
            {
                response = DBUtils.queryResultList<SalesPriceHistory>(QueryRegisterImpl.Instance.getQuerySearchSalesPriceHistory(condition));
            }
            catch (Exception ex)
            {
                ExceptionUtils.handleException(ex);
            }

            return response;
        }

        public List<GrossProfitInfo> searchGrossProfitByCondition(GrossProfitCondition condition)
        {
            List<GrossProfitInfo> response = null;

            try
            {
                response = DBUtils.queryResultList<GrossProfitInfo>(QueryRegisterImpl.Instance.getQuerySearchGrossProfitByCondition(condition));
            }
            catch (Exception ex)
            {
                ExceptionUtils.handleException(ex);
            }

            return response;
        }

        public int checkApprove(string company, int docEntry)
        {
            int response = -1;

            try
            {
                response = DBUtils.querySingleResult<int>(QueryRegisterImpl.Instance.getQueryCheckApprove(company, docEntry));
            }
            catch (Exception ex)
            {
                ExceptionUtils.handleException(ex);
            }

            return response;
        }
    }
}
