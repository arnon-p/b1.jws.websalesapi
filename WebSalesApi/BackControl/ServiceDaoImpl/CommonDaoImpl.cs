﻿using B1Utility.DataAccess;
using B1Utility.POCO;
using B1Utility.Utils;
using BackControl.Implement;
using BackControl.ServiceDao;
using CommonControl.Constants;
using CommonControl.POCO.Class;
using CommonControl.POCO.Common;
using CommonControl.POCO.Condition;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BackControl.ServiceDaoImpl
{
    public class CommonDaoImpl : ICommonDao
    {
        public List<SalesEmployee> getSalesEmployee(string company)
        {
            List<SalesEmployee> response = null;

            try
            {
                response = DBUtils.queryResultList<SalesEmployee>(QueryRegisterImpl.Instance.getQuerySalesEmployee(company));
            }
            catch (Exception ex)
            {
                ExceptionUtils.handleException(ex);
            }

            return response;
        }

        public List<PaymentTerms> getPaymentTerms(string company)
        {
            List<PaymentTerms> response = null;

            try
            {
                response = DBUtils.queryResultList<PaymentTerms>(QueryRegisterImpl.Instance.getQueryPaymentTerms(company));
            }
            catch (Exception ex)
            {
                ExceptionUtils.handleException(ex);
            }

            return response;
        }

        public List<BusinessPartnerGroup> getBusinessPartnerGroup(string company)
        {
            List<BusinessPartnerGroup> response = null;

            try
            {
                response = DBUtils.queryResultList<BusinessPartnerGroup>(QueryRegisterImpl.Instance.getQueryBusinessPartnerGroup(company));
            }
            catch (Exception ex)
            {
                ExceptionUtils.handleException(ex);
            }

            return response;
        }

        public List<ShippingType> getShippingType(string company)
        {
            List<ShippingType> response = null;

            try
            {
                response = DBUtils.queryResultList<ShippingType>(QueryRegisterImpl.Instance.getQueryShippingType(company));
            }
            catch (Exception ex)
            {
                ExceptionUtils.handleException(ex);
            }

            return response;
        }

        public List<ItemGroup> getItemGroup(string company)
        {
            List<ItemGroup> response = null;

            try
            {
                response = DBUtils.queryResultList<ItemGroup>(QueryRegisterImpl.Instance.getQueryItemGroup(company));
            }
            catch (Exception ex)
            {
                ExceptionUtils.handleException(ex);
            }

            return response;
        }

        public List<string> getWhse(string company)
        {
            List<string> response = null;

            try
            {
                response = DBUtils.queryResultList<string>(QueryRegisterImpl.Instance.getQueryWhse(company));
            }
            catch (Exception ex)
            {
                ExceptionUtils.handleException(ex);
            }

            return response;
        }


        public List<Properties> getProperties(string company)
        {
            List<Properties> response = null;

            try
            {
                response = DBUtils.queryResultList<Properties>(QueryRegisterImpl.Instance.getQueryProperties(company));
            }
            catch (Exception ex)
            {
                ExceptionUtils.handleException(ex);
            }

            return response;
        }


        public List<Country> getCountry(string company)
        {
            List<Country> response = null;

            try
            {
                response = DBUtils.queryResultList<Country>(QueryRegisterImpl.Instance.getQueryCountry(company));
            }
            catch (Exception ex)
            {
                ExceptionUtils.handleException(ex);
            }

            return response;
        }


        public List<TaxGroup> getTaxGroup(string company)
        {
            List<TaxGroup> response = null;

            try
            {
                response = DBUtils.queryResultList<TaxGroup>(QueryRegisterImpl.Instance.getQueryTaxGroup(company));
            }
            catch (Exception ex)
            {
                ExceptionUtils.handleException(ex);
            }

            return response;
        }


        public List<State> getState(string company)
        {
            List<State> response = null;

            try
            {
                response = DBUtils.queryResultList<State>(QueryRegisterImpl.Instance.getQueryState(company));
            }
            catch (Exception ex)
            {
                ExceptionUtils.handleException(ex);
            }

            return response;
        }


        public List<Dimension> getDimension(string company)
        {
            List<Dimension> response = null;

            try
            {
                response = DBUtils.queryResultList<Dimension>(QueryRegisterImpl.Instance.getQueryDimension(company));
            }
            catch (Exception ex)
            {
                ExceptionUtils.handleException(ex);
            }

            return response;
        }

        public List<PriceList> getPriceList(string company)
        {
            List<PriceList> response = null;

            try
            {
                response = DBUtils.queryResultList<PriceList>(QueryRegisterImpl.Instance.getQueryPriceList(company));
            }
            catch (Exception ex)
            {
                ExceptionUtils.handleException(ex);
            }

            return response;
        }

        public List<Brand> getBrand(string company)
        {
            List<Brand> response = null;

            try
            {
                response = DBUtils.queryResultList<Brand>(QueryRegisterImpl.Instance.getQueryBrand(company));
            }
            catch (Exception ex)
            {
                ExceptionUtils.handleException(ex);
            }

            return response;
        }

        public List<LocationAddress> getLocationAddress(string company)
        {
            List<LocationAddress> response = null;

            try
            {
                response = DBUtils.queryResultList<LocationAddress>(QueryRegisterImpl.Instance.getQueryLocationAddress(company));
            }
            catch (Exception ex)
            {
                ExceptionUtils.handleException(ex);
            }

            return response;
        }

        public List<UomGroupConverter> getUomGroupConverter(string company)
        {
            List<UomGroupConverter> response = null;

            try
            {
                response = DBUtils.queryResultList<UomGroupConverter>(QueryRegisterImpl.Instance.getQueryUomGroupConverter(company));
            }
            catch (Exception ex)
            {
                ExceptionUtils.handleException(ex);
            }

            return response;
        }

        public WebAdminSAPProfile getSapProfile(string company, WebAdminProfile obj)
        {
            WebAdminSAPProfile response = null;

            try
            {
                response = DBUtils.querySingleResult<WebAdminSAPProfile>(QueryRegisterImpl.Instance.getQuerySAPProfile(company, obj));

                if (response != null)
                {
                    response.TeamId = DBUtils.queryResultList<int>(QueryRegisterImpl.Instance.getQuerySAPProfileTeam(company, response.EmpId));
                    response.SalesEmployee = new List<SalesEmployee>();
                    
                    // position 1 = sales
                    if (CollectionUtils.IsAny(response.TeamId) && !"Sale".Equals(response.PosName))
                    {
                        response.SalesEmployee.AddRange(DBUtils.queryResultList<SalesEmployee>(QueryRegisterImpl.Instance.getQuerySAPProfileSalesEmployee(company, response.TeamId)));
                    }
                    else
                    {
                        response.SalesEmployee.Add(new SalesEmployee(response.SlpCode, response.SlpName));
                    }
                }
            }
            catch (Exception ex)
            {
                ExceptionUtils.handleException(ex);
            }

            return response;
        }
    }
}
