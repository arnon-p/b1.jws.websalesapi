﻿using B1Utility.DataAccess;
using B1Utility.POCO;
using B1Utility.Utils;
using BackControl.Implement;
using BackControl.ServiceDao;
using CommonControl.Constants;
using CommonControl.POCO.Class;
using CommonControl.POCO.Condition;
using CommonControl.POCO.Enum;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BackControl.ServiceDaoImpl
{
    public class MasterDaoImpl : IMasterDao
    {
        private List<string> ignoreBp;

        public MasterDaoImpl()
        {
            ignoreBp = new List<string>();
            ignoreBp.Add("CardType");
        }

        public List<ItemMaster> searchItemMasterByCondition(ref ItemMasterCondition condition)
        {
            List<ItemMaster> response = null;

            try
            {
                response = DBUtils.queryResultList<ItemMaster>(QueryRegisterImpl.Instance.getQuerySearchItemMasterByCondition(condition));

                // 23/07/2019 Query Row Count Paginable
                if (condition.Paginable != null)
                {
                    Paginable p = condition.Paginable.clone();
                    condition.Paginable = null;
                    p.RowCount = DBUtils.querySingleResult<int>(QueryRegisterImpl.Instance.BaseInstance.getQueryRowCount(QueryRegisterImpl.Instance.getQuerySearchItemMasterByCondition(condition)));
                    condition.Paginable = p;
                }
            }
            catch (Exception ex)
            {
                ExceptionUtils.handleException(ex);
            }

            return response;
        }


        public ItemMaster searchItemMasterByItemCode(ItemMasterCondition condition)
        {
            ItemMaster response = null;

            try
            {
                response = DBUtils.querySingleResult<ItemMaster>(QueryRegisterImpl.Instance.getQuerySearchItemMasterByItemCode(condition));

                if (response != null)
                {
                    response.OnHandList = DBUtils.queryResultList<ItemOnHand>(QueryRegisterImpl.Instance.getQuerySearchItemOnHandByItemCode(condition));
                }
            }
            catch (Exception ex)
            {
                ExceptionUtils.handleException(ex);
            }

            return response;
        }

        public List<ItemOnHand> searchItemOnHandByItemCode(ItemMasterCondition condition)
        {
            List<ItemOnHand> response = null;

            try
            {
                response = DBUtils.queryResultList<ItemOnHand>(QueryRegisterImpl.Instance.getQuerySearchItemOnHandByItemCode(condition));
            }
            catch (Exception ex)
            {
                ExceptionUtils.handleException(ex);
            }

            return response;
        }


        public List<BusinessPartner> searchBusinessPartnerByCondition(ref BusinessPartnerCondition condition)
        {
            List<BusinessPartner> response = null;

            try
            {
                response = DBUtils.queryResultList<BusinessPartner>(QueryRegisterImpl.Instance.getQuerySearchBusinessPartnerByCondition(condition));

                // 23/07/2019 Query Row Count Paginable
                if (condition.Paginable != null)
                {
                    Paginable p = condition.Paginable.clone();
                    condition.Paginable = null;
                    p.RowCount = DBUtils.querySingleResult<int>(QueryRegisterImpl.Instance.BaseInstance.getQueryRowCount(QueryRegisterImpl.Instance.getQuerySearchBusinessPartnerByCondition(condition)));
                    condition.Paginable = p;
                }
            }
            catch (Exception ex)
            {
                ExceptionUtils.handleException(ex);
            }

            return response;
        }

        public BusinessPartner searchBusinessPartnerByCardCode(BusinessPartnerCondition condition)
        {
            BusinessPartner response = null;

            try
            {
                response = DBUtils.querySingleResult<BusinessPartner>(QueryRegisterImpl.Instance.getQuerySearchBusinessPartnerByCardCode(condition));

                if (response != null)
                {
                    List<BusinessPartnerAddress> bpAddrList = DBUtils.queryResultList<BusinessPartnerAddress>(QueryRegisterImpl.Instance.getQuerySearchBusinessPartnerAddressByCardCode(condition));

                    if (CollectionUtils.IsAny(bpAddrList))
                    {
                        response.BpBillList = bpAddrList.Where(x => AddressType.B.Equals(x.AddressType)).ToList();
                        response.BpShipList = bpAddrList.Where(x => AddressType.S.Equals(x.AddressType)).ToList();
                    }

                    response.BpContactList = DBUtils.queryResultList<BusinessPartnerContact>(QueryRegisterImpl.Instance.getQuerySearchBusinessPartnerContactByCardCode(condition));

                    ServiceDataAccess.Instance.getCompanyB1DataAccess(condition.Company);
                    SAPbobsCOM.BusinessPartners oBp = B1DataAccess.Instance.Company.GetBusinessObject(SAPbobsCOM.BoObjectTypes.oBusinessPartners);
                    
                    if (oBp.GetByKey(condition.CardCode))
                    {
                        response.PropertyList = new List<bool>();
                        for (int i = 1; i <= 64; i++)
                        {
                            response.PropertyList.Add(SAPbobsCOM.BoYesNoEnum.tYES.Equals(oBp.get_Properties(i)));
                        }
                    }

                    response.Attachments = DBUtils.queryResultList<AttachInfo>(QueryRegisterImpl.Instance.getQueryBusinessPartnerAttachment(condition.Company, response.CardCode));
                }
            }
            catch (Exception ex)
            {
                ExceptionUtils.handleException(ex);
            }

            return response;
        }

        public List<BusinessPartnerAddress> searchBusinessPartnerAddressByCardCode(BusinessPartnerCondition condition)
        {
            List<BusinessPartnerAddress> response = null;

            try
            {
                response = DBUtils.queryResultList<BusinessPartnerAddress>(QueryRegisterImpl.Instance.getQuerySearchBusinessPartnerAddressByCardCode(condition));
            }
            catch (Exception ex)
            {
                ExceptionUtils.handleException(ex);
            }

            return response;
        }

        public List<BusinessPartnerContact> searchBusinessPartnerContactByCardCode(BusinessPartnerCondition condition)
        {
            List<BusinessPartnerContact> response = null;

            try
            {
                response = DBUtils.queryResultList<BusinessPartnerContact>(QueryRegisterImpl.Instance.getQuerySearchBusinessPartnerContactByCardCode(condition));
            }
            catch (Exception ex)
            {
                ExceptionUtils.handleException(ex);
            }

            return response;
        }


        public void createBusinessPartner(BusinessPartner bp)
        {
            ServiceDataAccess.Instance.getCompanyB1DataAccess(bp.Company);
            SAPbobsCOM.BusinessPartners oBp = null;

            try
            {
                oBp = B1DataAccess.Instance.Company.GetBusinessObject(SAPbobsCOM.BoObjectTypes.oBusinessPartners);

                ObjectUtils.copyPropertiesIncludeUDFToSapObject(bp, oBp, ignoreBp);

                oBp.CardType = SAPbobsCOM.BoCardTypes.cCustomer;
                oBp.Valid = SAPbobsCOM.BoYesNoEnum.tNO; // Active
                oBp.Frozen = SAPbobsCOM.BoYesNoEnum.tYES;   // Inactive
                oBp.FrozenFrom = DateTime.Today;

                if (bp.PriceListNum == 0)
                {
                    oBp.PriceListNum = 1;
                }

                if (CollectionUtils.IsAny(bp.BpShipList))
                {
                    SAPbobsCOM.BPAddresses oBpAddr = oBp.Addresses;

                    foreach (BusinessPartnerAddress bpAddr in bp.BpShipList)
                    {
                        ObjectUtils.copyPropertiesIncludeUDFToSapObject(bpAddr, oBpAddr);

                        oBpAddr.AddressName = bpAddr.Address;
                        oBpAddr.AddressType = SAPbobsCOM.BoAddressType.bo_ShipTo;

                        // UDF
                        if (!StringUtils.isBlankOrNull(bpAddr.BpBrnCode))
                        {
                            oBpAddr.UserFields.Fields.Item("U_ISS_BpBrnCode").Value = bpAddr.BpBrnCode;
                        }
                        if (!StringUtils.isBlankOrNull(bpAddr.BpBrnName))
                        {
                            oBpAddr.UserFields.Fields.Item("U_ISS_BpBrnName").Value = bpAddr.BpBrnName;
                        }
                        // UDF

                        oBpAddr.Add();
                    }
                }

                if (CollectionUtils.IsAny(bp.BpBillList))
                {
                    SAPbobsCOM.BPAddresses oBpAddr = oBp.Addresses;

                    foreach (BusinessPartnerAddress bpAddr in bp.BpBillList)
                    {
                        ObjectUtils.copyPropertiesIncludeUDFToSapObject(bpAddr, oBpAddr);

                        oBpAddr.AddressName = bpAddr.Address;
                        oBpAddr.AddressType = SAPbobsCOM.BoAddressType.bo_BillTo;

                        // UDF
                        if (!StringUtils.isBlankOrNull(bpAddr.BpBrnCode))
                        {
                            oBpAddr.UserFields.Fields.Item("U_ISS_BpBrnCode").Value = bpAddr.BpBrnCode;
                        }
                        if (!StringUtils.isBlankOrNull(bpAddr.BpBrnName))
                        {
                            oBpAddr.UserFields.Fields.Item("U_ISS_BpBrnName").Value = bpAddr.BpBrnName;
                        }
                        // UDF

                        oBpAddr.Add();
                    }
                }

                if (CollectionUtils.IsAny(bp.BpContactList))
                {
                    SAPbobsCOM.ContactEmployees oBpCon = oBp.ContactEmployees;

                    foreach (BusinessPartnerContact bpCon in bp.BpContactList)
                    {
                        ObjectUtils.copyPropertiesIncludeUDFToSapObject(bpCon, oBpCon);

                        oBpCon.Add();
                    }
                }

                if (CollectionUtils.IsAny(bp.PropertyList))
                {
                    int index = 1;
                    foreach (bool chk in bp.PropertyList)
                    {
                        oBp.set_Properties(index++, chk ? SAPbobsCOM.BoYesNoEnum.tYES : SAPbobsCOM.BoYesNoEnum.tNO);
                    }
                }
                
                if (CollectionUtils.IsAny(bp.Attachments))
                {
                    List<AttachInfo> attach = bp.Attachments.Where(x => !StringUtils.isBlankOrNull(x.TempKey)).ToList();

                    if (CollectionUtils.IsAny(attach))
                    {
                        List<AttachmentInfo> attachList = new List<AttachmentInfo>();
                        foreach (AttachInfo att in attach)
                        {
                            if (File.Exists(Path.Combine(att.SrcPath, att.NewFileName)))
                            {
                                attachList.Add(new AttachmentInfo(att.SrcPath, att.NewName, att.Ext));
                            }
                        }

                        if (CollectionUtils.IsAny(attachList))
                        {
                            oBp.AttachmentEntry = B1Utility.Implement.AttachmentImpl.Instance.createAttachment(attachList);
                        }
                    }
                }

                if (!TransactionUtils.checkSuccess(oBp.Add()))
                {
                    throw new Exception(B1DataAccess.Instance.Company.GetLastErrorDescription());
                }
            }
            catch (Exception ex)
            {
                ExceptionUtils.handleException(ex);
            }
            finally
            {
                ObjectUtils.finalReleaseComObject(oBp);
            }
        }

        public void updateBusinessPartner(BusinessPartner bp)
        {
            ServiceDataAccess.Instance.getCompanyB1DataAccess(bp.Company);
            SAPbobsCOM.BusinessPartners oBp = null;

            try
            {
                oBp = B1DataAccess.Instance.Company.GetBusinessObject(SAPbobsCOM.BoObjectTypes.oBusinessPartners);

                if (oBp.GetByKey(bp.CardCode))
                {
                    ObjectUtils.copyPropertiesIncludeUDFToSapObject(bp, oBp, ignoreBp);
                    // TODO
                    oBp.Valid = SAPbobsCOM.BoYesNoEnum.tNO; // Active
                    oBp.Frozen = SAPbobsCOM.BoYesNoEnum.tYES;   // Inactive

                    // Commitment limit must be greater than credit limit  [OCRD.DebtLine]
                    if(oBp.MaxCommitment < oBp.CreditLimit)
                    {
                        oBp.MaxCommitment = oBp.CreditLimit;
                    }

                    if (bp.PriceListNum == 0)
                    {
                        oBp.PriceListNum = 1;
                    }

                    if (CollectionUtils.IsAny(bp.BpShipList) || CollectionUtils.IsAny(bp.BpBillList))
                    {
                        SAPbobsCOM.BPAddresses oBpAddr = oBp.Addresses;

                        //for (int i = oBp.Addresses.Count - 1; i >= 0; i--)
                        //{
                        //    oBpAddr.SetCurrentLine(i);
                        //    oBpAddr.Delete();
                        //}

                        foreach (BusinessPartnerAddress bpAddr in bp.BpShipList)
                        {
                            if (bpAddr.VisOrder < 0)
                            {
                                oBpAddr.Add();
                                ObjectUtils.copyPropertiesIncludeUDFToSapObject(bpAddr, oBpAddr);
                                oBpAddr.AddressType = SAPbobsCOM.BoAddressType.bo_ShipTo;
                                oBpAddr.AddressName = bpAddr.Address;
                            }
                            else
                            {
                                for (int k = 0; k < oBpAddr.Count; k++)
                                {
                                    oBpAddr.SetCurrentLine(k);

                                    if (oBpAddr.AddressName.Equals(bpAddr.AddressName)
                                        && oBpAddr.AddressType.Equals(SAPbobsCOM.BoAddressType.bo_ShipTo))
                                    {
                                        ObjectUtils.copyPropertiesIncludeUDFToSapObject(bpAddr, oBpAddr);
                                        oBpAddr.AddressType = SAPbobsCOM.BoAddressType.bo_ShipTo;
                                        break;
                                    }
                                }
                                //oBpAddr.SetCurrentLine(bpAddr.VisOrder);
                                //ObjectUtils.copyPropertiesIncludeUDFToSapObject(bpAddr, oBpAddr);
                                //oBpAddr.AddressType = SAPbobsCOM.BoAddressType.bo_ShipTo;
                            }

                            // UDF
                            if (!StringUtils.isBlankOrNull(bpAddr.BpBrnCode))
                            {
                                oBpAddr.UserFields.Fields.Item("U_ISS_BpBrnCode").Value = bpAddr.BpBrnCode;
                            }
                            if (!StringUtils.isBlankOrNull(bpAddr.BpBrnName))
                            {
                                oBpAddr.UserFields.Fields.Item("U_ISS_BpBrnName").Value = bpAddr.BpBrnName;
                            }
                            // UDF
                        }

                        foreach (BusinessPartnerAddress bpAddr in bp.BpBillList)
                        {
                            if (bpAddr.VisOrder < 0)
                            {
                                oBpAddr.Add();
                                ObjectUtils.copyPropertiesIncludeUDFToSapObject(bpAddr, oBpAddr);
                                oBpAddr.AddressType = SAPbobsCOM.BoAddressType.bo_BillTo;
                                oBpAddr.AddressName = bpAddr.Address;
                            }
                            else
                            {
                                for (int k = 0; k < oBpAddr.Count; k++)
                                {
                                    oBpAddr.SetCurrentLine(k);

                                    if (oBpAddr.AddressName.Equals(bpAddr.AddressName)
                                        && oBpAddr.AddressType.Equals(SAPbobsCOM.BoAddressType.bo_BillTo))
                                    {
                                        ObjectUtils.copyPropertiesIncludeUDFToSapObject(bpAddr, oBpAddr);
                                        oBpAddr.AddressType = SAPbobsCOM.BoAddressType.bo_BillTo;
                                        break;
                                    }
                                }
                                //oBpAddr.SetCurrentLine(bpAddr.VisOrder);
                                //ObjectUtils.copyPropertiesIncludeUDFToSapObject(bpAddr, oBpAddr);
                                //oBpAddr.AddressType = SAPbobsCOM.BoAddressType.bo_BillTo;
                            }

                            // UDF
                            if (!StringUtils.isBlankOrNull(bpAddr.BpBrnCode))
                            {
                                oBpAddr.UserFields.Fields.Item("U_ISS_BpBrnCode").Value = bpAddr.BpBrnCode;
                            }
                            if (!StringUtils.isBlankOrNull(bpAddr.BpBrnName))
                            {
                                oBpAddr.UserFields.Fields.Item("U_ISS_BpBrnName").Value = bpAddr.BpBrnName;
                            }
                            // UDF
                        }

                        List<int> visOrder = new List<int>();
                        visOrder.AddRange(bp.BpBillList.Where(x => x.VisOrder >= 0).Select(x => x.VisOrder).ToList());
                        visOrder.AddRange(bp.BpShipList.Where(x => x.VisOrder >= 0).Select(x => x.VisOrder).ToList());
                        visOrder = DBUtils.queryResultList<int>(QueryRegisterImpl.Instance.getQueryCheckDeleteBPAddress(bp.Company, bp.CardCode, visOrder), true);
                        foreach(int v in visOrder)
                        {
                            oBpAddr.SetCurrentLine(v);
                            oBpAddr.Delete();
                        }
                    }

                    if (CollectionUtils.IsAny(bp.BpContactList))
                    {
                        SAPbobsCOM.ContactEmployees oBpCon = oBp.ContactEmployees;

                        foreach (BusinessPartnerContact bpCon in bp.BpContactList)
                        {
                            bpCon.CardCode = bp.CardCode;
                            if (bpCon.CntctCode > 0)
                            {
                                // already
                                for (int i = 0; i < oBpCon.Count; i++)
                                {
                                    oBpCon.SetCurrentLine(i);

                                    if (oBpCon.CardCode.Equals(bpCon.CardCode)
                                        && oBpCon.InternalCode.CompareTo(bpCon.CntctCode) == 0)
                                    {
                                        ObjectUtils.copyPropertiesIncludeUDFToSapObject(bpCon, oBpCon);

                                        break;
                                    }
                                }
                            }
                            else
                            {
                                ObjectUtils.copyPropertiesIncludeUDFToSapObject(bpCon, oBpCon);

                                oBpCon.Add();
                            }
                        }

                        // Check Delete
                        for (int i = oBpCon.Count - 1; i >= 0; i--)
                        {
                            oBpCon.SetCurrentLine(i);

                            if (oBpCon.InternalCode > 0)
                            {
                                BusinessPartnerContact bpCon = bp.BpContactList.Where(x => x.CardCode.Equals(oBpCon.CardCode)).Where(x => x.CntctCode.CompareTo(oBpCon.InternalCode) == 0).FirstOrDefault();
                                if (bpCon == null)
                                {
                                    oBpCon.Delete();
                                }
                            }
                        }
                    }

                    if (CollectionUtils.IsAny(bp.PropertyList))
                    {
                        int index = 1;
                        foreach (bool chk in bp.PropertyList)
                        {
                            oBp.set_Properties(index++, chk ? SAPbobsCOM.BoYesNoEnum.tYES : SAPbobsCOM.BoYesNoEnum.tNO);
                        }
                    }

                    if (CollectionUtils.IsAny(bp.Attachments))
                    {
                        List<AttachInfo> attach = bp.Attachments.Where(x => !StringUtils.isBlankOrNull(x.TempKey)).ToList();

                        if (CollectionUtils.IsAny(attach))
                        {
                            List<AttachmentInfo> attachList = new List<AttachmentInfo>();
                            foreach (AttachInfo att in attach)
                            {
                                if (File.Exists(Path.Combine(att.SrcPath, att.NewFileName)))
                                {
                                    attachList.Add(new AttachmentInfo(att.SrcPath, att.NewName, att.Ext));
                                }
                            }

                            if (CollectionUtils.IsAny(attachList))
                            {
                                if (oBp.AttachmentEntry > 0)
                                {
                                    B1Utility.Implement.AttachmentImpl.Instance.updateAttachment(oBp.AttachmentEntry, attachList);
                                }
                                else
                                {
                                    oBp.AttachmentEntry = B1Utility.Implement.AttachmentImpl.Instance.createAttachment(attachList);
                                }
                            }
                        }
                    }

                    if (!TransactionUtils.checkSuccess(oBp.Update()))
                    {
                        throw new Exception(B1DataAccess.Instance.Company.GetLastErrorDescription());
                    }
                }
            }
            catch (Exception ex)
            {
                ExceptionUtils.handleException(ex);
            }
            finally
            {
                ObjectUtils.finalReleaseComObject(oBp);
            }
        }


        public List<BarCode> searchItemBarCode(string company, string itemCode)
        {
            List<BarCode> response = null;

            try
            {
                response = DBUtils.queryResultList<BarCode>(QueryRegisterImpl.Instance.getQueryItemBarCode(company, itemCode));
            }
            catch (Exception ex)
            {
                ExceptionUtils.handleException(ex);
            }

            return response;
        }

        public List<BusinessPartnerCriteria> searchBusinessPartnerCriteria(BusinessPartnerCondition condition)
        {
            List<BusinessPartnerCriteria> response = null;

            try
            {
                response = DBUtils.queryResultList<BusinessPartnerCriteria>(QueryRegisterImpl.Instance.getQueryBusinessPartnerCriteria(condition));
            }
            catch (Exception ex)
            {
                ExceptionUtils.handleException(ex);
            }

            return response;
        }

        public List<ItemMasterCriteria> searchItemMasterCriteria(ItemMasterCondition condition)
        {
            List<ItemMasterCriteria> response = null;

            try
            {
                response = DBUtils.queryResultList<ItemMasterCriteria>(QueryRegisterImpl.Instance.getQueryItemMasterCriteria(condition));
            }
            catch (Exception ex)
            {
                ExceptionUtils.handleException(ex);
            }

            return response;
        }
    }
}
