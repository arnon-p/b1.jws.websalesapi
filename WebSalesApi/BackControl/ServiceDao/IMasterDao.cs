﻿using BackControl.Common;
using CommonControl.POCO.Class;
using CommonControl.POCO.Condition;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BackControl.ServiceDao
{
    public interface IMasterDao : IBase
    {
        List<ItemMaster> searchItemMasterByCondition(ref ItemMasterCondition condition);
        ItemMaster searchItemMasterByItemCode(ItemMasterCondition condition);
        List<ItemOnHand> searchItemOnHandByItemCode(ItemMasterCondition condition);
        List<BusinessPartner> searchBusinessPartnerByCondition(ref BusinessPartnerCondition condition);
        BusinessPartner searchBusinessPartnerByCardCode(BusinessPartnerCondition condition);
        List<BusinessPartnerAddress> searchBusinessPartnerAddressByCardCode(BusinessPartnerCondition condition);
        List<BusinessPartnerContact> searchBusinessPartnerContactByCardCode(BusinessPartnerCondition condition);
        void createBusinessPartner(BusinessPartner bp);
        void updateBusinessPartner(BusinessPartner bp);
        List<BarCode> searchItemBarCode(string company, string itemCode);
        List<BusinessPartnerCriteria> searchBusinessPartnerCriteria(BusinessPartnerCondition condition);
        List<ItemMasterCriteria> searchItemMasterCriteria(ItemMasterCondition condition);
    }
}
