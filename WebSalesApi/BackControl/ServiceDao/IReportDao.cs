﻿using BackControl.Common;
using CommonControl.POCO.Class;
using CommonControl.POCO.Common;
using CommonControl.POCO.Condition;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BackControl.ServiceDao
{
    public interface IReportDao : IBase
    {
        List<InventoryStatus> getReportInventoryStatus(ReportInventoryStatusCondition condition);
        List<InventoryStatusDocument> getReportInventoryStatusDocument(ReportInventoryStatusCondition condition);
        List<BatchNumberTransactionReport> getReportBatchNumberTransaction(ReportBatchNumberTransactionCondition condition);
        List<BatchNumberTransactionReportDetail> getReportBatchNumberTransactionDetail(ReportBatchNumberTransactionDetailCondition condition);
        List<CustomerPaymentReport> getReportCustomerPayment(ReportCustomerPaymentCondition condition);
        List<PurchaseOrderMovement> getReportPurchaseOrderMovement(ReportPurchaseOrderMovementCondition condition);
        List<SalesAnalysisReport> getReportSalesAnalysis(SalesAnalysisCondition condition);
    }
}
