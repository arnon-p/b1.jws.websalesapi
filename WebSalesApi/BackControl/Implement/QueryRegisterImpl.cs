﻿using B1Utility.DataAccess;
using B1Utility.Pattern;
using B1Utility.POCO;
using B1Utility.Utils;
using CommonControl.POCO.Class;
using CommonControl.POCO.Common;
using CommonControl.POCO.Condition;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BackControl.Implement
{
    public class QueryRegisterImpl
    {
        private B1Utility.Implement.QueryRegisterImpl baseInstance;
        private static QueryRegisterImpl instance;
        private QueryRegisterImpl() { }

        public static QueryRegisterImpl Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new QueryRegisterImpl();
                    instance.baseInstance = B1Utility.Implement.QueryRegisterImpl.Instance;
                }
                return instance;
            }
        }

        public B1Utility.Implement.QueryRegisterImpl BaseInstance
        {
            get { return baseInstance; }
        }

        public string getQuerySearchItemMasterByCondition(ItemMasterCondition condition)
        {
            TemplateSQL sql = new TemplateSQL(condition.Paginable);

            if (B1DataAccess.Instance.IsHana)
            {
            }
            else
            {
                sql.select(" select t0.ItemCode, t0.ItemName, t0.FrgnName, ");
                sql.select(" case when t0.ItemType = 'I' then 'Items' when t0.ItemType = 'L' then 'Labor' when t0.ItemType = 'T' then 'Travel' when t0.ItemType = 'F' then 'Fixed Assets' end as ItemType, ");
                sql.select(" t1.ItmsGrpCod as ItemGroupCode, t1.ItmsGrpNam as ItemGroupName, ");
                sql.select(" t2.UgpEntry as UomGroupEntry, t2.UgpCode as UomGroupCode, t2.UgpName as UomGroupName, ");
                sql.select(" t0.VatGourpSa, t3.UomEntry as SUomEntry, t3.UomCode as SUomCode, t3.UomName as SUomName, t0.NumInSale, ");
                sql.select(" t0.SalPackMsr, t0.SalPackUn, ");
                sql.select(" case when t0.GLMethod = 'W' then 'Warehouse' when t0.GLMethod = 'C' then 'Item Group' when t0.GLMethod = 'L' then 'Item Level' end as GLMethod, ");
                sql.select(" t4.UomEntry as IUomEntry, t4.UomCode as IUomCode, t4.UomName as IUomName, t0.IWeight1 as IWeight, ");
                sql.select(" t5.UomEntry as CUomEntry, t5.UomCode as CUomCode, t5.UomName as CUomName, t0.NumInCnt, ");
                sql.select(" case when t0.EvalSystem = 'A' then 'Moving Average' when t0.EvalSystem = 'S' then 'Standard' when t0.EvalSystem = 'F' then 'FIFO' when t0.EvalSystem = 'B' then 'Serial/Batch' end as EvalSystem, ");
                // --------- UDF
                sql.select(" t0.U_WGE_OItem, t0.U_WGE_ShelfLife, t0.U_WGE_Brand, t0.U_WGE_Channel, t0.U_WGE_SubCat, t0.U_WGE_PackagingSize, ");
                sql.select(" t0.U_WGE_StdYield, t0.U_WGE_StdManHour, t0.U_WGE_Package, t0.U_WGE_Properties, t0.U_WGE_InvPacking, t0.U_WGE_Rawmat, ");
                sql.select(" t0.U_WGE_Product, t0.U_WGE_ProductG, t0.U_WGE_BPCode, t0.U_WGE_BPName ");
                // --------- UDF
                sql.from(" from {:company}..oitm t0 (nolock) ");
                sql.leftjoin(" {:company}..oitb t1 (nolock) on t0.ItmsGrpCod = t1.ItmsGrpCod ");
                sql.leftjoin(" {:company}..ougp t2 (nolock) on t0.UgpEntry = t2.UgpEntry ");
                sql.leftjoin(" {:company}..ouom t3 (nolock) on t0.SUoMEntry = t3.UomEntry ");
                sql.leftjoin(" {:company}..ouom t4 (nolock) on t0.IUoMEntry = t4.UomEntry ");
                sql.leftjoin(" {:company}..ouom t5 (nolock) on t0.INUoMEntry = t5.UomEntry ");
                sql.where(" where 1 = 1 ");
                sql.where(" and (t0.InvntItem = 'Y' or t0.ItemCode in ('OR102004', 'OR102009') ) ");       // 20211003 - OR102004 fix show non inventory
                sql.where(" and t0.SellItem = 'Y' ");
                sql.where(" and t0.frozenFor = 'N' ");
            }
            // 20230421 - OR102009
            if (!StringUtils.isBlankOrNull(condition.ItemCode))
            {
                if (B1DataAccess.Instance.IsHana)
                {
                }
                else
                {
                    sql.where(" and t0.ItemCode like N'{:itemCode}' ");
                }

                sql.ConditionLike.Add("{:itemCode}", condition.ItemCode);
            }

            if (!StringUtils.isBlankOrNull(condition.ItemName))
            {
                if (B1DataAccess.Instance.IsHana)
                {
                }
                else
                {
                    sql.where(" and t0.ItemName like N'{:itemName}' ");
                }

                sql.ConditionLike.Add("{:itemName}", condition.ItemName);
            }

            if (condition.ItemGroupCode > 0)
            {
                if (B1DataAccess.Instance.IsHana)
                {
                }
                else
                {
                    sql.where(" and t1.ItmsGrpCod = {:itemGroupCode} ");
                }

                sql.ConditionLike.Add("{:itemGroupCode}", condition.ItemGroupCode);
            }
            
            if (condition.Paginable != null)
            {
                if (B1DataAccess.Instance.IsHana)
                {
                }
                else
                {
                    sql.orderBy(" t0.ItemCode ");
                }
            }

            sql.Condition.Add("{:company}", ServiceDataAccess.Instance.getCompanyName(condition.Company));

            return sql.generateSqlWithLog();
        }

        public string getQuerySearchItemMasterByItemCode(ItemMasterCondition condition)
        {
            TemplateSQL sql = new TemplateSQL();

            if (B1DataAccess.Instance.IsHana)
            {
            }
            else
            {
                sql.select(" select t0.ItemCode, t0.ItemName, t0.FrgnName, ");
                sql.select(" case when t0.ItemType = 'I' then 'Items' when t0.ItemType = 'L' then 'Labor' when t0.ItemType = 'T' then 'Travel' when t0.ItemType = 'F' then 'Fixed Assets' end as ItemType, ");
                sql.select(" t1.ItmsGrpCod as ItemGroupCode, t1.ItmsGrpNam as ItemGroupName, ");
                sql.select(" t2.UgpEntry as UomGroupEntry, t2.UgpCode as UomGroupCode, t2.UgpName as UomGroupName, ");
                sql.select(" t0.VatGourpSa, t3.UomEntry as SUomEntry, t3.UomCode as SUomCode, t3.UomName as SUomName, t0.NumInSale, ");
                sql.select(" t0.SalPackMsr, t0.SalPackUn, ");
                sql.select(" case when t0.GLMethod = 'W' then 'Warehouse' when t0.GLMethod = 'C' then 'Item Group' when t0.GLMethod = 'L' then 'Item Level' end as GLMethod, ");
                sql.select(" t4.UomEntry as IUomEntry, t4.UomCode as IUomCode, t4.UomName as IUomName, t0.IWeight1 as IWeight, ");
                sql.select(" t5.UomEntry as CUomEntry, t5.UomCode as CUomCode, t5.UomName as CUomName, t0.NumInCnt, ");
                sql.select(" case when t0.EvalSystem = 'A' then 'Moving Average' when t0.EvalSystem = 'S' then 'Standard' when t0.EvalSystem = 'F' then 'FIFO' when t0.EvalSystem = 'B' then 'Serial/Batch' end as EvalSystem, ");
                // --------- UDF
                sql.select(" t0.U_WGE_OItem, t0.U_WGE_ShelfLife, t0.U_WGE_Brand, t0.U_WGE_Channel, t0.U_WGE_SubCat, t0.U_WGE_PackagingSize, ");
                sql.select(" t0.U_WGE_StdYield, t0.U_WGE_StdManHour, t0.U_WGE_Package, t0.U_WGE_Properties, t0.U_WGE_InvPacking, t0.U_WGE_Rawmat, ");
                sql.select(" t0.U_WGE_Product, t0.U_WGE_ProductG, t0.U_WGE_BPCode, t0.U_WGE_BPName ");
                // --------- UDF
                sql.from(" from {:company}..oitm t0 (nolock) ");
                sql.leftjoin(" {:company}..oitb t1 (nolock) on t0.ItmsGrpCod = t1.ItmsGrpCod ");
                sql.leftjoin(" {:company}..ougp t2 (nolock) on t0.UgpEntry = t2.UgpEntry ");
                sql.leftjoin(" {:company}..ouom t3 (nolock) on t0.SUoMEntry = t3.UomEntry ");
                sql.leftjoin(" {:company}..ouom t4 (nolock) on t0.IUoMEntry = t4.UomEntry ");
                sql.leftjoin(" {:company}..ouom t5 (nolock) on t0.INUoMEntry = t5.UomEntry ");
                sql.where(" where 1 = 1 ");
                sql.where(" and (t0.InvntItem = 'Y' or t0.ItemCode in ('OR102004', 'OR102009') ) ");    // 20211003 - OR102004 fix show non inventory
                sql.where(" and t0.SellItem = 'Y' ");
                sql.where(" and t0.frozenFor = 'N' ");
                sql.where(" and t0.ItemCode = N'{:itemCode}' ");
            }

            sql.Condition.Add("{:itemCode}", condition.ItemCode);
            sql.Condition.Add("{:company}", ServiceDataAccess.Instance.getCompanyName(condition.Company));

            return sql.generateSqlWithLog();
        }

        public string getQuerySearchItemOnHandByItemCode(ItemMasterCondition condition)
        {
            TemplateSQL sql = new TemplateSQL();

            if (B1DataAccess.Instance.IsHana)
            {
            }
            else
            {
                sql.select(" select t0.ItemCode, t0.WhsCode, t2.WhsName, t0.OnHand, t0.IsCommited, t0.OnOrder, (t0.OnHand + t0.OnOrder - t0.IsCommited) as Available ");
                sql.from(" from {:company}..oitw t0 (nolock) ");
                sql.innerjoin(" {:company}..oitm t1 (nolock) on t0.ItemCode = t1.ItemCode ");
                sql.innerjoin(" {:company}..owhs t2 (nolock) on t0.WhsCode = t2.WhsCode ");
                sql.where(" where 1 = 1 ");
                sql.where(" and t1.InvntItem = 'Y' ");
                sql.where(" and t1.SellItem = 'Y' ");
                sql.where(" and t1.frozenFor = 'N' ");
                sql.where(" and t0.ItemCode = N'{:itemCode}' ");
            }

            sql.Condition.Add("{:itemCode}", condition.ItemCode);
            sql.Condition.Add("{:company}", ServiceDataAccess.Instance.getCompanyName(condition.Company));

            return sql.generateSqlWithLog();
        }

        public string getQuerySearchBusinessPartnerByCondition(BusinessPartnerCondition condition)
        {
            TemplateSQL sql = new TemplateSQL(condition.Paginable);

            if (B1DataAccess.Instance.IsHana)
            {
            }
            else
            {
                sql.select(" select t0.CardCode, t0.CardName, t0.CardType, t0.GroupCode, t1.GroupName, t0.LicTradNum as FederalTaxID, ");
                sql.select(" t0.Phone1, t0.Phone2, t0.Cellular, t0.Fax, t0.E_Mail as EmailAddress, ");
                sql.select(" t0.IntrntSite as Website ,t0.ShipType as ShippingType, t2.TrnspName as ShipTypeName, t0.SlpCode as SalesPersonCode, ");
                sql.select(" t0.Balance, t0.OrdersBal, t0.GroupNum as PayTermsGrpCode, t3.PymntGroup, t0.CreditLine as CreditLimit,  ");
                sql.select(@" t0.FatherCard, t0.Free_Text as ""FreeText"", case when t0.frozenFor = 'N' then 'Y' else 'N' end as validFor, t0.ListNum as PriceListNum, case when t0.frozenFor = 'N' then 'Active' else 'Inactive' end as Status, ");
                // --------- UDF
                sql.select(" t0.U_WGE_Billing, t0.U_WGE_PayDetail, t0.U_WGE_BillingIns, t0.U_WGE_BillState ");
                // --------- UDF
                sql.from(" from {:company}..ocrd t0 (nolock) ");
                sql.leftjoin(" {:company}..ocrg t1 (nolock) on t0.GroupCode = t1.GroupCode ");
                sql.leftjoin(" {:company}..oshp t2 (nolock) on t0.ShipType = t2.TrnspCode ");
                sql.leftjoin(" {:company}..octg t3 (nolock) on t0.GroupNum = t3.GroupNum ");
                sql.where(" where 1 = 1 ");
                sql.where(" and t0.CardType = 'C' ");
                //sql.where(" and t0.validFor = 'Y' ");
            }

            if (!StringUtils.isBlankOrNull(condition.CardCode))
            {
                if (B1DataAccess.Instance.IsHana)
                {
                }
                else
                {
                    sql.where(" and t0.CardCode like N'{:cardCode}' ");
                }
                sql.ConditionLike.Add("{:cardCode}", condition.CardCode);
            }

            if (!StringUtils.isBlankOrNull(condition.CardName))
            {
                if (B1DataAccess.Instance.IsHana)
                {
                }
                else
                {
                    sql.where(" and t0.CardName like N'{:cardName}' ");
                }
                sql.ConditionLike.Add("{:cardName}", condition.CardName);
            }

            if (CollectionUtils.IsAny(condition.SlpCode))
            {
                if (B1DataAccess.Instance.IsHana)
                {
                }
                else
                {
                    sql.where(" and t0.SlpCode in ({:slpCode}) ");
                }
                sql.Condition.Add("{:slpCode}", StringUtils.setInParameter(condition.SlpCode));
            }
            else
            {
                // CR Authorize
                if (CollectionUtils.IsAny(condition.UserSalesEmployee))
                {
                    // condition not selected
                    if (B1DataAccess.Instance.IsHana)
                    {
                    }
                    else
                    {
                        sql.where(" and t0.SlpCode in ({:slpCode}) ");
                    }
                    sql.Condition.Add("{:slpCode}", StringUtils.setInParameter(condition.UserSalesEmployee));
                }
            }

            if (condition.Paginable != null)
            {
                if (B1DataAccess.Instance.IsHana)
                {
                }
                else
                {
                    sql.orderBy(" t0.CardCode ");
                }
            }

            sql.Condition.Add("{:company}", ServiceDataAccess.Instance.getCompanyName(condition.Company));

            return sql.generateSqlWithLog();
        }

        public string getQuerySearchBusinessPartnerByCardCode(BusinessPartnerCondition condition)
        {
            TemplateSQL sql = new TemplateSQL();

            if (B1DataAccess.Instance.IsHana)
            {
            }
            else
            {
                sql.select(" select t0.CardCode, t0.CardName, t0.CardType, t0.GroupCode, t1.GroupName, t0.LicTradNum as FederalTaxID, ");
                sql.select(" t0.Phone1, t0.Phone2, t0.Cellular, t0.Fax, t0.E_Mail as EmailAddress, ");
                sql.select(" t0.IntrntSite as Website ,t0.ShipType as ShippingType, t2.TrnspName as ShipTypeName, t0.SlpCode as SalesPersonCode, ");
                sql.select(" t0.Balance, t0.OrdersBal, t0.GroupNum as PayTermsGrpCode, t3.PymntGroup, t0.CreditLine as CreditLimit,  ");
                sql.select(@" t0.FatherCard, t0.Free_Text as ""FreeText"", case when t0.frozenFor = 'N' then 'Y' else 'N' end as validFor, t0.ListNum as PriceListNum, case when t0.frozenFor = 'N' then 'Active' else 'Inactive' end as Status, ");
                // --------- UDF
                sql.select(" t0.U_WGE_Billing, t0.U_WGE_PayDetail, t0.U_WGE_BillingIns, t0.U_WGE_BillState ");
                // --------- UDF
                sql.from(" from {:company}..ocrd t0 (nolock) ");
                sql.leftjoin(" {:company}..ocrg t1 (nolock) on t0.GroupCode = t1.GroupCode ");
                sql.leftjoin(" {:company}..oshp t2 (nolock) on t0.ShipType = t2.TrnspCode ");
                sql.leftjoin(" {:company}..octg t3 (nolock) on t0.GroupNum = t3.GroupNum ");
                sql.where(" where 1 = 1 ");
                sql.where(" and t0.CardType = 'C' ");
                //sql.where(" and t0.validFor = 'Y' ");
                sql.where(" and t0.CardCode = N'{:cardCode}' ");
            }

            sql.Condition.Add("{:cardCode}", condition.CardCode);
            sql.Condition.Add("{:company}", ServiceDataAccess.Instance.getCompanyName(condition.Company));

            return sql.generateSqlWithLog();
        }

        public string getQuerySearchBusinessPartnerAddressByCardCode(BusinessPartnerCondition condition)
        {
            TemplateSQL sql = new TemplateSQL();

            if (B1DataAccess.Instance.IsHana)
            {
            }
            else
            {
                sql.select(" select t0.CardCode, t0.AdresType as AddressType, t0.Address, t0.Address as AddressName, t0.Address2 as AddressName2, t0.Address3 as AddressName3, ");
                sql.select(" t0.Street, t0.Block, t0.City, t0.ZipCode, t0.County, t0.State, t0.Country, (ROW_NUMBER() OVER(ORDER BY t0.Address, t0.AdresType) - 1) as VisOrder, ");
                sql.select(" concat(t0.Street, ' ', t0.Block, ' ', t0.City, ' ', t0.ZipCode) as AddrName, ");
                //sql.select(" concat(t0.Street, ' ', t0.Block, ' ', t0.City, ' ', t0.ZipCode, ' ', isnull(t1.Name, '')) as AddrName, ");
                sql.select(" SUBSTRING(t0.Block, 1, CASE CHARINDEX(' ', t0.Block) WHEN 0 THEN LEN(t0.Block) ELSE CHARINDEX(' ', t0.Block) -1 END) AS Tambon, ");
                sql.select(" SUBSTRING(t0.Block, CASE CHARINDEX(' ', t0.Block) WHEN 0 THEN LEN(t0.Block) + 1 ELSE CHARINDEX(' ', t0.Block) + 1 END, 1000) AS Amphur, ");
                // UDF
                sql.select(" t0.U_ISS_BpBrnCode as BpBrnCode, t0.U_ISS_BpBrnName as BpBrnName ");
                sql.from(" from {:company}..crd1 t0 (nolock) ");
                sql.leftjoin(" {:company}..ocry t1 (nolock) on t0.Country = t1.Code ");
                sql.where(" where 1 = 1 ");
                sql.where(" and t0.CardCode = N'{:cardCode}' ");
            }

            sql.Condition.Add("{:cardCode}", condition.CardCode);
            sql.Condition.Add("{:company}", ServiceDataAccess.Instance.getCompanyName(condition.Company));

            return sql.generateSqlWithLog();
        }

        public string getQuerySearchBusinessPartnerContactByCardCode(BusinessPartnerCondition condition)
        {
            TemplateSQL sql = new TemplateSQL();

            if (B1DataAccess.Instance.IsHana)
            {
            }
            else
            {
                sql.select(" select t0.CardCode, t0.CntctCode, t0.Name, t0.FirstName, t0.MiddleName, t0.LastName, t0.Title, t0.Position, ");
                sql.select(" t0.Address, t0.Tel1 as Phone1, t0.Tel2 as Phone2, t0.Cellolar as MobilePhone, t0.Fax, t0.E_MailL as E_Mail, t0.Notes1 as Remarks1, t0.Notes2 as Remarks2 ");
                sql.from(" from {:company}..ocpr t0 (nolock) ");
                sql.where(" where 1 = 1 ");
                sql.where(" and t0.CardCode = N'{:cardCode}' ");
            }

            sql.Condition.Add("{:cardCode}", condition.CardCode);
            sql.Condition.Add("{:company}", ServiceDataAccess.Instance.getCompanyName(condition.Company));

            return sql.generateSqlWithLog();
        }

        public string getQuerySalesEmployee(string company)
        {
            TemplateSQL sql = new TemplateSQL();

            if (B1DataAccess.Instance.IsHana)
            {
            }
            else
            {
                sql.select(" select SlpCode, SlpName ");
                sql.from(" from {:company}..oslp (nolock) ");
                sql.where(" where 1 = 1 ");
                sql.where(" and SlpName like 'S%' ");
                sql.orderBy(" SlpName ");
            }

            sql.Condition.Add("{:company}", ServiceDataAccess.Instance.getCompanyName(company));

            return sql.generateSqlWithLog();
        }

        public string getQueryPaymentTerms(string company)
        {
            TemplateSQL sql = new TemplateSQL();

            if (B1DataAccess.Instance.IsHana)
            {
            }
            else
            {
                sql.select(" select GroupNum, PymntGroup, ExtraDays ");
                sql.from(" from {:company}..octg (nolock) ");
                sql.where(" where 1 = 1 ");
                sql.orderBy(" GroupNum ");
            }

            sql.Condition.Add("{:company}", ServiceDataAccess.Instance.getCompanyName(company));

            return sql.generateSqlWithLog();
        }

        public string getQueryBusinessPartnerGroup(string company)
        {
            TemplateSQL sql = new TemplateSQL();

            if (B1DataAccess.Instance.IsHana)
            {
            }
            else
            {
                sql.select(" select GroupCode, GroupName ");
                sql.from(" from {:company}..ocrg (nolock) ");
                sql.where(" where 1 = 1 ");
                sql.where(" and GroupType = 'C' ");
                sql.orderBy(" GroupCode ");
            }

            sql.Condition.Add("{:company}", ServiceDataAccess.Instance.getCompanyName(company));

            return sql.generateSqlWithLog();
        }

        public string getQueryShippingType(string company)
        {
            TemplateSQL sql = new TemplateSQL();

            if (B1DataAccess.Instance.IsHana)
            {
            }
            else
            {
                sql.select(" select TrnspCode, TrnspName ");
                sql.from(" from {:company}..oshp (nolock) ");
                sql.where(" where 1 = 1 ");
                sql.orderBy(" TrnspCode ");
            }

            sql.Condition.Add("{:company}", ServiceDataAccess.Instance.getCompanyName(company));

            return sql.generateSqlWithLog();
        }

        public string getQueryItemGroup(string company)
        {
            TemplateSQL sql = new TemplateSQL();

            if (B1DataAccess.Instance.IsHana)
            {
            }
            else
            {
                sql.select(" select ItmsGrpCod, ItmsGrpNam ");
                sql.from(" from {:company}..oitb (nolock) ");
                sql.where(" where 1 = 1 ");
                sql.orderBy(" ItmsGrpCod ");
            }

            sql.Condition.Add("{:company}", ServiceDataAccess.Instance.getCompanyName(company));

            return sql.generateSqlWithLog();
        }

        public string getQueryWhse(string company)
        {
            TemplateSQL sql = new TemplateSQL();

            if (B1DataAccess.Instance.IsHana)
            {
            }
            else
            {
                sql.select(" select WhsCode ");
                sql.from(" from {:company}..owhs (nolock) ");
                sql.where(" where 1 = 1 ");
                sql.where(" and Inactive = 'N' ");
                sql.orderBy(" WhsCode ");
            }

            sql.Condition.Add("{:company}", ServiceDataAccess.Instance.getCompanyName(company));

            return sql.generateSqlWithLog();
        }

        public string getQueryProperties(string company)
        {
            TemplateSQL sql = new TemplateSQL();

            if (B1DataAccess.Instance.IsHana)
            {
            }
            else
            {
                sql.select(" select GroupCode, GroupName ");
                sql.from(" from {:company}..ocqg (nolock) ");
                sql.where(" where 1 = 1 ");
                sql.orderBy(" GroupCode ");
            }

            sql.Condition.Add("{:company}", ServiceDataAccess.Instance.getCompanyName(company));

            return sql.generateSqlWithLog();
        }

        public string getQueryCountry(string company)
        {
            TemplateSQL sql = new TemplateSQL();

            if (B1DataAccess.Instance.IsHana)
            {
            }
            else
            {
                sql.select(" select Code, Name ");
                sql.from(" from {:company}..ocry (nolock) ");
                sql.where(" where 1 = 1 ");
                sql.orderBy(" Code ");
            }

            sql.Condition.Add("{:company}", ServiceDataAccess.Instance.getCompanyName(company));

            return sql.generateSqlWithLog();
        }

        public string getQueryState(string company)
        {
            TemplateSQL sql = new TemplateSQL();

            if (B1DataAccess.Instance.IsHana)
            {
            }
            else
            {
                sql.select(" select Code, Name, Country ");
                sql.from(" from {:company}..ocst (nolock) ");
                sql.where(" where 1 = 1 ");
                sql.orderBy(" Code ");
            }

            sql.Condition.Add("{:company}", ServiceDataAccess.Instance.getCompanyName(company));

            return sql.generateSqlWithLog();
        }

        public string getQueryTaxGroup(string company)
        {
            TemplateSQL sql = new TemplateSQL();

            if (B1DataAccess.Instance.IsHana)
            {
            }
            else
            {
                sql.select(" select Code, Name, Rate ");
                sql.from(" from {:company}..ovtg (nolock) ");
                sql.where(" where 1 = 1 ");
                sql.where(" and Category = 'O' ");
                sql.where(" and Account is not null ");
                sql.orderBy(" Code ");
            }

            sql.Condition.Add("{:company}", ServiceDataAccess.Instance.getCompanyName(company));

            return sql.generateSqlWithLog();
        }

        public string getQueryPriceList(string company)
        {
            TemplateSQL sql = new TemplateSQL();

            if (B1DataAccess.Instance.IsHana)
            {
            }
            else
            {
                sql.select(" select ListNum, ListName, IsGrossPrc ");
                sql.from(" from {:company}..opln (nolock) ");
                sql.where(" where 1 = 1 ");
                sql.orderBy(" ListNum ");
            }

            sql.Condition.Add("{:company}", ServiceDataAccess.Instance.getCompanyName(company));

            return sql.generateSqlWithLog();
        }

        public string getQueryBrand(string company)
        {
            TemplateSQL sql = new TemplateSQL();

            if (B1DataAccess.Instance.IsHana)
            {
            }
            else
            {
                sql.select(" select distinct t0.U_WGE_Brand as Code, t0.U_WGE_Brand as Name ");
                sql.from(" from {:company}..oitm t0 (nolock) ");
                sql.where(" where 1 = 1 ");
                sql.where(" and t0.U_WGE_Brand not in ('', '-') ");
                sql.orderBy(" t0.U_WGE_Brand ");
            }

            sql.Condition.Add("{:company}", ServiceDataAccess.Instance.getCompanyName(company));

            return sql.generateSqlWithLog();
        }

        public string getQueryLocationAddress(string company)
        {
            TemplateSQL sql = new TemplateSQL();

            if (B1DataAccess.Instance.IsHana)
            {
            }
            else
            {
                // UDT
                sql.select(" select U_JWS_District as Tambon, U_JWS_County as Amphur, U_JWS_Province as City, U_JWS_ZipCode as ZipCode, ");
                sql.select(" concat(U_JWS_District, ' ', U_JWS_County, ' ', U_JWS_Province, ' ', U_JWS_ZipCode) as FullAddr ");
                sql.from(" from {:company}..[@JWS_ADDRESSCODE] t0 (nolock) ");
                sql.where(" where 1 = 1 ");
                sql.orderBy(" t0.Name ");
            }

            sql.Condition.Add("{:company}", ServiceDataAccess.Instance.getCompanyName(company));

            return sql.generateSqlWithLog();
        }

        public string getQueryItemBarCode(string company, string itemCode)
        {
            TemplateSQL sql = new TemplateSQL();

            if (B1DataAccess.Instance.IsHana)
            {
            }
            else
            {
                sql.select(" select BcdEntry, BcdCode, BcdName, UomEntry ");
                sql.from(" from {:company}..obcd (nolock) ");
                sql.where(" where 1 = 1 ");
                sql.where(" and ItemCode = N'{:itemCode}' ");
                //sql.where(" and UomEntry = {:uomEntry} ");
                sql.orderBy(" BcdEntry ");
            }

            sql.Condition.Add("{:itemCode}", itemCode);
            //sql.Condition.Add("{:uomEntry}", uomEntry);
            sql.Condition.Add("{:company}", ServiceDataAccess.Instance.getCompanyName(company));

            return sql.generateSqlWithLog();
        }

        public string getQueryInventoryStatus(ReportInventoryStatusCondition condition)
        {
            TemplateSQL sql = new TemplateSQL();

            if (B1DataAccess.Instance.IsHana)
            {
            }
            else
            {
                sql.select(" select t0.ItemCode, t0.ItemName, sum(t1.OnHand) as OnHand, sum(t1.IsCommited) as IsCommited, sum(t1.OnOrder) as OnOrder, ");
                sql.select(" (sum(t1.OnHand) + sum(t1.OnOrder) - sum(t1.IsCommited)) Avaliable, t0.InvntryUom ");
                sql.from(" from {:company}..oitm t0 (nolock) ");
                sql.innerjoin(" {:company}..oitw t1 (nolock) on t0.ItemCode = t1.ItemCode ");
                sql.where(" where 1 = 1 ");
            }

            if (!StringUtils.isBlankOrNull(condition.ItemCodeFrom) && !StringUtils.isBlankOrNull(condition.ItemCodeTo))
            {
                if (B1DataAccess.Instance.IsHana)
                {
                }
                else
                {
                    sql.where(" and t0.ItemCode between N'{:itemCodeFrom}' and N'{:itemCodeTo}' ");
                }
                sql.Condition.Add("{:itemCodeFrom}", condition.ItemCodeFrom);
                sql.Condition.Add("{:itemCodeTo}", condition.ItemCodeTo);
            }
            else if (!StringUtils.isBlankOrNull(condition.ItemCodeFrom) && StringUtils.isBlankOrNull(condition.ItemCodeTo))
            {
                if (B1DataAccess.Instance.IsHana)
                {
                }
                else
                {
                    sql.where(" and t0.ItemCode >= N'{:itemCodeFrom}' ");
                }
                sql.Condition.Add("{:itemCodeFrom}", condition.ItemCodeFrom);
            }
            else if (StringUtils.isBlankOrNull(condition.ItemCodeFrom) && !StringUtils.isBlankOrNull(condition.ItemCodeTo))
            {
                if (B1DataAccess.Instance.IsHana)
                {
                }
                else
                {
                    sql.where(" and t0.ItemCode <= N'{:itemCodeTo}' ");
                }
                sql.Condition.Add("{:itemCodeTo}", condition.ItemCodeTo);
            }

            if (condition.ItmsGrpCod > 0)
            {
                if (B1DataAccess.Instance.IsHana)
                {
                }
                else
                {
                    sql.where(" and t0.ItmsGrpCod = {:itmsGrpCod} ");
                }
                sql.Condition.Add("{:itmsGrpCod}", condition.ItmsGrpCod);
            }

            if (CollectionUtils.IsAny(condition.WhsCode))
            {
                if (B1DataAccess.Instance.IsHana)
                {
                }
                else
                {
                    sql.where(" and t1.WhsCode in ({:whsCode}) ");
                }
                sql.Condition.Add("{:whsCode}", StringUtils.setInParameter(condition.WhsCode));
            }

            if (B1DataAccess.Instance.IsHana)
            {
            }
            else
            {
                sql.groupBy(" t0.ItemCode, t0.ItemName, t0.InvntryUom ");
                sql.orderBy(" t0.ItemCode ");
            }

            sql.Condition.Add("{:company}", ServiceDataAccess.Instance.getCompanyName(condition.Company));

            return sql.generateSqlWithLog();
        }

        public string getQueryInventoryStatusDocument(ReportInventoryStatusCondition condition)
        {
            TemplateSQL sql = new TemplateSQL();

            if (B1DataAccess.Instance.IsHana)
            {
            }
            else
            {
                sql.select(" select t1.ItemCode, t0.DocEntry, t0.ObjType, t0.DocNum, concat('OR ', t0.DocNum) as DocNo, ");
                sql.select(" t0.CardCode, t0.CardName, t0.DocDate, t0.DocDueDate, sum(t1.OpenInvQty) as IsCommited, 0 as OnOrder ");
                sql.from(" from {:company}..ordr t0 (nolock) ");
                sql.innerjoin(" {:company}..rdr1 t1 (nolock) on t0.DocEntry = t1.DocEntry ");
                sql.where(" where 1 = 1 ");
                sql.where(" and t0.CANCELED = 'N' ");
                sql.where(" and t0.DocStatus = 'O' ");
                sql.where(" and t1.LineStatus = 'O' ");
                sql.where(" and t1.ItemCode = N'{:itemCode}' ");
                sql.groupBy(" t1.ItemCode, t0.DocEntry, t0.ObjType, t0.DocNum, t0.CardCode, t0.CardName, t0.DocDate, t0.DocDueDate ");

                sql.append(" union all ");

                sql.select(" select t1.ItemCode, t0.DocEntry, t0.ObjType, t0.DocNum, concat('IN ', t0.DocNum) as DocNo, ");
                sql.select(" t0.CardCode, t0.CardName, t0.DocDate, t0.DocDueDate, sum(t1.OpenInvQty) as IsCommited, 0 as OnOrder ");
                sql.from(" from {:company}..oinv t0 (nolock) ");
                sql.innerjoin(" {:company}..inv1 t1 (nolock) on t0.DocEntry = t1.DocEntry ");
                sql.where(" where 1 = 1 ");
                sql.where(" and t0.CANCELED = 'N' ");
                sql.where(" and t0.DocStatus = 'O' ");
                sql.where(" and t1.LineStatus = 'O' ");
                sql.where(" and t1.ItemCode = N'{:itemCode}' ");
                sql.where(" and t1.ShipDate is not null ");
                sql.where(" and t0.isIns = 'Y' ");
                
                sql.groupBy(" t1.ItemCode, t0.DocEntry, t0.ObjType, t0.DocNum, t0.CardCode, t0.CardName, t0.DocDate, t0.DocDueDate ");

                sql.append(" union all ");

                sql.select(" select t1.ItemCode, t0.DocEntry, t0.ObjType, t0.DocNum, concat('PO ', t0.DocNum) as DocNo, ");
                sql.select(" t0.CardCode, t0.CardName, t0.DocDate, t1.ShipDate as DocDueDate, 0 as IsCommited, sum(t1.OpenInvQty) as OnOrder ");
                sql.from(" from {:company}..opor t0 (nolock) ");
                sql.innerjoin(" {:company}..por1 t1 (nolock) on t0.DocEntry = t1.DocEntry ");
                sql.where(" where 1 = 1 ");
                sql.where(" and t0.CANCELED = 'N' ");
                sql.where(" and t0.DocStatus = 'O' ");
                sql.where(" and t1.LineStatus = 'O' ");
                sql.where(" and t1.ItemCode = N'{:itemCode}' ");
                sql.where(" and t1.ShipDate is not null ");
                sql.groupBy(" t1.ItemCode, t0.DocEntry, t0.ObjType, t0.DocNum, t0.CardCode, t0.CardName, t0.DocDate, t1.ShipDate ");
                sql.orderBy(" DocDueDate ");
            }

            sql.Condition.Add("{:itemCode}", condition.ItemCode);
            sql.Condition.Add("{:company}", ServiceDataAccess.Instance.getCompanyName(condition.Company));

            return sql.generateSqlWithLog();
        }

        public string getQueryBatchNumberTransactionReport(ReportBatchNumberTransactionCondition condition)
        {
            TemplateSQL sql = new TemplateSQL();

            if (B1DataAccess.Instance.IsHana)
            {
            }
            else
            {
                sql.select(" select t0.AbsEntry, t4.WhsCode, min(t0.ItemCode) as ItemCode, ");
                sql.select(" min(t0.SysNumber) as SysNumber, min(t0.DistNumber) as DistNumber, ");
                sql.select(" min(t3.ItemName) as ItemName, min(t0.MnfSerial) as MnfSerial, ");
                sql.select(" min(t0.LotNumber) as LotNumber, min(t0.ExpDate) as ExpDate, ");
                sql.select(" case when min(t0.Status) = '0' then 'Released' end as Status, min(t5.Location) as Location, ");
                sql.select(" min(t4.Quantity) as Quantity, min(t4.CommitQty) as CommitQty, ");
                sql.select(" min(t4.CountQty) as CountQty, min(t8.CCDNum) as CCDNum, ");
                sql.select(" min(t7.ItemCCDNum) as ItemCCDNum, ");
                sql.select(" min(t0.MnfDate) as MnfDate, min(t0.InDate) as InDate, ");
                sql.select(" min(t3.CardCode) as CardCode, min(t3.CardName) as CardName, min(cast(t0.Notes as nvarchar)) as Notes ");
                sql.from(" from {:company}..obtn t0 (nolock) ");
                sql.innerjoin(" {:company}..oitm t1 (nolock) on t0.ItemCode = t1.ItemCode ");
                sql.innerjoin(" {:company}..itl1 t2 (nolock) on t0.ItemCode = t2.ItemCode and t0.SysNumber = t2.SysNumber ");
                sql.innerjoin(" {:company}..oitl t3 (nolock) on t2.LogEntry = t3.LogEntry and t3.ManagedBy = '10000044' ");
                sql.leftjoin(" {:company}..obtq t4 (nolock) on t0.ItemCode = t4.ItemCode and t0.SysNumber = t4.SysNumber and t3.LocCode = t4.WhsCode ");
                sql.leftjoin(" {:company}..obtw t5 (nolock) on t0.ItemCode = t5.ItemCode and t0.SysNumber = t5.SysNumber and t3.LocCode = t5.WhsCode ");
                sql.leftjoin(" {:company}..ocrd t6 (nolock) on t3.CardCode = t6.CardCode ");
                sql.leftjoin(" {:company}..tcn1 t7 (nolock) on t0.TrackingNt = t7.AbsEntry and t0.TrackiNtLn = t7.LineNum ");
                sql.leftjoin(" {:company}..otcn t8 (nolock) on t7.AbsEntry = t8.AbsEntry ");
                sql.where(" where 1 = 1 ");
                sql.where(" and t1.InvntItem = 'Y' ");
                sql.where(" and t1.ManBtchNum = 'Y' ");
                // 20200924 change to show all
                //sql.where(" and t4.Quantity > 0 ");
            }

            // Item
            if (!StringUtils.isBlankOrNull(condition.ItemCodeFrom) && !StringUtils.isBlankOrNull(condition.ItemCodeTo))
            {
                if (B1DataAccess.Instance.IsHana)
                {
                }
                else
                {
                    sql.where(" and t0.ItemCode between N'{:itemCodeFrom}' and N'{:itemCodeTo}' ");
                }
                sql.Condition.Add("{:itemCodeFrom}", condition.ItemCodeFrom);
                sql.Condition.Add("{:itemCodeTo}", condition.ItemCodeTo);
            }
            else if (!StringUtils.isBlankOrNull(condition.ItemCodeFrom) && StringUtils.isBlankOrNull(condition.ItemCodeTo))
            {
                if (B1DataAccess.Instance.IsHana)
                {
                }
                else
                {
                    sql.where(" and t0.ItemCode >= N'{:itemCodeFrom}' ");
                }
                sql.Condition.Add("{:itemCodeFrom}", condition.ItemCodeFrom);
            }
            else if (StringUtils.isBlankOrNull(condition.ItemCodeFrom) && !StringUtils.isBlankOrNull(condition.ItemCodeTo))
            {
                if (B1DataAccess.Instance.IsHana)
                {
                }
                else
                {
                    sql.where(" and t0.ItemCode <= N'{:itemCodeTo}' ");
                }
                sql.Condition.Add("{:itemCodeTo}", condition.ItemCodeTo);
            }

            // Customer
            if (!StringUtils.isBlankOrNull(condition.CardCodeFrom) && !StringUtils.isBlankOrNull(condition.CardCodeTo))
            {
                if (B1DataAccess.Instance.IsHana)
                {
                }
                else
                {
                    sql.where(" and t3.CardCode between N'{:cardCodeFrom}' and N'{:cardCodeTo}' ");
                }
                sql.Condition.Add("{:cardCodeFrom}", condition.CardCodeFrom);
                sql.Condition.Add("{:cardCodeTo}", condition.CardCodeTo);
            }
            else if (!StringUtils.isBlankOrNull(condition.CardCodeFrom) && StringUtils.isBlankOrNull(condition.CardCodeTo))
            {
                if (B1DataAccess.Instance.IsHana)
                {
                }
                else
                {
                    sql.where(" and t3.CardCode >= N'{:cardCodeFrom}' ");
                }
                sql.Condition.Add("{:cardCodeFrom}", condition.CardCodeFrom);
            }
            else if (StringUtils.isBlankOrNull(condition.CardCodeFrom) && !StringUtils.isBlankOrNull(condition.CardCodeTo))
            {
                if (B1DataAccess.Instance.IsHana)
                {
                }
                else
                {
                    sql.where(" and t3.CardCode <= N'{:cardCodeTo}' ");
                }
                sql.Condition.Add("{:cardCodeTo}", condition.CardCodeTo);
            }

            // Posting Date
            if (condition.DocDateFrom.Year > 1 && condition.DocDateTo.Year > 1)
            {
                if (B1DataAccess.Instance.IsHana)
                {
                }
                else
                {
                    sql.where(" and t0.MnfDate between N'{:docDateFrom}' and N'{:docDateTo}' ");
                }
                sql.Condition.Add("{:docDateFrom}", condition.DocDateFrom);
                sql.Condition.Add("{:docDateTo}", condition.DocDateTo);
            }
            else if (condition.DocDateFrom.Year > 1 && condition.DocDateTo.Year == 1)
            {
                if (B1DataAccess.Instance.IsHana)
                {
                }
                else
                {
                    sql.where(" and t0.MnfDate >= N'{:docDateFrom}' ");
                }
                sql.Condition.Add("{:docDateFrom}", condition.DocDateFrom);
            }
            else if (condition.DocDateFrom.Year == 1 && condition.DocDateTo.Year > 1)
            {
                if (B1DataAccess.Instance.IsHana)
                {
                }
                else
                {
                    sql.where(" and t0.MnfDate <= N'{:docDateTo}' ");
                }
                sql.Condition.Add("{:docDateTo}", condition.DocDateTo);
            }

            // Expried Date
            if (condition.ExpDateFrom.Year > 1 && condition.ExpDateTo.Year > 1)
            {
                if (B1DataAccess.Instance.IsHana)
                {
                }
                else
                {
                    sql.where(" and t0.ExpDate between N'{:expDateFrom}' and N'{:expDateTo}' ");
                }
                sql.Condition.Add("{:expDateFrom}", condition.ExpDateFrom);
                sql.Condition.Add("{:expDateTo}", condition.ExpDateTo);
            }
            else if (condition.ExpDateFrom.Year > 1 && condition.ExpDateTo.Year == 1)
            {
                if (B1DataAccess.Instance.IsHana)
                {
                }
                else
                {
                    sql.where(" and t0.ExpDate >= N'{:expDateFrom}' ");
                }
                sql.Condition.Add("{:expDateFrom}", condition.ExpDateFrom);
            }
            else if (condition.ExpDateFrom.Year == 1 && condition.ExpDateTo.Year > 1)
            {
                if (B1DataAccess.Instance.IsHana)
                {
                }
                else
                {
                    sql.where(" and t0.ExpDate <= N'{:expDateTo}' ");
                }
                sql.Condition.Add("{:expDateTo}", condition.ExpDateTo);
            }

            // Item Group
            if (condition.ItmsGrpCod > 0)
            {
                if (B1DataAccess.Instance.IsHana)
                {
                }
                else
                {
                    sql.where(" and t1.ItmsGrpCod = {:itmsGrpCod} ");
                }
                sql.Condition.Add("{:itmsGrpCod}", condition.ItmsGrpCod);
            }

            // Warehouse
            if (CollectionUtils.IsAny(condition.WhsCode))
            {
                if (B1DataAccess.Instance.IsHana)
                {
                }
                else
                {
                    sql.where(" and t4.WhsCode in ({:whsCode}) ");
                }
                sql.Condition.Add("{:whsCode}", StringUtils.setInParameter(condition.WhsCode));
            }

            sql.groupBy(" t0.AbsEntry, t4.WhsCode ");
            sql.orderBy(" min(t0.ItemCode), min(t0.DistNumber), t4.WhsCode ");

            sql.Condition.Add("{:company}", ServiceDataAccess.Instance.getCompanyName(condition.Company));

            return sql.generateSqlWithLog();
        }

        public string getQueryBatchNumberTransactionReportDetail(ReportBatchNumberTransactionDetailCondition condition)
        {
            TemplateSQL sql = new TemplateSQL();

            if (B1DataAccess.Instance.IsHana)
            {
            }
            else
            {
                sql.select(" select t0.TransId, t0.ApplyEntry, t0.ApplyLine, t0.ApplyType, min(t0.LogEntry) as LogEntry, ");
                sql.select(" min(t0.AppDocNum) as AppDocNum, min(t0.DocDate) as DocDate, ");
                sql.select(" t0.LocCode, min(t0.CardCode) as CardCode, ");
                sql.select(" min(t0.CardName) as CardName, sum(t1.Quantity) as Quantity, ");
                sql.select(" sum(t1.AllocQty) as AllocQty, sum(t1.OrderedQty) as OrderedQty, t0.Instance, ");
                sql.select(" min(t4.BinCode) as BinCode, min(t4.SnBTransRptFirstBinViewBinCount) as SnBTransRptFirstBinViewBinCount, ");
                sql.select(" case when t0.ApplyType = '67' then concat('IM ', min(t0.AppDocNum)) ");
                sql.select(" when t0.ApplyType = '15' then concat('DN ', min(t0.AppDocNum)) ");
                sql.select(" when t0.ApplyType = '60' then concat('SO ', min(t0.AppDocNum)) ");
                sql.select(" when t0.ApplyType = '14' then concat('CN ', min(t0.AppDocNum)) ");
                sql.select(" when t0.ApplyType = '20' then concat('PD ', min(t0.AppDocNum)) ");
                sql.select(" when t0.ApplyType = '13' then concat('IN ', min(t0.AppDocNum)) ");
                sql.select(" when t0.ApplyType = '59' then concat('SI ', min(t0.AppDocNum)) ");
                sql.select(" when t0.ApplyType = '234000031' then concat('RR ', min(t0.AppDocNum)) ");
                sql.select(" when t0.ApplyType = '1250000001' then concat('TR ', min(t0.AppDocNum)) ");
                sql.select(" else cast(min(t0.AppDocNum) as nvarchar) end as PrefixDocNum, ");
                sql.select(" case when sum(t1.Quantity) > 0 then '0' ");
                sql.select(" when sum(t1.Quantity) = 0 and sum(t1.AllocQty) != 0 then '2' ");
                sql.select(" when sum(t1.OrderedQty) != 0 then '3' ");
                sql.select(" when sum(t1.Quantity) < 0 then '1' end as Direction, ");
                sql.select(" case when sum(t1.Quantity) > 0 then 'In' ");
                sql.select(" when sum(t1.Quantity) = 0 and sum(t1.AllocQty) != 0 then 'Allocated' ");
                sql.select(" when sum(t1.OrderedQty) != 0 then 'Return Candidate' ");
                sql.select(" when sum(t1.Quantity) < 0 then 'Out' end as DirectionName ");
                sql.from(" from {:company}..oitl t0 (nolock) ");
                sql.innerjoin(" {:company}..itl1 t1 (nolock) on t0.LogEntry = t1.LogEntry ");
                sql.innerjoin(" {:company}..oitm t2 (nolock) on t0.ItemCode = t2.ItemCode ");
                sql.leftjoin(" {:company}..ocrd t3 (nolock) on t0.CardCode = t3.CardCode ");
                sql.leftjoin(" {:company}..B1_SnBTransRptFirstBinView t4 on t0.LogEntry = t4.ITLEntry and t1.MdAbsEntry = t4.SnBMDAbs ");
                sql.leftjoin(" {:company}..obtn t5 (nolock) on t1.SysNumber = t5.SysNumber and t0.ItemCode = t5.ItemCode ");
                sql.where(" where 1 = 1 ");
                sql.where(" and t0.ItemCode = N'{:itemCode}' ");
                sql.where(" and t5.DistNumber = N'{:distNumber}' ");
                sql.where(" and t0.LocCode = N'{:whsCode}' ");
            }

            // Customer
            if (!StringUtils.isBlankOrNull(condition.CardCodeFrom) && !StringUtils.isBlankOrNull(condition.CardCodeTo))
            {
                if (B1DataAccess.Instance.IsHana)
                {
                }
                else
                {
                    sql.where(" and t0.CardCode between N'{:cardCodeFrom}' and N'{:cardCodeTo}' ");
                }
                sql.Condition.Add("{:cardCodeFrom}", condition.CardCodeFrom);
                sql.Condition.Add("{:cardCodeTo}", condition.CardCodeTo);
            }
            else if (!StringUtils.isBlankOrNull(condition.CardCodeFrom) && StringUtils.isBlankOrNull(condition.CardCodeTo))
            {
                if (B1DataAccess.Instance.IsHana)
                {
                }
                else
                {
                    sql.where(" and t0.CardCode >= N'{:cardCodeFrom}' ");
                }
                sql.Condition.Add("{:cardCodeFrom}", condition.CardCodeFrom);
            }
            else if (StringUtils.isBlankOrNull(condition.CardCodeFrom) && !StringUtils.isBlankOrNull(condition.CardCodeTo))
            {
                if (B1DataAccess.Instance.IsHana)
                {
                }
                else
                {
                    sql.where(" and t0.CardCode <= N'{:cardCodeTo}' ");
                }
                sql.Condition.Add("{:cardCodeTo}", condition.CardCodeTo);
            }

            sql.groupBy(" t0.TransId, t0.ApplyEntry, t0.ApplyLine, t0.ApplyType, t0.LocCode, t0.Instance, t0.LocType ");
            sql.orderBy(" t0.TransId, min(t0.LogEntry) ");

            sql.Condition.Add("{:itemCode}", condition.ItemCode);
            sql.Condition.Add("{:distNumber}", condition.BatchNum);
            sql.Condition.Add("{:whsCode}", condition.WhsCode);
            sql.Condition.Add("{:company}", ServiceDataAccess.Instance.getCompanyName(condition.Company));

            return sql.generateSqlWithLog();
        }

        public string getQueryUomGroupConverter(string company, int ugpEntry)
        {
            TemplateSQL sql = new TemplateSQL();

            if (B1DataAccess.Instance.IsHana)
            {
            }
            else
            {
                sql.select(" select t0.UgpEntry, t0.BaseUom, t2.UomCode as BaseUomCode, t1.UomEntry, t3.UomCode, t1.AltQty, t1.BaseQty ");
                sql.from(" from {:company}..ougp t0 (nolock) ");
                sql.innerjoin(" {:company}..ugp1 t1 (nolock) on t0.UgpEntry = t1.UgpEntry ");
                sql.leftjoin(" {:company}..ouom t2 (nolock) on t0.BaseUom = t2.UomEntry ");
                sql.leftjoin(" {:company}..ouom t3 (nolock) on t1.UomEntry = t3.UomEntry ");
                sql.where(" where 1 = 1 ");
                sql.where(" and t0.UgpEntry = {:ugpEntry} ");
                sql.orderBy(" t0.UgpEntry, t1.LineNum ");
            }

            sql.Condition.Add("{:ugpEntry}", ugpEntry);
            sql.Condition.Add("{:company}", ServiceDataAccess.Instance.getCompanyName(company));

            return sql.generateSqlWithLog();
        }

        public string getQueryUomGroupConverter(string company, string itemCode)
        {
            TemplateSQL sql = new TemplateSQL();

            if (B1DataAccess.Instance.IsHana)
            {
            }
            else
            {
                sql.select(" select t0.UgpEntry, t0.BaseUom, t2.UomCode as BaseUomCode, t1.UomEntry, t3.UomCode, t1.AltQty, t1.BaseQty ");
                sql.from(" from {:company}..ougp t0 (nolock) ");
                sql.innerjoin(" {:company}..ugp1 t1 (nolock) on t0.UgpEntry = t1.UgpEntry ");
                sql.leftjoin(" {:company}..ouom t2 (nolock) on t0.BaseUom = t2.UomEntry ");
                sql.leftjoin(" {:company}..ouom t3 (nolock) on t1.UomEntry = t3.UomEntry ");
                sql.leftjoin(" {:company}..oitm t4 (nolock) on t0.UgpEntry = t4.UgpEntry ");
                sql.where(" where 1 = 1 ");
                sql.where(" and t4.ItemCode = N'{:itemCode}' ");
                sql.orderBy(" t0.UgpEntry, t1.LineNum ");
            }

            sql.Condition.Add("{:itemCode}", itemCode);
            sql.Condition.Add("{:company}", ServiceDataAccess.Instance.getCompanyName(company));

            return sql.generateSqlWithLog();
        }

        public string getQueryUomGroupConverter(string company)
        {
            TemplateSQL sql = new TemplateSQL();

            if (B1DataAccess.Instance.IsHana)
            {
            }
            else
            {
                sql.select(" select t0.UgpEntry, t0.BaseUom, t2.UomCode as BaseUomCode, t1.UomEntry, t3.UomCode, t1.AltQty, t1.BaseQty ");
                sql.from(" from {:company}..ougp t0 (nolock) ");
                sql.innerjoin(" {:company}..ugp1 t1 (nolock) on t0.UgpEntry = t1.UgpEntry ");
                sql.leftjoin(" {:company}..ouom t2 (nolock) on t0.BaseUom = t2.UomEntry ");
                sql.leftjoin(" {:company}..ouom t3 (nolock) on t1.UomEntry = t3.UomEntry ");
                sql.where(" where 1 = 1 ");
                sql.orderBy(" t0.UgpEntry, t1.LineNum ");
            }

            sql.Condition.Add("{:company}", ServiceDataAccess.Instance.getCompanyName(company));

            return sql.generateSqlWithLog();
        }

        public string getQueryItemMasterForSO(ItemMasterCondition condition)
        {
            TemplateSQL sql = new TemplateSQL(condition.Paginable);

            if (B1DataAccess.Instance.IsHana)
            {
            }
            else
            {
                sql.select(" select t0.ItemCode, t0.ItemName, t5.Substitute as BpItemName, ");
                sql.select(" t0.UgpEntry, t0.CodeBars, t1.UomEntry, t1.UomCode, t1.UomName, ");
                sql.select(" t2.UomEntry as InUomEntry, t2.UomCode as InUomCode, t2.UomName as InUomName, ");
                sql.select(" t3.Code as TaxGroup, t3.Rate as TaxRate, t4.InStockQty, t4.AvaliableQty, ");
                sql.select(" t0.ManBtchNum as IsBatch, t6.Price as UnitPrice, isnull(t0.DfltWH, t7.DftResWhs) as WhsCode, ");
                sql.select(" (select b.Memo from {:company}..ocrd a left join {:company}..oslp b on a.SlpCode = b.SlpCode where a.CardCode = '{:cardCode}') as CostingCode ");
                sql.from(" from {:company}..oitm t0 (nolock) ");
                sql.leftjoin(" {:company}..ouom t1 (nolock) on t0.SUoMEntry = t1.UomEntry ");
                sql.leftjoin(" {:company}..ouom t2 (nolock) on t0.IUoMEntry = t2.UomEntry ");
                sql.leftjoin(" {:company}..ovtg t3 (nolock) on t0.VatGourpSa = t3.Code ");
                sql.leftjoin(" ( ");
                sql.append(" select ItemCode, sum(OnHand) as InStockQty, sum(OnHand - IsCommited + OnOrder) as AvaliableQty from {:company}..oitw group by ItemCode ");
                sql.append(" ) t4 on t0.ItemCode = t4.ItemCode ");
                sql.leftjoin(" {:company}..oscn t5 on t0.ItemCode = t5.ItemCode and t5.CardCode = N'{:cardCode}' ");
                sql.leftjoin(" {:company}..itm1 t6 on t0.ItemCode = t6.ItemCode and t6.PriceList = (select ListNum from {:company}..ocrd where CardCode = '{:cardCode}') ");
                sql.leftjoin(" {:company}..oadm t7 on 1 = 1 ");
                sql.where(" where 1 = 1 ");
                sql.where(" and (t0.InvntItem = 'Y' or t0.ItemCode in ('OR102004', 'OR102009') ) ");
                sql.where(" and t0.SellItem = 'Y' ");
                sql.where(" and t0.frozenFor = 'N' ");
            }

            if (!StringUtils.isBlankOrNull(condition.ItemCode))
            {
                if (B1DataAccess.Instance.IsHana)
                {
                }
                else
                {
                    sql.where(" and t0.ItemCode like N'{:itemCode}' ");
                }
                sql.ConditionLike.Add("{:itemCode}", condition.ItemCode);
            }

            if (!StringUtils.isBlankOrNull(condition.ItemName))
            {
                if (B1DataAccess.Instance.IsHana)
                {
                }
                else
                {
                    sql.where(" and t0.ItemName like N'{:itemName}' ");
                }
                sql.ConditionLike.Add("{:itemName}", condition.ItemName);
            }

            if (!StringUtils.isBlankOrNull(condition.BpItemName))
            {
                if (B1DataAccess.Instance.IsHana)
                {
                }
                else
                {
                    sql.where(" and t5.Substitute like N'{:bpItemName}' ");
                }
                sql.ConditionLike.Add("{:bpItemName}", condition.BpItemName);
            }

            if (condition.Paginable != null)
            {
                if (B1DataAccess.Instance.IsHana)
                {
                }
                else
                {
                    sql.orderBy(" t0.ItemCode ");
                }
            }

            //sql.Condition.Add("{:slpCode}", condition.SlpCode);
            sql.Condition.Add("{:cardCode}", condition.CardCode);
            sql.Condition.Add("{:company}", ServiceDataAccess.Instance.getCompanyName(condition.Company));

            return sql.generateSqlWithLog();
        }

        public string getQueryBusinessPartnerForSO(BusinessPartnerCondition condition)
        {
            TemplateSQL sql = new TemplateSQL(condition.Paginable);

            if (B1DataAccess.Instance.IsHana)
            {
            }
            else
            {
                sql.select(" select t0.CardCode, t0.CardName, t0.LicTradNum as FederalTaxID, ");
                sql.select(" t0.Balance, t0.OrdersBal, t0.GroupNum as PayTermsGrpCode, t2.PymntGroup, t0.fatherCard, t0.SlpCode as SalesPersonCode, ");
                sql.select(" t0.BillToDef, t0.ShipToDef, t0.ListNum as PriceListNum, ");
                sql.select(" t3.SlpName as SalesPersonName, t4.ListName as PriceListName, t5.CntctCode as CntctPrsnDef, ");
                //
                sql.select(" case when t0.fatherCard is null or t0.fatherCard = '' then t0.CreditLine else t6.CreditLine end as CreditLimit, ");
                sql.select(" case when t0.fatherCard is null or t0.fatherCard = '' then t0.DebtLine else t6.DebtLine end as CommitmentLimit, ");
                sql.select(" case when t0.fatherCard is null or t0.fatherCard = '' then t0.OrdersBal else t6.OrdersBal end as Orders, ");
                sql.select(" case when t0.fatherCard is null or t0.fatherCard = '' then t0.Balance else t6.Balance end as OutstandingDebt, ");
                sql.select(" case when t0.fatherCard is null then t0.DNotesBal else t6.DNotesBal end as OverdueDebt, ");
                //
                // UDF
                sql.select(" t1.U_ISS_BpBrnCode as BpBrnCode, t1.U_ISS_BpBrnName as BpBrnName, (select Code from {:company}..[@ISS_BRANCH] where U_IsDefault = 'Y') as VatBranch ");
                // UDF
                sql.from(" from {:company}..ocrd t0 (nolock) ");
                sql.leftjoin(" {:company}..crd1 t1 (nolock) on t0.CardCode = t1.CardCode and t0.ShipToDef = t1.Address and t1.AdresType = 'S' ");
                sql.leftjoin(" {:company}..octg t2 (nolock) on t0.GroupNum = t2.GroupNum ");
                sql.leftjoin(" {:company}..oslp t3 (nolock) on t0.SlpCode = t3.SlpCode ");
                sql.leftjoin(" {:company}..opln t4 (nolock) on t0.ListNum = t4.ListNum ");
                sql.leftjoin(" {:company}..ocpr t5 (nolock) on t0.CntctPrsn = t5.Name and t0.CardCode = t5.CardCode ");
                sql.leftjoin(" {:company}..ocrd t6 (nolock) on t0.fatherCard = t6.CardCode ");
                sql.where(" where 1 = 1 ");
                sql.where(" and t0.CardType = 'C' ");
                sql.where(" and t0.frozenFor = 'N' ");
            }

            // CR Authorize
            if (CollectionUtils.IsAny(condition.UserSalesEmployee))
            {
                if (B1DataAccess.Instance.IsHana)
                {
                }
                else
                {
                    sql.where(" and t0.SlpCode in ({:slpCode}) ");
                }
                sql.Condition.Add("{:slpCode}", StringUtils.setInParameter(condition.UserSalesEmployee));
            }

            if (!StringUtils.isBlankOrNull(condition.CardCode))
            {
                if (B1DataAccess.Instance.IsHana)
                {
                }
                else
                {
                    sql.where(" and t0.CardCode like N'{:cardCode}' ");
                }
                sql.ConditionLike.Add("{:cardCode}", condition.CardCode);
            }

            if (!StringUtils.isBlankOrNull(condition.CardName))
            {
                if (B1DataAccess.Instance.IsHana)
                {
                }
                else
                {
                    sql.where(" and t0.CardName like N'{:cardName}' ");
                }
                sql.ConditionLike.Add("{:cardName}", condition.CardName);
            }

            if (condition.Paginable != null)
            {
                if (B1DataAccess.Instance.IsHana)
                {
                }
                else
                {
                    sql.orderBy(" t0.CardCode ");
                }
            }

            sql.Condition.Add("{:company}", ServiceDataAccess.Instance.getCompanyName(condition.Company));

            return sql.generateSqlWithLog();
        }

        public string getQueryItemBatchNumberSelection(ItemMasterCondition condition)
        {
            TemplateSQL sql = new TemplateSQL();

            if (B1DataAccess.Instance.IsHana)
            {
            }
            else
            {
                sql.select(" SELECT t3.AbsEntry, t0.ItemCode, t1.WhsCode, t0.BatchNum as BatchNumber, t0.Quantity, ");
                sql.select(" t3.ExpDate, t3.MnfDate, t3.InDate, t3.MnfSerial, t3.LotNumber, t3.location, ");
                sql.select(" DATEDIFF(DD, GETDATE(), t3.ExpDate) as Days, ");
                sql.select(" case when DATEDIFF(DD, GETDATE(), t3.ExpDate) < 90 then 'R' ");
                sql.select(" when DATEDIFF(DD, GETDATE(), t3.ExpDate) < 180 then 'Y' ");
                sql.select(" when DATEDIFF(DD, GETDATE(), t3.ExpDate) > 180 then 'G' end as Color ");
                sql.from(" from {:company}..oibt t0 (nolock) ");
                sql.innerjoin(" {:company}..oitw t1 (nolock) on t0.ItemCode = t1.ItemCode and t0.WhsCode = t1.WhsCode ");
                sql.innerjoin(" {:company}..oitm t2 (nolock) on t1.ItemCode = t2.ItemCode ");
                sql.innerjoin(" {:company}..obtn t3 (nolock) on t0.ItemCode = t3.ItemCode and t0.SysNumber = t3.SysNumber ");
                sql.where(" where 1 = 1 ");
                sql.where(" and t0.Quantity <> 0 ");
                sql.where(" and t0.ItemCode = N'{:itemCode}' ");
                sql.where(" and t1.WhsCode = N'{:whsCode}' ");
                //sql.append(" group by t0.ItemCode, t1.WhsCode, t0.BatchNum, t0.Quantity ");
            }

            sql.Condition.Add("{:itemCode}", condition.ItemCode);
            sql.Condition.Add("{:whsCode}", condition.WhsCode);
            sql.Condition.Add("{:company}", ServiceDataAccess.Instance.getCompanyName(condition.Company));

            return sql.generateSqlWithLog();
        }

        public string getQueryItemWhsOnHand(ItemMasterCondition condition)
        {
            TemplateSQL sql = new TemplateSQL();

            if (B1DataAccess.Instance.IsHana)
            {
            }
            else
            {
                sql.select(" select t0.ItemCode, t0.WhsCode, t1.WhsName, t0.OnHand, t0.IsCommited, t0.OnOrder, ");
                sql.select(" ((t0.OnHand + t0.OnOrder) - t0.IsCommited) as Avaliable ");
                sql.from(" from {:company}..oitw t0 (nolock) ");
                sql.innerjoin(" {:company}..owhs t1 (nolock) on t0.WhsCode = t1.WhsCode ");
                sql.where(" where 1 = 1 ");
                sql.where(" and ((t0.OnHand + t0.OnOrder) - t0.IsCommited) <> 0 ");
                sql.where(" and t0.ItemCode = N'{:itemCode}' ");
            }

            sql.Condition.Add("{:itemCode}", condition.ItemCode);
            sql.Condition.Add("{:company}", ServiceDataAccess.Instance.getCompanyName(condition.Company));

            return sql.generateSqlWithLog();
        }

        public string getQueryCustomerPaymentReport(ReportCustomerPaymentCondition condition)
        {
            TemplateSQL sql = new TemplateSQL();

            if (B1DataAccess.Instance.IsHana)
            {
            }
            else
            {
                sql.select(" select T0.CardCode, T0.CardName, T2.PymntGroup AS PaymentTerm,  ");
                sql.select(" (CASE WHEN T1.FatherCard != '' THEN (SELECT CreditLine FROM OCRD WHERE CardCode = T1.FatherCard) ELSE T1.CreditLine END) AS CreditLimit, ");
                sql.select(" (CASE WHEN T1.FatherCard != '' THEN (SELECT Balance + OrdersBal FROM OCRD WHERE CardCode = T1.FatherCard) ELSE (T1.Balance + T1.OrdersBal) END) AS CreditBalance, ");
                sql.select(" (CASE WHEN T1.FatherCard != '' THEN (SELECT CreditLine - Balance FROM OCRD WHERE CardCode = T1.FatherCard) ELSE (T1.CreditLine - T1.Balance ) END) AS AvalBalance, ");
                sql.select(" T0.DocEntry, T0.DocNum, T0.DocDate, T0.NumAtCard, T0.DocDueDate, (CASE WHEN T0.ReceiptNum != '' THEN T4.DocDueDate ELSE GETDATE() END) AS IncomingDate, ");
                sql.select(" T3.SlpCode, T3.SlpName, T0.DocTotal, T0.PaidToDate AS PaidTotal, (T0.DocTotal - T0.PaidToDate) as BalanceDue, ");
                sql.select(" (CASE WHEN T0.DocTotal - T0.PaidToDate = 0 THEN 'Paid' ELSE 'UnPaid' END) AS Status, ");
                sql.select(" (CASE WHEN T4.CashSumSy <> 0 THEN 'Cash' WHEN T4.CheckSumSy <> 0 THEN 'Check' WHEN T4.TrsfrSumsy <> 0 THEN 'Transfer' END) AS 'PaymentMethod', ");
                sql.select(" (CASE WHEN T0.ReceiptNum != '' THEN (DateDiff(DD, T0.DocDueDate, T4.DocDueDate)) ELSE (DateDiff(DD, T0.DocDueDate, GETDATE())) END) AS Days ");
                sql.from(" from {:company}..oinv T0 (nolock) ");
                sql.innerjoin(" {:company}..ocrd T1 (nolock) on T0.CardCode = T1.CardCode ");
                sql.innerjoin(" {:company}..octg T2 (nolock) on T1.GroupNum = T2.GroupNum ");
                sql.innerjoin(" {:company}..oslp T3 (nolock) on T1.SlpCode = T3.SlpCode ");
                sql.leftjoin(" {:company}..orct T4 (nolock) on T0.ReceiptNum = T4.DocNum ");
                sql.leftjoin(" {:company}..itr1 T5 (nolock) on T0.TransId = T5.TransId ");
                sql.leftjoin("{:company}.. oitr T6 (nolock) on T5.ReconNum = T6.ReconNum ");
                sql.where(" where 1 = 1 ");
                sql.where(" and T0.CardCode LIKE 'C%' ");
                //sql.where(" and (T0.CardCode LIKE 'C%' AND T6.ReconType IS NULL AND T0.ReceiptNum IS NULL) ");
                //sql.where(" or (T0.CardCode LIKE 'C%' AND T6.ReconType IS NOT NULL AND T0.ReceiptNum IS NOT NULL) ");
            }

            // Customer
            if (!StringUtils.isBlankOrNull(condition.CardCodeFrom) && !StringUtils.isBlankOrNull(condition.CardCodeTo))
            {
                if (B1DataAccess.Instance.IsHana)
                {
                }
                else
                {
                    sql.where(" and t0.CardCode between N'{:cardCodeFrom}' and N'{:cardCodeTo}' ");
                }
                sql.Condition.Add("{:cardCodeFrom}", condition.CardCodeFrom);
                sql.Condition.Add("{:cardCodeTo}", condition.CardCodeTo);
            }
            else if (!StringUtils.isBlankOrNull(condition.CardCodeFrom) && StringUtils.isBlankOrNull(condition.CardCodeTo))
            {
                if (B1DataAccess.Instance.IsHana)
                {
                }
                else
                {
                    sql.where(" and t0.CardCode >= N'{:cardCodeFrom}' ");
                }
                sql.Condition.Add("{:cardCodeFrom}", condition.CardCodeFrom);
            }
            else if (StringUtils.isBlankOrNull(condition.CardCodeFrom) && !StringUtils.isBlankOrNull(condition.CardCodeTo))
            {
                if (B1DataAccess.Instance.IsHana)
                {
                }
                else
                {
                    sql.where(" and t0.CardCode <= N'{:cardCodeTo}' ");
                }
                sql.Condition.Add("{:cardCodeTo}", condition.CardCodeTo);
            }

            // Invoice Posting Date
            if (condition.DocDateFrom.Year > 1 && condition.DocDateTo.Year > 1)
            {
                if (B1DataAccess.Instance.IsHana)
                {
                }
                else
                {
                    sql.where(" and t0.DocDate between N'{:docDateFrom}' and N'{:docDateTo}' ");
                }
                sql.Condition.Add("{:docDateFrom}", condition.DocDateFrom);
                sql.Condition.Add("{:docDateTo}", condition.DocDateTo);
            }
            else if (condition.DocDateFrom.Year > 1 && condition.DocDateTo.Year == 1)
            {
                if (B1DataAccess.Instance.IsHana)
                {
                }
                else
                {
                    sql.where(" and t0.DocDate >= N'{:docDateFrom}' ");
                }
                sql.Condition.Add("{:docDateFrom}", condition.DocDateFrom);
            }
            else if (condition.DocDateFrom.Year == 1 && condition.DocDateTo.Year > 1)
            {
                if (B1DataAccess.Instance.IsHana)
                {
                }
                else
                {
                    sql.where(" and t0.DocDate <= N'{:docDateTo}' ");
                }
                sql.Condition.Add("{:docDateTo}", condition.DocDateTo);
            }

            // Invoice Due Date
            if (condition.DocDueDateFrom.Year > 1 && condition.DocDueDateTo.Year > 1)
            {
                if (B1DataAccess.Instance.IsHana)
                {
                }
                else
                {
                    sql.where(" and t0.DocDueDate between N'{:docDueDateFrom}' and N'{:docDueDateTo}' ");
                }
                sql.Condition.Add("{:docDueDateFrom}", condition.DocDueDateFrom);
                sql.Condition.Add("{:docDueDateTo}", condition.DocDueDateTo);
            }
            else if (condition.DocDueDateFrom.Year > 1 && condition.DocDueDateTo.Year == 1)
            {
                if (B1DataAccess.Instance.IsHana)
                {
                }
                else
                {
                    sql.where(" and t0.DocDueDate >= N'{:docDueDateFrom}' ");
                }
                sql.Condition.Add("{:docDueDateFrom}", condition.DocDueDateFrom);
            }
            else if (condition.DocDueDateFrom.Year == 1 && condition.DocDueDateTo.Year > 1)
            {
                if (B1DataAccess.Instance.IsHana)
                {
                }
                else
                {
                    sql.where(" and t0.DocDueDate <= N'{:docDueDateTo}' ");
                }
                sql.Condition.Add("{:docDueDateTo}", condition.DocDueDateTo);
            }

            if (!StringUtils.isBlankOrNull(condition.Status))
            {
                if (B1DataAccess.Instance.IsHana)
                {
                }
                else
                {
                    if ("P".Equals(condition.Status))
                    {
                        sql.where(" and (T0.DocTotal - T0.PaidToDate) = 0");
                    }
                    else if ("U".Equals(condition.Status))
                    {
                        sql.where(" and (T0.DocTotal - T0.PaidToDate) != 0");
                    }
                }
            }

            // Sales Employee
            if (CollectionUtils.IsAny(condition.SlpCode))
            {
                if (B1DataAccess.Instance.IsHana)
                {
                }
                else
                {
                    sql.where(" and t3.SlpCode in ({:slpCode}) ");
                }
                sql.Condition.Add("{:slpCode}", StringUtils.setInParameter(condition.SlpCode));
            }
            else
            {
                // CR Authorize
                if (CollectionUtils.IsAny(condition.UserSalesEmployee))
                {
                    if (B1DataAccess.Instance.IsHana)
                    {
                    }
                    else
                    {
                        sql.where(" and t3.SlpCode in ({:slpCode}) ");
                    }
                    sql.Condition.Add("{:slpCode}", StringUtils.setInParameter(condition.UserSalesEmployee));
                }
            }
            //if (condition.SlpCodeFrom > 0 && condition.SlpCodeTo > 0)
            //{
            //    if (B1DataAccess.Instance.IsHana)
            //    {
            //    }
            //    else
            //    {
            //        sql.where(" and t3.SlpCode between {:slpCodeFrom} and {:slpCodeTo} ");
            //    }
            //    sql.Condition.Add("{:slpCodeFrom}", condition.SlpCodeFrom);
            //    sql.Condition.Add("{:slpCodeTo}", condition.SlpCodeTo);
            //}
            //else if (condition.SlpCodeFrom > 0 && condition.SlpCodeTo == 0)
            //{
            //    if (B1DataAccess.Instance.IsHana)
            //    {
            //    }
            //    else
            //    {
            //        sql.where(" and t3.SlpCode >= {:slpCodeFrom} ");
            //    }
            //    sql.Condition.Add("{:slpCodeFrom}", condition.SlpCodeFrom);
            //}
            //else if (condition.SlpCodeFrom == 0 && condition.SlpCodeTo > 0)
            //{
            //    if (B1DataAccess.Instance.IsHana)
            //    {
            //    }
            //    else
            //    {
            //        sql.where(" and t3.SlpCode <= {:slpCodeTo} ");
            //    }
            //    sql.Condition.Add("{:slpCodeTo}", condition.SlpCodeTo);
            //}

            if (B1DataAccess.Instance.IsHana)
            {
            }
            else
            {
                sql.groupBy(" T0.DocEntry, T0.DocNum, T0.NumAtCard, T0.DocDate, T0.DocDueDate, T0.ReceiptNum, T0.CardCode, T0.CardName, T0.DocTotal, T0.PaidToDate, ");
                sql.append(" T1.FatherCard, T1.CreditLine, T1.Balance, T1.OrdersBal, T2.PymntGroup, T2.ExtraDays,  ");
                sql.append(" T3.SlpCode, T3.SlpName, T4.DocDueDate, T4.TaxDate, T4.CashSumSy, T4.CheckSumSy, T4.TrsfrSumSy ");
            }


            sql.Condition.Add("{:company}", ServiceDataAccess.Instance.getCompanyName(condition.Company));

            return sql.generateSqlWithLog();
        }

        public string getQueryFindSpecialPrice(ItemPriceCondition condition)
        {
            TemplateSQL sql = new TemplateSQL();

            if (B1DataAccess.Instance.IsHana)
            {
            }
            else
            {
                sql.select(" select t0.CardCode, t0.ItemCode, t0.ListNum as OSPPListNum, t0.SrcPrice, ");
                sql.select(" t0.Discount as OSPPDiscount, t0.Price as OSPPPrice, t0.Currency as OSPPCurrency, t0.AutoUpdt as OSPPAutoUpdt, ");
                sql.select(" t1.LINENUM, t1.ListNum as SPP1ListNum, t1.Discount as SPP1Discount, t1.Price as SPP1Price, ");
                sql.select(" t1.Currency as SPP1Currency, t1.AutoUpdt as SPP1AutoUpdt, ");
                sql.select(" t2.SPP2LNum, t2.UomEntry as SPP2UomEntry, t2.Amount as SPP2Quantity, t2.Discount as SPP2Discount, ");
                sql.select(" t2.Price as SPP2Price, t2.Currency as SPP2Currency, t3.UomEntry as PLUomEntry, ");
                sql.select(" t4.Price, t6.BaseQty as InvQty, t3.Price as UnitPrice ");
                //sql.select(" t4.Price, t6.BaseQty as InvQty, (t4.Price * t6.BaseQty) as UnitPrice ");
                sql.from(" from {:company}..ospp t0 (nolock) ");
                sql.leftjoin(" {:company}..spp1 t1 (nolock) on t0.CardCode = t1.CardCode and t0.ItemCode = t1.ItemCode ");
                sql.append(" and t1.FromDate <= '{:fromDate}' and (t1.ToDate is null or t1.ToDate >= '{:toDate}') ");
                sql.leftjoin(" {:company}..spp2 t2 (nolock) on t1.CardCode = t2.CardCode and t1.ItemCode = t2.ItemCode and t1.LINENUM = t2.SPP1LNum ");
                sql.append(" and ((t2.UomEntry = {:uomEntry} and t2.Amount <= {:quantity} ) or (t2.UomEntry = {:uomEntry} and t2.Amount <= {:quantity}) ) ");
                sql.leftjoin(" {:company}..itm9 t3 (nolock) on t0.ItemCode = t3.ItemCode and t3.PriceList = {:priceList} and t3.UomEntry = {:uomEntry} ");
                sql.leftjoin(" {:company}..itm1 t4 (nolock) on t0.ItemCode = t4.ItemCode and t4.PriceList = {:priceList} ");
                sql.leftjoin(" {:company}..oitm t5 (nolock) on t0.ItemCode = t5.ItemCode ");
                sql.leftjoin(" {:company}..ugp1 t6 (nolock) on t5.UgpEntry = t6.UgpEntry and t5.SUomEntry = t6.UomEntry  ");
                sql.where(" where 1 = 1 ");
                sql.where(" and t0.ItemCode = N'{:itemCode}' ");
                sql.where(" and (t0.CardCode = N'{:cardCode}' or t0.CardCode = N'{:cardCodePL}' ) ");
                sql.where(" and (t2.Amount is null or (t2.UomEntry = {:uomEntry} and t2.Amount in ( ");
                sql.where(" (select MAX(u0.Amount) from {:company}..spp2 u0  ");
                sql.where(" where 1 = 1 ");
                sql.where(" and u0.CardCode = t2.CardCode ");
                sql.where(" and u0.ItemCode = t2.ItemCode ");
                sql.where(" and u0.SPP1LNum = t2.SPP1LNum ");
                sql.where(" and u0.UomEntry = t2.UomEntry ");
                sql.where(" and u0.Amount <= {:quantity}  )) )) ");
            }

            sql.Condition.Add("{:itemCode}", condition.ItemCode);
            sql.Condition.Add("{:fromDate}", condition.Date);
            sql.Condition.Add("{:toDate}", condition.Date);
            sql.Condition.Add("{:uomEntry}", condition.UomEntry);
            sql.Condition.Add("{:priceList}", condition.PriceList);
            sql.Condition.Add("{:quantity}", condition.Quantity);
            sql.Condition.Add("{:cardCode}", condition.CardCode);
            sql.Condition.Add("{:cardCodePL}", "*" + condition.PriceList);
            sql.Condition.Add("{:company}", ServiceDataAccess.Instance.getCompanyName(condition.Company));

            return sql.generateSqlWithLog();
        }

        public string getQueryCheckDimension(string company)
        {
            TemplateSQL sql = new TemplateSQL();

            if (B1DataAccess.Instance.IsHana)
            {
            }
            else
            {
                sql.select(" select t0.DimCode ");
                sql.from(" from {:company}..odim t0 (nolock) ");
                sql.where(" where 1 = 1 ");
                sql.where(" and t0.DimActive = 'Y' ");
            }

            sql.Condition.Add("{:company}", ServiceDataAccess.Instance.getCompanyName(company));

            return sql.generateSqlWithLog();
        }

        public string getQueryDimension(string company)
        {
            TemplateSQL sql = new TemplateSQL();

            if (B1DataAccess.Instance.IsHana)
            {
            }
            else
            {
                sql.select(" select distinct t0.DimCode, t0.OcrCode, t0.OcrName ");
                sql.from(" from {:company}..oocr t0 (nolock) ");
                sql.innerjoin(" {:company}..ocr1 t1 (nolock) ON t0.OcrCode = t1.OcrCode ");
                sql.where(" where 1 = 1 ");
                sql.where(" and t0.Active = 'Y' ");
                sql.where(" and (t1.ValidFrom is null or t1.ValidFrom <= GETDATE() ) ");
                sql.where(" and (t1.ValidTo is null or t1.ValidTo >= GETDATE() ) ");
                sql.orderBy(" t0.OcrCode ");
            }

            sql.Condition.Add("{:company}", ServiceDataAccess.Instance.getCompanyName(company));

            return sql.generateSqlWithLog();
        }

        public string getQuerySearchSeriesByCondition(SeriesCondition condition)
        {
            TemplateSQL sql = new TemplateSQL();

            if (B1DataAccess.Instance.IsHana)
            {
            }
            else
            {
                sql.select(" select t0.Series, t0.SeriesName ");
                sql.from(" from {:company}..nnm1 t0 (nolock) ");
                sql.innerjoin(" {:company}..ofpr t1 (nolock) ON t0.Indicator = t1.Indicator ");
                sql.where(" where 1 = 1 ");
                sql.where(" and t0.ObjectCode = '17' ");
                sql.where(" and t0.Locked = 'N' ");
                sql.where(" and t1.PeriodStat in ('N', 'S') ");
                sql.where(" and t0.SeriesName like 'SO%' ");
                sql.where(" and '{:docDate}' between t1.F_RefDate and t1.T_RefDate ");
            }

            sql.Condition.Add("{:docDate}", condition.DocDate);
            sql.Condition.Add("{:company}", ServiceDataAccess.Instance.getCompanyName(condition.Company));

            return sql.generateSqlWithLog();
        }

        public string getQueryPurchaseOrderMovement(ReportPurchaseOrderMovementCondition condition)
        {
            TemplateSQL sql = new TemplateSQL();

            if (B1DataAccess.Instance.IsHana)
            {
            }
            else
            {
                sql.select(" select t0.DocEntry, t0.DocNum, t0.DocDate, t0.DocDueDate, t2.SlpCode, t2.SlpName, t0.CardCode, t0.CardName, t1.ItemCode, t1.OcrCode, ");
                sql.select(" t1.Dscription as ItemName, t1.Quantity, t1.UomCode, t1.OpenQty, t1.Currency, STR(t1.Price,12,2) AS Price, t0.U_WGE_P08, t0.U_WGE_S07, ");
                sql.select(" t0.U_WGE_P04, t0.U_WGE_P05, t0.U_WGE_P10 ");
                sql.from(" from {:company}..opor t0 (nolock) ");
                sql.innerjoin(" {:company}..por1 t1 (nolock) ON t0.DocEntry = t1.DocEntry ");
                sql.innerjoin(" {:company}..oslp t2 (nolock) ON t0.SlpCode = t2.SlpCode ");
                sql.where(" where 1 = 1 ");
                sql.where(" and t0.CANCELED = 'N' ");
            }

            // Customer
            if (!StringUtils.isBlankOrNull(condition.CardCodeFrom) && !StringUtils.isBlankOrNull(condition.CardCodeTo))
            {
                if (B1DataAccess.Instance.IsHana)
                {
                }
                else
                {
                    sql.where(" and t0.CardCode between N'{:cardCodeFrom}' and N'{:cardCodeTo}' ");
                }
                sql.Condition.Add("{:cardCodeFrom}", condition.CardCodeFrom);
                sql.Condition.Add("{:cardCodeTo}", condition.CardCodeTo);
            }
            else if (!StringUtils.isBlankOrNull(condition.CardCodeFrom) && StringUtils.isBlankOrNull(condition.CardCodeTo))
            {
                if (B1DataAccess.Instance.IsHana)
                {
                }
                else
                {
                    sql.where(" and t0.CardCode >= N'{:cardCodeFrom}' ");
                }
                sql.Condition.Add("{:cardCodeFrom}", condition.CardCodeFrom);
            }
            else if (StringUtils.isBlankOrNull(condition.CardCodeFrom) && !StringUtils.isBlankOrNull(condition.CardCodeTo))
            {
                if (B1DataAccess.Instance.IsHana)
                {
                }
                else
                {
                    sql.where(" and t0.CardCode <= N'{:cardCodeTo}' ");
                }
                sql.Condition.Add("{:cardCodeTo}", condition.CardCodeTo);
            }

            // Items
            if (!StringUtils.isBlankOrNull(condition.ItemCodeFrom) && !StringUtils.isBlankOrNull(condition.ItemCodeTo))
            {
                if (B1DataAccess.Instance.IsHana)
                {
                }
                else
                {
                    sql.where(" and t1.ItemCode between N'{:itemCodeFrom}' and N'{:itemCodeTo}' ");
                }
                sql.Condition.Add("{:itemCodeFrom}", condition.ItemCodeFrom);
                sql.Condition.Add("{:itemCodeTo}", condition.ItemCodeTo);
            }
            else if (!StringUtils.isBlankOrNull(condition.ItemCodeFrom) && StringUtils.isBlankOrNull(condition.ItemCodeTo))
            {
                if (B1DataAccess.Instance.IsHana)
                {
                }
                else
                {
                    sql.where(" and t1.ItemCode >= N'{:itemCodeFrom}' ");
                }
                sql.Condition.Add("{:itemCodeFrom}", condition.ItemCodeFrom);
            }
            else if (StringUtils.isBlankOrNull(condition.ItemCodeFrom) && !StringUtils.isBlankOrNull(condition.ItemCodeTo))
            {
                if (B1DataAccess.Instance.IsHana)
                {
                }
                else
                {
                    sql.where(" and t1.ItemCode <= N'{:itemCodeTo}' ");
                }
                sql.Condition.Add("{:itemCodeTo}", condition.ItemCodeTo);
            }

            // Estimate time of arrival (Posting Date)
            if (condition.EtaDateFrom.Year > 1 && condition.EtaDateTo.Year > 1)
            {
                if (B1DataAccess.Instance.IsHana)
                {
                }
                else
                {
                    sql.where(" and t0.U_WGE_P05 between N'{:etaDateFrom}' and N'{:etaDateTo}' ");
                    //sql.where(" and t0.DocDate between N'{:etaDateFrom}' and N'{:etaDateTo}' ");
                }
                sql.Condition.Add("{:etaDateFrom}", condition.EtaDateFrom);
                sql.Condition.Add("{:etaDateTo}", condition.EtaDateTo);
            }
            else if (condition.EtaDateFrom.Year > 1 && condition.EtaDateTo.Year == 1)
            {
                if (B1DataAccess.Instance.IsHana)
                {
                }
                else
                {
                    sql.where(" and t0.U_WGE_P05 >= N'{:etaDateFrom}' ");
                    //sql.where(" and t0.DocDate >= N'{:etaDateFrom}' ");
                }
                sql.Condition.Add("{:etaDateFrom}", condition.EtaDateFrom);
            }
            else if (condition.EtaDateFrom.Year == 1 && condition.EtaDateTo.Year > 1)
            {
                if (B1DataAccess.Instance.IsHana)
                {
                }
                else
                {
                    sql.where(" and t0.U_WGE_P05 <= N'{:etaDateTo}' ");
                    //sql.where(" and t0.DocDate <= N'{:etaDateTo}' ");
                }
                sql.Condition.Add("{:etaDateTo}", condition.EtaDateTo);
            }

            if (!StringUtils.isBlankOrNull(condition.DocStatus))
            {
                if (B1DataAccess.Instance.IsHana)
                {
                }
                else
                {
                    sql.where(" and t0.DocStatus = '{:docStatus}' ");
                }
                sql.Condition.Add("{:docStatus}", condition.DocStatus);
            }

            sql.Condition.Add("{:company}", ServiceDataAccess.Instance.getCompanyName(condition.Company));

            return sql.generateSqlWithLog();
        }

        public string getQuerySearchSalesOrderByCondition(SalesOrderCondition condition)
        {
            TemplateSQL sql = new TemplateSQL(condition.Paginable);

            if (B1DataAccess.Instance.IsHana)
            {
            }
            else
            {
                sql.select(" select t0.DocEntry, t0.DocNum, t0.CardCode, t0.CardName, t0.DocDate, t0.DocDueDate, t0.TaxDate, ");
                sql.select(" t0.DocStatus, t0.CANCELED, t0.SlpCode as SalesPersonCode, t0.DocTotal, ");
                sql.select(" '{:isDraft}' as IsDraft, case when 'Y' = '{:isDraft}' then t0.DocEntry else t0.DocNum end as DocKey, ");
                sql.select(" case when 'Y' = '{:isDraft}' and t1.U_JWS_Status = 'Y' then 'Approved' ");
                sql.select(" when 'Y' = '{:isDraft}' and t1.U_JWS_Status = 'N' then 'Rejected' ");
                sql.select(" when 'Y' = '{:isDraft}' and t1.U_JWS_Status = 'W' then 'Pending' end as WddStatus ");
                sql.from(" from {:company}..{:tableName} t0 (nolock) ");

                sql.leftjoin(" ( ");
                sql.append(" select a.U_JWS_DocEntry, ");
                sql.append(" case when a.Rejected > 0 then 'N' ");
                sql.append(" when a.Pending > 0 then 'W' ");
                sql.append(" when a.Approved > 0 and a.Rejected = 0 then 'Y' end as U_JWS_Status ");
                sql.append(" from ( ");

                sql.append(" select t0.U_JWS_DocEntry, ");
                sql.append(" sum(case when t0.U_JWS_Status = 'W' then 1 else 0 end) as Pending, ");
                sql.append(" sum(case when t0.U_JWS_Status = 'Y' then 1 else 0 end) as Approved, ");
                sql.append(" sum(case when t0.U_JWS_Status = 'N' then 1 else 0 end) as Rejected ");
                sql.append(" from {:company}..[@JWS_OAWS] t0 (nolock) ");
                sql.append(" where 1 = 1 ");
                sql.append(" group by t0.U_JWS_DocEntry ");

                sql.append(" ) a ");

                sql.append(" ) t1 on t0.DocEntry = t1.U_JWS_DocEntry ");

                sql.where(" where 1 = 1 ");
                sql.where(" and (t1.U_JWS_Status in ('Y', 'N', 'W') or 'N' = '{:isDraft}') ");
            }

            if (condition.DocKey > 0)
            {
                if (B1DataAccess.Instance.IsHana)
                {
                }
                else
                {
                    if (condition.IsDraft)
                    {
                        sql.where(" and t0.DocEntry = {:docKey} ");
                    }
                    else
                    {
                        sql.where(" and t0.DocNum = {:docKey} ");
                    }
                    sql.Condition.Add("{:docKey}", condition.DocKey);
                }
            }

            if (!StringUtils.isBlankOrNull(condition.DocStatus))
            {
                if (B1DataAccess.Instance.IsHana)
                {
                }
                else
                {
                    sql.where(" and t0.DocStatus = '{:docStatus}' ");
                }
                sql.Condition.Add("{:docStatus}", condition.DocStatus);
            }

            if (CollectionUtils.IsAny(condition.SlpCode))
            {
                if (B1DataAccess.Instance.IsHana)
                {
                }
                else
                {
                    sql.where(" and t0.SlpCode in ({:slpCode}) ");
                }
                sql.Condition.Add("{:slpCode}", StringUtils.setInParameter(condition.SlpCode));
            }
            else
            {
                // CR Authorize
                if (CollectionUtils.IsAny(condition.UserSalesEmployee))
                {
                    if (B1DataAccess.Instance.IsHana)
                    {
                    }
                    else
                    {
                        sql.where(" and t0.SlpCode in ({:slpCode}) ");
                    }
                    sql.Condition.Add("{:slpCode}", StringUtils.setInParameter(condition.UserSalesEmployee));
                }
            }

            if (!StringUtils.isBlankOrNull(condition.CardCode))
            {
                if (B1DataAccess.Instance.IsHana)
                {
                }
                else
                {
                    sql.where(" and t0.CardCode like N'{:cardCode}' ");
                }
                sql.ConditionLike.Add("{:cardCode}", condition.CardCode);
            }

            if (!StringUtils.isBlankOrNull(condition.CardName))
            {
                if (B1DataAccess.Instance.IsHana)
                {
                }
                else
                {
                    sql.where(" and t0.CardName like N'{:cardName}' ");
                }
                sql.ConditionLike.Add("{:cardName}", condition.CardName);
            }

            // Posting Date
            if (condition.DocDateFrom.Year > 1 && condition.DocDateTo.Year > 1)
            {
                if (B1DataAccess.Instance.IsHana)
                {
                }
                else
                {
                    sql.where(" and t0.DocDate between N'{:docDateFrom}' and N'{:docDateTo}' ");
                }
                sql.Condition.Add("{:docDateFrom}", condition.DocDateFrom);
                sql.Condition.Add("{:docDateTo}", condition.DocDateTo);
            }
            else if (condition.DocDateFrom.Year > 1 && condition.DocDateTo.Year == 1)
            {
                if (B1DataAccess.Instance.IsHana)
                {
                }
                else
                {
                    sql.where(" and t0.DocDate >= N'{:docDateFrom}' ");
                }
                sql.Condition.Add("{:docDateFrom}", condition.DocDateFrom);
            }
            else if (condition.DocDateFrom.Year == 1 && condition.DocDateTo.Year > 1)
            {
                if (B1DataAccess.Instance.IsHana)
                {
                }
                else
                {
                    sql.where(" and t0.DocDate <= N'{:docDateTo}' ");
                }
                sql.Condition.Add("{:docDateTo}", condition.DocDateTo);
            }

            // Delivery Date
            if (condition.DocDueDateFrom.Year > 1 && condition.DocDueDateTo.Year > 1)
            {
                if (B1DataAccess.Instance.IsHana)
                {
                }
                else
                {
                    sql.where(" and t0.DocDueDate between N'{:docDueDateFrom}' and N'{:docDueDateTo}' ");
                }
                sql.Condition.Add("{:docDueDateFrom}", condition.DocDueDateFrom);
                sql.Condition.Add("{:docDueDateTo}", condition.DocDueDateTo);
            }
            else if (condition.DocDueDateFrom.Year > 1 && condition.DocDueDateTo.Year == 1)
            {
                if (B1DataAccess.Instance.IsHana)
                {
                }
                else
                {
                    sql.where(" and t0.DocDueDate >= N'{:docDueDateFrom}' ");
                }
                sql.Condition.Add("{:docDueDateFrom}", condition.DocDueDateFrom);
            }
            else if (condition.DocDueDateFrom.Year == 1 && condition.DocDueDateTo.Year > 1)
            {
                if (B1DataAccess.Instance.IsHana)
                {
                }
                else
                {
                    sql.where(" and t0.DocDueDate <= N'{:docDueDateTo}' ");
                }
                sql.Condition.Add("{:docDueDateTo}", condition.DocDueDateTo);
            }

            if (condition.Paginable != null)
            {
                if (B1DataAccess.Instance.IsHana)
                {
                }
                else
                {
                    sql.orderBy(" t0.DocEntry desc ");
                }
            }

            sql.Condition.Add("{:isDraft}", condition.IsDraft ? "Y" : "N");
            sql.Condition.Add("{:tableName}", condition.IsDraft ? "ODRF" : "ORDR");
            sql.Condition.Add("{:company}", ServiceDataAccess.Instance.getCompanyName(condition.Company));

            return sql.generateSqlWithLog();
        }

        public string getQuerySearchSalesOrderByDocEntry(SalesOrderCondition condition)
        {
            TemplateSQL sql = new TemplateSQL();

            if (B1DataAccess.Instance.IsHana)
            {
            }
            else
            {
                sql.select(" select top 1 t0.Series, t1.SeriesName, t0.DocEntry, t0.DocNum, t0.CardCode, t0.CardName, ");
                sql.select(" t0.DocDate, t0.DocDueDate, t0.TaxDate, t0.DocStatus, t0.CANCELED, t0.CntctCode as ContactPersonCode, ");
                sql.select(" t0.NumAtCard, t0.SlpCode as SalesPersonCode, t3.SlpName as SalesPersonName, t0.OwnerCode as DocumentsOwner, ");
                sql.select(" t0.DiscPrcnt as DiscountPercent, t0.Comments, ");
                sql.select(" (t0.DocTotal - t0.VatSum - t0.DiscSum) as TotalBeforeDiscount, t0.DiscSum as DiscountAmount, t0.VatSum as VatAmount, t0.DocTotal, ");
                sql.select(" t0.GroupNum as PaymentGroupCode, t0.PayToCode, t0.Address, t0.ShipToCode, t0.Address2, ");
                sql.select(" '{:isDraft}' as IsDraft, case when 'Y' = '{:isDraft}' then t0.DocEntry else t0.DocNum end as DocKey, ");
                //sql.select(" case when 'Y' = '{:isDraft}' and t0.WddStatus = 'Y' then 'Approved' ");
                //sql.select(" when 'Y' = '{:isDraft}' and t0.WddStatus = 'N' then 'Rejected' ");
                //sql.select(" when 'Y' = '{:isDraft}' and t0.WddStatus = 'W' then 'Pending' ");
                //sql.select(" when 'Y' = '{:isDraft}' and t0.WddStatus = '-' then 'Draft' end as WddStatus, ");
                sql.select(" case when t2.U_JWS_Status is null then '-' else t2.U_JWS_Status end as WddStatus, ");
                // UDF
                sql.select(" t0.U_ISS_VatBranch as VatBranch, t0.U_ISS_BpTaxId as BpTaxId, t0.U_ISS_BpBrnCode as BpBrnCode, t0.U_WGE_InvRemark as InvRemark ");
                // UDF
                sql.from(" from {:company}..{:tableName} t0 (nolock) ");
                sql.innerjoin(" {:company}..nnm1 t1 (nolock) on t0.Series = t1.Series ");
                // UDF
                sql.leftjoin(" {:company}..[@JWS_OAWS] t2 (nolock) on t0.DocEntry = t2.U_JWS_DocEntry and 'Y' = '{:isDraft}' ");
                // UDF
                sql.leftjoin(" {:company}..oslp t3 (nolock) on t0.SlpCode = t3.SlpCode ");
                sql.where(" where 1 = 1 ");
                sql.where(" and t0.DocEntry = {:docEntry} ");
            }

            if (!StringUtils.isBlankOrNull(condition.Code))
            {
                if (B1DataAccess.Instance.IsHana)
                {
                }
                else
                {
                    sql.where(" and t2.Code = '{:code}' ");
                }
                sql.Condition.Add("{:code}", condition.Code);
            }

            sql.Condition.Add("{:isDraft}", condition.IsDraft ? "Y" : "N");
            sql.Condition.Add("{:docEntry}", condition.DocEntry);
            sql.Condition.Add("{:tableName}", condition.IsDraft ? "odrf" : "ordr");
            sql.Condition.Add("{:company}", ServiceDataAccess.Instance.getCompanyName(condition.Company));

            return sql.generateSqlWithLog();
        }

        public string getQuerySalesOrderLineByDocEntry(string company, bool isDraft, int docEntry)
        {
            TemplateSQL sql = new TemplateSQL();

            if (B1DataAccess.Instance.IsHana)
            {
            }
            else
            {
                sql.select(" select t0.LineNum, t0.ItemCode, t0.Dscription as ItemName, t0.Quantity, t0.InvQty, ");
                sql.select(" t0.PriceBefDi as UnitPrice, t0.PriceBefDi as PriceBeforeDiscount, t0.VatSum as VatAmount, ");
                sql.select(" (t0.PriceBefDi - (t0.PriceBefDi - t0.Price)) as PriceAfterDiscount, t0.PriceAfVAT as PriceAfterVat, ");
                sql.select(" t0.DiscPrcnt as DiscountPercent, (t0.PriceBefDi - t0.Price) as DiscountAmount, t0.VatGroup, t0.UomEntry, t0.UomCode, ");
                sql.select(" t0.LineTotal as LineTotalAmount, t0.WhsCode as WarehouseCode, t0.OcrCode as CostingCode, t0.OcrCode2 as CostingCode2, ");
                sql.select(" t0.OcrCode3 as CostingCode3, t0.OcrCode4 as CostingCode4, t0.OcrCode5 as CostingCode5, t1.ManBtchNum as IsBatch, t1.UgpEntry, ");
                sql.select(@" t0.FreeTxt as ""FreeText"", ");
                // WGE UDF
                sql.select(" t0.U_WGE_FS as FreeSampling ");
                // WGE UDF
                sql.from(" from {:company}..{:tableName} t0 (nolock) ");
                sql.innerjoin(" {:company}..oitm t1 (nolock) on t0.ItemCode = t1.ItemCode ");
                sql.where(" where 1 = 1 ");
                sql.where(" and t0.DocEntry = {:docEntry} ");
            }

            sql.Condition.Add("{:docEntry}", docEntry);
            sql.Condition.Add("{:tableName}", isDraft ? "DRF1" : "RDR1");
            sql.Condition.Add("{:company}", ServiceDataAccess.Instance.getCompanyName(company));

            return sql.generateSqlWithLog();
        }

        public string getQuerySalesOrderBatchLineByDocEntry(string company, int docEntry)
        {
            TemplateSQL sql = new TemplateSQL();

            if (B1DataAccess.Instance.IsHana)
            {
            }
            else
            {
                sql.select(" select t0.BaseLinNum as LineNum, t0.ItemCode, t1.AbsEntry, t0.BatchNum as BatchNumber, t0.Quantity, t1.ExpDate, ");
                sql.select(" DATEDIFF(DD, t0.DocDate, t1.ExpDate) as Days, ");
                sql.select(" case when DATEDIFF(DD, t0.DocDate, t1.ExpDate) < 90 then 'R' ");
                sql.select(" when DATEDIFF(DD, t0.DocDate, t1.ExpDate) < 180 then 'Y' ");
                sql.select(" when DATEDIFF(DD, t0.DocDate, t1.ExpDate) > 180 then 'G' end as Color ");
                sql.from(" from {:company}..ibt1 t0 (nolock) ");
                sql.innerjoin(" {:company}..obtn t1 (nolock) on t0.ItemCode = t1.ItemCode and t0.BatchNum = t1.DistNumber ");
                sql.where(" where 1 = 1 ");
                sql.where(" and t0.BaseEntry = {:docEntry} ");
                sql.where(" and t0.BaseType = '17' ");
                sql.where(" and t0.Quantity > 0 ");
            }

            sql.Condition.Add("{:docEntry}", docEntry);
            sql.Condition.Add("{:company}", ServiceDataAccess.Instance.getCompanyName(company));

            return sql.generateSqlWithLog();
        }

        public string getQuerySalesOrderDraftBatchLineByDocEntry(string company, int docEntry)
        {
            TemplateSQL sql = new TemplateSQL();

            if (B1DataAccess.Instance.IsHana)
            {
            }
            else
            {
                sql.select(" select t0.LineNum, t0.ItemCode, t1.AbsEntry, t1.DistNumber as BatchNumber, t0.Quantity, t1.ExpDate, ");
                sql.select(" DATEDIFF(DD, t2.DocDate, t1.ExpDate) as Days, ");
                sql.select(" case when DATEDIFF(DD, t2.DocDate, t1.ExpDate) < 90 then 'R' ");
                sql.select(" when DATEDIFF(DD, t2.DocDate, t1.ExpDate) < 180 then 'Y' ");
                sql.select(" when DATEDIFF(DD, t2.DocDate, t1.ExpDate) > 180 then 'G' end as Color ");
                sql.from(" from {:company}..drf16 t0 (nolock) ");
                sql.innerjoin(" {:company}..obtn t1 (nolock) on t0.ItemCode = t1.ItemCode and t0.ObjAbs = t1.AbsEntry ");
                sql.innerjoin(" {:company}..odrf t2 (nolock) on t0.AbsEntry = t2.DocEntry ");
                sql.where(" where 1 = 1 ");
                sql.where(" and t0.Quantity > 0 ");
                sql.where(" and t0.AbsEntry = {:docEntry} ");
            }

            sql.Condition.Add("{:docEntry}", docEntry);
            sql.Condition.Add("{:company}", ServiceDataAccess.Instance.getCompanyName(company));

            return sql.generateSqlWithLog();
        }

        public string getQueryBusinessPartnerCriteria(BusinessPartnerCondition condition)
        {
            TemplateSQL sql = new TemplateSQL();

            if (B1DataAccess.Instance.IsHana)
            {
            }
            else
            {
                sql.select(" select top 15 t0.CardCode, t0.CardName ");
                sql.from(" from {:company}..ocrd t0 (nolock) ");
                sql.where(" where 1 = 1 ");
                sql.where(" and t0.frozenFor = 'N' ");
            }

            if (!"S".Equals(condition.CardType))
            {
                if (CollectionUtils.IsAny(condition.UserSalesEmployee))
                {
                    if (B1DataAccess.Instance.IsHana)
                    {
                    }
                    else
                    {
                        sql.where(" and t0.SlpCode in ({:slpCode}) ");
                    }
                    sql.Condition.Add("{:slpCode}", StringUtils.setInParameter(condition.UserSalesEmployee));
                }
            }

            if (!StringUtils.isBlankOrNull(condition.CardCode))
            {
                if (B1DataAccess.Instance.IsHana)
                {
                }
                else
                {
                    sql.where(" and (t0.CardCode like N'{:cardCode}' ");
                    sql.where(" or t0.CardName like N'{:cardCode}') ");
                }
                sql.ConditionLike.Add("{:cardCode}", condition.CardCode);
            }

            if (!StringUtils.isBlankOrNull(condition.CardName))
            {
                if (B1DataAccess.Instance.IsHana)
                {
                }
                else
                {
                    sql.where(" and t0.CardName like N'{:cardName}' ");
                }
                sql.ConditionLike.Add("{:cardName}", condition.CardName);
            }

            if (!StringUtils.isBlankOrNull(condition.CardType))
            {
                if (B1DataAccess.Instance.IsHana)
                {
                }
                else
                {
                    sql.where(" and t0.CardType = N'{:cardType}' ");
                }
                sql.ConditionLike.Add("{:cardType}", condition.CardType);
            }

            if (B1DataAccess.Instance.IsHana)
            {
            }
            else
            {
                sql.orderBy(" t0.CardCode ");
            }

            sql.Condition.Add("{:company}", ServiceDataAccess.Instance.getCompanyName(condition.Company));

            return sql.generateSqlWithLog();
        }

        public string getQueryItemMasterCriteria(ItemMasterCondition condition)
        {
            TemplateSQL sql = new TemplateSQL();

            if (B1DataAccess.Instance.IsHana)
            {
            }
            else
            {
                sql.select(" select top 15 t0.ItemCode, t0.ItemName ");
                sql.from(" from {:company}..oitm t0 (nolock) ");
                sql.where(" where 1 = 1 ");
                sql.where(" and t0.frozenFor = 'N' ");
                //sql.where(" and t0.InvntryItem = 'Y' ");
                sql.where(" and t0.SellItem = 'Y' ");
            }

            if (!StringUtils.isBlankOrNull(condition.ItemCode))
            {
                if(B1DataAccess.Instance.IsHana)
                {
                }
                else
                {
                    sql.where(" and (t0.ItemCode like N'{:itemCode}' ");
                    sql.where(" or t0.ItemName like N'{:itemCode}') ");
                }
                sql.ConditionLike.Add("{:itemCode}", condition.ItemCode);
            }

            if (!StringUtils.isBlankOrNull(condition.ItemName))
            {
                if (B1DataAccess.Instance.IsHana)
                {
                }
                else
                {
                    sql.where(" and t0.ItemName like N'{:itemName}' ");
                }
                sql.ConditionLike.Add("{:itemName}", condition.ItemName);
            }

            if (B1DataAccess.Instance.IsHana)
            {
            }
            else
            {
                sql.orderBy(" t0.ItemCode ");
            }

            sql.Condition.Add("{:company}", ServiceDataAccess.Instance.getCompanyName(condition.Company));

            return sql.generateSqlWithLog();
        }

        public string getQueryRelationDraftSalesOrder(SalesOrderCondition condition)
        {
            TemplateSQL sql = new TemplateSQL();

            if (B1DataAccess.Instance.IsHana)
            {
            }
            else
            {
                sql.select(" select t0.ObjType, 'Draft' as ObjTypeStr, t0.DocEntry, t0.DocEntry as DocNum, t0.DocDate, ");
                sql.select(" case when t0.CANCELED in ('C', 'Y') then 'Cancellation' ");
                sql.select(" when t0.WddStatus = 'Y' then 'Approved' ");
                sql.select(" when t0.WddStatus = 'N' then 'Rejected' ");
                sql.select(" when t1.draftKey is not null then 'Generated' ");
                sql.select(" when t0.WddStatus = 'W' then 'Pending' end as DocStatus, ");
                sql.select(" t3.USERID as UserId, t3.U_NAME as Username, concat(t2.firstName, ' ', t2.lastName) as Name, t0.CANCELED ");
                sql.from(" from {:company}..odrf t0 (nolock) ");
                sql.leftjoin(" {:company}..ordr t1 (nolock) on t0.DocEntry = t1.draftKey ");
                sql.leftjoin(" {:company}..ohem t2 (nolock) on t0.SlpCode = t2.salesPrson ");
                sql.leftjoin(" {:company}..ousr t3 (nolock) on t2.userId = t3.USERID ");
                sql.where(" where 1 = 1 ");
                sql.where(" and t0.ObjType = '17' ");
                sql.where(" and t1.DocEntry = {:docEntry} ");
            }

            sql.Condition.Add("{:docEntry}", condition.DocEntry);
            sql.Condition.Add("{:company}", ServiceDataAccess.Instance.getCompanyName(condition.Company));

            return sql.generateSqlWithLog();
        }

        public string getQueryRelationSalesOrder(SalesOrderCondition condition)
        {
            TemplateSQL sql = new TemplateSQL();

            if (B1DataAccess.Instance.IsHana)
            {
            }
            else
            {
                sql.select(" select t0.ObjType, 'Sales Order' as ObjTypeStr, t0.ObjType as BaseType, t0.draftKey as BaseEntry, t0.DocEntry, t0.DocNum, t0.DocDate, ");
                sql.select(" case when t0.CANCELED in ('C', 'Y') then 'Cancellation' ");
                sql.select(" when t0.DocStatus = 'C' then 'Closed' ");
                sql.select(" when t0.DocStatus = 'O' then 'Open' end as DocStatus, ");
                sql.select(" t2.USERID as UserId, t2.U_NAME as Username, concat(t1.firstName, ' ', t1.lastName) as Name, t0.CANCELED ");
                sql.from(" from {:company}..ordr t0 (nolock) ");
                sql.leftjoin(" {:company}..ohem t1 (nolock) on t0.OwnerCode = t1.empID ");
                sql.leftjoin(" {:company}..ousr t2 (nolock) on t1.userId = t2.USERID ");
                sql.where(" where 1 = 1 ");
                sql.where(" and t0.DocEntry = {:docEntry} ");
            }

            sql.Condition.Add("{:docEntry}", condition.DocEntry);
            sql.Condition.Add("{:company}", ServiceDataAccess.Instance.getCompanyName(condition.Company));

            return sql.generateSqlWithLog();
        }

        public string getQueryRelationOrderPickList(SalesOrderCondition condition)
        {
            TemplateSQL sql = new TemplateSQL();

            if (B1DataAccess.Instance.IsHana)
            {
            }
            else
            {
                sql.select(" Select t0.ObjType, 'Pick List' as ObjTypeStr, '17' as BaseType, t1.OrderEntry as BaseEntry, ");
                sql.select(" t0.AbsEntry as DocEntry, t0.AbsEntry as DocNum, t0.PickDate as DocDate, ");
                sql.select(" case when t0.Canceled = 'Y' then 'Cancellation' ");
                sql.select(" when t0.Status = 'R' then 'Released' ");
                sql.select(" when t0.Status = 'P' then 'Partially Picked' ");
                sql.select(" when t0.Status = 'D' then 'Partially Delivered' ");
                sql.select(" when t0.Status = 'C' then 'Closed' ");
                sql.select(" when t0.Status = 'Y' then 'Picked' end as DocStatus, ");
                sql.select(" t3.USERID as UserId, t3.U_NAME as Username, concat(t4.firstName, ' ', t4.lastName) as Name, t0.Canceled ");
                sql.from(" from {:company}..opkl t0 (nolock) ");
                sql.innerjoin(" {:company}..pkl1 t1 (nolock) on t0.AbsEntry = t1.AbsEntry ");
                sql.innerjoin(" {:company}..ordr t2 (nolock) on t1.OrderEntry = t2.DocEntry ");
                sql.leftjoin(" {:company}..ousr t3 (nolock) on t0.OwnerCode = t3.USERID ");
                sql.leftjoin(" {:company}..ohem t4 (nolock) on t3.USERID = t4.userId ");
                sql.where(" where 1 = 1 ");
                sql.where(" and t1.OrderEntry = {:soDocEntry} ");
            }

            sql.Condition.Add("{:soDocEntry}", condition.DocEntry);
            sql.Condition.Add("{:company}", ServiceDataAccess.Instance.getCompanyName(condition.Company));

            return sql.generateSqlWithLog();
        }

        public string getQueryRelationDelivery(SalesOrderCondition condition)
        {
            TemplateSQL sql = new TemplateSQL();

            if (B1DataAccess.Instance.IsHana)
            {
            }
            else
            {
                sql.select(" select distinct t0.ObjType, 'Delivery' as ObjTypeStr, t0.BaseType, t0.BaseEntry, t1.DocEntry, t1.DocNum, t1.DocDate, ");
                sql.select(" case when t1.CANCELED in ('C','Y') then 'Cancellation' ");
                sql.select(" when t1.DocStatus = 'C' then 'Closed' ");
                sql.select(" when t1.DocStatus = 'O' then 'Open' end as DocStatus, ");
                sql.select(" t3.USERID as UserId, t3.U_NAME as Username, concat(t4.firstName, ' ', t4.lastName) as Name, t1.CANCELED ");
                sql.from(" from {:company}..dln1 t0 (nolock) ");
                sql.innerjoin(" {:company}..odln t1 (nolock) on t0.DocEntry = t1.DocEntry ");
                sql.leftjoin(" {:company}..ordr t2 (nolock) on t0.BaseEntry = t2.DocEntry ");
                sql.leftjoin(" {:company}..ousr t3 (nolock) on t1.UserSign = t3.USERID ");
                sql.leftjoin(" {:company}..ohem t4 (nolock) on t3.USERID = t4.userId ");
                sql.where(" where 1 = 1 ");
                sql.where(" and t0.BaseType = 17 ");
                sql.where(" and t0.BaseEntry is not null ");
                sql.where(" and t0.BaseEntry > 0 ");
                sql.where(" and t2.DocEntry = {:soDocEntry} ");
            }

            sql.Condition.Add("{:soDocEntry}", condition.DocEntry);
            sql.Condition.Add("{:company}", ServiceDataAccess.Instance.getCompanyName(condition.Company));

            return sql.generateSqlWithLog();
        }

        public string getQueryRelationOrderInvoice(SalesOrderCondition condition)
        {
            TemplateSQL sql = new TemplateSQL();

            if (B1DataAccess.Instance.IsHana)
            {
            }
            else
            {
                sql.select(" select distinct t1.ObjType, 'Invoice' as ObjTypeStr, t0.BaseType, t0.BaseEntry, t1.DocEntry, t1.DocNum, t1.DocDate, ");
                sql.select(" case when t1.CANCELED in ('C','Y') then 'Cancellation' ");
                sql.select(" when t1.DocStatus = 'C' then 'Closed' ");
                sql.select(" when t1.DocStatus = 'O' then 'Open' end as DocStatus, ");
                sql.select(" t3.USERID as UserId, t3.U_NAME as Username, concat(t4.firstName, ' ', t4.lastName) as Name, t1.CANCELED ");
                sql.from(" from {:company}..inv1 t0 (nolock) ");
                sql.innerjoin(" {:company}..oinv t1 (nolock) on t0.DocEntry = t1.DocEntry ");
                sql.leftjoin(" {:company}..ordr t2 (nolock) on t0.BaseEntry = t2.DocEntry ");
                sql.leftjoin(" {:company}..ousr t3 (nolock) on t1.UserSign = t3.USERID ");
                sql.leftjoin(" {:company}..ohem t4 (nolock) on t3.USERID = t4.userId ");
                sql.where(" where 1 = 1 ");
                sql.where(" and t0.BaseType = 17 ");
                sql.where(" and t0.BaseEntry is not null ");
                sql.where(" and t0.BaseEntry > 0 ");
                sql.where(" and t2.DocEntry = {:soDocEntry} ");
            }

            sql.Condition.Add("{:soDocEntry}", condition.DocEntry);
            sql.Condition.Add("{:company}", ServiceDataAccess.Instance.getCompanyName(condition.Company));

            return sql.generateSqlWithLog();
        }

        public string getQueryRelationDeliveryInvoice(string company, int doDocEntry)
        {
            TemplateSQL sql = new TemplateSQL();

            if (B1DataAccess.Instance.IsHana)
            {
            }
            else
            {
                sql.select(" select distinct t1.ObjType, 'Invoice' as ObjTypeStr, t0.BaseType, t0.BaseEntry, t1.DocEntry, t1.DocNum, t1.DocDate, ");
                sql.select(" case when t1.CANCELED in ('C','Y') then 'Cancellation' ");
                sql.select(" when t1.DocStatus = 'C' then 'Closed' ");
                sql.select(" when t1.DocStatus = 'O' then 'Open' end as DocStatus, ");
                sql.select(" t3.USERID as UserId, t3.U_NAME as Username, concat(t4.firstName, ' ', t4.lastName) as Name, t1.CANCELED ");
                sql.from(" from {:company}..inv1 t0 (nolock) ");
                sql.innerjoin(" {:company}..oinv t1 (nolock) on t0.DocEntry = t1.DocEntry ");
                sql.leftjoin(" {:company}..odln t2 (nolock) on t0.BaseEntry = t2.DocEntry ");
                sql.leftjoin(" {:company}..ousr t3 (nolock) on t1.UserSign = t3.USERID ");
                sql.leftjoin(" {:company}..ohem t4 (nolock) on t3.USERID = t4.userId ");
                sql.where(" where 1 = 1 ");
                sql.where(" and t0.BaseType = 15 ");
                sql.where(" and t0.BaseEntry is not null ");
                sql.where(" and t0.BaseEntry > 0 ");
                sql.where(" and t2.DocEntry = {:doDocEntry} ");
            }

            sql.Condition.Add("{:doDocEntry}", doDocEntry);
            sql.Condition.Add("{:company}", ServiceDataAccess.Instance.getCompanyName(company));

            return sql.generateSqlWithLog();
        }

        public string getQueryRelationIncoming(string company, int invDocEntry)
        {
            TemplateSQL sql = new TemplateSQL();

            if (B1DataAccess.Instance.IsHana)
            {
            }
            else
            {
                sql.select(" select distinct t1.ObjType, 'Incomimg' as ObjTypeStr, t0.InvType as BaseType, t0.baseAbs as BaseEntry, t1.DocEntry, t1.DocNum, t1.DocDate, ");
                sql.select(" case when t1.Canceled = 'N' then 'Paid' ");
                sql.select(" when t1.Canceled = 'Y' then 'Cancellation' end as DocStatus, ");
                sql.select(" t3.USERID as UserId, t3.U_NAME as Username, concat(t4.firstName, ' ', t4.lastName) as Name, t1.Canceled ");
                sql.from(" from {:company}..rct2 t0 (nolock) ");
                sql.innerjoin(" {:company}..orct t1 (nolock) on t0.DocNum = t1.DocEntry ");
                sql.leftjoin(" {:company}..oinv t2 (nolock) on t0.baseAbs = t2.DocEntry ");
                sql.leftjoin(" {:company}..ousr t3 (nolock) on t1.UserSign = t3.USERID ");
                sql.leftjoin(" {:company}..ohem t4 (nolock) on t3.USERID = t4.userId ");
                sql.where(" where 1 = 1 ");
                sql.where(" and t0.InvType = 13 ");
                sql.where(" and t0.baseAbs is not null ");
                sql.where(" and t0.baseAbs > 0 ");
                sql.where(" and t2.DocEntry = {:invDocEntry} ");
            }

            sql.Condition.Add("{:invDocEntry}", invDocEntry);
            sql.Condition.Add("{:company}", ServiceDataAccess.Instance.getCompanyName(company));

            return sql.generateSqlWithLog();
        }

        public string searchOrderStatusByCondition(ReportOrderStatusCondition condition)
        {
            TemplateSQL sql = new TemplateSQL(condition.Paginable);

            if (B1DataAccess.Instance.IsHana)
            {
            }
            else
            {
                sql.select(" select t0.DocEntry as SODocEntry, t0.DocNum as SODocNum, t0.CardCode, t0.CardName, t0.DocDueDate as SODocDueDate, t0.NumAtCard, ");
                sql.select(" t6.USERID as SOUserId, t6.U_NAME as SOUsername, concat(t5.firstName, ' ', t5.lastName) as SOName, ");
                sql.select(" t0.CreateDate as SOCreateDate, right(concat('000000', t0.CreateTS), 6) as SOCreateTime, ");
                sql.select(" t1.ItemCode, t1.Dscription as ItemName, isnull(t2.Quantity, t1.Quantity) as Quantity, ");
                sql.select(" isnull(t2.UomEntry, t1.UomEntry) as UomEntry, isnull(t1.UomCode, t1.UomCode) as UomCode, ");
                sql.select(" isnull(t1.unitMsr, t1.unitMsr) as UomName, isnull(t1.WhsCode, t1.WhsCode) as WhsCode, ");
                sql.select(" t2.BaseEntry as DOBaseEntry, t2.DocEntry as DODocEntry, t2.DocNum as DODocNum, ");
                sql.select(" t2.DocDate as DODocDate, t2.UserId as DOUserId, t2.Username as DOUsername, ");
                sql.select(" t2.Name as DOEmpName, t2.CreateDate as DOCreateDate, t2.CreateTime as DOCreateTime, ");
                sql.select(" t3.BaseEntry as INVBaseEntry, t3.DocEntry as INVDocEntry, t3.DocNum as INVDocNum, ");
                sql.select(" t3.DocDate as INVDocDate, t3.UserId as INVUserId, t3.Username as INVUsername, ");
                sql.select(" t3.Name as INVEmpName, t3.CreateDate as INVCreateDate, t3.CreateTime as INVCreateTime, ");
                sql.select(" t4.baseAbs as RCTBaseEntry, t4.DocEntry as RCTDocEntry, t4.DocNum as RCTDocNum, ");
                sql.select(" t4.DocDate as RCTDocDate, t4.UserId as RCTUserId, t4.Username as RCTUsername, ");
                sql.select(" t4.Name as RCTEmpName, t4.CreateDate as RCTCreateDate, t4.CreateTime as RCTCreateTime ");
                sql.from(" from {:company}..ordr t0 (nolock) ");
                sql.innerjoin(" {:company}..rdr1 t1 (nolock) on t0.DocEntry = t1.DocEntry ");
                sql.leftjoin(" ( ");
                    sql.select(" select t0.ObjType, t1.DocEntry, t1.DocNum, t1.DocDate, ");
                    sql.select(" t3.USERID as UserId, t3.U_NAME as Username, concat(t4.firstName, ' ', t4.lastName) as Name, ");
                    sql.select(" t0.BaseEntry, t0.BaseType, t0.Quantity, t0.LineNum, t0.BaseLine, t0.UomEntry, t0.UomCode, t0.unitMsr, ");
                    sql.select(" t0.WhsCode, t1.CreateDate, right(concat('000000', t1.CreateTS), 6) as CreateTime ");
                    sql.from(" from {:company}..dln1 t0 (nolock) ");
                    sql.innerjoin(" {:company}..odln t1 (nolock) on t0.DocEntry = t1.DocEntry ");
                    sql.leftjoin(" {:company}..ousr t3 (nolock) on t1.UserSign = t3.USERID ");
                    sql.leftjoin(" {:company}..ohem t4 (nolock) on t3.USERID = t4.userId ");
                    sql.where(" where 1 = 1 ");
                    sql.where(" and t0.BaseType = 17 ");
                    sql.where(" and t0.BaseEntry is not null ");
                    sql.where(" and t0.BaseEntry > 0 ");
                    sql.where(" and t1.CANCELED = 'N' ");
                sql.append(" ) t2 on t1.LineNum = t2.BaseLine and t1.DocEntry = t2.BaseEntry ");
                sql.leftjoin(" ( ");
                    sql.select(" select t0.ObjType, t1.DocEntry, t1.DocNum, t1.DocDate, ");
                    sql.select(" t3.USERID as UserId, t3.U_NAME as Username, concat(t4.firstName, ' ', t4.lastName) as Name, ");
                    sql.select(" t0.BaseEntry, t0.BaseType, t0.Quantity, t0.LineNum, t0.BaseLine, t1.CreateDate, right(concat('000000', t1.CreateTS), 6) as CreateTime ");
                    sql.from(" from {:company}..inv1 t0 (nolock) ");
                    sql.innerjoin(" {:company}..oinv t1 (nolock) on t0.DocEntry = t1.DocEntry ");
                    sql.leftjoin(" {:company}..ousr t3 (nolock) on t1.UserSign = t3.USERID ");
                    sql.leftjoin(" {:company}..ohem t4 (nolock) on t3.USERID = t4.userId ");
                    sql.where(" where 1 = 1 ");
                    sql.where(" and t0.BaseType = 15 ");
                    sql.where(" and t0.BaseEntry is not null ");
                    sql.where(" and t0.BaseEntry > 0 ");
                    sql.where(" and t1.CANCELED = 'N' ");
                sql.append(" ) t3 on t2.LineNum = t3.BaseLine and t2.DocEntry = t3.BaseEntry ");
                sql.leftjoin(" ( ");
                    sql.select(" select t1.ObjType, t1.DocEntry, t1.DocNum, t1.DocDate, ");
                    sql.select(" t3.USERID as UserId, t3.U_NAME as Username, concat(t4.firstName, ' ', t4.lastName) as Name, ");
                    sql.select(" t0.baseAbs, t0.InvType, t1.CreateDate, right(concat('0000', t1.DocTime, '00'), 6) as CreateTime ");
                    sql.from(" from {:company}..rct2 t0 (nolock) ");
                    sql.innerjoin(" {:company}..orct t1 (nolock) on t0.DocNum = t1.DocEntry ");
                    sql.leftjoin(" {:company}..ousr t3 (nolock) on t1.UserSign = t3.USERID ");
                    sql.leftjoin(" {:company}..ohem t4 (nolock) on t3.USERID = t4.userId ");
                    sql.where(" where 1 = 1 ");
                    sql.where(" and t0.InvType = 13 ");
                    sql.where(" and t0.baseAbs is not null ");
                    sql.where(" and t0.baseAbs > 0 ");
                    sql.where(" and t1.Canceled = 'N' ");
                sql.append(" ) t4 on t3.DocEntry = t4.baseAbs ");
                sql.leftjoin(" {:company}..ohem t5 on t0.OwnerCode = t5.empID ");
                sql.leftjoin(" {:company}..ousr t6 on t5.userId = t6.USERID ");
                sql.leftjoin(" {:company}..ocrd t7 on t0.CardCode = t7.CardCode ");
                sql.where(" where 1 = 1 ");
            }

            // CR Authorize
            if (CollectionUtils.IsAny(condition.UserSalesEmployee))
            {
                if (B1DataAccess.Instance.IsHana)
                {
                }
                else
                {
                    sql.where(" and t7.SlpCode in ({:slpCode}) ");
                }
                sql.Condition.Add("{:slpCode}", StringUtils.setInParameter(condition.UserSalesEmployee));
            }

            if (!StringUtils.isBlankOrNull(condition.CardCodeFrom) && !StringUtils.isBlankOrNull(condition.CardCodeTo))
            {
                if(B1DataAccess.Instance.IsHana)
                {
                }
                else
                {
                    sql.where(" and t0.CardCode between N'{:cardCodeFrom}' and N'{:cardCodeTo}' ");
                }
                sql.Condition.Add("{:cardCodeFrom}", condition.CardCodeFrom);
                sql.Condition.Add("{:cardCodeTo}", condition.CardCodeTo);
            }
            else if (!StringUtils.isBlankOrNull(condition.CardCodeFrom) && StringUtils.isBlankOrNull(condition.CardCodeTo))
            {
                if (B1DataAccess.Instance.IsHana)
                {
                }
                else
                {
                    sql.where(" and t0.CardCode >= N'{:cardCodeFrom}' ");
                }
                sql.Condition.Add("{:cardCodeFrom}", condition.CardCodeFrom);
            }
            else if (StringUtils.isBlankOrNull(condition.CardCodeFrom) && !StringUtils.isBlankOrNull(condition.CardCodeTo))
            {
                if (B1DataAccess.Instance.IsHana)
                {
                }
                else
                {
                    sql.where(" and t0.CardCode <= N'{:cardCodeTo}' ");
                }
                sql.Condition.Add("{:cardCodeTo}", condition.CardCodeTo);
            }

            if (!StringUtils.isBlankOrNull(condition.ItemCodeFrom) && !StringUtils.isBlankOrNull(condition.ItemCodeTo))
            {
                if (B1DataAccess.Instance.IsHana)
                {
                }
                else
                {
                    sql.where(" and t1.ItemCode between N'{:itemCodeFrom}' and N'{:itemCodeTo}' ");
                }
                sql.Condition.Add("{:itemCodeFrom}", condition.ItemCodeFrom);
                sql.Condition.Add("{:itemCodeTo}", condition.ItemCodeTo);
            }
            else if (!StringUtils.isBlankOrNull(condition.ItemCodeFrom) && StringUtils.isBlankOrNull(condition.ItemCodeTo))
            {
                if (B1DataAccess.Instance.IsHana)
                {
                }
                else
                {
                    sql.where(" and t1.ItemCode >= N'{:itemCodeFrom}' ");
                }
                sql.Condition.Add("{:itemCodeFrom}", condition.ItemCodeFrom);
            }
            else if (StringUtils.isBlankOrNull(condition.ItemCodeFrom) && !StringUtils.isBlankOrNull(condition.ItemCodeTo))
            {
                if (B1DataAccess.Instance.IsHana)
                {
                }
                else
                {
                    sql.where(" and t1.ItemCode <= N'{:itemCodeTo}' ");
                }
                sql.Condition.Add("{:itemCodeTo}", condition.ItemCodeTo);
            }

            if (condition.DocDueDateFrom.Year > 1 && condition.DocDueDateTo.Year > 1)
            {
                if (B1DataAccess.Instance.IsHana)
                {
                }
                else
                {
                    sql.where(" and t0.DocDueDate between N'{:docDueDateFrom}' and N'{:docDueDateTo}' ");
                }
                sql.Condition.Add("{:docDueDateFrom}", condition.DocDueDateFrom);
                sql.Condition.Add("{:docDueDateTo}", condition.DocDueDateTo);
            }
            else if (condition.DocDueDateFrom.Year > 1 && condition.DocDueDateTo.Year == 1)
            {
                if (B1DataAccess.Instance.IsHana)
                {
                }
                else
                {
                    sql.where(" and t0.DocDueDate >= N'{:docDueDateFrom}' ");
                }
                sql.Condition.Add("{:docDueDateFrom}", condition.DocDueDateFrom);
            }
            else if (condition.DocDueDateFrom.Year == 1 && condition.DocDueDateTo.Year > 1)
            {
                if (B1DataAccess.Instance.IsHana)
                {
                }
                else
                {
                    sql.where(" and t0.DocDueDate <= N'{:docDueDateTo}' ");
                }
                sql.Condition.Add("{:docDueDateTo}", condition.DocDueDateTo);
            }

            if (!StringUtils.isBlankOrNull(condition.NumAtCard))
            {
                if (B1DataAccess.Instance.IsHana)
                {
                }
                else
                {
                    sql.where(" and t0.NumAtCard = N'{:numAtCard}' ");
                }
                sql.Condition.Add("{:numAtCard}", condition.NumAtCard);
            }

            if (condition.Paginable != null)
            {
                if (B1DataAccess.Instance.IsHana)
                {
                }
                else
                {
                    sql.orderBy(" t0.DocEntry ");
                }
            }

            sql.Condition.Add("{:company}", ServiceDataAccess.Instance.getCompanyName(condition.Company));

            return sql.generateSqlWithLog();
        }

        public string getQuerySalesAnalysis(SalesAnalysisCondition condition)
        {
            TemplateSQL sql = new TemplateSQL();

            if (B1DataAccess.Instance.IsHana)
            {
            }
            else
            {
                sql.select(" select t0.SlpCode, t2.SlpName, t0.CardCode, t0.CardName, t1.ItemCode, t1.ItemName, t0.UomEntry, t0.UomCode, t0.UomName, ");

                int month = DateTimeUtils.getMonthDifference(condition.DocDateFrom, condition.DocDateTo);
                for (int i = 0; i <= month; i++)
                {
                    DateTime dateFrom = condition.DocDateFrom.AddMonths(i);
                    int index = i + 1;
                    sql.select(StringUtils.format(" '{0}' as Month{1}, '{2}' as Year{3}, concat(DATENAME(month, '{4}'), ' ({5})') as MonthYear{6}, ", dateFrom.Month, index, dateFrom.Year, index, dateFrom.ToString("yyyy-MM-dd"), dateFrom.Year, index));
                    //sql.select(StringUtils.format(" '{0}' as Month{1}, '{2}' as Year{3}, ", dateFrom.Month, i + 1, dateFrom.Year, i + 1));
                    sql.select(StringUtils.format(" sum(case when FORMAT(t0.DocDate, 'yyyyMM') = '{0}' then t0.Quantity else 0 end) as Qty{1}, ", dateFrom.ToString("yyyyMM"), index));
                    sql.select(StringUtils.format(" sum(case when FORMAT(t0.DocDate, 'yyyyMM') = '{0}' then t0.Price else 0 end) as Total{1}, ", dateFrom.ToString("yyyyMM"), index));

                    // support 24 months
                    if (i == 24)
                    {
                        break;
                    }
                }

                sql.select(" sum(t0.Quantity) as YtdQuantity, sum(t0.Price) as YtdTotal ");
                sql.from(" from ( ");
                    sql.select(" select t0.SlpCode, t0.CardCode, t0.CardName, t0.DocDate, t1.ItemCode, ");
                    sql.select(" t1.UomEntry, t1.UomCode, t1.unitMsr as UomName, t1.Quantity, (t1.Quantity * t1.Price) as Price ");
                    sql.from(" from {:company}..oinv t0 (nolock) ");
                    sql.innerjoin(" {:company}..inv1 t1 (nolock) on t0.DocEntry = t1.DocEntry ");
                    sql.where(" where 1 = 1 ");
                    sql.where(" and t0.CANCELED = 'N' ");
                    sql.append(" union all ");
                    sql.select(" select t0.SlpCode, t0.CardCode, t0.CardName, t0.DocDate, t1.ItemCode, ");
                    sql.select(" t1.UomEntry, t1.UomCode, t1.unitMsr as UomName, (t1.Quantity * -1), ((t1.Quantity * t1.Price) * -1) ");
                    sql.from(" from {:company}..orin t0 (nolock) ");
                    sql.innerjoin(" {:company}..rin1 t1 (nolock) on t0.DocEntry = t1.DocEntry ");
                    sql.where(" where 1 = 1 ");
                    sql.where(" and t0.CANCELED = 'N' ");
                sql.append(" ) t0 ");
                sql.innerjoin(" {:company}..oitm t1 on t0.ItemCode = t1.ItemCode ");
                sql.leftjoin(" {:company}..oslp t2 on t0.SlpCode = t2.SlpCode ");
                sql.where(" where 1 = 1 ");
            }

            // Sales Employee
            if (CollectionUtils.IsAny(condition.SlpCode))
            {
                if (B1DataAccess.Instance.IsHana)
                {
                }
                else
                {
                    sql.where(" and t0.SlpCode in ({:slpCode}) ");
                }
                sql.Condition.Add("{:slpCode}", StringUtils.setInParameter(condition.SlpCode));
            }
            else
            {
                // CR Authorize
                if (CollectionUtils.IsAny(condition.UserSalesEmployee))
                {
                    // condition not selected
                    if (B1DataAccess.Instance.IsHana)
                    {
                    }
                    else
                    {
                        sql.where(" and t0.SlpCode in ({:slpCode}) ");
                    }
                    sql.Condition.Add("{:slpCode}", StringUtils.setInParameter(condition.UserSalesEmployee));
                }
            }
            //if (condition.SlpCodeFrom > 0 && condition.SlpCodeTo > 0)
            //{
            //    if (B1DataAccess.Instance.IsHana)
            //    {
            //    }
            //    else
            //    {
            //        sql.where(" and t0.SlpCode between {:slpCodeFrom} and {:slpCodeTo} ");
            //    }
            //    sql.Condition.Add("{:slpCodeFrom}", condition.SlpCodeFrom);
            //    sql.Condition.Add("{:slpCodeTo}", condition.SlpCodeTo);
            //}
            //else if (condition.SlpCodeFrom > 0 && condition.SlpCodeTo == 0)
            //{
            //    if (B1DataAccess.Instance.IsHana)
            //    {
            //    }
            //    else
            //    {
            //        sql.where(" and t0.SlpCode >= {:slpCodeFrom} ");
            //    }
            //    sql.Condition.Add("{:slpCodeFrom}", condition.SlpCodeFrom);
            //}
            //else if (condition.SlpCodeFrom == 0 && condition.SlpCodeTo > 0)
            //{
            //    if (B1DataAccess.Instance.IsHana)
            //    {
            //    }
            //    else
            //    {
            //        sql.where(" and t0.SlpCode <= {:slpCodeTo} ");
            //    }
            //    sql.Condition.Add("{:slpCodeTo}", condition.SlpCodeTo);
            //}

            // Customer
            if (!StringUtils.isBlankOrNull(condition.CardCodeFrom) && !StringUtils.isBlankOrNull(condition.CardCodeTo))
            {
                if (B1DataAccess.Instance.IsHana)
                {
                }
                else
                {
                    sql.where(" and t0.CardCode between N'{:cardCodeFrom}' and N'{:cardCodeTo}' ");
                }
                sql.Condition.Add("{:cardCodeFrom}", condition.CardCodeFrom);
                sql.Condition.Add("{:cardCodeTo}", condition.CardCodeTo);
            }
            else if (!StringUtils.isBlankOrNull(condition.CardCodeFrom) && StringUtils.isBlankOrNull(condition.CardCodeTo))
            {
                if (B1DataAccess.Instance.IsHana)
                {
                }
                else
                {
                    sql.where(" and t0.CardCode >= N'{:cardCodeFrom}' ");
                }
                sql.Condition.Add("{:cardCodeFrom}", condition.CardCodeFrom);
            }
            else if (StringUtils.isBlankOrNull(condition.CardCodeFrom) && !StringUtils.isBlankOrNull(condition.CardCodeTo))
            {
                if (B1DataAccess.Instance.IsHana)
                {
                }
                else
                {
                    sql.where(" and t0.CardCode <= N'{:cardCodeTo}' ");
                }
                sql.Condition.Add("{:cardCodeTo}", condition.CardCodeTo);
            }

            // Invoice Date (Posting Date)
            if (condition.DocDateFrom.Year > 1 && condition.DocDateTo.Year > 1)
            {
                if (B1DataAccess.Instance.IsHana)
                {
                }
                else
                {
                    sql.where(" and t0.DocDate between N'{:docDateFrom}' and N'{:docDateTo}' ");
                }
                sql.Condition.Add("{:docDateFrom}", condition.DocDateFrom);
                sql.Condition.Add("{:docDateTo}", condition.DocDateTo);
            }
            else if (condition.DocDateFrom.Year > 1 && condition.DocDateTo.Year == 1)
            {
                if (B1DataAccess.Instance.IsHana)
                {
                }
                else
                {
                    sql.where(" and t0.DocDate >= N'{:docDateFrom}' ");
                }
                sql.Condition.Add("{:docDateFrom}", condition.DocDateFrom);
            }
            else if (condition.DocDateFrom.Year == 1 && condition.DocDateTo.Year > 1)
            {
                if (B1DataAccess.Instance.IsHana)
                {
                }
                else
                {
                    sql.where(" and t0.DocDate <= N'{:docDateTo}' ");
                }
                sql.Condition.Add("{:docDateTo}", condition.DocDateTo);
            }

            sql.groupBy(" t0.SlpCode, t2.SlpName, t0.CardCode, t0.CardName, t1.ItemCode, t1.ItemName, t0.UomEntry, t0.UomCode, t0.UomName ");

            if(condition.Paginable != null)
            {
                if(B1DataAccess.Instance.IsHana)
                {
                }
                else
                {
                    sql.orderBy(" t0.SlpCode, t0.ItemCode ");
                }
            }

            sql.Condition.Add("{:company}", ServiceDataAccess.Instance.getCompanyName(condition.Company));

            return sql.generateSqlWithLog();
        }

        public string getQueryCheckDeleteBPAddress(string company, string cardCode, List<int> visOrder)
        {
            TemplateSQL sql = new TemplateSQL();

            if (B1DataAccess.Instance.IsHana)
            {
            }
            else
            {
                sql.select(" select t0.VisOrder ");
                sql.from(" from ( ");
                    sql.select(" select (ROW_NUMBER() OVER(ORDER BY t0.Address, t0.AdresType) - 1) as VisOrder ");
                    sql.from(" from {:company}..crd1 t0 (nolock) ");
                    sql.where(" where 1 = 1 ");
                    sql.where(" and t0.CardCode = N'{:cardCode}' ");
                sql.from(" ) t0 ");
                sql.where(" where 1 = 1 ");
                sql.where(" and t0.VisOrder not in ({:visOrder}) ");
            }

            sql.Condition.Add("{:cardCode}", cardCode);
            sql.Condition.Add("{:visOrder}", StringUtils.setInParameter(visOrder));
            sql.Condition.Add("{:company}", ServiceDataAccess.Instance.getCompanyName(company));

            return sql.generateSqlWithLog();
        }

        public string getQuerySalesOrderAttachment(string company, bool isDraft, int docEntry)
        {
            TemplateSQL sql = new TemplateSQL();

            if (B1DataAccess.Instance.IsHana)
            {
            }
            else
            {
                sql.select(" select concat(t1.FileName, '.', t1.FileExt) as FileName, t1.FileName as Name, t1.FileExt as Ext, t1.Date ");
                sql.from(" from {:company}..{:tableName} t0 (nolock) ");
                sql.innerjoin(" {:company}..ATC1 t1 (nolock) on t0.AtcEntry = t1.AbsEntry ");
                sql.where(" where 1 = 1 ");
                sql.where(" and t0.DocEntry = {:docEntry} ");
                sql.orderBy(" t1.Line ");
            }

            sql.Condition.Add("{:docEntry}", docEntry);
            sql.Condition.Add("{:tableName}", isDraft ? "ODRF" : "ORDR");
            sql.Condition.Add("{:company}", ServiceDataAccess.Instance.getCompanyName(company));

            return sql.generateSqlWithLog();
        }

        public string getQueryBusinessPartnerAttachment(string company, string cardCode)
        {
            TemplateSQL sql = new TemplateSQL();

            if (B1DataAccess.Instance.IsHana)
            {
            }
            else
            {
                sql.select(" select concat(t1.FileName, '.', t1.FileExt) as FileName, t1.FileName as Name, t1.FileExt as Ext, t1.Date ");
                sql.from(" from {:company}..OCRD t0 (nolock) ");
                sql.innerjoin(" {:company}..ATC1 t1 (nolock) on t0.AtcEntry = t1.AbsEntry ");
                sql.where(" where 1 = 1 ");
                sql.where(" and t0.CardCode = '{:cardCode}' ");
                sql.orderBy(" t1.Line ");
            }

            sql.Condition.Add("{:cardCode}", cardCode);
            sql.Condition.Add("{:company}", ServiceDataAccess.Instance.getCompanyName(company));

            return sql.generateSqlWithLog();
        }

        public string getQueryApproverEmail(string company, int docEntry)
        {
            TemplateSQL sql = new TemplateSQL();

            if (B1DataAccess.Instance.IsHana)
            {
            }
            else
            {
                sql.select(" select distinct t1.U_JWS_Email as Email, concat(t2.firstName, ' ', t2.lastName) as FullName ");
                sql.from(" from {:company}..[@JWS_OAWS] t0 (nolock) ");
                sql.innerjoin(" {:company}..[@JWS_AWS1] t1 (nolock) on t0.Code = t1.U_JWS_ParentKey and t0.U_JWS_CurrentStage = t1.U_JWS_Stage ");
                sql.innerjoin(" {:company}..OHEM t2 (nolock) on t1.U_JWS_EmpId = t2.empID and t1.U_JWS_ExtEmpId = t2.ExtEmpNo ");
                sql.where(" where 1 = 1 ");
                sql.where(" and t1.U_JWS_Status = 'W' ");
                sql.where(" and t1.U_JWS_Email is not null ");
                sql.where(" and t0.U_JWS_Status = 'W' ");
                sql.where(" and t0.U_JWS_DocEntry = {:docEntry} ");
            }

            sql.Condition.Add("{:docEntry}", docEntry);
            sql.Condition.Add("{:company}", ServiceDataAccess.Instance.getCompanyName(company));

            return sql.generateSqlWithLog();
        }

        public string getQueryOriginatorEmail(string company, int docEntry)
        {
            TemplateSQL sql = new TemplateSQL();

            if (B1DataAccess.Instance.IsHana)
            {
            }
            else
            {
                sql.select(" select distinct t0.U_JWS_Email as Email, concat(t1.firstName, ' ', t1.lastName) as FullName, ");
                sql.select(" t1.U_JWS_EmailCent as EmailCenter, t1.U_JWS_EmailSup as EmailSupport ");
                sql.from(" from {:company}..[@JWS_OAWS] t0 (nolock) ");
                sql.innerjoin(" {:company}..OHEM t1 (nolock) on t0.U_JWS_EmpId = t1.empID and t0.U_JWS_ExtEmpId = t1.ExtEmpNo ");
                sql.where(" where 1 = 1 ");
                //sql.where(" and t0.U_JWS_Status in ('Y', 'N') ");
                sql.where(" and t0.U_JWS_Email is not null ");
                sql.where(" and t0.U_JWS_DocEntry = {:docEntry} ");
            }

            sql.Condition.Add("{:docEntry}", docEntry);
            sql.Condition.Add("{:company}", ServiceDataAccess.Instance.getCompanyName(company));

            return sql.generateSqlWithLog();
        }

        //public string getQueryUpdateAWS1(ApproveSalesOrderResult obj)
        //{
        //    TemplateSQL sql = new TemplateSQL();

        //    if (B1DataAccess.Instance.IsHana)
        //    {
        //    }
        //    else
        //    {
        //        sql.append(" update {:company}..[@JWS_AWS1] set U_JWS_Status = '{:status}', U_JWS_Remark = N'{:remark}', U_JWS_UpdateDate = CAST(GETDATE() as date), U_JWS_UpdateTime = FORMAT(GETDATE(), 'HHmm') ");
        //        sql.append(" where 1 = 1 ");
        //        sql.append(" and U_JWS_DocEntry = {:docEntry} ");
        //        //sql.append(" and U_JWS_UserID = {:userId}; ");
        //        sql.append(" and U_JWS_EmpId = {:empId}; ");
        //        sql.append(" and U_JWS_ExtEmpId = {:extEmpId}; ");
        //        sql.append(" and Code = {:code}; ");
        //    }

        //    sql.Condition.Add("{:status}", obj.WddStatus);
        //    sql.Condition.Add("{:remark}", obj.Remark);
        //    sql.Condition.Add("{:docEntry}", obj.DocEntry);
        //    //sql.Condition.Add("{:userId}", obj.UserId);
        //    sql.Condition.Add("{:empId}", obj.EmpId);
        //    sql.Condition.Add("{:extEmpId}", obj.ExtEmpId);
        //    sql.Condition.Add("{:code}", obj.Code);
        //    sql.Condition.Add("{:company}", ServiceDataAccess.Instance.getCompanyName(obj.Company));

        //    return sql.generateSqlWithLog();
        //}

        public string getQueryUpdateAWS1(List<ApproveSalesOrderResult> obj)
        {
            TemplateSQL sql = new TemplateSQL();

            if (B1DataAccess.Instance.IsHana)
            {
            }
            else
            {
                foreach (ApproveSalesOrderResult so in obj)
                {
                    sql.append(StringUtils.format(" update {0}..[@JWS_AWS1] set U_JWS_Status = '{1}', U_JWS_Remark = N'{2}', U_JWS_UpdateDate = CAST(GETDATE() as date), U_JWS_UpdateTime = FORMAT(GETDATE(), 'HHmm'), U_JWS_UpdateBy = {3} where 1 = 1 and U_JWS_DocEntry = {4} and U_JWS_EmpId = {5} and U_JWS_ExtEmpId = '{6}' and Code = {7}; ", ServiceDataAccess.Instance.getCompanyName(so.Company), so.WddStatus, so.Remark, so.UserId, so.DocEntry, so.EmpId, so.ExtEmpId, so.Code));
                }
            }

            return sql.generateSqlWithLog();
        }

        public string getQueryUpdateAWS1(ApproveSalesOrderResult obj)
        {
            TemplateSQL sql = new TemplateSQL();

            if (B1DataAccess.Instance.IsHana)
            {
            }
            else
            {
                sql.append(StringUtils.format(" update {0}..[@JWS_AWS1] set U_JWS_Status = '{1}', U_JWS_Remark = N'{2}', U_JWS_UpdateDate = CAST(GETDATE() as date), U_JWS_UpdateTime = FORMAT(GETDATE(), 'HHmm'), U_JWS_UpdateBy = {3} where 1 = 1 and U_JWS_DocEntry = {4} and U_JWS_EmpId = {5} and U_JWS_ExtEmpId = '{6}' and Code = {7}; ", ServiceDataAccess.Instance.getCompanyName(obj.Company), obj.WddStatus, obj.Remark, obj.UserId, obj.DocEntry, obj.EmpId, obj.ExtEmpId, obj.Code));
            }

            return sql.generateSqlWithLog();
        }

        public string getQueryCheckUpdateAWS1(ApproveSalesOrderResult obj)
        {
            TemplateSQL sql = new TemplateSQL();

            if (B1DataAccess.Instance.IsHana)
            {
            }
            else
            {
                sql.select(" select count(t0.U_JWS_DocEntry) as cnt ");
                sql.from(" from [@JWS_AWS1] t0 (nolock) ");
                sql.where(" where 1 = 1 ");
                sql.where(" and t0.U_JWS_DocEntry = {:docEntry} ");
                sql.where(" and t0.U_JWS_EmpId = {:empId} ");
                sql.where(" and t0.U_JWS_ExtEmpId = '{:extEmpId}' ");
                sql.where(" and t0.Code = {:code} ");
            }

            sql.Condition.Add("{:docEntry}", obj.DocEntry);
            sql.Condition.Add("{:empId}", obj.EmpId);
            sql.Condition.Add("{:extEmpId}", obj.ExtEmpId);
            sql.Condition.Add("{:code}", obj.Code);

            return sql.generateSqlWithLog();
        }

        public string getQuerySAPProfile(string company, WebAdminProfile obj)
        {
            TemplateSQL sql = new TemplateSQL();

            if (B1DataAccess.Instance.IsHana)
            {
            }
            else
            {
                sql.select(" select t0.empID, t0.userId, t0.ExtEmpNo as ExtEmpId, t0.position, t1.name as PosName, t2.SlpCode, t2.SlpName ");
                sql.from(" from {:company}..ohem t0 (nolock) ");
                sql.leftjoin(" {:company}..ohps t1 (nolock) on t0.position = t1.posID ");
                sql.leftjoin(" {:company}..oslp t2 (nolock) on t0.salesPrson = t2.SlpCode ");
                sql.where(" where 1 = 1 ");
                sql.where(" and t0.Active = 'Y' ");
                sql.where(" and t0.userId > 0 ");
                sql.where(" and t0.empID = {:empId} ");
                sql.where(" and t0.ExtEmpNo = {:extEmpId} ");
            }

            sql.Condition.Add("{:empId}", obj.EmployeeId);
            sql.Condition.Add("{:extEmpId}", obj.ExtEmpId);
            sql.Condition.Add("{:company}", ServiceDataAccess.Instance.getCompanyName(company));

            return sql.generateSqlWithLog();
        }

        public string getQuerySAPProfileTeam(string company, int empId)
        {
            TemplateSQL sql = new TemplateSQL();

            if (B1DataAccess.Instance.IsHana)
            {
            }
            else
            {
                sql.select(" select teamID ");
                sql.from(" from {:company}..htm1 t0 (nolock) ");
                sql.where(" where 1 = 1 ");
                sql.where(" and t0.empID = {:empId} ");
            }

            sql.Condition.Add("{:empId}", empId);
            sql.Condition.Add("{:company}", ServiceDataAccess.Instance.getCompanyName(company));

            return sql.generateSqlWithLog();
        }

        public string getQuerySAPProfileSalesEmployee(string company, List<int> teamId)
        {
            TemplateSQL sql = new TemplateSQL();

            if (B1DataAccess.Instance.IsHana)
            {
            }
            else
            {
                sql.select(" select distinct t2.SlpCode, t2.SlpName ");
                sql.from(" from {:company}..ohem t0 (nolock) ");
                sql.innerjoin(" {:company}..htm1 t1 (nolock) on t0.empID = t1.empID ");
                sql.innerjoin(" {:company}..oslp t2 (nolock) on t0.salesPrson = t2.SlpCode ");
                sql.where(" where 1 = 1 ");
                sql.where(" and t0.Active = 'Y' ");
                sql.where(" and t1.teamID in ({:teamId}) ");
                sql.orderBy(" t2.SlpName ");
            }

            sql.Condition.Add("{:teamId}", StringUtils.setInParameter(teamId));
            sql.Condition.Add("{:company}", ServiceDataAccess.Instance.getCompanyName(company));

            return sql.generateSqlWithLog();
        }

        public string getQuerySearchApprovalSalesOrderByCondition(ApproveSalesOrderCondition condition)
        {
            TemplateSQL sql = new TemplateSQL(condition.Paginable);

            if (B1DataAccess.Instance.IsHana)
            {
            }
            else
            {
                sql.select(" select t0.U_JWS_CreateDate as CreateDate, t2.DocEntry, t2.DocDueDate as DeliveryDate, ");
                sql.select(" t2.CardCode, t2.CardName, t2.SlpCode, t2.DocTotal, t0.U_JWS_Status as WddStatus, ");
                sql.select(" t1.U_JWS_ParentKey as ParentKey, t1.Code, t3.SlpName, t0.U_JWS_WtmName as WtmName ");
                sql.from(" from {:company}..[@JWS_OAWS] t0 (nolock) ");
                sql.innerjoin(" {:company}..[@JWS_AWS1] t1 (nolock) on t0.Code = t1.U_JWS_ParentKey and t0.U_JWS_CurrentStage = t1.U_JWS_Stage ");
                sql.innerjoin(" {:company}..odrf t2 (nolock) on t0.U_JWS_DocEntry = t2.DocEntry and t0.U_JWS_ObjType = t2.ObjType ");
                sql.innerjoin(" {:company}..oslp t3 (nolock) on t2.SlpCode = t3.SlpCode ");
                sql.where(" where 1 = 1 ");
                //sql.where(" and t0.U_JWS_Status = 'W' ");
                //sql.where(" and t2.DocStatus = 'O' ");
                //sql.where(" and t1.U_JWS_Active = 'Y' ");
                sql.where(" and t0.U_JWS_Status = N'{:status}' ");
                //sql.where(" and t1.U_JWS_UserID = {:userId} ");
                sql.where(" and t1.U_JWS_EmpID = {:empId} ");
                sql.where(" and t1.U_JWS_ExtEmpID = N'{:extEmpId}' ");
            }

            // create date
            if (condition.CreateDateFrom.Year > 1 && condition.CreateDateTo.Year > 1)
            {
                if (B1DataAccess.Instance.IsHana)
                {
                }
                else
                {
                    sql.where(" and t0.U_JWS_CreateDate between N'{:createDateFrom}' and N'{:createDateTo}' ");
                }
                sql.Condition.Add("{:createDateFrom}", condition.CreateDateFrom);
                sql.Condition.Add("{:createDateTo}", condition.CreateDateTo);
            }
            else if (condition.CreateDateFrom.Year > 1 && condition.CreateDateTo.Year == 1)
            {
                if (B1DataAccess.Instance.IsHana)
                {
                }
                else
                {
                    sql.where(" and t0.U_JWS_CreateDate >= N'{:createDateFrom}' ");
                }
                sql.Condition.Add("{:createDateFrom}", condition.CreateDateFrom);
            }
            else if (condition.CreateDateFrom.Year == 1 && condition.CreateDateTo.Year > 1)
            {
                if (B1DataAccess.Instance.IsHana)
                {
                }
                else
                {
                    sql.where(" and t0.U_JWS_CreateDate <= N'{:createDateTo}' ");
                }
                sql.Condition.Add("{:createDateTo}", condition.CreateDateTo);
            }

            // sales employee
            if (CollectionUtils.IsAny(condition.SlpCode))
            {
                if (B1DataAccess.Instance.IsHana)
                {
                }
                else
                {
                    sql.where(" and t2.SlpCode in ({:slpCode}) ");
                }
                sql.Condition.Add("{:slpCode}", StringUtils.setInParameter(condition.SlpCode));
            }

            // customer code
            if (!StringUtils.isBlankOrNull(condition.CardCode))
            {
                if (B1DataAccess.Instance.IsHana)
                {
                }
                else
                {
                    sql.where(" and t2.CardCode = N'{:cardCode}' ");
                }
                sql.Condition.Add("{:cardCode}", condition.CardCode);
            }

            // customer name
            if (!StringUtils.isBlankOrNull(condition.CardName))
            {
                if (B1DataAccess.Instance.IsHana)
                {
                }
                else
                {
                    sql.where(" and t2.CardName = N'{:cardName}' ");
                }
                sql.Condition.Add("{:cardName}", condition.CardName);
            }

            // document entry
            if (condition.DocEntry > 0)
            {
                if (B1DataAccess.Instance.IsHana)
                {
                }
                else
                {
                    sql.where(" and t2.DocEntry = {:docEntry} ");
                }
                sql.Condition.Add("{:docEntry}", condition.DocEntry);
            }

            if (condition.Paginable != null)
            {
                if (B1DataAccess.Instance.IsHana)
                {
                }
                else
                {
                    sql.orderBy(" t0.U_JWS_CreateDate desc ");
                }
            }

            sql.Condition.Add("{:status}", condition.WddStatus);
            //sql.Condition.Add("{:userId}", condition.UserId);
            sql.Condition.Add("{:empId}", condition.EmpId);
            sql.Condition.Add("{:extEmpId}", condition.ExtEmpId);
            sql.Condition.Add("{:company}", ServiceDataAccess.Instance.getCompanyName(condition.Company));

            return sql.generateSqlWithLog();
        }

        public string getQueryUpdateOAWS(ApproveDocumentInfo obj)
        {
            TemplateSQL sql = new TemplateSQL();

            if (B1DataAccess.Instance.IsHana)
            {
            }
            else
            {
                sql.append(" update {:company}..[@JWS_OAWS] set ");

                // Approve
                //if ("Y".Equals(obj.WddStatus))
                //{
                //    sql.append(" U_JWS_TargetType = N'{:targetType}', ");
                //    sql.append(" U_JWS_TargetEntry = {:targetEntry}, ");
                //    sql.append(" U_JWS_TargetRef = {:targetRef}, ");
                //    sql.Condition.Add("{:targetType}", obj.ObjType);
                //    sql.Condition.Add("{:targetEntry}", obj.DocEntry);
                //    sql.Condition.Add("{:targetRef}", obj.DocNum);
                //}

                sql.append(" U_JWS_Status = N'{:status}', ");
                sql.append(" U_JWS_UpdateDate = CAST(GETDATE() as date), ");
                sql.append(" U_JWS_UpdateTime = FORMAT(GETDATE(), 'HHmm'), ");
                sql.append(" U_JWS_UpdateBy = {:userId} ");
                sql.append(" where 1 = 1 ");
                sql.append(" and U_JWS_Status = 'W' ");
                sql.append(" and U_JWS_DocEntry = {:docEntry} ");
                sql.append(" and Code = {:code} ");
            }

            sql.Condition.Add("{:code}", obj.ParentKey);
            sql.Condition.Add("{:status}", obj.WddStatus);
            sql.Condition.Add("{:docEntry}", obj.DraftEntry);
            sql.Condition.Add("{:userId}", obj.UserId);
            sql.Condition.Add("{:company}", ServiceDataAccess.Instance.getCompanyName(obj.Company));

            return sql.generateSqlWithLog();
        }

        public string getQueryUpdateOAWSClose(ApproveDocumentInfo obj)
        {
            TemplateSQL sql = new TemplateSQL();

            if (B1DataAccess.Instance.IsHana)
            {
            }
            else
            {
                sql.append(" update {:company}..[@JWS_OAWS] set ");
                sql.append(" U_JWS_Status = N'C', ");
                sql.append(" U_JWS_UpdateDate = CAST(GETDATE() as date), ");
                sql.append(" U_JWS_UpdateTime = FORMAT(GETDATE(), 'HHmm'), ");
                sql.append(" U_JWS_UpdateBy = {:userId} ");
                sql.append(" where 1 = 1 ");
                sql.append(" and U_JWS_Status = 'W' ");
                sql.append(" and U_JWS_DocEntry = {:docEntry} ");
            }

            sql.Condition.Add("{:docEntry}", obj.DraftEntry);
            sql.Condition.Add("{:userId}", obj.UserId);
            sql.Condition.Add("{:company}", ServiceDataAccess.Instance.getCompanyName(obj.Company));

            return sql.generateSqlWithLog();
        }

        public string getQueryUpdateOAWSNextStage(ApproveDocumentInfo obj)
        {
            TemplateSQL sql = new TemplateSQL();

            if (B1DataAccess.Instance.IsHana)
            {
            }
            else
            {
                sql.append(" update {:company}..[@JWS_OAWS] set ");
                sql.append(" U_JWS_CurrentStage = U_JWS_CurrentStage + 1, ");
                sql.append(" U_JWS_UpdateDate = CAST(GETDATE() as date), ");
                sql.append(" U_JWS_UpdateTime = FORMAT(GETDATE(), 'HHmm'), ");
                sql.append(" U_JWS_UpdateBy = {:userId} ");
                sql.append(" where 1 = 1 ");
                sql.append(" and U_JWS_Status = 'W' ");
                sql.append(" and U_JWS_DocEntry = {:docEntry} ");
                sql.append(" and Code = {:code} ");
            }

            //sql.Condition.Add("{:status}", obj.WddStatus);
            sql.Condition.Add("{:code}", obj.ParentKey);
            sql.Condition.Add("{:docEntry}", obj.DraftEntry);
            sql.Condition.Add("{:userId}", obj.UserId);
            sql.Condition.Add("{:company}", ServiceDataAccess.Instance.getCompanyName(obj.Company));

            return sql.generateSqlWithLog();
        }

        //public string getQueryCheckConvertApproveDocument(int docEntry)
        //{
        //    TemplateSQL sql = new TemplateSQL();

        //    if (B1DataAccess.Instance.IsHana)
        //    {
        //    }
        //    else
        //    {
        //        sql.select(" select case when t1.Rejected >= t0.U_JWS_MaxRejReqr then 'N' ");
        //        sql.select(" when t1.Rejected < t0.U_JWS_MaxRejReqr and t1.Approved >= t0.U_JWS_MaxReqr then 'Y' else 'W' end as 'WddStatus' ");
        //        sql.from(" from [@JWS_OAWS] t0 ");
        //        sql.innerjoin(" ( ");
        //        sql.append(" select t0.U_JWS_DocEntry, ");
        //        sql.append(" count(case when t0.U_JWS_WddStatus = 'Y' then t0.U_JWS_WddStatus end) as Approved, ");
        //        sql.append(" count(case when t0.U_JWS_WddStatus = 'N' then t0.U_JWS_WddStatus end) as Rejected ");
        //        sql.append(" from [@JWS_AWS1] t0 ");
        //        sql.append(" where 1 = 1 ");
        //        sql.append(" and t0.U_JWS_DocEntry = {:docEntry} ");
        //        sql.append(" and t0.U_JWS_Active = 'Y' ");
        //        sql.append(" group By t0.U_JWS_DocEntry ");
        //        sql.append(" ) t1 on t0.U_JWS_DocEntry = t1.U_JWS_DocEntry ");
        //        sql.where(" where 1 = 1 ");
        //        sql.where(" and t0.U_JWS_WddStatus = 'W' ");
        //        sql.where(" and t0.U_JWS_DocEntry = {:docEntry} ");
        //    }

        //    sql.Condition.Add("{:docEntry}", docEntry);
        //    //sql.Condition.Add("{:company}", ServiceDataAccess.Instance.getCompanyName(obj.Company));

        //    return sql.generateSqlWithLog();
        //}

        public string getQueryCheckApproveDocumentInfo(string company, int docEntry)
        {
            TemplateSQL sql = new TemplateSQL();

            if (B1DataAccess.Instance.IsHana)
            {
            }
            else
            {
                sql.select(" select t0.Code, t1.U_JWS_Stage as Stage, ");
                sql.select(" min(t0.U_JWS_TotalStage) as TotalStage, min(t0.U_JWS_CurrentStage) as CurrentStage, ");
                sql.select(" min(t1.U_JWS_MaxReqr) as MaxReqr, min(t1.U_JWS_MaxRejReqr) as MaxRejReqr, ");
                sql.select(" sum(case when t1.U_JWS_Status = 'Y' then 1 else 0 end) as CntApproved, ");
                sql.select(" sum(case when t1.U_JWS_Status = 'N' then 1 else 0 end) as CntRejected ");
                sql.from(" from {:company}..[@JWS_OAWS] t0 (nolock) ");
                sql.innerjoin(" {:company}..[@JWS_AWS1] t1 (nolock) on t0.Code = t1.U_JWS_ParentKey ");
                sql.where(" where 1 = 1 ");
                sql.where(" and t0.U_JWS_DocEntry = {:docEntry} ");
                sql.groupBy(" t0.Code, t1.U_JWS_Stage ");
            }

            sql.Condition.Add("{:docEntry}", docEntry);
            sql.Condition.Add("{:company}", ServiceDataAccess.Instance.getCompanyName(company));

            return sql.generateSqlWithLog();
        }

        public string getQuerySearchSalesPriceHistory(SalesPriceHistoryCondition condition)
        {
            TemplateSQL sql = new TemplateSQL();

            if (B1DataAccess.Instance.IsHana)
            {
            }
            else
            {
                // fix 20 record
                sql.select(" select top 20 t0.DocEntry, t0.DocNum, t0.DocDate, t0.DocDueDate, ");
                sql.select(" t1.ItemCode, t1.Quantity, t1.PriceBefDi as UnitPrice, ");
                sql.select(" t1.DiscPrcnt as DiscountPercent, t1.Price as PriceAfterDiscount, t1.UomCode ");
                sql.from(" from {:company}..ordr t0 (nolock) ");
                sql.innerjoin(" {:company}..rdr1 t1 (nolock) on t0.DocEntry = t1.DocEntry ");
                sql.where(" where 1 = 1 ");
                sql.where(" and t0.CANCELED = 'N' ");
                sql.where(" and t0.CardCode = N'{:cardCode}' ");
                sql.where(" and t1.ItemCode = N'{:itemCode}' ");
            }

            if (!condition.IsDraft && condition.DocEntry > 0)
            {
                if (B1DataAccess.Instance.IsHana)
                {

                }
                else
                {
                    sql.where(" and t0.DocEntry not in ({:docEntry}) ");
                    sql.Condition.Add("{:docEntry}", condition.DocEntry);
                }
            }

            if (B1DataAccess.Instance.IsHana)
            {

            }
            else
            {
                sql.orderBy(" t0.DocDate desc ");
            }

            sql.Condition.Add("{:cardCode}", condition.CardCode);
            sql.Condition.Add("{:itemCode}", condition.ItemCode);
            sql.Condition.Add("{:company}", ServiceDataAccess.Instance.getCompanyName(condition.Company));

            return sql.generateSqlWithLog();
        }

        public string getQueryBusinessPartnerOverdueDebt(string company, List<string> cardCode)
        {
            TemplateSQL sql = new TemplateSQL();

            if (B1DataAccess.Instance.IsHana)
            {
            }
            else
            {
                sql.select(" select t0.CardCode, sum(t0.Bal) as OverdueDebt ");
                sql.from(" from ( ");

                sql.select(" SELECT T0.[TransId], T0.[Line_ID], T0.[BPLName], MAX(T0.[ShortName]) as CardCode, ");
                sql.select(" MAX(T0.[Debit]) - MAX(T0.[Credit]) as Bal, MAX(T0.[FCDebit]) - MAX(T0.[FCCredit]) as FCBal, MAX(T0.[SYSDeb]) - MAX(T0.[SYSCred]) as SYSBal ");
                sql.from(" FROM {:company}..[JDT1] T0 (nolock) ");
                sql.innerjoin(" {:company}..[ITR1] T1 (nolock) ON  T1.[TransId] = T0.[TransId]  AND  T1.[TransRowId] = T0.[Line_ID] ");
                sql.innerjoin(" {:company}..[OITR] T2 (nolock) ON  T2.[ReconNum] = T1.[ReconNum] ");
                sql.innerjoin(" {:company}..[OJDT] T3 (nolock) ON  T3.[TransId] = T0.[TransId] ");
                sql.innerjoin(" {:company}..[OCRD] T4 (nolock) ON  T4.[CardCode] = T0.[ShortName] ");
                sql.leftjoin(" {:company}..[B1_JournalTransSourceView] T5  ON  T5.[ObjType] = T0.[TransType]  AND  T5.[DocEntry] = T0.[CreatedBy]  AND  (T5.[TransType] <> 'I'  OR  (T5.[TransType] = 'I'  AND  T5.[InstlmntID] = T0.[SourceLine] )) ");
                sql.where(" WHERE T0.[RefDate] <= '{:refDate}' ");
                sql.where(" AND  T0.[RefDate] <= '{:refDate}' ");
                sql.where(" AND  T4.[CardType] = 'C' ");
                sql.where(" AND  T4.[Balance] <> 0 ");
                sql.where(" AND  T4.[CardCode] in ({:cardCode}) ");
                //sql.where(" AND  T4.[CardCode] >= N'{:cardCode}' ");
                //sql.where(" AND  T4.[CardCode] <= N'{:cardCode} ");
                sql.where(" AND  T2.[ReconDate] > '{:refDate}' ");
                sql.where(" AND  T1.[IsCredit] = 'C' ");
                sql.groupBy(" T0.[TransId], T0.[Line_ID], T0.[BPLName] ");
                sql.having(" MAX(T0.[BalFcCred]) <>- SUM(T1.[ReconSumFC])  OR  MAX(T0.[BalDueCred]) <>- SUM(T1.[ReconSum]) ");

                sql.append(" UNION ALL ");

                sql.select(" SELECT T0.[TransId], T0.[Line_ID], T0.[BPLName], MAX(T0.[ShortName]), ");
                sql.select(" MAX(T0.[Debit]) - MAX(T0.[Credit]), MAX(T0.[FCDebit]) - MAX(T0.[FCCredit]), MAX(T0.[SYSDeb]) - MAX(T0.[SYSCred]) ");
                sql.from(" FROM  {:company}..[JDT1] T0 (nolock) ");
                sql.innerjoin(" {:company}..[ITR1] T1 (nolock) ON  T1.[TransId] = T0.[TransId]  AND  T1.[TransRowId] = T0.[Line_ID] ");
                sql.innerjoin(" {:company}..[OITR] T2 (nolock) ON  T2.[ReconNum] = T1.[ReconNum] ");
                sql.innerjoin(" {:company}..[OJDT] T3 (nolock) ON  T3.[TransId] = T0.[TransId] ");
                sql.innerjoin(" {:company}..[OCRD] T4 (nolock) ON  T4.[CardCode] = T0.[ShortName] ");
                sql.leftjoin(" {:company}..[B1_JournalTransSourceView] T5  ON T5.[ObjType] = T0.[TransType]  AND  T5.[DocEntry] = T0.[CreatedBy]  AND  (T5.[TransType] <> 'I'  OR  (T5.[TransType] = 'I'  AND  T5.[InstlmntID] = T0.[SourceLine] )) ");
                sql.where(" WHERE T0.[RefDate] <= '{:refDate}' ");
                sql.where(" AND  T0.[RefDate] <= '{:refDate}' ");
                sql.where(" AND  T4.[CardType] = 'C' ");
                sql.where(" AND  T4.[Balance] <> 0 ");
                sql.where(" AND  T4.[CardCode] in ({:cardCode}) ");
                //sql.where(" AND  T4.[CardCode] >= N'{:cardCode} ");
                //sql.where(" AND  T4.[CardCode] <= N'{:cardCode} ");
                sql.where(" AND  T2.[ReconDate] > '{:refDate}' ");
                sql.where(" AND  T1.[IsCredit] = 'C' ");
                sql.groupBy(" T0.[TransId], T0.[Line_ID], T0.[BPLName] ");
                sql.having(" MAX(T0.[BalFcDeb]) <>- SUM(T1.[ReconSumFC])  OR  MAX(T0.[BalDueDeb]) <>- SUM(T1.[ReconSum]) ");

                sql.append(" UNION ALL ");

                sql.select(" SELECT T0.[TransId], T0.[Line_ID], T0.[BPLName], MAX(T0.[ShortName]), ");
                sql.select(" MAX(T0.[Debit]) - MAX(T0.[Credit]), MAX(T0.[FCDebit]) - MAX(T0.[FCCredit]), MAX(T0.[SYSDeb]) - MAX(T0.[SYSCred]) ");
                sql.from(" FROM  {:company}..[JDT1] T0 (nolock) ");
                sql.innerjoin(" {:company}..[OJDT] T1 (nolock) ON  T1.[TransId] = T0.[TransId]  ");
                sql.innerjoin(" {:company}..[OCRD] T2 (nolock) ON  T2.[CardCode] = T0.[ShortName]  ");
                sql.leftjoin(" [B1_JournalTransSourceView] T3  ON  T3.[ObjType] = T0.[TransType]  AND  T3.[DocEntry] = T0.[CreatedBy]  AND  (T3.[TransType] <> 'I'  OR  (T3.[TransType] = 'I'  AND  T3.[InstlmntID] = T0.[SourceLine] )) ");
                sql.where(" WHERE T0.[RefDate] <= '{:refDate}' ");
                sql.where(" AND  T0.[RefDate] <= '{:refDate}' ");
                sql.where(" AND  T2.[CardType] = 'C' ");
                sql.where(" AND  T2.[Balance] <> 0 ");
                sql.where(" AND  T2.[CardCode] in ({:cardCode}) ");
                //sql.where(" AND  T2.[CardCode] >= N'{:cardCode} ");
                //sql.where(" AND  T2.[CardCode] <= N'{:cardCode} ");
                sql.where(" AND  (T0.[BalDueCred] <> T0.[BalDueDeb]  OR  T0.[BalFcCred] <> T0.[BalFcDeb] ) ");
                sql.where(" AND  NOT EXISTS ( ");

                sql.append(" SELECT U0.[TransId], U0.[TransRowId] ");
                sql.append(" FROM  {:company}..[ITR1] U0 ");
                sql.append(" INNER  JOIN {:company}..[OITR] U1  ON  U1.[ReconNum] = U0.[ReconNum] ");
                sql.append(" WHERE T0.[TransId] = U0.[TransId] ");
                sql.append(" AND  T0.[Line_ID] = U0.[TransRowId]  ");
                sql.append(" AND  U1.[ReconDate] > '{:refDate}' ");
                sql.append(" GROUP BY U0.[TransId], U0.[TransRowId]) ");

                sql.groupBy(" T0.[TransId], T0.[Line_ID], T0.[BPLName] ");
                sql.from(" ) t0 ");
                sql.groupBy(" t0.CardCode ");
            }

            sql.Condition.Add("{:cardCode}", StringUtils.setInParameter(cardCode));
            sql.Condition.Add("{:refDate}", DateTimeUtils.getCurrentCompanyDate());
            sql.Condition.Add("{:company}", ServiceDataAccess.Instance.getCompanyName(company));

            return sql.generateSqlWithLog();
        }

        public string getQuerySearchGrossProfitByCondition(GrossProfitCondition condition)
        {
            TemplateSQL sql = new TemplateSQL();

            if (B1DataAccess.Instance.IsHana)
            {
            }
            else
            {
                sql.select(" select a.*, case when a.ProfitPercent < 0 then 'R' end as Color ");
                sql.from(" from ( ");
                sql.select(" select (t0.DocEntry+t0.LineNum) as DocEntry, t0.ItemCode, t0.Dscription as ItemName, t0.GrossBuyPr as BasePrice, t0.GPTtlBasPr as TotalBasePrice, ");
                sql.select(" t0.Price as SalesPrice, t0.Quantity, t0.GrssProfit as GrossProfit, ");
                sql.select(" case when t0.Price = 0 then 0  ");
                sql.select(" when t0.GrossBuyPr > 0 and t0.GPTtlBasPr > 0 then (((t0.GrssProfit / t0.Price) * 100) / t0.Quantity) ");
                sql.select(" else 100 end as ProfitPercent, t0.LineNum ");
                sql.from(" from {:company}..drf1 t0 (nolock)  ");
                sql.where(" where 1 = 1 ");
                sql.where(" and t0.ObjType = '17' ");
                sql.where(" and t0.DocEntry = {:docEntry} ");
                sql.append(" ) a ");
                sql.where(" where 1 = 1 ");
            }

            sql.Condition.Add("{:docEntry}", condition.DocEntry);
            sql.Condition.Add("{:company}", ServiceDataAccess.Instance.getCompanyName(condition.Company));

            return sql.generateSqlWithLog();
        }

        public string getQueryCheckDraftConverted(string company, int draftEntry)
        {
            TemplateSQL sql = new TemplateSQL();

            if (B1DataAccess.Instance.IsHana)
            {
            }
            else
            {
                sql.select(" select count(t0.DocEntry) ");
                sql.from(" from {:company}..ordr t0 (nolock)  ");
                sql.where(" where 1 = 1 ");
                sql.where(" and t0.draftKey = {:draftKey} ");
            }

            sql.Condition.Add("{:draftKey}", draftEntry);
            sql.Condition.Add("{:company}", ServiceDataAccess.Instance.getCompanyName(company));

            return sql.generateSqlWithLog();
        }

        public string getQueryCheckApprove(string company, int docEntry)
        {
            TemplateSQL sql = new TemplateSQL();

            if (B1DataAccess.Instance.IsHana)
            {
            }
            else
            {
                sql.select(" select count(*) ");
                sql.from(" from {:company}..[@JWS_OAWS] t0 (nolock) ");
                sql.where(" where 1 = 1 ");
                sql.where(" and t0.U_JWS_Status = 'Y' ");
                sql.where(" and t0.U_JWS_DocEntry = {:docEntry} ");
            }

            sql.Condition.Add("{:docEntry}", docEntry);
            sql.Condition.Add("{:company}", ServiceDataAccess.Instance.getCompanyName(company));

            return sql.generateSqlWithLog();
        }
    }
}
