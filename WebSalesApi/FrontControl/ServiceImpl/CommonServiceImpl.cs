﻿using BackControl.ServiceDao;
using BackControl.ServiceDaoImpl;
using FrontControl.Service;
using FrontControl.Lookup;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CommonControl.POCO.Condition;
using B1Utility.Utils;
using CommonControl.POCO.Common;
using CommonControl.POCO.Class;

namespace FrontControl.ServiceImpl
{
    public class CommonServiceImpl : ICommonService
    {
        private CommonDaoImpl commonDao;

        public CommonServiceImpl()
        {
            commonDao = new CommonDaoImpl();
        }

        public List<SalesEmployee> getSalesEmployee(string company)
        {
            List<SalesEmployee> response = null;

            try
            {
                response = commonDao.getSalesEmployee(company);
            }
            catch (Exception ex)
            {
                ExceptionUtils.handleException(ex);
            }

            return response;
        }

        public List<PaymentTerms> getPaymentTerms(string company)
        {
            List<PaymentTerms> response = null;

            try
            {
                response = commonDao.getPaymentTerms(company);
            }
            catch (Exception ex)
            {
                ExceptionUtils.handleException(ex);
            }

            return response;
        }

        public List<BusinessPartnerGroup> getBusinessPartnerGroup(string company)
        {
            List<BusinessPartnerGroup> response = null;

            try
            {
                response = commonDao.getBusinessPartnerGroup(company);
            }
            catch (Exception ex)
            {
                ExceptionUtils.handleException(ex);
            }

            return response;
        }

        public List<ShippingType> getShippingType(string company)
        {
            List<ShippingType> response = null;

            try
            {
                response = commonDao.getShippingType(company);
            }
            catch (Exception ex)
            {
                ExceptionUtils.handleException(ex);
            }

            return response;
        }


        public List<ItemGroup> getItemGroup(string company)
        {
            List<ItemGroup> response = null;

            try
            {
                response = commonDao.getItemGroup(company);
            }
            catch (Exception ex)
            {
                ExceptionUtils.handleException(ex);
            }

            return response;
        }

        public List<string> getWhse(string company)
        {
            List<string> response = null;

            try
            {
                response = commonDao.getWhse(company);
            }
            catch (Exception ex)
            {
                ExceptionUtils.handleException(ex);
            }

            return response;
        }


        public List<Properties> getProperties(string company)
        {
            List<Properties> response = null;

            try
            {
                response = commonDao.getProperties(company);
            }
            catch (Exception ex)
            {
                ExceptionUtils.handleException(ex);
            }

            return response;
        }


        public List<Country> getCountry(string company)
        {
            List<Country> response = null;

            try
            {
                response = commonDao.getCountry(company);
            }
            catch (Exception ex)
            {
                ExceptionUtils.handleException(ex);
            }

            return response;
        }


        public List<TaxGroup> getTaxGroup(string company)
        {
            List<TaxGroup> response = null;

            try
            {
                response = commonDao.getTaxGroup(company);
            }
            catch (Exception ex)
            {
                ExceptionUtils.handleException(ex);
            }

            return response;
        }

        public List<State> getState(string company)
        {
            List<State> response = null;

            try
            {
                response = commonDao.getState(company);
            }
            catch (Exception ex)
            {
                ExceptionUtils.handleException(ex);
            }

            return response;
        }


        public List<Dimension> getDimension(string company)
        {
            List<Dimension> response = null;

            try
            {
                response = commonDao.getDimension(company);
            }
            catch (Exception ex)
            {
                ExceptionUtils.handleException(ex);
            }

            return response;
        }

        public List<PriceList> getPriceList(string company)
        {
            List<PriceList> response = null;

            try
            {
                response = commonDao.getPriceList(company);
            }
            catch (Exception ex)
            {
                ExceptionUtils.handleException(ex);
            }

            return response;
        }

        public List<Brand> getBrand(string company)
        {
            List<Brand> response = null;

            try
            {
                response = commonDao.getBrand(company);
            }
            catch (Exception ex)
            {
                ExceptionUtils.handleException(ex);
            }

            return response;
        }

        public List<LocationAddress> getLocationAddress(string company)
        {
            List<LocationAddress> response = null;

            try
            {
                response = commonDao.getLocationAddress(company);
            }
            catch (Exception ex)
            {
                ExceptionUtils.handleException(ex);
            }

            return response;
        }

        public List<UomGroupConverter> getUomGroupConverter(string company)
        {
            List<UomGroupConverter> response = null;

            try
            {
                response = commonDao.getUomGroupConverter(company);
            }
            catch (Exception ex)
            {
                ExceptionUtils.handleException(ex);
            }

            return response;
        }

        public WebAdminSAPProfile getSapProfile(string company, WebAdminProfile obj)
        {
            WebAdminSAPProfile response = null;

            try
            {
                response = commonDao.getSapProfile(company, obj);
            }
            catch (Exception ex)
            {
                ExceptionUtils.handleException(ex);
            }

            return response;
        }
    }
}
