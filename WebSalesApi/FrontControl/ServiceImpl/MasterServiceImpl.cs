﻿using BackControl.ServiceDao;
using BackControl.ServiceDaoImpl;
using FrontControl.Service;
using FrontControl.Lookup;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CommonControl.POCO.Class;
using CommonControl.POCO.Condition;
using B1Utility.Utils;

namespace FrontControl.ServiceImpl
{
    public class MasterServiceImpl : IMasterService
    {
        private MasterDaoImpl masterDao;

        public MasterServiceImpl()
        {
            masterDao = new MasterDaoImpl();
        }

        public List<ItemMaster> searchItemMasterByCondition(ref ItemMasterCondition condition)
        {
            List<ItemMaster> response = null;

            try
            {
                response = masterDao.searchItemMasterByCondition(ref condition);
            }
            catch (Exception ex)
            {
                ExceptionUtils.handleException(ex);
            }

            return response;
        }

        public ItemMaster searchItemMasterByItemCode(ItemMasterCondition condition)
        {
            ItemMaster response = null;

            try
            {
                response = masterDao.searchItemMasterByItemCode(condition);
            }
            catch (Exception ex)
            {
                ExceptionUtils.handleException(ex);
            }

            return response;
        }

        public List<ItemOnHand> searchItemOnHandByItemCode(ItemMasterCondition condition)
        {
            List<ItemOnHand> response = null;

            try
            {
                response = masterDao.searchItemOnHandByItemCode(condition);
            }
            catch (Exception ex)
            {
                ExceptionUtils.handleException(ex);
            }

            return response;
        }

        public List<BusinessPartner> searchBusinessPartnerByCondition(ref BusinessPartnerCondition condition)
        {
            List<BusinessPartner> response = null;

            try
            {
                response = masterDao.searchBusinessPartnerByCondition(ref condition);
            }
            catch (Exception ex)
            {
                ExceptionUtils.handleException(ex);
            }

            return response;
        }

        public BusinessPartner searchBusinessPartnerByCardCode(BusinessPartnerCondition condition)
        {
            BusinessPartner response = null;

            try
            {
                response = masterDao.searchBusinessPartnerByCardCode(condition);
            }
            catch (Exception ex)
            {
                ExceptionUtils.handleException(ex);
            }

            return response;
        }

        public List<BusinessPartnerAddress> searchBusinessPartnerAddressByCardCode(BusinessPartnerCondition condition)
        {
            List<BusinessPartnerAddress> response = null;

            try
            {
                response = masterDao.searchBusinessPartnerAddressByCardCode(condition);
            }
            catch (Exception ex)
            {
                ExceptionUtils.handleException(ex);
            }

            return response;
        }

        public List<BusinessPartnerContact> searchBusinessPartnerContactByCardCode(BusinessPartnerCondition condition)
        {
            List<BusinessPartnerContact> response = null;

            try
            {
                response = masterDao.searchBusinessPartnerContactByCardCode(condition);
            }
            catch (Exception ex)
            {
                ExceptionUtils.handleException(ex);
            }

            return response;
        }

        public void createBusinessPartner(BusinessPartner bp)
        {
            try
            {
                masterDao.createBusinessPartner(bp);
            }
            catch (Exception ex)
            {
                ExceptionUtils.handleException(ex);
            }
        }

        public void updateBusinessPartner(BusinessPartner bp)
        {
            try
            {
                masterDao.updateBusinessPartner(bp);
            }
            catch (Exception ex)
            {
                ExceptionUtils.handleException(ex);
            }
        }

        public List<BusinessPartnerCriteria> searchBusinessPartnerCriteria(BusinessPartnerCondition condition)
        {
            List<BusinessPartnerCriteria> response = null;

            try
            {
                response = masterDao.searchBusinessPartnerCriteria(condition);
            }
            catch (Exception ex)
            {
                ExceptionUtils.handleException(ex);
            }

            return response;
        }

        public List<ItemMasterCriteria> searchItemMasterCriteria(ItemMasterCondition condition)
        {
            List<ItemMasterCriteria> response = null;

            try
            {
                response = masterDao.searchItemMasterCriteria(condition);
            }
            catch (Exception ex)
            {
                ExceptionUtils.handleException(ex);
            }

            return response;
        }
    }
}
