﻿using BackControl.ServiceDao;
using BackControl.ServiceDaoImpl;
using FrontControl.Service;
using FrontControl.Lookup;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CommonControl.Common;
using CommonControl.POCO.Class;
using CommonControl.POCO.Condition;
using B1Utility.Utils;
using CommonControl.POCO.Enum;
using CommonControl.POCO.Common;

namespace FrontControl.ServiceImpl
{
    public class DocumentServiceImpl : IDocumentService
    {
        private Object lockThis = new Object();
        private DocumentDaoImpl documentDao;

        public DocumentServiceImpl()
        {
            documentDao = new DocumentDaoImpl();
        }

        public List<BusinessPartnerForSO> searchBusinessPartnerForSO(ref BusinessPartnerCondition condition)
        {
            List<BusinessPartnerForSO> response = null;

            try
            {
                response = documentDao.searchBusinessPartnerForSO(ref condition);
            }
            catch (Exception ex)
            {
                ExceptionUtils.handleException(ex);
            }

            return response;
        }

        public List<ItemMasterForSO> searchItemMasterForSO(ref ItemMasterCondition condition)
        {
            List<ItemMasterForSO> response = null;

            try
            {
                response = documentDao.searchItemMasterForSO(ref condition);
            }
            catch (Exception ex)
            {
                ExceptionUtils.handleException(ex);
            }

            return response;
        }

        public ItemPrice getItemPrice(ItemPriceCondition condition)
        {
            ItemPrice response = null;

            try
            {
                response = documentDao.getItemPrice(condition);
            }
            catch (Exception ex)
            {
                ExceptionUtils.handleException(ex);
            }

            return response;
        }


        public List<BatchNumberDetail> searchItemBatchNumberSelection(ItemMasterCondition condition)
        {
            List<BatchNumberDetail> response = null;

            try
            {
                response = documentDao.searchItemBatchNumberSelection(condition);
            }
            catch (Exception ex)
            {
                ExceptionUtils.handleException(ex);
            }

            return response;
        }

        public List<ItemWhsOnHand> searchItemWhsOnHand(ItemMasterCondition condition)
        {
            List<ItemWhsOnHand> response = null;

            try
            {
                response = documentDao.searchItemWhsOnHand(condition);
            }
            catch (Exception ex)
            {
                ExceptionUtils.handleException(ex);
            }

            return response;
        }


        public List<SalesOrderResponse> createSalesOrderList(List<SalesOrder> objList)
        {
            List<SalesOrderResponse> response = null;

            try
            {
                response = documentDao.createSalesOrderList(objList);
            }
            catch (Exception ex)
            {
                ExceptionUtils.handleException(ex);
            }

            return response;
        }

        public SalesOrderResponse createSalesOrder(SalesOrder obj)
        {
            SalesOrderResponse response = null;

            try
            {
                response = documentDao.createSalesOrder(obj);
            }
            catch (Exception ex)
            {
                ExceptionUtils.handleException(ex);
            }

            return response;
        }


        public List<SeriesDocument> searchSeriesByCondition(SeriesCondition condition)
        {
            List<SeriesDocument> response = null;

            try
            {
                response = documentDao.searchSeriesByCondition(condition);
            }
            catch (Exception ex)
            {
                ExceptionUtils.handleException(ex);
            }

            return response;
        }


        public List<SalesOrder> searchSalesOrderByCondition(ref SalesOrderCondition condition)
        {
            List<SalesOrder> response = null;

            try
            {
                response = documentDao.searchSalesOrderByCondition(ref condition);
            }
            catch (Exception ex)
            {
                ExceptionUtils.handleException(ex);
            }

            return response;
        }

        public SalesOrder searchSalesOrderByDocEntry(SalesOrderCondition condition)
        {
            SalesOrder response = null;

            try
            {
                response = documentDao.searchSalesOrderByDocEntry(condition);
            }
            catch (Exception ex)
            {
                ExceptionUtils.handleException(ex);
            }

            return response;
        }

        public List<DocumentRelation> searchSalesOrderDocumentRelation(SalesOrderCondition condition)
        {
            List<DocumentRelation> response = null;

            try
            {
                response = documentDao.searchSalesOrderDocumentRelation(condition);
            }
            catch (Exception ex)
            {
                ExceptionUtils.handleException(ex);
            }

            return response;
        }

        public List<OrderStatus> searchOrderStatusByCondition(ref ReportOrderStatusCondition condition)
        {
            List<OrderStatus> response = null;

            try
            {
                response = documentDao.searchOrderStatusByCondition(ref condition);
            }
            catch (Exception ex)
            {
                ExceptionUtils.handleException(ex);
            }

            return response;
        }

        public List<SalesOrderResponse> updateSalesOrderList(List<SalesOrder> objList)
        {
            List<SalesOrderResponse> response = null;

            try
            {
                response = documentDao.updateSalesOrderList(objList);
            }
            catch (Exception ex)
            {
                ExceptionUtils.handleException(ex);
            }

            return response;
        }

        public SalesOrderResponse updateSalesOrder(SalesOrder obj)
        {
            SalesOrderResponse response = null;

            try
            {
                response = documentDao.updateSalesOrder(obj);
            }
            catch (Exception ex)
            {
                ExceptionUtils.handleException(ex);
            }

            return response;
        }

        public ApproveDocumentInfo approveSalesOrder(ApproveDocumentInfo obj)
        {
            ApproveDocumentInfo response = null;

            try
            {
                response = documentDao.convertDocumentWithoutApprove(obj);
            }
            catch (Exception ex)
            {
                ExceptionUtils.handleException(ex);
            }

            return response;
        }

        public List<EmailSenderInfo> getApproverEmail(string company, int docEntry)
        {
            List<EmailSenderInfo> response = null;

            try
            {
                response = documentDao.getApproverEmail(company, docEntry);
            }
            catch (Exception ex)
            {
                ExceptionUtils.handleException(ex);
            }

            return response;
        }

        public List<ApproveDocumentInfo> approveDocument(List<ApproveSalesOrderResult> obj)
        {
            List<ApproveDocumentInfo> response = null;

            lock (lockThis)
            {
                try
                {
                    response = documentDao.approveDocument(obj);
                }
                catch (Exception ex)
                {
                    ExceptionUtils.handleException(ex);
                }
            }

            return response;
        }

        public List<ApproveSalesOrder> searchApprovalSalesOrderByCondition(ref ApproveSalesOrderCondition condition)
        {
            List<ApproveSalesOrder> response = null;

            try
            {
                response = documentDao.searchApprovalSalesOrderByCondition(ref condition);
            }
            catch (Exception ex)
            {
                ExceptionUtils.handleException(ex);
            }

            return response;
        }

        public EmailSenderInfo getOriginatorEmail(string company, int docEntry)
        {
            EmailSenderInfo response = null;

            try
            {
                response = documentDao.getOriginatorEmail(company, docEntry);
            }
            catch (Exception ex)
            {
                ExceptionUtils.handleException(ex);
            }

            return response;
        }

        public List<SalesPriceHistory> searchSalesPriceHistoryByCondition(SalesPriceHistoryCondition condition)
        {
            List<SalesPriceHistory> response = null;

            try
            {
                response = documentDao.searchSalesPriceHistoryByCondition(condition);
            }
            catch (Exception ex)
            {
                ExceptionUtils.handleException(ex);
            }

            return response;
        }

        public List<GrossProfitInfo> searchGrossProfitByCondition(GrossProfitCondition condition)
        {
            List<GrossProfitInfo> response = null;

            try
            {
                response = documentDao.searchGrossProfitByCondition(condition);
            }
            catch (Exception ex)
            {
                ExceptionUtils.handleException(ex);
            }

            return response;
        }

        public int checkApprove(string company, int docEntry)
        {
            int response = -1;

            try
            {
                response = documentDao.checkApprove(company, docEntry);
            }
            catch (Exception ex)
            {
                ExceptionUtils.handleException(ex);
            }

            return response;
        }
    }
}
