﻿using BackControl.ServiceDao;
using BackControl.ServiceDaoImpl;
using FrontControl.Service;
using FrontControl.Lookup;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CommonControl.POCO.Class;
using CommonControl.POCO.Condition;
using B1Utility.Utils;
using CommonControl.POCO.Common;

namespace FrontControl.ServiceImpl
{
    public class ReportServiceImpl : IReportService
    {
        private ReportDaoImpl reportDao;

        public ReportServiceImpl()
        {
            reportDao = new ReportDaoImpl();
        }

        public List<InventoryStatus> getReportInventoryStatus(ReportInventoryStatusCondition condition)
        {
            List<InventoryStatus> response = null;

            try
            {
                response = reportDao.getReportInventoryStatus(condition);
            }
            catch (Exception ex)
            {
                ExceptionUtils.handleException(ex);
            }

            return response;
        }

        public List<InventoryStatusDocument> getReportInventoryStatusDocument(ReportInventoryStatusCondition condition)
        {
            List<InventoryStatusDocument> response = null;

            try
            {
                response = reportDao.getReportInventoryStatusDocument(condition);
            }
            catch (Exception ex)
            {
                ExceptionUtils.handleException(ex);
            }

            return response;
        }


        public List<BatchNumberTransactionReport> getReportBatchNumberTransaction(ReportBatchNumberTransactionCondition condition)
        {
            List<BatchNumberTransactionReport> response = null;

            try
            {
                response = reportDao.getReportBatchNumberTransaction(condition);
            }
            catch (Exception ex)
            {
                ExceptionUtils.handleException(ex);
            }

            return response;
        }

        public List<BatchNumberTransactionReportDetail> getReportBatchNumberTransactionDetail(ReportBatchNumberTransactionDetailCondition condition)
        {
            List<BatchNumberTransactionReportDetail> response = null;

            try
            {
                response = reportDao.getReportBatchNumberTransactionDetail(condition);
            }
            catch (Exception ex)
            {
                ExceptionUtils.handleException(ex);
            }

            return response;
        }

        public List<CustomerPaymentReport> getReportCustomerPayment(ReportCustomerPaymentCondition condition)
        {
            List<CustomerPaymentReport> response = null;

            try
            {
                response = reportDao.getReportCustomerPayment(condition);
            }
            catch (Exception ex)
            {
                ExceptionUtils.handleException(ex);
            }

            return response;
        }


        public List<PurchaseOrderMovement> getReportPurchaseOrderMovement(ReportPurchaseOrderMovementCondition condition)
        {
            List<PurchaseOrderMovement> response = null;

            try
            {
                response = reportDao.getReportPurchaseOrderMovement(condition);
            }
            catch (Exception ex)
            {
                ExceptionUtils.handleException(ex);
            }

            return response;
        }

        public List<SalesAnalysisReport> getReportSalesAnalysis(SalesAnalysisCondition condition)
        {
            List<SalesAnalysisReport> response = null;

            try
            {
                response = reportDao.getReportSalesAnalysis(condition);
            }
            catch (Exception ex)
            {
                ExceptionUtils.handleException(ex);
            }

            return response;
        }
    }
}
