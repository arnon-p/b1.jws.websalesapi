﻿using CommonControl.Enum;
using FrontControl.Common;
using FrontControl.ServiceImpl;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FrontControl.Lookup
{
    public class LookupUtils
    {
        private Dictionary<ServiceType, IBaseService> serviceLookup;
        private static LookupUtils instance;
        private LookupUtils() { init(); }

        public static LookupUtils Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new LookupUtils();
                }
                return instance;
            }
        }

        private void init()
        {
            serviceLookup = new Dictionary<ServiceType, IBaseService>();
            serviceLookup.Add(ServiceType.COMMON, new CommonServiceImpl());
            serviceLookup.Add(ServiceType.MASTER, new MasterServiceImpl());
            serviceLookup.Add(ServiceType.DOCUMENT, new DocumentServiceImpl());
            serviceLookup.Add(ServiceType.REPORT, new ReportServiceImpl());
        }

        public CommonServiceImpl getCommonServiceImpl()
        {
            return (CommonServiceImpl)serviceLookup[ServiceType.COMMON];
        }

        public MasterServiceImpl getMasterServiceImpl()
        {
            return (MasterServiceImpl)serviceLookup[ServiceType.MASTER];
        }

        public DocumentServiceImpl getDocumentServiceImpl()
        {
            return (DocumentServiceImpl)serviceLookup[ServiceType.DOCUMENT];
        }

        public ReportServiceImpl getReportServiceImpl()
        {
            return (ReportServiceImpl)serviceLookup[ServiceType.REPORT];
        }
    }
}
