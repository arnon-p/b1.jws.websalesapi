﻿using CommonControl.Common;
using CommonControl.POCO.Class;
using CommonControl.POCO.Common;
using CommonControl.POCO.Condition;
using FrontControl.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FrontControl.Service
{
    public interface IDocumentService : IBaseService
    {
        List<BusinessPartnerForSO> searchBusinessPartnerForSO(ref BusinessPartnerCondition condition);
        List<ItemMasterForSO> searchItemMasterForSO(ref ItemMasterCondition condition);
        ItemPrice getItemPrice(ItemPriceCondition condition);
        List<BatchNumberDetail> searchItemBatchNumberSelection(ItemMasterCondition condition);
        List<ItemWhsOnHand> searchItemWhsOnHand(ItemMasterCondition condition);
        List<SalesOrderResponse> createSalesOrderList(List<SalesOrder> objList);
        SalesOrderResponse createSalesOrder(SalesOrder obj);
        List<SalesOrderResponse> updateSalesOrderList(List<SalesOrder> objList);
        SalesOrderResponse updateSalesOrder(SalesOrder obj);
        List<SeriesDocument> searchSeriesByCondition(SeriesCondition condition);
        List<SalesOrder> searchSalesOrderByCondition(ref SalesOrderCondition condition);
        SalesOrder searchSalesOrderByDocEntry(SalesOrderCondition condition);
        List<DocumentRelation> searchSalesOrderDocumentRelation(SalesOrderCondition condition);
        List<OrderStatus> searchOrderStatusByCondition(ref ReportOrderStatusCondition condition);
        ApproveDocumentInfo approveSalesOrder(ApproveDocumentInfo obj);
        List<EmailSenderInfo> getApproverEmail(string company, int docEntry);
        List<ApproveDocumentInfo> approveDocument(List<ApproveSalesOrderResult> obj);
        List<ApproveSalesOrder> searchApprovalSalesOrderByCondition(ref ApproveSalesOrderCondition condition);
        EmailSenderInfo getOriginatorEmail(string company, int docEntry);
        List<SalesPriceHistory> searchSalesPriceHistoryByCondition(SalesPriceHistoryCondition condition);
        List<GrossProfitInfo> searchGrossProfitByCondition(GrossProfitCondition condition);
    }
}
