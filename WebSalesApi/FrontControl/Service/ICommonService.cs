﻿using CommonControl.POCO.Class;
using CommonControl.POCO.Common;
using CommonControl.POCO.Condition;
using FrontControl.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FrontControl.Service
{
    public interface ICommonService : IBaseService
    {
        List<SalesEmployee> getSalesEmployee(string company);
        List<PaymentTerms> getPaymentTerms(string company);
        List<BusinessPartnerGroup> getBusinessPartnerGroup(string company);
        List<ShippingType> getShippingType(string company);
        List<ItemGroup> getItemGroup(string company);
        List<string> getWhse(string company);
        List<Properties> getProperties(string company);
        List<Country> getCountry(string company);
        List<TaxGroup> getTaxGroup(string company);
        List<State> getState(string company);
        List<Dimension> getDimension(string company);
        List<PriceList> getPriceList(string company);
        List<Brand> getBrand(string company);
        List<LocationAddress> getLocationAddress(string company);
        List<UomGroupConverter> getUomGroupConverter(string company);
        WebAdminSAPProfile getSapProfile(string company, WebAdminProfile obj);
    }
}
