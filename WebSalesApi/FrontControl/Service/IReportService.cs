﻿using CommonControl.POCO.Class;
using CommonControl.POCO.Common;
using CommonControl.POCO.Condition;
using FrontControl.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FrontControl.Service
{
    public interface IReportService : IBaseService
    {
        List<InventoryStatus> getReportInventoryStatus(ReportInventoryStatusCondition condition);
        List<InventoryStatusDocument> getReportInventoryStatusDocument(ReportInventoryStatusCondition condition);
        List<BatchNumberTransactionReport> getReportBatchNumberTransaction(ReportBatchNumberTransactionCondition condition);
        List<BatchNumberTransactionReportDetail> getReportBatchNumberTransactionDetail(ReportBatchNumberTransactionDetailCondition condition);
        List<CustomerPaymentReport> getReportCustomerPayment(ReportCustomerPaymentCondition condition);
        List<PurchaseOrderMovement> getReportPurchaseOrderMovement(ReportPurchaseOrderMovementCondition condition);
        List<SalesAnalysisReport> getReportSalesAnalysis(SalesAnalysisCondition condition);
    }
}
