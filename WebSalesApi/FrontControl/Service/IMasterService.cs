﻿using CommonControl.POCO.Class;
using CommonControl.POCO.Condition;
using FrontControl.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FrontControl.Service
{
    public interface IMasterService : IBaseService
    {
        List<ItemMaster> searchItemMasterByCondition(ref ItemMasterCondition condition);
        ItemMaster searchItemMasterByItemCode(ItemMasterCondition condition);
        List<ItemOnHand> searchItemOnHandByItemCode(ItemMasterCondition condition);
        List<BusinessPartner> searchBusinessPartnerByCondition(ref BusinessPartnerCondition condition);
        BusinessPartner searchBusinessPartnerByCardCode(BusinessPartnerCondition condition);
        List<BusinessPartnerAddress> searchBusinessPartnerAddressByCardCode(BusinessPartnerCondition condition);
        List<BusinessPartnerContact> searchBusinessPartnerContactByCardCode(BusinessPartnerCondition condition);
        void createBusinessPartner(BusinessPartner bp);
        void updateBusinessPartner(BusinessPartner bp);
        List<BusinessPartnerCriteria> searchBusinessPartnerCriteria(BusinessPartnerCondition condition);
        List<ItemMasterCriteria> searchItemMasterCriteria(ItemMasterCondition condition);
    }
}
