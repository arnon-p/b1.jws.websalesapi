﻿using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CommonControl.Enum
{
    [JsonConverter(typeof(StringEnumConverter))]
    public enum ServiceType
    {
        COMMON = 0,
        MASTER = 1,
        DOCUMENT = 2,
        REPORT = 3
    }
}
