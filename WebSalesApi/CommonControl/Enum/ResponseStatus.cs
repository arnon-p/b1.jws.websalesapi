﻿using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CommonControl.Enum
{
    [JsonConverter(typeof(StringEnumConverter))]
    public enum ResponseStatus
    {
        NONE,
        SUCCESS,    //Success
        UNSUCCESS,  //Unsuccess
        ERROR,      //System error
        INVALID,    //Validate not pass
        NOT_FOUND,  //Query result not found
        AUTH_NOTPASS,   //Authenticate not pass.
        AU_UN,  //Username not found.
        AU_WP,  //Wrong password.
        AU_ND,  //Null device id
        AU_NT,  //Token not found
        AU_NM,  //Token not match
        AU_WR,  //Service wrong role
        AU_CH,  //Invalid captcha
        RE_DU,   //This username already used.
        AU_PD,   //Username Permission Denied
        AU_UL,   //User Limit
        DUPLICATE,   //User Limit
        AU_NP   // Profile not found.
    }
}