﻿using B1Utility.Constants;

namespace CommonControl.Constants
{
    public class ServiceConstants : CommonConstants
    {
        public const string ERROR_MESSAGE_DETAIL = "{0} : {1}";
        public const string ERROR_MESSAGE_DETAIL_DATA = "{0} : {1} : {2}";
        public const string ERROR_MESSAGE = "{0}";
        public const string INTERNAL_ERROR = "INTERNAL_ERROR";
        public const string EXPORT_REPORT_DATETIME = "yyyyMMdd-HHmmss";
        public const string ONHAND_REPORT = "OnHandReport";
        public const string ONHAND_COST_REPORT = "OnHandCostReport";
        public const string SALES_BACK_ORDER_REPORT = "SalesBackOrderReport";
        public const string SALES_ORDER_REPORT = "SalesOrderReport";
        //public const string SEARCH_DELIVERY = "SEARCH_DELIVERY";
        public const string JOB = "JOB";
        //public const string DATE_FORMAT = "yyyy-MM";
        public const string STATUS_PENDING = "W";
        public const string STATUS_APPROVE = "Y";
        public const string STATUS_REJECT = "N";
        public const string EMAIL_SUBJECT_PENDING = "[{0} Approval] Sales Order Draft No. {1} created by web sales.";
        public const string EMAIL_BODY_PENDING = "<html><span>Dear : {0}</span><br /><br /><span>Please approve sales order draft no {1} as soon as posible.</span><br /><span>Web Sales Link : </span><a href={2}>Click !!</a><br /><br /><span>Best regards,</span><br /><span>System support</span></html>";
        public const string EMAIL_SUBJECT_APPROVE = "[{0} Approval] Sales Order No. {1} created by web sales.";
        public const string EMAIL_BODY_APPROVE = "<html><span>Dear : {0}</span><br /><br /><span>SO Draft Number {1} is Approved</span><br /><span>SO No. {2}</span><br /><span>Comment : {3}</span><br /><br /><span>Best regards,</span><br /><span>System support</span></html>";
        public const string EMAIL_SUBJECT_REJECT = "[{0} Approval] Sales Order Draft No. {1} rejected.";
        public const string EMAIL_BODY_REJECT = "<html><span>Dear : {0}</span><br /><br /><span>SO Draft Number {1} is Rejected</span><br /><span>Comment : {2}</span><br /><span>Web Sales Link : </span><a href={3}>Click !!</a><br /><br /><span>Best regards,</span><br /><span>System support</span></html>";
        public const string EMAIL_SUBJECT_CREATE = "[{0} Approval] Sales Order No. {1} created by web sales.";
        public const string EMAIL_BODY_CREATE = "<html><span>Dear : {0}</span><br /><br /><span>Sales Order Draft No. {1} has been approved and convert to Sales Order No. {2}.</span><br /><span>Web Sales Link : </span><a href={3}>Click !!</a><br /><br /><span>Best regards,</span><br /><span>System support</span></html>";
        public const string EMAIL_SUBJECT_ORIGINATOR_CREATE = "[{0} Approval] Sales Order Draft No. {1} created by web sales.";
        public const string EMAIL_BODY_ORIGINATOR_CREATE = "<html><span>Dear : {0}</span><br /><br /><span>Sales order draft no {1} created by web sales.</span><br /><span>This document is in the process of approval.</span><br /><span>Web Sales Link : </span><a href={2}>Click !!</a><br /><br /><span>Best regards,</span><br /><span>System support</span></html>";
        public const string EMAIL_SUBJECT_ERROR_CONVERT = "[{0} Approval] (Error) Sales Order Draft No. {1} convert failure.";
        public const string EMAIL_BODY_ERROR_CONVERT = "<html><span>Dear : {0}</span><br /><br /><span>{1}</span><br /><br /><span>Please contact system support as soon as posible.</span><br /><span>Web Sales Link : </span><a href={2}>Click !!</a><br /><br /><span>Best regards,</span><br /><span>System support</span></html>";
        public const string EMAIL_ADMIN = "wirote.p@winnergroup.co.th";
    }
}
