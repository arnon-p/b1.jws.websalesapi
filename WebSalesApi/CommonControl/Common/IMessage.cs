﻿using CommonControl.Common;
using CommonControl.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CommonControl.Common
{
    public class IMessage : IResponse
    {
        private Message response;

        public Message Response
        {
            get { return response; }
            set
            {
                response = value;

                if (!response.Success)
                {
                    ErrorMessage = MessageResponseUtils.getLastMessageError(response);
                }
            }
        }

    }
}
