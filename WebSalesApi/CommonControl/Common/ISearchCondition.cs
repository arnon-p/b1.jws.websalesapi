﻿using B1Utility.POCO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CommonControl.Common
{
    public class ISearchCondition : ICondition
    {
        private Paginable paginable;
        private List<int> userSalesEmployee;

        public Paginable Paginable { get => paginable; set => paginable = value; }
        public List<int> UserSalesEmployee { get => userSalesEmployee; set => userSalesEmployee = value; }
    }
}
