﻿using B1Utility.POCO;
using CommonControl.Enum;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CommonControl.Common
{
    public class IResponse
    {
        private ResponseStatus status;
        private string errorMessage;
        private int responseTime;
        private Paginable paginable;

        //public IResponse()
        //{
        //    responseTime = DateTime.Now;
        //}

        public ResponseStatus Status
        {
            get { return status; }
            set { status = value; }
        }

        public string ErrorMessage
        {
            get { return errorMessage; }
            set { errorMessage = value; }
        }

        public int ResponseTime
        {
            get { return responseTime; }
            set { responseTime = value; }
        }

        public Paginable Paginable { get => paginable; set => paginable = value; }
    }
}
