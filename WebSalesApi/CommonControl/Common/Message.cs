﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CommonControl.Common
{
    public class Message
    {
        private bool success;
        private int docEntry;
        private int docNum;
        private string refKey;
        private string errorMessage;
        private List<Message> childMessage;

        public bool Success
        {
            get { return success; }
            set { success = value; }
        }

        public int DocEntry
        {
            get { return docEntry; }
            set { docEntry = value; }
        }

        public int DocNum
        {
            get { return docNum; }
            set { docNum = value; }
        }

        public string RefKey
        {
            get { return refKey; }
            set { refKey = value; }
        }
        public string ErrorMessage
        {
            get { return errorMessage; }
            set { errorMessage = value; }
        }

        public List<Message> ChildMessage
        {
            get { return childMessage; }
            set { childMessage = value; }
        }
    }
}
