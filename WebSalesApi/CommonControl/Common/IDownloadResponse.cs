﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace CommonControl.Common
{
    public class IDownloadResponse : IResponse
    {
        //private Stream streamContent;
        private string fileName;

        //public Stream StreamContent { get => streamContent; set => streamContent = value; }
        public string FileName { get => fileName; set => fileName = value; }
    }
}
