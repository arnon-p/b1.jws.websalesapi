﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CommonControl.Common
{
    public class MessageResponse : ICommon
    {
        private bool success;
        private bool duplicate;
        private List<Message> msgList;

        public bool Success
        {
            get { return success; }
            set { success = value; }
        }

        public bool Duplicate
        {
            get { return duplicate; }
            set { duplicate = value; }
        }

        public List<Message> MsgList
        {
            get { return msgList; }
            set { msgList = value; }
        }
    }
}
