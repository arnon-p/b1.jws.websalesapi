﻿using CommonControl.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CommonControl.POCO.Condition
{
    public class ReportBatchNumberTransactionDetailCondition : ICondition
    {
        private string cardCodeFrom;
        private string cardCodeTo;
        private string itemCode;
        private string batchNum;
        private string whsCode;

        public string CardCodeFrom { get => cardCodeFrom; set => cardCodeFrom = value; }
        public string CardCodeTo { get => cardCodeTo; set => cardCodeTo = value; }
        public string ItemCode { get => itemCode; set => itemCode = value; }
        public string BatchNum { get => batchNum; set => batchNum = value; }
        public string WhsCode { get => whsCode; set => whsCode = value; }
    }
}
