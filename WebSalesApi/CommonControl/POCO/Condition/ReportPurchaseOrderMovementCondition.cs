﻿using CommonControl.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CommonControl.POCO.Condition
{
    public class ReportPurchaseOrderMovementCondition : ICondition
    {
        private string cardCodeFrom;
        private string cardCodeTo;
        private string itemCodeFrom;
        private string itemCodeTo;
        private DateTime etaDateFrom;
        private DateTime etaDateTo;
        private string docStatus;

        public string CardCodeFrom { get => cardCodeFrom; set => cardCodeFrom = value; }
        public string CardCodeTo { get => cardCodeTo; set => cardCodeTo = value; }
        public string ItemCodeFrom { get => itemCodeFrom; set => itemCodeFrom = value; }
        public string ItemCodeTo { get => itemCodeTo; set => itemCodeTo = value; }
        public DateTime EtaDateFrom { get => etaDateFrom; set => etaDateFrom = value; }
        public DateTime EtaDateTo { get => etaDateTo; set => etaDateTo = value; }
        public string DocStatus { get => docStatus; set => docStatus = value; }
    }
}
