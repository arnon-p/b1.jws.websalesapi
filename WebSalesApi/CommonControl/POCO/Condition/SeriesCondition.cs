﻿using CommonControl.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CommonControl.POCO.Condition
{
    public class SeriesCondition : ICondition
    {
        private DateTime docDate;

        public DateTime DocDate { get => docDate; set => docDate = value; }
    }
}
