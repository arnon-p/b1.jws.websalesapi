﻿using CommonControl.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CommonControl.POCO.Condition
{
    public class SalesOrderCondition : ISearchCondition
    {
        private bool isDraft;
        private int docKey;
        //private int draftKey;
        private int docEntry;
        private int docNum;
        private string cardCode;
        private string cardName;
        private DateTime docDateFrom;
        private DateTime docDateTo;
        private DateTime docDueDateFrom;
        private DateTime docDueDateTo;
        private List<int> slpCode;
        private string docStatus;

        // approve so
        private string code;

        public bool IsDraft { get => isDraft; set => isDraft = value; }
        public int DocKey { get => docKey; set => docKey = value; }
        //public int DraftKey { get => draftKey; set => draftKey = value; }
        public int DocEntry { get => docEntry; set => docEntry = value; }
        public int DocNum { get => docNum; set => docNum = value; }
        public string CardCode { get => cardCode; set => cardCode = value; }
        public string CardName { get => cardName; set => cardName = value; }
        public DateTime DocDateFrom { get => docDateFrom; set => docDateFrom = value; }
        public DateTime DocDateTo { get => docDateTo; set => docDateTo = value; }
        public DateTime DocDueDateFrom { get => docDueDateFrom; set => docDueDateFrom = value; }
        public DateTime DocDueDateTo { get => docDueDateTo; set => docDueDateTo = value; }
        public List<int> SlpCode { get => slpCode; set => slpCode = value; }
        public string DocStatus { get => docStatus; set => docStatus = value; }
        public string Code { get => code; set => code = value; }
    }
}
