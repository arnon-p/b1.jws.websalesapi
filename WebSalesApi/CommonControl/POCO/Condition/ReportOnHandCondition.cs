﻿using B1Utility.Enum;
using CommonControl.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CommonControl.POCO.Condition
{
    public class ReportOnHandCondition : ICondition
    {
        private CrystalDecisions.Shared.ExportFormatType exportType;
        private string reportName;
        private string itemCodeFrom;
        private string itemCodeTo;
        private List<string> brand;
        private List<string> channel;
        private List<string> whsCode;

        public CrystalDecisions.Shared.ExportFormatType ExportType { get => exportType; set => exportType = value; }
        public string ReportName { get => reportName; set => reportName = value; }
        public string ItemCodeFrom { get => itemCodeFrom; set => itemCodeFrom = value; }
        public string ItemCodeTo { get => itemCodeTo; set => itemCodeTo = value; }
        public List<string> Brand { get => brand; set => brand = value; }
        public List<string> Channel { get => channel; set => channel = value; }
        public List<string> WhsCode { get => whsCode; set => whsCode = value; }
    }
}
