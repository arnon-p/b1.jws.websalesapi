﻿using CommonControl.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CommonControl.POCO.Condition
{
    public class ItemMasterCondition : ISearchCondition
    {
        private string itemCode;
        private string itemName;
        private string cardCode;
        private string whsCode;
        private DateTime date;
        private int priceListNum;
        private int itemGroupCode;
        private string bpItemName;
        private int slpCode;

        public string ItemCode { get => itemCode; set => itemCode = value; }
        public string ItemName { get => itemName; set => itemName = value; }
        public string CardCode { get => cardCode; set => cardCode = value; }
        public string WhsCode { get => whsCode; set => whsCode = value; }
        public DateTime Date { get => date; set => date = value; }
        public int PriceListNum { get => priceListNum; set => priceListNum = value; }
        public int ItemGroupCode { get => itemGroupCode; set => itemGroupCode = value; }
        public string BpItemName { get => bpItemName; set => bpItemName = value; }
        public int SlpCode { get => slpCode; set => slpCode = value; }
    }
}
