﻿using CommonControl.Common;
using CrystalDecisions.Shared;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CommonControl.POCO.Condition
{
    public class ReportSalesBackorderCondition : ICondition
    {
        private CrystalDecisions.Shared.ExportFormatType exportType;
        private string reportName;
        private string cardCodeFrom;
        private string cardCodeTo;
        private DateTime docDueDateFrom;
        private DateTime docDueDateTo;
        private List<int> slpCode;
        private int slpCodeFrom;
        private int slpCodeTo;
        private string itemCodeFrom;
        private string itemCodeTo;

        public ExportFormatType ExportType { get => exportType; set => exportType = value; }
        public string ReportName { get => reportName; set => reportName = value; }
        public string CardCodeFrom { get => cardCodeFrom; set => cardCodeFrom = value; }
        public string CardCodeTo { get => cardCodeTo; set => cardCodeTo = value; }
        public DateTime DocDueDateFrom { get => docDueDateFrom; set => docDueDateFrom = value; }
        public DateTime DocDueDateTo { get => docDueDateTo; set => docDueDateTo = value; }
        public List<int> SlpCode { get => slpCode; set => slpCode = value; }
        public int SlpCodeFrom { get => slpCodeFrom; set => slpCodeFrom = value; }
        public int SlpCodeTo { get => slpCodeTo; set => slpCodeTo = value; }
        public string ItemCodeFrom { get => itemCodeFrom; set => itemCodeFrom = value; }
        public string ItemCodeTo { get => itemCodeTo; set => itemCodeTo = value; }
    }
}
