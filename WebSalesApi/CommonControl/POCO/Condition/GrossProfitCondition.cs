﻿using CommonControl.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CommonControl.POCO.Condition
{
    public class GrossProfitCondition : ICondition
    {
        private int docEntry;

        public int DocEntry { get => docEntry; set => docEntry = value; }
    }
}
