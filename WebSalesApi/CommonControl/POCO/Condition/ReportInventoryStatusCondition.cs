﻿using CommonControl.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CommonControl.POCO.Condition
{
    public class ReportInventoryStatusCondition : ICondition
    {
        private string itemCode;
        private string itemCodeFrom;
        private string itemCodeTo;
        private int itmsGrpCod;
        private List<string> whsCode;

        public string ItemCode { get => itemCode; set => itemCode = value; }
        public string ItemCodeFrom { get => itemCodeFrom; set => itemCodeFrom = value; }
        public string ItemCodeTo { get => itemCodeTo; set => itemCodeTo = value; }
        public int ItmsGrpCod { get => itmsGrpCod; set => itmsGrpCod = value; }
        public List<string> WhsCode { get => whsCode; set => whsCode = value; }
    }
}
