﻿using CommonControl.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CommonControl.POCO.Condition
{
    public class ReportBatchNumberTransactionCondition : ICondition
    {
        private string itemCodeFrom;
        private string itemCodeTo;
        private string cardCodeFrom;
        private string cardCodeTo;
        private DateTime docDateFrom;
        private DateTime docDateTo;
        private DateTime expDateFrom;
        private DateTime expDateTo;
        private int itmsGrpCod;
        private List<string> whsCode;

        public string ItemCodeFrom { get => itemCodeFrom; set => itemCodeFrom = value; }
        public string ItemCodeTo { get => itemCodeTo; set => itemCodeTo = value; }
        public string CardCodeFrom { get => cardCodeFrom; set => cardCodeFrom = value; }
        public string CardCodeTo { get => cardCodeTo; set => cardCodeTo = value; }
        public DateTime DocDateFrom { get => docDateFrom; set => docDateFrom = value; }
        public DateTime DocDateTo { get => docDateTo; set => docDateTo = value; }
        public DateTime ExpDateFrom { get => expDateFrom; set => expDateFrom = value; }
        public DateTime ExpDateTo { get => expDateTo; set => expDateTo = value; }
        public int ItmsGrpCod { get => itmsGrpCod; set => itmsGrpCod = value; }
        public List<string> WhsCode { get => whsCode; set => whsCode = value; }
    }
}
