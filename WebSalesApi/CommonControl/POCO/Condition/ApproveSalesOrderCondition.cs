﻿using CommonControl.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CommonControl.POCO.Condition
{
    public class ApproveSalesOrderCondition : ISearchCondition
    {
        private int docEntry;
        private DateTime createDateFrom;
        private DateTime createDateTo;
        private string cardCode;
        private string cardName;
        private List<int> slpCode;
        private string wddStatus;
        private int userId;
        private int empId;
        private string extEmpId;

        public int DocEntry { get => docEntry; set => docEntry = value; }
        public DateTime CreateDateFrom { get => createDateFrom; set => createDateFrom = value; }
        public DateTime CreateDateTo { get => createDateTo; set => createDateTo = value; }
        public string CardCode { get => cardCode; set => cardCode = value; }
        public string CardName { get => cardName; set => cardName = value; }
        public List<int> SlpCode { get => slpCode; set => slpCode = value; }
        public string WddStatus { get => wddStatus; set => wddStatus = value; }
        public int UserId { get => userId; set => userId = value; }
        public int EmpId { get => empId; set => empId = value; }
        public string ExtEmpId { get => extEmpId; set => extEmpId = value; }
    }
}
