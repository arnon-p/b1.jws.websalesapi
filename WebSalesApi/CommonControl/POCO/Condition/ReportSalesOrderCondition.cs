﻿using B1Utility.Enum;
using CommonControl.Common;
using CrystalDecisions.Shared;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CommonControl.POCO.Condition
{
    public class ReportSalesOrderCondition : ICondition
    {
        private CrystalDecisions.Shared.ExportFormatType exportType;
        private string reportName;
        private bool isDraft;
        private int docEntry;

        public ExportFormatType ExportType { get => exportType; set => exportType = value; }
        public string ReportName { get => reportName; set => reportName = value; }
        public bool IsDraft { get => isDraft; set => isDraft = value; }
        public int DocEntry { get => docEntry; set => docEntry = value; }
    }
}
