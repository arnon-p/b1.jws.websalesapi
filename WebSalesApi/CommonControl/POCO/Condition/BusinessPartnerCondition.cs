﻿using CommonControl.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CommonControl.POCO.Condition
{
    public class BusinessPartnerCondition : ISearchCondition
    {
        private string cardCode;
        private string cardName;
        private string cardType;
        private List<int> slpCode;

        public string CardCode { get => cardCode; set => cardCode = value; }
        public string CardName { get => cardName; set => cardName = value; }
        public string CardType { get => cardType; set => cardType = value; }
        public List<int> SlpCode { get => slpCode; set => slpCode = value; }
    }
}
