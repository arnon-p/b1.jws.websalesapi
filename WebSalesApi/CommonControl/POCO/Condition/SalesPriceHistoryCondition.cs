﻿using CommonControl.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CommonControl.POCO.Condition
{
    public class SalesPriceHistoryCondition : ISearchCondition
    {
        private string cardCode;
        private string itemCode;
        private bool isDraft;
        private int docEntry;

        public string CardCode { get => cardCode; set => cardCode = value; }
        public string ItemCode { get => itemCode; set => itemCode = value; }
        public bool IsDraft { get => isDraft; set => isDraft = value; }
        public int DocEntry { get => docEntry; set => docEntry = value; }
    }
}
