﻿using CommonControl.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CommonControl.POCO.Condition
{
    public class ReportOrderStatusCondition : ISearchCondition
    {
        private string cardCodeFrom;
        private string cardCodeTo;
        private string itemCodeFrom;
        private string itemCodeTo;
        private string numAtCard;
        private DateTime docDueDateFrom;
        private DateTime docDueDateTo;

        public string CardCodeFrom { get => cardCodeFrom; set => cardCodeFrom = value; }
        public string CardCodeTo { get => cardCodeTo; set => cardCodeTo = value; }
        public string ItemCodeFrom { get => itemCodeFrom; set => itemCodeFrom = value; }
        public string ItemCodeTo { get => itemCodeTo; set => itemCodeTo = value; }
        public string NumAtCard { get => numAtCard; set => numAtCard = value; }
        public DateTime DocDueDateFrom { get => docDueDateFrom; set => docDueDateFrom = value; }
        public DateTime DocDueDateTo { get => docDueDateTo; set => docDueDateTo = value; }
    }
}
