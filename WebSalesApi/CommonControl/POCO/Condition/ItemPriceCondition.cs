﻿using CommonControl.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CommonControl.POCO.Condition
{
    public class ItemPriceCondition : ICondition
    {
        private string cardCode;
        private string itemCode;
        private double quantity;
        private DateTime date;
        private int uomEntry;
        private int priceList;

        public string CardCode { get => cardCode; set => cardCode = value; }
        public string ItemCode { get => itemCode; set => itemCode = value; }
        public double Quantity { get => quantity; set => quantity = value; }
        public DateTime Date { get => date; set => date = value; }
        public int UomEntry { get => uomEntry; set => uomEntry = value; }
        public int PriceList { get => priceList; set => priceList = value; }
    }
}
