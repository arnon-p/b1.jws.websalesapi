﻿using CommonControl.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CommonControl.POCO.Condition
{
    public class ReportCustomerPaymentCondition : ISearchCondition
    {
        // Aging Report
        private string cardCodeFrom;
        private string cardCodeTo;
        private List<int> slpCode;
        //private int slpCodeFrom;
        //private int slpCodeTo;
        private DateTime docDateFrom;
        private DateTime docDateTo;
        private DateTime docDueDateFrom;
        private DateTime docDueDateTo;
        private string status;

        public string CardCodeFrom { get => cardCodeFrom; set => cardCodeFrom = value; }
        public string CardCodeTo { get => cardCodeTo; set => cardCodeTo = value; }
        public List<int> SlpCode { get => slpCode; set => slpCode = value; }
        //public int SlpCodeFrom { get => slpCodeFrom; set => slpCodeFrom = value; }
        //public int SlpCodeTo { get => slpCodeTo; set => slpCodeTo = value; }
        public DateTime DocDateFrom { get => docDateFrom; set => docDateFrom = value; }
        public DateTime DocDateTo { get => docDateTo; set => docDateTo = value; }
        public DateTime DocDueDateFrom { get => docDueDateFrom; set => docDueDateFrom = value; }
        public DateTime DocDueDateTo { get => docDueDateTo; set => docDueDateTo = value; }
        public string Status { get => status; set => status = value; }
    }
}
