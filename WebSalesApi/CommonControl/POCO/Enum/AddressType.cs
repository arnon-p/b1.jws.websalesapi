﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;

namespace CommonControl.POCO.Enum
{
    public enum AddressType
    {
        [Description("Bill To")]
        B = 0,
        [Description("Ship To")]
        S = 1
    }
}
