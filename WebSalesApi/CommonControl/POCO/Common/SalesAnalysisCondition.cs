﻿using CommonControl.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CommonControl.POCO.Common
{
    public class SalesAnalysisCondition : ISearchCondition
    {
        private List<int> slpCode;
        //private int slpCodeFrom;
        //private int slpCodeTo;
        private string cardCodeFrom;
        private string cardCodeTo;
        private DateTime docDateFrom;
        private DateTime docDateTo;

        public List<int> SlpCode { get => slpCode; set => slpCode = value; }
        //public int SlpCodeFrom { get => slpCodeFrom; set => slpCodeFrom = value; }
        //public int SlpCodeTo { get => slpCodeTo; set => slpCodeTo = value; }
        public string CardCodeFrom { get => cardCodeFrom; set => cardCodeFrom = value; }
        public string CardCodeTo { get => cardCodeTo; set => cardCodeTo = value; }
        public DateTime DocDateFrom { get => docDateFrom; set => docDateFrom = value; }
        public DateTime DocDateTo { get => docDateTo; set => docDateTo = value; }
        //private string itemCodeFrom;
        //private string itemCodeTo;
    }
}
