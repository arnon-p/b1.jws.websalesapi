﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CommonControl.POCO.Common
{
    public class AddressType
    {
        private string value;
        private string name;

        public AddressType()
        {
        }

        public AddressType(string v, string n)
        {
            Value = v;
            Name = n;
        }

        public string Value { get => value; set => this.value = value; }
        public string Name { get => name; set => name = value; }
    }
}
