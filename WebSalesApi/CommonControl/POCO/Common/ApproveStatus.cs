﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CommonControl.POCO.Common
{
    public class ApproveStatus
    {
        private string code;
        private string name;

        public ApproveStatus()
        {
        }

        public ApproveStatus(string code, string name)
        {
            this.code = code;
            this.name = name;
        }
        public string Code { get => code; set => code = value; }
        public string Name { get => name; set => name = value; }
    }
}
