﻿using CommonControl.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CommonControl.POCO.Common
{
    public class PaymentTerms
    {
        private int groupNum;
        private string pymntGroup;
        private int extraDays;

        public int GroupNum { get => groupNum; set => groupNum = value; }
        public string PymntGroup { get => pymntGroup; set => pymntGroup = value; }
        public int ExtraDays { get => extraDays; set => extraDays = value; }
    }
}
