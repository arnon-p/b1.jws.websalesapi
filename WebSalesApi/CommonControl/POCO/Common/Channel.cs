﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CommonControl.POCO.Common
{
    public class Channel
    {
        private string code;
        private string name;

        public Channel()
        {
        }

        public Channel(string code, string name)
        {
            this.Code = code;
            this.Name = name;
        }

        public string Code { get => code; set => code = value; }
        public string Name { get => name; set => name = value; }
    }
}
