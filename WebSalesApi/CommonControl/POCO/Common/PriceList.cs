﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CommonControl.POCO.Common
{
    public class PriceList
    {
        private int listNum;
        private string listName;
        private bool isGrossPrc;

        public int ListNum { get => listNum; set => listNum = value; }
        public string ListName { get => listName; set => listName = value; }
        public bool IsGrossPrc { get => isGrossPrc; set => isGrossPrc = value; }
    }
}
