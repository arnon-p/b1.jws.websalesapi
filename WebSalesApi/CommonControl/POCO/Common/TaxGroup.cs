﻿using CommonControl.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CommonControl.POCO.Common
{
    public class TaxGroup
    {
        private string code;
        private string name;
        private double rate;

        public string Code { get => code; set => code = value; }
        public string Name { get => name; set => name = value; }
        public double Rate { get => rate; set => rate = value; }
    }
}
