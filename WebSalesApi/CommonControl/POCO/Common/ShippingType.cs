﻿using CommonControl.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CommonControl.POCO.Common
{
    public class ShippingType
    {
        private int trnspCode;
        private string trnspName;

        public int TrnspCode { get => trnspCode; set => trnspCode = value; }
        public string TrnspName { get => trnspName; set => trnspName = value; }
    }
}
