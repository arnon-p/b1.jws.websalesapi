﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CommonControl.POCO.Common
{
    public class EmailServerInfo
    {
        private string smtpHost;
        private int smtpPort;
        private string emailUser;
        private string emailPass;

        public EmailServerInfo()
        {
        }

        public EmailServerInfo(string pHost, int pPort, string pEmailUser, string pEmailPass)
        {
            this.smtpHost = pHost;
            this.smtpPort = pPort;
            this.emailUser = pEmailUser;
            this.emailPass = pEmailPass;
        }

        public string SmtpHost { get => smtpHost; set => smtpHost = value; }
        public int SmtpPort { get => smtpPort; set => smtpPort = value; }
        public string EmailUser { get => emailUser; set => emailUser = value; }
        public string EmailPass { get => emailPass; set => emailPass = value; }
    }
}
