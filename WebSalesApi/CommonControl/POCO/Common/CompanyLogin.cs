﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CommonControl.POCO.Common
{
    public class CompanyLogin
    {
        private string code;
        private string name;

        public CompanyLogin()
        {
        }

        public CompanyLogin(string code, string name)
        {
            this.code = code;
            this.name = name;
        }

        public string Code { get => code; set => code = value; }
        public string Name { get => name; set => name = value; }
    }
}
