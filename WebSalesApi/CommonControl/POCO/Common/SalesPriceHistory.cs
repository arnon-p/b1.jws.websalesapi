﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CommonControl.POCO.Common
{
    public class SalesPriceHistory
    {
        private int docEntry;
        private int docNum;
        private DateTime docDate;
        private DateTime docDueDate;
        private double quantity;
        private double unitPrice;
        private double discountPercent;
        private double priceAfterDiscount;
        private string uomCode;

        public int DocEntry { get => docEntry; set => docEntry = value; }
        public int DocNum { get => docNum; set => docNum = value; }
        public DateTime DocDate { get => docDate; set => docDate = value; }
        public DateTime DocDueDate { get => docDueDate; set => docDueDate = value; }
        public double Quantity { get => quantity; set => quantity = value; }
        public double UnitPrice { get => unitPrice; set => unitPrice = value; }
        public double DiscountPercent { get => discountPercent; set => discountPercent = value; }
        public double PriceAfterDiscount { get => priceAfterDiscount; set => priceAfterDiscount = value; }
        public string UomCode { get => uomCode; set => uomCode = value; }
    }
}
