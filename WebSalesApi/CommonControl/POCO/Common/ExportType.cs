﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CommonControl.POCO.Common
{
    public class ExportType
    {
        private int code;
        private string name;

        public ExportType()
        {
        }

        public ExportType(int code, string name)
        {
            this.code = code;
            this.name = name;
        }

        public int Code { get => code; set => code = value; }
        public string Name { get => name; set => name = value; }
    }
}
