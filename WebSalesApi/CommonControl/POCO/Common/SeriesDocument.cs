﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CommonControl.POCO.Common
{
    public class SeriesDocument
    {
        private int series;
        private string seriesName;

        public int Series { get => series; set => series = value; }
        public string SeriesName { get => seriesName; set => seriesName = value; }
    }
}
