﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CommonControl.POCO.Common
{
    public class EmailMessageInfo
    {
        public string Subject { get; set; }
        public List<string> Mailto { get; set; }
        public string Email { get; set; }
        public string Body { get; set; }
    }
}
