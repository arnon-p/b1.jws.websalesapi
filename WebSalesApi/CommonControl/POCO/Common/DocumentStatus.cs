﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CommonControl.POCO.Common
{
    public class DocumentStatus
    {
        private string value;
        private string name;

        public DocumentStatus()
        {
        }

        public DocumentStatus(string v, string n)
        {
            Value = v;
            Name = n;
        }

        public string Value { get => value; set => this.value = value; }
        public string Name { get => name; set => name = value; }
    }
}
