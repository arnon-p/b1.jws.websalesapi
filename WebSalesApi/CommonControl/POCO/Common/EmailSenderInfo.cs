﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CommonControl.POCO.Common
{
    public class EmailSenderInfo
    {
        public string Email { get; set; }
        public string FullName { get; set; }
        public string EmailCenter { get; set; }
        public string EmailSupport { get; set; }
    }
}
