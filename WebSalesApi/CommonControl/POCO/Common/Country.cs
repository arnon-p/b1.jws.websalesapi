﻿using CommonControl.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CommonControl.POCO.Common
{
    public class Country
    {
        private string code;
        private string name;

        public string Code { get => code; set => code = value; }
        public string Name { get => name; set => name = value; }
    }
}
