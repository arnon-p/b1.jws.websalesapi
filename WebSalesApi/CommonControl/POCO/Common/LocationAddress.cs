﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CommonControl.POCO.Common
{
    public class LocationAddress
    {
        private string tambon;
        private string amphur;
        private string city;
        private string zipCode;
        private string fullAddr;

        public string Tambon { get => tambon; set => tambon = value; }
        public string Amphur { get => amphur; set => amphur = value; }
        public string City { get => city; set => city = value; }
        public string ZipCode { get => zipCode; set => zipCode = value; }
        public string FullAddr { get => fullAddr; set => fullAddr = value; }
    }
}
