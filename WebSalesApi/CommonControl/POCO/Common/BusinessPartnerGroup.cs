﻿using CommonControl.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CommonControl.POCO.Common
{
    public class BusinessPartnerGroup
    {
        private int groupCode;
        private string groupName;

        public int GroupCode { get => groupCode; set => groupCode = value; }
        public string GroupName { get => groupName; set => groupName = value; }
    }
}
