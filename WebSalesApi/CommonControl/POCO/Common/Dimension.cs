﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CommonControl.POCO.Common
{
    public class Dimension
    {
        private string dimCode;
        private string ocrCode;
        private string ocrName;

        public string DimCode { get => dimCode; set => dimCode = value; }
        public string OcrCode { get => ocrCode; set => ocrCode = value; }
        public string OcrName { get => ocrName; set => ocrName = value; }
    }
}
