﻿using CommonControl.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CommonControl.POCO.Common
{
    public class ItemGroup
    {
        private int itmsGrpCod;
        private string itmsGrpNam;

        public int ItmsGrpCod { get => itmsGrpCod; set => itmsGrpCod = value; }
        public string ItmsGrpNam { get => itmsGrpNam; set => itmsGrpNam = value; }
    }
}
