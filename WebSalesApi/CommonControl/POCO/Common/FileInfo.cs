﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CommonControl.POCO.Common
{
    public class FileInfo
    {
        private DateTime date;
        private string fileName;

        public FileInfo()
        {
        }

        public FileInfo(DateTime date, string fileName)
        {
            this.date = date;
            this.fileName = fileName;
        }

        public DateTime Date { get => date; set => date = value; }
        public string FileName { get => fileName; set => fileName = value; }
    }
}
