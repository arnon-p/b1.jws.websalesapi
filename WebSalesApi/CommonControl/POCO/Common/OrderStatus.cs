﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CommonControl.POCO.Common
{
    public class OrderStatus
    {
        // Sales Order
        private int sODocEntry;
        private int sODocNum;
        private string cardCode;
        private string cardName;
        private DateTime sODocDueDate;
        private string numAtCard;
        private string itemCode;
        private string itemName;
        private double quantity;
        private int uomEntry;
        private string uomCode;
        private string uomName;
        private string whsCode;
        private string sOEmpName;
        private DateTime sOCreateDate;
        private string sOCreateTime;

        // Delivery
        private int dOBaseEntry;
        private int dODocEntry;
        private int dODocNum;
        private DateTime dODocDate;
        private int dOUserId;
        private string dOUsername;
        private string dOEmpName;
        private DateTime dOCreateDate;
        private string dOCreateTime;

        // Invoice
        private int iNVBaseEntry;
        private int iNVDocEntry;
        private int iNVDocNum;
        private DateTime iNVDocDate;
        private int iNVUserId;
        private string iNVUsername;
        private string iNVEmpName;
        private DateTime iNVCreateDate;
        private string iNVCreateTime;

        // Incoming
        private int rCTBaseEntry;
        private int rCTDocEntry;
        private int rCTDocNum;
        private DateTime rCTDocDate;
        private int rCTUserId;
        private string rCTUsername;
        private string rCTEmpName;
        private DateTime rCTCreateDate;
        private string rCTCreateTime;

        public int SODocEntry { get => sODocEntry; set => sODocEntry = value; }
        public int SODocNum { get => sODocNum; set => sODocNum = value; }
        public string CardCode { get => cardCode; set => cardCode = value; }
        public string CardName { get => cardName; set => cardName = value; }
        public DateTime SODocDueDate { get => sODocDueDate; set => sODocDueDate = value; }
        public string NumAtCard { get => numAtCard; set => numAtCard = value; }
        public string ItemCode { get => itemCode; set => itemCode = value; }
        public string ItemName { get => itemName; set => itemName = value; }
        public double Quantity { get => quantity; set => quantity = value; }
        public int UomEntry { get => uomEntry; set => uomEntry = value; }
        public string UomCode { get => uomCode; set => uomCode = value; }
        public string UomName { get => uomName; set => uomName = value; }
        public string WhsCode { get => whsCode; set => whsCode = value; }
        public string SOEmpName { get => sOEmpName; set => sOEmpName = value; }
        public DateTime SOCreateDate { get => sOCreateDate; set => sOCreateDate = value; }
        public string SOCreateTime { get => sOCreateTime; set => sOCreateTime = value; }
        public int DOBaseEntry { get => dOBaseEntry; set => dOBaseEntry = value; }
        public int DODocEntry { get => dODocEntry; set => dODocEntry = value; }
        public int DODocNum { get => dODocNum; set => dODocNum = value; }
        public DateTime DODocDate { get => dODocDate; set => dODocDate = value; }
        public int DOUserId { get => dOUserId; set => dOUserId = value; }
        public string DOUsername { get => dOUsername; set => dOUsername = value; }
        public string DOEmpName { get => dOEmpName; set => dOEmpName = value; }
        public DateTime DOCreateDate { get => dOCreateDate; set => dOCreateDate = value; }
        public string DOCreateTime { get => dOCreateTime; set => dOCreateTime = value; }
        public int INVBaseEntry { get => iNVBaseEntry; set => iNVBaseEntry = value; }
        public int INVDocEntry { get => iNVDocEntry; set => iNVDocEntry = value; }
        public int INVDocNum { get => iNVDocNum; set => iNVDocNum = value; }
        public DateTime INVDocDate { get => iNVDocDate; set => iNVDocDate = value; }
        public int INVUserId { get => iNVUserId; set => iNVUserId = value; }
        public string INVUsername { get => iNVUsername; set => iNVUsername = value; }
        public string INVEmpName { get => iNVEmpName; set => iNVEmpName = value; }
        public DateTime INVCreateDate { get => iNVCreateDate; set => iNVCreateDate = value; }
        public string INVCreateTime { get => iNVCreateTime; set => iNVCreateTime = value; }
        public int RCTBaseEntry { get => rCTBaseEntry; set => rCTBaseEntry = value; }
        public int RCTDocEntry { get => rCTDocEntry; set => rCTDocEntry = value; }
        public int RCTDocNum { get => rCTDocNum; set => rCTDocNum = value; }
        public DateTime RCTDocDate { get => rCTDocDate; set => rCTDocDate = value; }
        public int RCTUserId { get => rCTUserId; set => rCTUserId = value; }
        public string RCTUsername { get => rCTUsername; set => rCTUsername = value; }
        public string RCTEmpName { get => rCTEmpName; set => rCTEmpName = value; }
        public DateTime RCTCreateDate { get => rCTCreateDate; set => rCTCreateDate = value; }
        public string RCTCreateTime { get => rCTCreateTime; set => rCTCreateTime = value; }
    }
}
