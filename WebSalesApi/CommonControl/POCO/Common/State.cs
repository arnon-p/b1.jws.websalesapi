﻿using CommonControl.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CommonControl.POCO.Common
{
    public class State
    {
        private string code;
        private string name;
        private string country;

        public string Code { get => code; set => code = value; }
        public string Name { get => name; set => name = value; }
        public string Country { get => country; set => country = value; }
    }
}
