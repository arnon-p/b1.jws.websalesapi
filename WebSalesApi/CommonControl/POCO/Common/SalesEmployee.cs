﻿using CommonControl.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CommonControl.POCO.Common
{
    public class SalesEmployee
    {
        private int slpCode;
        private string slpName;

        public SalesEmployee()
        {
        }

        public SalesEmployee(int slpCode, string slpName)
        {
            this.slpCode = slpCode;
            this.slpName = slpName;
        }

        public int SlpCode { get => slpCode; set => slpCode = value; }
        public string SlpName { get => slpName; set => slpName = value; }
    }
}
