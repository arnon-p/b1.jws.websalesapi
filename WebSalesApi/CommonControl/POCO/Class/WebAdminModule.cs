﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CommonControl.POCO.Class
{
    public class WebAdminModule
    {
        private string moduleId;
        private string moduleCode;
        private string moduleName;
        private string status;
        private List<string> userList;

        public string ModuleId { get => moduleId; set => moduleId = value; }
        public string ModuleCode { get => moduleCode; set => moduleCode = value; }
        public string ModuleName { get => moduleName; set => moduleName = value; }
        public string Status { get => status; set => status = value; }
        public List<string> UserList { get => userList; set => userList = value; }
    }
}
