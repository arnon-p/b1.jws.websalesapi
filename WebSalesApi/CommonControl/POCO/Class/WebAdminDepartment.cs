﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CommonControl.POCO.Class
{
    public class WebAdminDepartment
    {
        private string departmentId;
        private string departmentName;
        private string status;

        public string DepartmentId { get => departmentId; set => departmentId = value; }
        public string DepartmentName { get => departmentName; set => departmentName = value; }
        public string Status { get => status; set => status = value; }
    }
}
