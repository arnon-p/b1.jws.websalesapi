﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CommonControl.POCO.Class
{
    public class SalesAnalysisMonth
    {
        private int month;
        private int year;
        private string monthYear;
        private double quantity;
        private double total;
        
        public SalesAnalysisMonth() { }
        public SalesAnalysisMonth(int month, int year, string monthYear, double quantity, double total)
        {
            this.month = month;
            this.year = year;
            this.monthYear = monthYear;
            this.quantity = quantity;
            this.total = total;
        }

        public int Month { get => month; set => month = value; }
        public int Year { get => year; set => year = value; }
        public string MonthYear { get => monthYear; set => monthYear = value; }
        public double Quantity { get => quantity; set => quantity = value; }
        public double Total { get => total; set => total = value; }
    }
}
