﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CommonControl.POCO.Class
{
    public class ItemMasterForSO
    {
        private string itemCode;
        private string itemName;
        private string bpItemName;
        private int ugpEntry;
        private string codeBars;
        private double unitPrice;
        private double discountPercent;
        private double price;
        private int uomEntry;
        private string uomCode;
        private string uomName;
        private string taxGroup;
        private double taxRate;
        private double inStockQty;
        private double avaliableQty;
        private int inUomEntry;
        private string inUomCode;
        private string inUomName;
        private bool isBatch;
        private bool isSpecialPrice;
        private string whsCode;
        private string costingCode;
        private List<BarCode> barList;
        private List<UomGroupConverter> uomConvertList;

        public string ItemCode { get => itemCode; set => itemCode = value; }
        public string ItemName { get => itemName; set => itemName = value; }
        public string BpItemName { get => bpItemName; set => bpItemName = value; }
        public int UgpEntry { get => ugpEntry; set => ugpEntry = value; }
        public string CodeBars { get => codeBars; set => codeBars = value; }
        public double UnitPrice { get => unitPrice; set => unitPrice = value; }
        public double DiscountPercent { get => discountPercent; set => discountPercent = value; }
        public double Price { get => price; set => price = value; }
        public int UomEntry { get => uomEntry; set => uomEntry = value; }
        public string UomCode { get => uomCode; set => uomCode = value; }
        public string UomName { get => uomName; set => uomName = value; }
        public string TaxGroup { get => taxGroup; set => taxGroup = value; }
        public double TaxRate { get => taxRate; set => taxRate = value; }
        public double InStockQty { get => inStockQty; set => inStockQty = value; }
        public double AvaliableQty { get => avaliableQty; set => avaliableQty = value; }
        public int InUomEntry { get => inUomEntry; set => inUomEntry = value; }
        public string InUomCode { get => inUomCode; set => inUomCode = value; }
        public string InUomName { get => inUomName; set => inUomName = value; }
        public bool IsBatch { get => isBatch; set => isBatch = value; }
        public bool IsSpecialPrice { get => isSpecialPrice; set => isSpecialPrice = value; }
        public string WhsCode { get => whsCode; set => whsCode = value; }
        public string CostingCode { get => costingCode; set => costingCode = value; }
        public List<BarCode> BarList { get => barList; set => barList = value; }
        public List<UomGroupConverter> UomConvertList { get => uomConvertList; set => uomConvertList = value; }
    }
}
