﻿using CommonControl.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CommonControl.POCO.Class
{
    public class BarCode
    {
        private int bcdEntry;
        private string bcdCode;
        private string bcdName;
        private int uomEntry;

        public int BcdEntry { get => bcdEntry; set => bcdEntry = value; }
        public string BcdCode { get => bcdCode; set => bcdCode = value; }
        public string BcdName { get => bcdName; set => bcdName = value; }
        public int UomEntry { get => uomEntry; set => uomEntry = value; }
    }
}
