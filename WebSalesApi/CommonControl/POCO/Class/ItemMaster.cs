﻿using CommonControl.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CommonControl.POCO.Class
{
    public class ItemMaster
    {
        private string itemCode;
        private string itemName;
        private string frgnName;
        private string itemType;
        private int itemGroupCode;
        private string itemGroupName;
        private int uomGroupEntry;
        private string uomGroupCode;
        private string uomGroupName;
        // sales tab
        private string vatGourpSa;
        private int sUomEntry;
        private string sUomCode;
        private string sUomName;
        private double numInSale;
        private string salPackMsr;
        private double salPackUn;
        // inventory tab
        private string gLMethod;
        private int iUomEntry;
        private string iUomCode;
        private string iUomName;
        private double iWeight;
        private int cUomEntry;
        private string cUomCode;
        private string cUomName;
        private double numInCnt;
        private string evalSystem;
        private List<ItemOnHand> onHandList;

        // UDF
        private string u_WGE_OItem;
        private int u_WGE_ShelfLife;
        private string u_WGE_Brand;
        private string u_WGE_Channel;
        private string u_WGE_SubCat;
        private string u_WGE_PackagingSize;
        private double u_WGE_StdYield;
        private double u_WGE_StdManHour;
        private string u_WGE_Package;
        private string u_WGE_Properties;
        private double u_WGE_InvPacking;
        private string u_WGE_Rawmat;
        private string u_WGE_Product;
        private string u_WGE_ProductG;
        private string u_WGE_BPCode;
        private string u_WGE_BPName;

        public string ItemCode { get => itemCode; set => itemCode = value; }
        public string ItemName { get => itemName; set => itemName = value; }
        public string FrgnName { get => frgnName; set => frgnName = value; }
        public string ItemType { get => itemType; set => itemType = value; }
        public int ItemGroupCode { get => itemGroupCode; set => itemGroupCode = value; }
        public string ItemGroupName { get => itemGroupName; set => itemGroupName = value; }
        public int UomGroupEntry { get => uomGroupEntry; set => uomGroupEntry = value; }
        public string UomGroupCode { get => uomGroupCode; set => uomGroupCode = value; }
        public string UomGroupName { get => uomGroupName; set => uomGroupName = value; }
        // sales tab
        public string VatGourpSa { get => vatGourpSa; set => vatGourpSa = value; }
        public int SUomEntry { get => sUomEntry; set => sUomEntry = value; }
        public string SUomCode { get => sUomCode; set => sUomCode = value; }
        public string SUomName { get => sUomName; set => sUomName = value; }
        public double NumInSale { get => numInSale; set => numInSale = value; }
        public string SalPackMsr { get => salPackMsr; set => salPackMsr = value; }
        public double SalPackUn { get => salPackUn; set => salPackUn = value; }
        // inventory tab
        public string GLMethod { get => gLMethod; set => gLMethod = value; }
        public int IUomEntry { get => iUomEntry; set => iUomEntry = value; }
        public string IUomCode { get => iUomCode; set => iUomCode = value; }
        public string IUomName { get => iUomName; set => iUomName = value; }
        public double IWeight { get => iWeight; set => iWeight = value; }
        public int CUomEntry { get => cUomEntry; set => cUomEntry = value; }
        public string CUomCode { get => cUomCode; set => cUomCode = value; }
        public string CUomName { get => cUomName; set => cUomName = value; }
        public double NumInCnt { get => numInCnt; set => numInCnt = value; }
        public string EvalSystem { get => evalSystem; set => evalSystem = value; }
        public List<ItemOnHand> OnHandList { get => onHandList; set => onHandList = value; }
        // UDF
        public string U_WGE_OItem { get => u_WGE_OItem; set => u_WGE_OItem = value; }
        public int U_WGE_ShelfLife { get => u_WGE_ShelfLife; set => u_WGE_ShelfLife = value; }
        public string U_WGE_Brand { get => u_WGE_Brand; set => u_WGE_Brand = value; }
        public string U_WGE_Channel { get => u_WGE_Channel; set => u_WGE_Channel = value; }
        public string U_WGE_SubCat { get => u_WGE_SubCat; set => u_WGE_SubCat = value; }
        public string U_WGE_PackagingSize { get => u_WGE_PackagingSize; set => u_WGE_PackagingSize = value; }
        public double U_WGE_StdYield { get => u_WGE_StdYield; set => u_WGE_StdYield = value; }
        public double U_WGE_StdManHour { get => u_WGE_StdManHour; set => u_WGE_StdManHour = value; }
        public string U_WGE_Package { get => u_WGE_Package; set => u_WGE_Package = value; }
        public string U_WGE_Properties { get => u_WGE_Properties; set => u_WGE_Properties = value; }
        public double U_WGE_InvPacking { get => u_WGE_InvPacking; set => u_WGE_InvPacking = value; }
        public string U_WGE_Rawmat { get => u_WGE_Rawmat; set => u_WGE_Rawmat = value; }
        public string U_WGE_Product { get => u_WGE_Product; set => u_WGE_Product = value; }
        public string U_WGE_ProductG { get => u_WGE_ProductG; set => u_WGE_ProductG = value; }
        public string U_WGE_BPCode { get => u_WGE_BPCode; set => u_WGE_BPCode = value; }
        public string U_WGE_BPName { get => u_WGE_BPName; set => u_WGE_BPName = value; }
    }
}
