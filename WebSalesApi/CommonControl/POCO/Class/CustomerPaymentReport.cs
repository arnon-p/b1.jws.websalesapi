﻿using CommonControl.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CommonControl.POCO.Class
{
    public class CustomerPaymentReport
    {
        private string cardCode;
        private string cardName;
        private string paymentTerm;
        private double creditLimit;
        private double creditBalance;
        private double avalBalance;
        private string docEntry;
        private string docNum;
        private DateTime docDate;
        private DateTime docDueDate;
        private string numAtCard;
        private DateTime incomingDate;
        private int slpCode;
        private string slpName;
        private double docTotal;
        private double paidTotal;
        private double balanceDue;
        private string status;
        private string paymentMethod;
        private int days;

        public string CardCode { get => cardCode; set => cardCode = value; }
        public string CardName { get => cardName; set => cardName = value; }
        public string PaymentTerm { get => paymentTerm; set => paymentTerm = value; }
        public double CreditLimit { get => creditLimit; set => creditLimit = value; }
        public double CreditBalance { get => creditBalance; set => creditBalance = value; }
        public double AvalBalance { get => avalBalance; set => avalBalance = value; }
        public string DocEntry { get => docEntry; set => docEntry = value; }
        public string DocNum { get => docNum; set => docNum = value; }
        public DateTime DocDate { get => docDate; set => docDate = value; }
        public DateTime DocDueDate { get => docDueDate; set => docDueDate = value; }
        public string NumAtCard { get => numAtCard; set => numAtCard = value; }
        public DateTime IncomingDate { get => incomingDate; set => incomingDate = value; }
        public int SlpCode { get => slpCode; set => slpCode = value; }
        public string SlpName { get => slpName; set => slpName = value; }
        public double DocTotal { get => docTotal; set => docTotal = value; }
        public double PaidTotal { get => paidTotal; set => paidTotal = value; }
        public double BalanceDue { get => balanceDue; set => balanceDue = value; }
        public string Status { get => status; set => status = value; }
        public string PaymentMethod { get => paymentMethod; set => paymentMethod = value; }
        public int Days { get => days; set => days = value; }
    }
}
