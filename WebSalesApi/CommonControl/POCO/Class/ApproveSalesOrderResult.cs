﻿using CommonControl.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CommonControl.POCO.Class
{
    public class ApproveSalesOrderResult : ICommon
    {
        public int DocEntry { get; set; }
        public string WddStatus { get; set; }
        public string Remark { get; set; }

        public int UserId { get; set; }
        public int EmpId { get; set; }
        public string ExtEmpId { get; set; }
        public int ParentKey { get; set; }
        public int Code { get; set; }
    }
}
