﻿using CommonControl.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CommonControl.POCO.Class
{
    public class BatchNumberTransactionReport
    {
        private int absEntry;
        private string whsCode;
        private string itemCode;
        private int sysNumber;
        private string distNumber;
        private string itemName;
        private string mnfSerial;
        private string lotNumber;
        private DateTime expDate;
        private string status;
        private string location;
        private double quantity;
        private double commitQty;
        private double countQty;
        private string cCDNum;
        private string itemCCDNum;
        private DateTime mnfDate;
        private DateTime inDate;
        private string cardCode;
        private string cardName;
        private string notes;

        public int AbsEntry { get => absEntry; set => absEntry = value; }
        public string WhsCode { get => whsCode; set => whsCode = value; }
        public string ItemCode { get => itemCode; set => itemCode = value; }
        public int SysNumber { get => sysNumber; set => sysNumber = value; }
        public string DistNumber { get => distNumber; set => distNumber = value; }
        public string ItemName { get => itemName; set => itemName = value; }
        public string MnfSerial { get => mnfSerial; set => mnfSerial = value; }
        public string LotNumber { get => lotNumber; set => lotNumber = value; }
        public DateTime ExpDate { get => expDate; set => expDate = value; }
        public string Status { get => status; set => status = value; }
        public string Location { get => location; set => location = value; }
        public double Quantity { get => quantity; set => quantity = value; }
        public double CommitQty { get => commitQty; set => commitQty = value; }
        public double CountQty { get => countQty; set => countQty = value; }
        public string CCDNum { get => cCDNum; set => cCDNum = value; }
        public string ItemCCDNum { get => itemCCDNum; set => itemCCDNum = value; }
        public DateTime MnfDate { get => mnfDate; set => mnfDate = value; }
        public DateTime InDate { get => inDate; set => inDate = value; }
        public string CardCode { get => cardCode; set => cardCode = value; }
        public string CardName { get => cardName; set => cardName = value; }
        public string Notes { get => notes; set => notes = value; }
    }
}
