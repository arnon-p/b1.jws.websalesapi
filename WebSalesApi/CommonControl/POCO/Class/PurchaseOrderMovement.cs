﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CommonControl.POCO.Class
{
    public class PurchaseOrderMovement
    {
        private int docEntry;
        private int docNum;
        private DateTime docDate;
        private DateTime docDueDate;
        private int slpCode;
        private string slpName;
        private string cardCode;
        private string cardName;
        private string itemCode;
        private string itemName;
        private string ocrCode;
        private double quantity;
        private string uomCode;
        private double openQty;
        private string currency;    //cost
        private double price;       //cost
        private string u_WGE_P08;   //cost
        private string u_WGE_S07;
        private DateTime u_WGE_P04;
        private DateTime u_WGE_P05;
        private DateTime u_WGE_P10;

        public int DocEntry { get => docEntry; set => docEntry = value; }
        public int DocNum { get => docNum; set => docNum = value; }
        public DateTime DocDate { get => docDate; set => docDate = value; }
        public DateTime DocDueDate { get => docDueDate; set => docDueDate = value; }
        public int SlpCode { get => slpCode; set => slpCode = value; }
        public string SlpName { get => slpName; set => slpName = value; }
        public string CardCode { get => cardCode; set => cardCode = value; }
        public string CardName { get => cardName; set => cardName = value; }
        public string ItemCode { get => itemCode; set => itemCode = value; }
        public string ItemName { get => itemName; set => itemName = value; }
        public string OcrCode { get => ocrCode; set => ocrCode = value; }
        public double Quantity { get => quantity; set => quantity = value; }
        public string UomCode { get => uomCode; set => uomCode = value; }
        public double OpenQty { get => openQty; set => openQty = value; }
        public string Currency { get => currency; set => currency = value; }
        public double Price { get => price; set => price = value; }
        public string U_WGE_P08 { get => u_WGE_P08; set => u_WGE_P08 = value; }
        public string U_WGE_S07 { get => u_WGE_S07; set => u_WGE_S07 = value; }
        public DateTime U_WGE_P04 { get => u_WGE_P04; set => u_WGE_P04 = value; }
        public DateTime U_WGE_P05 { get => u_WGE_P05; set => u_WGE_P05 = value; }
        public DateTime U_WGE_P10 { get => u_WGE_P10; set => u_WGE_P10 = value; }
    }
}
