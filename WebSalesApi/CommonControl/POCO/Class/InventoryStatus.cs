﻿using CommonControl.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CommonControl.POCO.Class
{
    public class InventoryStatus
    {
        private string itemCode;
        private string itemName;
        private double onHand;
        private double isCommited;
        private double onOrder;
        private double avaliable;
        private string invntryUom;

        public string ItemCode { get => itemCode; set => itemCode = value; }
        public string ItemName { get => itemName; set => itemName = value; }
        public double OnHand { get => onHand; set => onHand = value; }
        public double IsCommited { get => isCommited; set => isCommited = value; }
        public double OnOrder { get => onOrder; set => onOrder = value; }
        public double Avaliable { get => avaliable; set => avaliable = value; }
        public string InvntryUom { get => invntryUom; set => invntryUom = value; }
    }
}
