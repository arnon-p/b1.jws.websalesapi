﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CommonControl.POCO.Class
{
    public class ApproveSalesOrder
    {
        public DateTime CreateDate { get; set; }
        public DateTime DeliveryDate { get; set; }
        public int DocEntry { get; set; }
        public string CardCode { get; set; }
        public string CardName { get; set; }
        public int SlpCode { get; set; }
        public string SlpName { get; set; }
        public double DocTotal { get; set; }
        public string WddStatus { get; set; }
        public int ParentKey { get; set; }
        public int Code { get; set; }
        public string WtmName { get; set; }
    }
}
