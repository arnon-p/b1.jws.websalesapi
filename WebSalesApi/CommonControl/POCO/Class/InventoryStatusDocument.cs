﻿using CommonControl.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CommonControl.POCO.Class
{
    public class InventoryStatusDocument
    {
        private string itemCode;
        private int docEntry;
        private string objType;
        private int docNum;
        private string docNo;
        private string cardCode;
        private string cardName;
        private DateTime docDate;
        private DateTime docDueDate;
        private double isCommited;
        private double onOrder;

        public string ItemCode { get => itemCode; set => itemCode = value; }
        public int DocEntry { get => docEntry; set => docEntry = value; }
        public string ObjType { get => objType; set => objType = value; }
        public int DocNum { get => docNum; set => docNum = value; }
        public string DocNo { get => docNo; set => docNo = value; }
        public string CardCode { get => cardCode; set => cardCode = value; }
        public string CardName { get => cardName; set => cardName = value; }
        public DateTime DocDate { get => docDate; set => docDate = value; }
        public DateTime DocDueDate { get => docDueDate; set => docDueDate = value; }
        public double IsCommited { get => isCommited; set => isCommited = value; }
        public double OnOrder { get => onOrder; set => onOrder = value; }
    }
}
