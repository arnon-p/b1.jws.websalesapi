﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CommonControl.POCO.Class
{
    public class WebAdminPosition
    {
        private string positionId;
        private string positionName;
        private string status;

        public string PositionId { get => positionId; set => positionId = value; }
        public string PositionName { get => positionName; set => positionName = value; }
        public string Status { get => status; set => status = value; }
    }
}
