﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CommonControl.POCO.Class
{
    public class SalesAnalysisTemp
    {
        private int slpCode;
        private string slpName;
        private string cardCode;
        private string cardName;
        private string itemCode;
        private string itemName;
        private int uomEntry;
        private string uomCode;
        private string uomName;
        private double ytdQuantity;
        private double ytdTotal;

        //
        private int month1;
        private int year1;
        private string monthYear1;
        private double qty1;
        private double total1;

        private int month2;
        private int year2;
        private string monthYear2;
        private double qty2;
        private double total2;

        private int month3;
        private int year3;
        private string monthYear3;
        private double qty3;
        private double total3;

        private int month4;
        private int year4;
        private string monthYear4;
        private double qty4;
        private double total4;

        private int month5;
        private int year5;
        private string monthYear5;
        private double qty5;
        private double total5;

        private int month6;
        private int year6;
        private string monthYear6;
        private double qty6;
        private double total6;

        private int month7;
        private int year7;
        private string monthYear7;
        private double qty7;
        private double total7;

        private int month8;
        private int year8;
        private string monthYear8;
        private double qty8;
        private double total8;

        private int month9;
        private int year9;
        private string monthYear9;
        private double qty9;
        private double total9;

        private int month10;
        private int year10;
        private string monthYear10;
        private double qty10;
        private double total10;

        private int month11;
        private int year11;
        private string monthYear11;
        private double qty11;
        private double total11;

        private int month12;
        private int year12;
        private string monthYear12;
        private double qty12;
        private double total12;

        private int month13;
        private int year13;
        private string monthYear13;
        private double qty13;
        private double total13;

        private int month14;
        private int year14;
        private string monthYear14;
        private double qty14;
        private double total14;

        private int month15;
        private int year15;
        private string monthYear15;
        private double qty15;
        private double total15;

        private int month16;
        private int year16;
        private string monthYear16;
        private double qty16;
        private double total16;

        private int month17;
        private int year17;
        private string monthYear17;
        private double qty17;
        private double total17;

        private int month18;
        private int year18;
        private string monthYear18;
        private double qty18;
        private double total18;

        private int month19;
        private int year19;
        private string monthYear19;
        private double qty19;
        private double total19;

        private int month20;
        private int year20;
        private string monthYear20;
        private double qty20;
        private double total20;

        private int month21;
        private int year21;
        private string monthYear21;
        private double qty21;
        private double total21;

        private int month22;
        private int year22;
        private string monthYear22;
        private double qty22;
        private double total22;

        private int month23;
        private int year23;
        private string monthYear23;
        private double qty23;
        private double total23;

        private int month24;
        private int year24;
        private string monthYear24;
        private double qty24;
        private double total24;

        public SalesAnalysisReport cloneMapping()
        {
            SalesAnalysisReport sales = new SalesAnalysisReport();

            sales.SlpCode = this.slpCode;
            sales.SlpName = this.slpName;
            sales.CardCode = this.cardCode;
            sales.CardName = this.cardName;
            sales.ItemCode = this.itemCode;
            sales.ItemName = this.itemName;
            sales.UomEntry = this.uomEntry;
            sales.UomCode = this.uomCode;
            sales.UomName = this.uomName;
            sales.YtdQuantity = this.ytdQuantity;
            sales.YtdTotal = this.ytdTotal;

            sales.Months = new List<SalesAnalysisMonth>();
            sales.Months.Add(new SalesAnalysisMonth(this.month1, this.year1, this.monthYear1, this.qty1, this.total1));
            sales.Months.Add(new SalesAnalysisMonth(this.month2, this.year2, this.monthYear2, this.qty2, this.total2));
            sales.Months.Add(new SalesAnalysisMonth(this.month3, this.year3, this.monthYear3, this.qty3, this.total3));
            sales.Months.Add(new SalesAnalysisMonth(this.month4, this.year4, this.monthYear4, this.qty4, this.total4));
            sales.Months.Add(new SalesAnalysisMonth(this.month5, this.year5, this.monthYear5, this.qty5, this.total5));
            sales.Months.Add(new SalesAnalysisMonth(this.month6, this.year6, this.monthYear6, this.qty6, this.total6));
            sales.Months.Add(new SalesAnalysisMonth(this.month7, this.year7, this.monthYear7, this.qty7, this.total7));
            sales.Months.Add(new SalesAnalysisMonth(this.month8, this.year8, this.monthYear8, this.qty8, this.total8));
            sales.Months.Add(new SalesAnalysisMonth(this.month9, this.year9, this.monthYear9, this.qty9, this.total9));
            sales.Months.Add(new SalesAnalysisMonth(this.month10, this.year10, this.monthYear10, this.qty10, this.total10));
            sales.Months.Add(new SalesAnalysisMonth(this.month11, this.year11, this.monthYear11, this.qty11, this.total11));
            sales.Months.Add(new SalesAnalysisMonth(this.month12, this.year12, this.monthYear12, this.qty12, this.total12));
            sales.Months.Add(new SalesAnalysisMonth(this.month13, this.year13, this.monthYear13, this.qty13, this.total13));
            sales.Months.Add(new SalesAnalysisMonth(this.month14, this.year14, this.monthYear14, this.qty14, this.total14));
            sales.Months.Add(new SalesAnalysisMonth(this.month15, this.year15, this.monthYear15, this.qty15, this.total15));
            sales.Months.Add(new SalesAnalysisMonth(this.month16, this.year16, this.monthYear16, this.qty16, this.total16));
            sales.Months.Add(new SalesAnalysisMonth(this.month17, this.year17, this.monthYear17, this.qty17, this.total17));
            sales.Months.Add(new SalesAnalysisMonth(this.month18, this.year18, this.monthYear18, this.qty18, this.total18));
            sales.Months.Add(new SalesAnalysisMonth(this.month19, this.year19, this.monthYear19, this.qty19, this.total19));
            sales.Months.Add(new SalesAnalysisMonth(this.month20, this.year20, this.monthYear20, this.qty20, this.total20));
            sales.Months.Add(new SalesAnalysisMonth(this.month21, this.year21, this.monthYear21, this.qty21, this.total21));
            sales.Months.Add(new SalesAnalysisMonth(this.month22, this.year22, this.monthYear22, this.qty22, this.total22));
            sales.Months.Add(new SalesAnalysisMonth(this.month23, this.year23, this.monthYear23, this.qty23, this.total23));
            sales.Months.Add(new SalesAnalysisMonth(this.month24, this.year24, this.monthYear24, this.qty24, this.total24));

            for(int i = sales.Months.Count - 1; i >= 0; i--)
            {
                if(sales.Months[i].Month == 0 && sales.Months[i].Year == 0)
                {
                    sales.Months.RemoveAt(i);
                }
            }

            return sales;
        }

        public int SlpCode { get => slpCode; set => slpCode = value; }
        public string SlpName { get => slpName; set => slpName = value; }
        public string CardCode { get => cardCode; set => cardCode = value; }
        public string CardName { get => cardName; set => cardName = value; }
        public string ItemCode { get => itemCode; set => itemCode = value; }
        public string ItemName { get => itemName; set => itemName = value; }
        public int UomEntry { get => uomEntry; set => uomEntry = value; }
        public string UomCode { get => uomCode; set => uomCode = value; }
        public string UomName { get => uomName; set => uomName = value; }
        public double YtdQuantity { get => ytdQuantity; set => ytdQuantity = value; }
        public double YtdTotal { get => ytdTotal; set => ytdTotal = value; }
        public int Month1 { get => month1; set => month1 = value; }
        public int Year1 { get => year1; set => year1 = value; }
        public double Qty1 { get => qty1; set => qty1 = value; }
        public double Total1 { get => total1; set => total1 = value; }
        public int Month2 { get => month2; set => month2 = value; }
        public int Year2 { get => year2; set => year2 = value; }
        public double Qty2 { get => qty2; set => qty2 = value; }
        public double Total2 { get => total2; set => total2 = value; }
        public int Month3 { get => month3; set => month3 = value; }
        public int Year3 { get => year3; set => year3 = value; }
        public double Qty3 { get => qty3; set => qty3 = value; }
        public double Total3 { get => total3; set => total3 = value; }
        public int Month4 { get => month4; set => month4 = value; }
        public int Year4 { get => year4; set => year4 = value; }
        public double Qty4 { get => qty4; set => qty4 = value; }
        public double Total4 { get => total4; set => total4 = value; }
        public int Month5 { get => month5; set => month5 = value; }
        public int Year5 { get => year5; set => year5 = value; }
        public double Qty5 { get => qty5; set => qty5 = value; }
        public double Total5 { get => total5; set => total5 = value; }
        public int Month6 { get => month6; set => month6 = value; }
        public int Year6 { get => year6; set => year6 = value; }
        public double Qty6 { get => qty6; set => qty6 = value; }
        public double Total6 { get => total6; set => total6 = value; }
        public int Month7 { get => month7; set => month7 = value; }
        public int Year7 { get => year7; set => year7 = value; }
        public double Qty7 { get => qty7; set => qty7 = value; }
        public double Total7 { get => total7; set => total7 = value; }
        public int Month8 { get => month8; set => month8 = value; }
        public int Year8 { get => year8; set => year8 = value; }
        public double Qty8 { get => qty8; set => qty8 = value; }
        public double Total8 { get => total8; set => total8 = value; }
        public int Month9 { get => month9; set => month9 = value; }
        public int Year9 { get => year9; set => year9 = value; }
        public double Qty9 { get => qty9; set => qty9 = value; }
        public double Total9 { get => total9; set => total9 = value; }
        public int Month10 { get => month10; set => month10 = value; }
        public int Year10 { get => year10; set => year10 = value; }
        public double Qty10 { get => qty10; set => qty10 = value; }
        public double Total10 { get => total10; set => total10 = value; }
        public int Month11 { get => month11; set => month11 = value; }
        public int Year11 { get => year11; set => year11 = value; }
        public double Qty11 { get => qty11; set => qty11 = value; }
        public double Total11 { get => total11; set => total11 = value; }
        public int Month12 { get => month12; set => month12 = value; }
        public int Year12 { get => year12; set => year12 = value; }
        public double Qty12 { get => qty12; set => qty12 = value; }
        public double Total12 { get => total12; set => total12 = value; }
        public int Month13 { get => month13; set => month13 = value; }
        public int Year13 { get => year13; set => year13 = value; }
        public double Qty13 { get => qty13; set => qty13 = value; }
        public double Total13 { get => total13; set => total13 = value; }
        public int Month14 { get => month14; set => month14 = value; }
        public int Year14 { get => year14; set => year14 = value; }
        public double Qty14 { get => qty14; set => qty14 = value; }
        public double Total14 { get => total14; set => total14 = value; }
        public int Month15 { get => month15; set => month15 = value; }
        public int Year15 { get => year15; set => year15 = value; }
        public double Qty15 { get => qty15; set => qty15 = value; }
        public double Total15 { get => total15; set => total15 = value; }
        public int Month16 { get => month16; set => month16 = value; }
        public int Year16 { get => year16; set => year16 = value; }
        public double Qty16 { get => qty16; set => qty16 = value; }
        public double Total16 { get => total16; set => total16 = value; }
        public int Month17 { get => month17; set => month17 = value; }
        public int Year17 { get => year17; set => year17 = value; }
        public double Qty17 { get => qty17; set => qty17 = value; }
        public double Total17 { get => total17; set => total17 = value; }
        public int Month18 { get => month18; set => month18 = value; }
        public int Year18 { get => year18; set => year18 = value; }
        public double Qty18 { get => qty18; set => qty18 = value; }
        public double Total18 { get => total18; set => total18 = value; }
        public int Month19 { get => month19; set => month19 = value; }
        public int Year19 { get => year19; set => year19 = value; }
        public double Qty19 { get => qty19; set => qty19 = value; }
        public double Total19 { get => total19; set => total19 = value; }
        public int Month20 { get => month20; set => month20 = value; }
        public int Year20 { get => year20; set => year20 = value; }
        public double Qty20 { get => qty20; set => qty20 = value; }
        public double Total20 { get => total20; set => total20 = value; }
        public int Month21 { get => month21; set => month21 = value; }
        public int Year21 { get => year21; set => year21 = value; }
        public double Qty21 { get => qty21; set => qty21 = value; }
        public double Total21 { get => total21; set => total21 = value; }
        public int Month22 { get => month22; set => month22 = value; }
        public int Year22 { get => year22; set => year22 = value; }
        public double Qty22 { get => qty22; set => qty22 = value; }
        public double Total22 { get => total22; set => total22 = value; }
        public int Month23 { get => month23; set => month23 = value; }
        public int Year23 { get => year23; set => year23 = value; }
        public double Qty23 { get => qty23; set => qty23 = value; }
        public double Total23 { get => total23; set => total23 = value; }
        public int Month24 { get => month24; set => month24 = value; }
        public int Year24 { get => year24; set => year24 = value; }
        public double Qty24 { get => qty24; set => qty24 = value; }
        public double Total24 { get => total24; set => total24 = value; }
        public string MonthYear1 { get => monthYear1; set => monthYear1 = value; }
        public string MonthYear2 { get => monthYear2; set => monthYear2 = value; }
        public string MonthYear3 { get => monthYear3; set => monthYear3 = value; }
        public string MonthYear4 { get => monthYear4; set => monthYear4 = value; }
        public string MonthYear5 { get => monthYear5; set => monthYear5 = value; }
        public string MonthYear6 { get => monthYear6; set => monthYear6 = value; }
        public string MonthYear7 { get => monthYear7; set => monthYear7 = value; }
        public string MonthYear8 { get => monthYear8; set => monthYear8 = value; }
        public string MonthYear9 { get => monthYear9; set => monthYear9 = value; }
        public string MonthYear10 { get => monthYear10; set => monthYear10 = value; }
        public string MonthYear11 { get => monthYear11; set => monthYear11 = value; }
        public string MonthYear12 { get => monthYear12; set => monthYear12 = value; }
        public string MonthYear13 { get => monthYear13; set => monthYear13 = value; }
        public string MonthYear14 { get => monthYear14; set => monthYear14 = value; }
        public string MonthYear15 { get => monthYear15; set => monthYear15 = value; }
        public string MonthYear16 { get => monthYear16; set => monthYear16 = value; }
        public string MonthYear17 { get => monthYear17; set => monthYear17 = value; }
        public string MonthYear18 { get => monthYear18; set => monthYear18 = value; }
        public string MonthYear19 { get => monthYear19; set => monthYear19 = value; }
        public string MonthYear20 { get => monthYear20; set => monthYear20 = value; }
        public string MonthYear21 { get => monthYear21; set => monthYear21 = value; }
        public string MonthYear22 { get => monthYear22; set => monthYear22 = value; }
        public string MonthYear23 { get => monthYear23; set => monthYear23 = value; }
        public string MonthYear24 { get => monthYear24; set => monthYear24 = value; }
    }
}
