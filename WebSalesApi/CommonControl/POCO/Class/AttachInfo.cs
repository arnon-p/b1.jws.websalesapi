﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CommonControl.POCO.Class
{
    public class AttachInfo
    {
        private string fileName;
        private string name;
        private string ext;
        private DateTime date;
        private string tempKey;
        private string srcPath;
        private string newFileName;
        private string newName;

        public string FileName { get => fileName; set => fileName = value; }
        public string Name { get => name; set => name = value; }
        public string Ext { get => ext; set => ext = value; }
        public DateTime Date { get => date; set => date = value; }
        public string TempKey { get => tempKey; set => tempKey = value; }
        public string SrcPath { get => srcPath; set => srcPath = value; }
        public string NewFileName { get => newFileName; set => newFileName = value; }
        public string NewName { get => newName; set => newName = value; }
    }
}
