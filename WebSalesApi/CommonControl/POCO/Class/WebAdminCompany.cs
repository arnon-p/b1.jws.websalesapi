﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CommonControl.POCO.Class
{
    public class WebAdminCompany
    {
        private string companyId;
        private string companyCode;
        private string companyName;
        private string status;

        public string CompanyId { get => companyId; set => companyId = value; }
        public string CompanyCode { get => companyCode; set => companyCode = value; }
        public string CompanyName { get => companyName; set => companyName = value; }
        public string Status { get => status; set => status = value; }
    }
}
