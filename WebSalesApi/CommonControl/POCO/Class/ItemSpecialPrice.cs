﻿using CommonControl.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CommonControl.POCO.Class
{
    public class ItemSpecialPrice
    {
        private string cardCode;
        private string itemCode;
        private int oSPPListNum;
        private double srcPrice;
        private double oSPPDiscount;
        private double oSPPPrice;
        private double oSPPCurrency;
        private string oSPPAutoUpdt;
        private double lINENUM;
        private double sPP1ListNum;
        private double sPP1Discount;
        private double sPP1Price;
        private string sPP1Currency;
        private string sPP1AutoUpdt;
        private int sPP2LNum;
        private int sPP2UomEntry;
        private double sPP2Quantity;
        private double sPP2Discount;
        private double sPP2Price;
        private string sPP2Currency;
        private int pLUomEntry;
        private double price;
        private double unitPrice;
        private double invQty;

        public string CardCode { get => cardCode; set => cardCode = value; }
        public string ItemCode { get => itemCode; set => itemCode = value; }
        public int OSPPListNum { get => oSPPListNum; set => oSPPListNum = value; }
        public double SrcPrice { get => srcPrice; set => srcPrice = value; }
        public double OSPPDiscount { get => oSPPDiscount; set => oSPPDiscount = value; }
        public double OSPPPrice { get => oSPPPrice; set => oSPPPrice = value; }
        public double OSPPCurrency { get => oSPPCurrency; set => oSPPCurrency = value; }
        public string OSPPAutoUpdt { get => oSPPAutoUpdt; set => oSPPAutoUpdt = value; }
        public double LINENUM { get => lINENUM; set => lINENUM = value; }
        public double SPP1ListNum { get => sPP1ListNum; set => sPP1ListNum = value; }
        public double SPP1Discount { get => sPP1Discount; set => sPP1Discount = value; }
        public double SPP1Price { get => sPP1Price; set => sPP1Price = value; }
        public string SPP1Currency { get => sPP1Currency; set => sPP1Currency = value; }
        public string SPP1AutoUpdt { get => sPP1AutoUpdt; set => sPP1AutoUpdt = value; }
        public int SPP2LNum { get => sPP2LNum; set => sPP2LNum = value; }
        public int SPP2UomEntry { get => sPP2UomEntry; set => sPP2UomEntry = value; }
        public double SPP2Quantity { get => sPP2Quantity; set => sPP2Quantity = value; }
        public double SPP2Discount { get => sPP2Discount; set => sPP2Discount = value; }
        public double SPP2Price { get => sPP2Price; set => sPP2Price = value; }
        public string SPP2Currency { get => sPP2Currency; set => sPP2Currency = value; }
        public int PLUomEntry { get => pLUomEntry; set => pLUomEntry = value; }
        public double Price { get => price; set => price = value; }
        public double UnitPrice { get => unitPrice; set => unitPrice = value; }
        public double InvQty { get => invQty; set => invQty = value; }
    }
}
