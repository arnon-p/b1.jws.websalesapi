﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CommonControl.POCO.Class
{
    public class WebAdminUserBean
    {
        private string userId;
        private string userName;
        private string userLevel;
        private string status;
        private WebAdminProfile profileBean;
        private List<WebAdminModule> moduleList;

        public string UserId { get => userId; set => userId = value; }
        public string UserName { get => userName; set => userName = value; }
        public string UserLevel { get => userLevel; set => userLevel = value; }
        public string Status { get => status; set => status = value; }
        public WebAdminProfile ProfileBean { get => profileBean; set => profileBean = value; }
        public List<WebAdminModule> ModuleList { get => moduleList; set => moduleList = value; }
    }
}
