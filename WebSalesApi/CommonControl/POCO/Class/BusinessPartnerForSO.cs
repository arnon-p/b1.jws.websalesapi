﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CommonControl.POCO.Class
{
    public class BusinessPartnerForSO
    {
        private string cardCode;
        private string cardName;
        private string federalTaxID;
        private int payTermsGrpCode;
        private string pymntGroup;
        private double creditLimit;
        private double commitmentLimit;
        private double orders;
        private double outstandingDebt;
        private double overdueDebt;
        private string fatherCard;
        private int salesPersonCode;
        private string salesPersonName;
        private string vatBranch;
        private string bpBrnCode;
        private string bpBrnName;
        private string billToDef;
        private string shipToDef;
        private int cntctPrsnDef;
        private int priceListNum;
        private string priceListName;
        private List<BusinessPartnerAddress> bpShipList;
        private List<BusinessPartnerAddress> bpBillList;
        private List<BusinessPartnerContact> bpContactList;

        public string CardCode { get => cardCode; set => cardCode = value; }
        public string CardName { get => cardName; set => cardName = value; }
        public string FederalTaxID { get => federalTaxID; set => federalTaxID = value; }
        public int PayTermsGrpCode { get => payTermsGrpCode; set => payTermsGrpCode = value; }
        public string PymntGroup { get => pymntGroup; set => pymntGroup = value; }
        public double CreditLimit { get => creditLimit; set => creditLimit = value; }
        public double CommitmentLimit { get => commitmentLimit; set => commitmentLimit = value; }
        public double Orders { get => orders; set => orders = value; }
        public double OutstandingDebt { get => outstandingDebt; set => outstandingDebt = value; }
        public double OverdueDebt { get => overdueDebt; set => overdueDebt = value; }
        public string FatherCard { get => fatherCard; set => fatherCard = value; }
        public int SalesPersonCode { get => salesPersonCode; set => salesPersonCode = value; }
        public string SalesPersonName { get => salesPersonName; set => salesPersonName = value; }
        public string VatBranch { get => vatBranch; set => vatBranch = value; }
        public string BpBrnCode { get => bpBrnCode; set => bpBrnCode = value; }
        public string BpBrnName { get => bpBrnName; set => bpBrnName = value; }
        public string BillToDef { get => billToDef; set => billToDef = value; }
        public string ShipToDef { get => shipToDef; set => shipToDef = value; }
        public int CntctPrsnDef { get => cntctPrsnDef; set => cntctPrsnDef = value; }
        public int PriceListNum { get => priceListNum; set => priceListNum = value; }
        public string PriceListName { get => priceListName; set => priceListName = value; }
        public List<BusinessPartnerAddress> BpShipList { get => bpShipList; set => bpShipList = value; }
        public List<BusinessPartnerAddress> BpBillList { get => bpBillList; set => bpBillList = value; }
        public List<BusinessPartnerContact> BpContactList { get => bpContactList; set => bpContactList = value; }
    }
}
