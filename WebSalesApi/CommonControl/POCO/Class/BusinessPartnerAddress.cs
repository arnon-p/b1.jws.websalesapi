﻿using B1Utility.Utils;
using CommonControl.Common;
using CommonControl.POCO.Enum;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CommonControl.POCO.Class
{
    public class BusinessPartnerAddress
    {
        private string cardCode;
        private AddressType addressType;
        private string address;
        private string addressName;
        private string addressName2;
        private string addressName3;
        private string street;
        private string block;
        private string city;
        private string zipCode;
        private string county;
        private string state;
        private string country;
        private int visOrder;
        private string addrName;

        //
        // province = city
        private string amphur;
        private string tambon;

        // UDF
        private string bpBrnCode; //u_ISS_BpBrnCode;
        private string bpBrnName; //u_ISS_BpBrnName;

        public BusinessPartnerAddress()
        {
            visOrder = -1;
        }

        public string CardCode { get => cardCode; set => cardCode = value; }
        public AddressType AddressType { get => addressType; set => addressType = value; }
        public string Address { get => address; set => address = value; }
        public string AddressName { get => addressName; set => addressName = value; }
        public string AddressName2 { get => addressName2; set => addressName2 = value; }
        public string AddressName3 { get => addressName3; set => addressName3 = value; }
        public string Street { get => street; set => street = value; }
        public string Block { get => block; set => block = value; }
        public string City { get => city; set => city = value; }
        public string ZipCode { get => zipCode; set => zipCode = value; }
        public string County { get => county; set => county = value; }
        public string State { get => state; set => state = value; }
        public string Country { get => country; set => country = value; }
        public int VisOrder { get => visOrder; set => visOrder = value; }
        public string AddrName { get => addrName; set => addrName = value; }
        public string BpBrnCode { get => bpBrnCode; set => bpBrnCode = value; }
        public string BpBrnName { get => bpBrnName; set => bpBrnName = value; }
        public string Amphur { get => amphur; set => amphur = value; }
        public string Tambon { get => tambon; set => tambon = value; }
    }
}
