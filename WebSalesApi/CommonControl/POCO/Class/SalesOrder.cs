﻿using CommonControl.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CommonControl.POCO.Class
{
    public class SalesOrder : ICommon
    {
        private bool isDraft;
        private int docKey;
        private int series;
        private string seriesName;
        private int docEntry;
        private int docNum;
        private string docStatus;
        private string canceled;
        private string cardCode;
        private string cardName;
        private DateTime docDate;
        private DateTime docDueDate;
        private DateTime taxDate;
        private int contactPersonCode;
        private string numAtCard;
        private int salesPersonCode;
        private string salesPersonName;
        private int documentsOwner;
        private double totalBeforeDiscount;
        private double discountPercent;
        private double discountAmount;
        private double vatAmount;
        private double docTotal;
        private string comments;
        private int paymentGroupCode;
        private string payToCode;
        private string address;
        private string shipToCode;
        private string address2;
        private string wddStatus;
        private List<SalesOrderLine> lines;
        private List<BusinessPartnerAddress> bpShipList;
        private List<BusinessPartnerAddress> bpBillList;
        private List<BusinessPartnerContact> bpContactList;
        private List<AttachInfo> attachments;
        private List<DocumentRelation> relations;

        // UDF
        private string vatBranch;//u_ISS_VatBranch;
        private string bpTaxId;//u_ISS_BpTaxId;
        private string bpBrnCode;//u_ISS_BpBrnCode;
        private string invRemark;//u_WGE_InvRemark;

        public bool IsDraft { get => isDraft; set => isDraft = value; }
        public int DocKey { get => docKey; set => docKey = value; }
        public int Series { get => series; set => series = value; }
        public string SeriesName { get => seriesName; set => seriesName = value; }
        public int DocEntry { get => docEntry; set => docEntry = value; }
        public int DocNum { get => docNum; set => docNum = value; }
        public string DocStatus { get => docStatus; set => docStatus = value; }
        public string Canceled { get => canceled; set => canceled = value; }
        public string CardCode { get => cardCode; set => cardCode = value; }
        public string CardName { get => cardName; set => cardName = value; }
        public DateTime DocDate { get => docDate; set => docDate = value; }
        public DateTime DocDueDate { get => docDueDate; set => docDueDate = value; }
        public DateTime TaxDate { get => taxDate; set => taxDate = value; }
        public int ContactPersonCode { get => contactPersonCode; set => contactPersonCode = value; }
        public string NumAtCard { get => numAtCard; set => numAtCard = value; }
        public int SalesPersonCode { get => salesPersonCode; set => salesPersonCode = value; }
        public string SalesPersonName { get => salesPersonName; set => salesPersonName = value; }
        public int DocumentsOwner { get => documentsOwner; set => documentsOwner = value; }
        public double TotalBeforeDiscount { get => totalBeforeDiscount; set => totalBeforeDiscount = value; }
        public double DiscountPercent { get => discountPercent; set => discountPercent = value; }
        public double DiscountAmount { get => discountAmount; set => discountAmount = value; }
        public double VatAmount { get => vatAmount; set => vatAmount = value; }
        public double DocTotal { get => docTotal; set => docTotal = value; }
        public string Comments { get => comments; set => comments = value; }
        public int PaymentGroupCode { get => paymentGroupCode; set => paymentGroupCode = value; }
        public string PayToCode { get => payToCode; set => payToCode = value; }
        public string Address { get => address; set => address = value; }
        public string ShipToCode { get => shipToCode; set => shipToCode = value; }
        public string Address2 { get => address2; set => address2 = value; }
        public string WddStatus { get => wddStatus; set => wddStatus = value; }
        public List<SalesOrderLine> Lines { get => lines; set => lines = value; }
        public List<BusinessPartnerAddress> BpShipList { get => bpShipList; set => bpShipList = value; }
        public List<BusinessPartnerAddress> BpBillList { get => bpBillList; set => bpBillList = value; }
        public List<BusinessPartnerContact> BpContactList { get => bpContactList; set => bpContactList = value; }
        public List<AttachInfo> Attachments { get => attachments; set => attachments = value; }
        public List<DocumentRelation> Relations { get => relations; set => relations = value; }
        public string VatBranch { get => vatBranch; set => vatBranch = value; }
        public string BpTaxId { get => bpTaxId; set => bpTaxId = value; }
        public string BpBrnCode { get => bpBrnCode; set => bpBrnCode = value; }
        public string InvRemark { get => invRemark; set => invRemark = value; }
    }
}
