﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CommonControl.POCO.Class
{
    public class BusinessPartnerCriteria
    {
        private string cardCode;
        private string cardName;

        public string CardCode { get => cardCode; set => cardCode = value; }
        public string CardName { get => cardName; set => cardName = value; }
    }
}
