﻿using CommonControl.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CommonControl.POCO.Class
{
    public class BusinessPartner : ICommon
    {
        private string cardCode;
        private string cardName;
        private string cardType;
        private int groupCode;
        private string groupName;
        private string federalTaxID;
        private string phone1;
        private string phone2;
        private string cellular;
        private string fax;
        private string emailAddress;
        private string website;
        private int shippingType;
        private string shipTypeName;
        private int salesPersonCode;
        private double balance;
        private double ordersBal;
        private int payTermsGrpCode;
        private string pymntGroup;
        private double creditLimit;
        private string fatherCard;
        private string freeText;
        private string validFor;
        private int priceListNum;
        private string status;
        private List<BusinessPartnerAddress> bpShipList;
        private List<BusinessPartnerAddress> bpBillList;
        private List<BusinessPartnerContact> bpContactList;
        private List<bool> propertyList;
        private List<AttachInfo> attachments;

        // UDF
        private string u_WGE_Billing;
        private string u_WGE_PayDetail;
        private string u_WGE_BillingIns;
        private string u_WGE_BillState;

        public string CardCode { get => cardCode; set => cardCode = value; }
        public string CardName { get => cardName; set => cardName = value; }
        public string CardType { get => cardType; set => cardType = value; }
        public int GroupCode { get => groupCode; set => groupCode = value; }
        public string GroupName { get => groupName; set => groupName = value; }
        public string FederalTaxID { get => federalTaxID; set => federalTaxID = value; }
        public string Phone1 { get => phone1; set => phone1 = value; }
        public string Phone2 { get => phone2; set => phone2 = value; }
        public string Cellular { get => cellular; set => cellular = value; }
        public string Fax { get => fax; set => fax = value; }
        public string EmailAddress { get => emailAddress; set => emailAddress = value; }
        public string Website { get => website; set => website = value; }
        public int ShippingType { get => shippingType; set => shippingType = value; }
        public string ShipTypeName { get => shipTypeName; set => shipTypeName = value; }
        public int SalesPersonCode { get => salesPersonCode; set => salesPersonCode = value; }
        public double Balance { get => balance; set => balance = value; }
        public double OrdersBal { get => ordersBal; set => ordersBal = value; }
        public int PayTermsGrpCode { get => payTermsGrpCode; set => payTermsGrpCode = value; }
        public string PymntGroup { get => pymntGroup; set => pymntGroup = value; }
        public double CreditLimit { get => creditLimit; set => creditLimit = value; }
        public string FatherCard { get => fatherCard; set => fatherCard = value; }
        public string FreeText { get => freeText; set => freeText = value; }
        public string ValidFor { get => validFor; set => validFor = value; }
        public int PriceListNum { get => priceListNum; set => priceListNum = value; }
        public string Status { get => status; set => status = value; }
        public List<BusinessPartnerAddress> BpShipList { get => bpShipList; set => bpShipList = value; }
        public List<BusinessPartnerAddress> BpBillList { get => bpBillList; set => bpBillList = value; }
        public List<BusinessPartnerContact> BpContactList { get => bpContactList; set => bpContactList = value; }
        public List<bool> PropertyList { get => propertyList; set => propertyList = value; }
        public List<AttachInfo> Attachments { get => attachments; set => attachments = value; }
        public string U_WGE_Billing { get => u_WGE_Billing; set => u_WGE_Billing = value; }
        public string U_WGE_PayDetail { get => u_WGE_PayDetail; set => u_WGE_PayDetail = value; }
        public string U_WGE_BillingIns { get => u_WGE_BillingIns; set => u_WGE_BillingIns = value; }
        public string U_WGE_BillState { get => u_WGE_BillState; set => u_WGE_BillState = value; }
    }
}
