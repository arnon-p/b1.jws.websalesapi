﻿using B1Utility.Utils;
using CommonControl.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CommonControl.POCO.Class
{
    public class SalesOrderLine
    {
        private int lineNum;
        private int ugpEntry;
        private string itemCode;
        private string itemName;
        private double quantity;
        private double invQty;
        private double unitPrice;
        private double discountPercent;
        private double discountAmount;
        private double priceBeforeDiscount;
        private double priceAfterDiscount;
        private double priceAfterVat;
        private string vatGroup;
        private double vatAmount;
        private double lineTotalAmount;
        private int uomEntry;
        private string uomCode;
        private string warehouseCode;
        private string costingCode;
        private string costingCode2;
        private string costingCode3;
        private string costingCode4;
        private string costingCode5;
        private List<BatchNumbers> batchs;
        private List<UomGroupConverter> uomConvertList;
        private bool isBatch;
        private bool isSpecialPrice;
        private string freeText;

        // WGE UDF
        private string freeSampling; //U_WGE_FS
        // WGE UDF

        public int LineNum { get => lineNum; set => lineNum = value; }
        public int UgpEntry { get => ugpEntry; set => ugpEntry = value; }
        public string ItemCode { get => itemCode; set => itemCode = value; }
        public string ItemName { get => itemName; set => itemName = value; }
        public double Quantity { get => quantity; set => quantity = value; }
        public double InvQty { get => invQty; set => invQty = value; }
        public double UnitPrice { get => unitPrice; set => unitPrice = value; }
        public double DiscountPercent { get => discountPercent; set => discountPercent = value; }
        public double DiscountAmount { get => discountAmount; set => discountAmount = value; }
        public double PriceBeforeDiscount { get => priceBeforeDiscount; set => priceBeforeDiscount = value; }
        public double PriceAfterDiscount { get => priceAfterDiscount; set => priceAfterDiscount = value; }
        public double PriceAfterVat { get => priceAfterVat; set => priceAfterVat = value; }
        public string VatGroup { get => vatGroup; set => vatGroup = value; }
        public double VatAmount { get => vatAmount; set => vatAmount = value; }
        public double LineTotalAmount { get => lineTotalAmount; set => lineTotalAmount = value; }
        public int UomEntry { get => uomEntry; set => uomEntry = value; }
        public string UomCode { get => uomCode; set => uomCode = value; }
        public string WarehouseCode { get => warehouseCode; set => warehouseCode = value; }
        public string CostingCode { get => costingCode; set => costingCode = value; }
        public string CostingCode2 { get => costingCode2; set => costingCode2 = value; }
        public string CostingCode3 { get => costingCode3; set => costingCode3 = value; }
        public string CostingCode4 { get => costingCode4; set => costingCode4 = value; }
        public string CostingCode5 { get => costingCode5; set => costingCode5 = value; }
        public List<BatchNumbers> Batchs { get => batchs; set => batchs = value; }
        public List<UomGroupConverter> UomConvertList { get => uomConvertList; set => uomConvertList = value; }
        public bool IsBatch { get => isBatch; set => isBatch = value; }
        public bool IsSpecialPrice { get => isSpecialPrice; set => isSpecialPrice = value; }
        public string FreeText { get => freeText; set => freeText = value; }
        public string FreeSampling { get => freeSampling; set => freeSampling = value; }
    }
}
