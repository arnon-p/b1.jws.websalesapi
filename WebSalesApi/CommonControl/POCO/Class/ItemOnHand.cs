﻿using CommonControl.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CommonControl.POCO.Class
{
    public class ItemOnHand
    {
        private string itemCode;
        private string whsCode;
        private string whsName;
        private double onHand;
        private double isCommited;
        private double onOrder;
        private double available;

        public string ItemCode { get => itemCode; set => itemCode = value; }
        public string WhsCode { get => whsCode; set => whsCode = value; }
        public string WhsName { get => whsName; set => whsName = value; }
        public double OnHand { get => onHand; set => onHand = value; }
        public double IsCommited { get => isCommited; set => isCommited = value; }
        public double OnOrder { get => onOrder; set => onOrder = value; }
        public double Available { get => available; set => available = value; }
    }
}
