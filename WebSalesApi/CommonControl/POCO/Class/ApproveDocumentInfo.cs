﻿using CommonControl.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CommonControl.POCO.Class
{
    public class ApproveDocumentInfo
    {
        public string Company { get; set; }
        public string ObjType { get; set; }
        public int DocEntry { get; set; }
        public int DocNum { get; set; }
        public int DraftEntry { get; set; }
        public string WddStatus { get; set; }
        public int ParentKey { get; set; }
        public int Code { get; set; }
        public bool IsNextStage { get; set; }
        public int Approved { get; set; }
        public int CntApproved { get; set; }
        public int IndexApprove { get; set; }
        public bool IsError { get; set; }
        public string ErrorMessage { get; set; }
        public int UserId { get; set; }
        public int EmpId { get; set; }
        public string ExtEmpId { get; set; }
    }
}
