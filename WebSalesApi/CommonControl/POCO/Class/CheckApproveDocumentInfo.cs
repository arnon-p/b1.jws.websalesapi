﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CommonControl.POCO.Class
{
    public class CheckApproveDocumentInfo
    {
        public int Code { get; set; }
        public int TotalStage { get; set; }
        public int CurrentStage { get; set; }
        public int Stage { get; set; }
        public int MaxReqr { get; set; }
        public int MaxRejReqr { get; set; }
        public int CntApproved { get; set; }
        public int CntRejected { get; set; }
    }
}