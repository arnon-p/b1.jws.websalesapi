﻿using CommonControl.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CommonControl.POCO.Class
{
    public class GrossProfitInfo : ICommon
    {
        private int docEntry;
        private string itemCode;
        private string itemName;
        private double basePrice;
        private double totalBasePrice;
        private double salesPrice;
        private double quantity;
        private double grossProfit;
        private double profitPercent;
        private string color;
        private int lineNum;

        public int DocEntry { get => docEntry; set => docEntry = value; }
        public string ItemCode { get => itemCode; set => itemCode = value; }
        public string ItemName { get => itemName; set => itemName = value; }
        public double BasePrice { get => basePrice; set => basePrice = value; }
        public double TotalBasePrice { get => totalBasePrice; set => totalBasePrice = value; }
        public double SalesPrice { get => salesPrice; set => salesPrice = value; }
        public double Quantity { get => quantity; set => quantity = value; }
        public double GrossProfit { get => grossProfit; set => grossProfit = value; }
        public double ProfitPercent { get => profitPercent; set => profitPercent = value; }
        public string Color { get => color; set => color = value; }
        public int LineNum { get => lineNum; set => lineNum = value; }
    }
}
