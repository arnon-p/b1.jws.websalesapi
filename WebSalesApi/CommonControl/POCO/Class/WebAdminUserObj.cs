﻿using CommonControl.Enum;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CommonControl.POCO.Class
{
    public class WebAdminUserObj
    {
        private string company;
        private string token;
        private ResponseStatus status;
        private WebAdminUserBean bean;
        private string responseMsgEn;
        private string responseMsgTh;
        private WebAdminSAPProfile sapProfile;

        public string Company { get => company; set => company = value; }
        public string Token { get => token; set => token = value; }
        public ResponseStatus Status { get => status; set => status = value; }
        public WebAdminUserBean Bean { get => bean; set => bean = value; }
        public string ResponseMsgEn { get => responseMsgEn; set => responseMsgEn = value; }
        public string ResponseMsgTh { get => responseMsgTh; set => responseMsgTh = value; }
        public WebAdminSAPProfile SapProfile { get => sapProfile; set => sapProfile = value; }
    }
}
