﻿using CommonControl.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CommonControl.POCO.Class
{
    public class UomGroupConverter
    {
        private int ugpEntry;
        private int baseUom;
        private string baseUomCode;
        private int uomEntry;
        private string uomCode;
        private double altQty;
        private double baseQty;

        public int UgpEntry { get => ugpEntry; set => ugpEntry = value; }
        public int BaseUom { get => baseUom; set => baseUom = value; }
        public string BaseUomCode { get => baseUomCode; set => baseUomCode = value; }
        public int UomEntry { get => uomEntry; set => uomEntry = value; }
        public string UomCode { get => uomCode; set => uomCode = value; }
        public double AltQty { get => altQty; set => altQty = value; }
        public double BaseQty { get => baseQty; set => baseQty = value; }
    }
}
