﻿using CommonControl.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CommonControl.POCO.Class
{
    public class BatchNumberDetail
    {
        private string itemCode;
        private string whsCode;
        private int absEntry;
        private string batchNumber;
        private double quantity;
        private DateTime expDate;
        private DateTime mnfDate;
        private DateTime inDate;
        private string mnfSerial;
        private string lotNumber;
        private string location;
        private int days;
        private string color;

        public string ItemCode { get => itemCode; set => itemCode = value; }
        public string WhsCode { get => whsCode; set => whsCode = value; }
        public int AbsEntry { get => absEntry; set => absEntry = value; }
        public string BatchNumber { get => batchNumber; set => batchNumber = value; }
        public double Quantity { get => quantity; set => quantity = value; }
        public DateTime ExpDate { get => expDate; set => expDate = value; }
        public DateTime MnfDate { get => mnfDate; set => mnfDate = value; }
        public DateTime InDate { get => inDate; set => inDate = value; }
        public string MnfSerial { get => mnfSerial; set => mnfSerial = value; }
        public string LotNumber { get => lotNumber; set => lotNumber = value; }
        public string Location { get => location; set => location = value; }
        public int Days { get => days; set => days = value; }
        public string Color { get => color; set => color = value; }
    }
}
