﻿using CommonControl.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CommonControl.POCO.Class
{
    public class SalesOrderResponse
    {
        private string company;
        private string objType;
        private int docEntry;
        private int docNum;
        private int draftEntry;

        public string Company { get => company; set => company = value; }
        public string ObjType { get => objType; set => objType = value; }
        public int DocEntry { get => docEntry; set => docEntry = value; }
        public int DocNum { get => docNum; set => docNum = value; }
        public int DraftEntry { get => draftEntry; set => draftEntry = value; }
    }
}
