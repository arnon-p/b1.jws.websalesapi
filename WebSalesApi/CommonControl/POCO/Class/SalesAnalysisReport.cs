﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CommonControl.POCO.Class
{
    public class SalesAnalysisReport
    {
        private int slpCode;
        private string slpName;
        private string cardCode;
        private string cardName;
        private string itemCode;
        private string itemName;
        private int uomEntry;
        private string uomCode;
        private string uomName;
        private double ytdQuantity;
        private double ytdTotal;
        private List<SalesAnalysisMonth> months;

        public int SlpCode { get => slpCode; set => slpCode = value; }
        public string SlpName { get => slpName; set => slpName = value; }
        public string CardCode { get => cardCode; set => cardCode = value; }
        public string CardName { get => cardName; set => cardName = value; }
        public string ItemCode { get => itemCode; set => itemCode = value; }
        public string ItemName { get => itemName; set => itemName = value; }
        public int UomEntry { get => uomEntry; set => uomEntry = value; }
        public string UomCode { get => uomCode; set => uomCode = value; }
        public string UomName { get => uomName; set => uomName = value; }
        public double YtdQuantity { get => ytdQuantity; set => ytdQuantity = value; }
        public double YtdTotal { get => ytdTotal; set => ytdTotal = value; }
        public List<SalesAnalysisMonth> Months { get => months; set => months = value; }
    }
}
