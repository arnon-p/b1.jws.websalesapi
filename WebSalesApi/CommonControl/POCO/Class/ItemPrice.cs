﻿using CommonControl.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CommonControl.POCO.Class
{
    public class ItemPrice
    {
        private bool isSpecialPrice;
        private double price;
        private double unitPrice;
        private double specialPrice;
        private double discount;

        public bool IsSpecialPrice { get => isSpecialPrice; set => isSpecialPrice = value; }
        public double Price { get => price; set => price = value; }
        public double UnitPrice { get => unitPrice; set => unitPrice = value; }
        public double SpecialPrice { get => specialPrice; set => specialPrice = value; }
        public double Discount { get => discount; set => discount = value; }
    }
}
