﻿using CommonControl.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CommonControl.POCO.Class
{
    public class BatchNumbers
    {
        private string itemCode;
        private int lineNum;
        private int absEntry;
        private string batchNumber;
        private double quantity;
        private DateTime expDate;
        private int days;
        private string color;

        public string ItemCode { get => itemCode; set => itemCode = value; }
        public int LineNum { get => lineNum; set => lineNum = value; }
        public int AbsEntry { get => absEntry; set => absEntry = value; }
        public string BatchNumber { get => batchNumber; set => batchNumber = value; }
        public double Quantity { get => quantity; set => quantity = value; }
        public DateTime ExpDate { get => expDate; set => expDate = value; }
        public int Days { get => days; set => days = value; }
        public string Color { get => color; set => color = value; }
    }
}
