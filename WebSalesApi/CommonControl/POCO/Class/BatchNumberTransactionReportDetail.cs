﻿using CommonControl.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CommonControl.POCO.Class
{
    public class BatchNumberTransactionReportDetail
    {
        private int transId;
        private int applyEntry;
        private int applyLine;
        private string applyType;
        private int logEntry;
        private int appDocNum;
        private DateTime docDate;
        private string locCode;
        private string cardCode;
        private string cardName;
        private double quantity;
        private double allocQty;
        private string instance;
        private string binCode;
        private int snBTransRptFirstBinViewBinCount;
        private string prefixDocNum;
        private string direction;
        private string directionName;

        public int TransId { get => transId; set => transId = value; }
        public int ApplyEntry { get => applyEntry; set => applyEntry = value; }
        public int ApplyLine { get => applyLine; set => applyLine = value; }
        public string ApplyType { get => applyType; set => applyType = value; }
        public int LogEntry { get => logEntry; set => logEntry = value; }
        public int AppDocNum { get => appDocNum; set => appDocNum = value; }
        public DateTime DocDate { get => docDate; set => docDate = value; }
        public string LocCode { get => locCode; set => locCode = value; }
        public string CardCode { get => cardCode; set => cardCode = value; }
        public string CardName { get => cardName; set => cardName = value; }
        public double Quantity { get => quantity; set => quantity = value; }
        public double AllocQty { get => allocQty; set => allocQty = value; }
        public string Instance { get => instance; set => instance = value; }
        public string BinCode { get => binCode; set => binCode = value; }
        public int SnBTransRptFirstBinViewBinCount { get => snBTransRptFirstBinViewBinCount; set => snBTransRptFirstBinViewBinCount = value; }
        public string PrefixDocNum { get => prefixDocNum; set => prefixDocNum = value; }
        public string Direction { get => direction; set => direction = value; }
        public string DirectionName { get => directionName; set => directionName = value; }
    }
}
