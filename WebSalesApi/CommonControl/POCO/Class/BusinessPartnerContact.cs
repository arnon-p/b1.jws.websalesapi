﻿using CommonControl.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CommonControl.POCO.Class
{
    public class BusinessPartnerContact
    {
        private string cardCode;
        private int cntctCode;
        private string name;
        private string firstName;
        private string middleName;
        private string lastName;
        private string title;
        private string position;
        private string address;
        private string phone1;
        private string phone2;
        private string mobilePhone;
        private string fax;
        private string e_Mail;
        private string remarks1;
        private string remarks2;

        public string CardCode { get => cardCode; set => cardCode = value; }
        public int CntctCode { get => cntctCode; set => cntctCode = value; }
        public string Name { get => name; set => name = value; }
        public string FirstName { get => firstName; set => firstName = value; }
        public string MiddleName { get => middleName; set => middleName = value; }
        public string LastName { get => lastName; set => lastName = value; }
        public string Title { get => title; set => title = value; }
        public string Position { get => position; set => position = value; }
        public string Address { get => address; set => address = value; }
        public string Phone1 { get => phone1; set => phone1 = value; }
        public string Phone2 { get => phone2; set => phone2 = value; }
        public string MobilePhone { get => mobilePhone; set => mobilePhone = value; }
        public string Fax { get => fax; set => fax = value; }
        public string E_Mail { get => e_Mail; set => e_Mail = value; }
        public string Remarks1 { get => remarks1; set => remarks1 = value; }
        public string Remarks2 { get => remarks2; set => remarks2 = value; }
    }
}
