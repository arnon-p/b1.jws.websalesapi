﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CommonControl.POCO.Class
{
    public class DocumentRelation
    {
        private string objType;
        private string objTypeStr;
        private int docEntry;
        private int docNum;
        private DateTime docDate;
        private string docStatus;
        private bool canceled;
        private int userId;
        private string username;
        private string name;
        //private string baseType;
        //private int baseEntry;

        public string ObjType { get => objType; set => objType = value; }
        public string ObjTypeStr { get => objTypeStr; set => objTypeStr = value; }
        public int DocEntry { get => docEntry; set => docEntry = value; }
        public int DocNum { get => docNum; set => docNum = value; }
        public DateTime DocDate { get => docDate; set => docDate = value; }
        public string DocStatus { get => docStatus; set => docStatus = value; }
        public bool Canceled { get => canceled; set => canceled = value; }
        public int UserId { get => userId; set => userId = value; }
        public string Username { get => username; set => username = value; }
        public string Name { get => name; set => name = value; }
        //public string BaseType { get => baseType; set => baseType = value; }
        //public int BaseEntry { get => baseEntry; set => baseEntry = value; }
    }
}
