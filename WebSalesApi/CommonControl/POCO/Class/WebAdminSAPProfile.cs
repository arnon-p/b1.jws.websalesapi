﻿using CommonControl.POCO.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CommonControl.POCO.Class
{
    public class WebAdminSAPProfile
    {
        private int empId;
        private string extEmpId;
        private int userId;
        private int slpCode;
        private string slpName;
        private int position;
        private string posName;
        private List<int> teamId;
        private List<SalesEmployee> salesEmployee;

        public int EmpId { get => empId; set => empId = value; }
        public string ExtEmpId { get => extEmpId; set => extEmpId = value; }
        public int UserId { get => userId; set => userId = value; }
        public int SlpCode { get => slpCode; set => slpCode = value; }
        public string SlpName { get => slpName; set => slpName = value; }
        public int Position { get => position; set => position = value; }
        public string PosName { get => posName; set => posName = value; }
        public List<int> TeamId { get => teamId; set => teamId = value; }
        public List<SalesEmployee> SalesEmployee { get => salesEmployee; set => salesEmployee = value; }
    }
}
