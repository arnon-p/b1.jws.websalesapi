﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CommonControl.POCO.Class
{
    public class WebAdminProfile
    {
        private string employeeId;
        private string extEmpId;
        private string firstName;
        private string lastName;
        private WebAdminDepartment department;
        private WebAdminPosition position;
        private List<WebAdminCompany> companyList;
        private string telNo;

        public string EmployeeId { get => employeeId; set => employeeId = value; }
        public string ExtEmpId { get => extEmpId; set => extEmpId = value; }
        public string FirstName { get => firstName; set => firstName = value; }
        public string LastName { get => lastName; set => lastName = value; }
        public WebAdminDepartment Department { get => department; set => department = value; }
        public WebAdminPosition Position { get => position; set => position = value; }
        public List<WebAdminCompany> CompanyList { get => companyList; set => companyList = value; }
        public string TelNo { get => telNo; set => telNo = value; }
    }
}
