﻿using CommonControl.Common;
using CommonControl.POCO.Class;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CommonControl.POCO.Response
{
    public class SearchBusinessPartnerContactByCardCodeResponse : IResponse
    {
        private List<BusinessPartnerContact> response;

        public List<BusinessPartnerContact> Response { get => response; set => response = value; }
    }
}
