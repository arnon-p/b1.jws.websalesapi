﻿using CommonControl.Common;
using CommonControl.POCO.Class;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CommonControl.POCO.Response
{
    public class SearchBusinessPartnerForSOResponse : IResponse
    {
        private List<BusinessPartnerForSO> response;

        public List<BusinessPartnerForSO> Response { get => response; set => response = value; }
    }
}
