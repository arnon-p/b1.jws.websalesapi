﻿using CommonControl.Common;
using CommonControl.POCO.Class;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CommonControl.POCO.Response
{
    public class GrossProfitResponse : IResponse
    {
        private List<GrossProfitInfo> response;

        public List<GrossProfitInfo> Response { get => response; set => response = value; }
    }
}
