﻿using B1Utility.POCO;
using CommonControl.Common;
using CommonControl.POCO.Class;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CommonControl.POCO.Response
{
    public class SearchBusinessPartnerAddressByCardCodeResponse : IResponse
    {
        private List<BusinessPartnerAddress> response;

        public List<BusinessPartnerAddress> Response { get => response; set => response = value; }
    }
}
