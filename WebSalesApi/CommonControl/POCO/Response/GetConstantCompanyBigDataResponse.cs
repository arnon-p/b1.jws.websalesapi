﻿using CommonControl.Common;
using CommonControl.POCO.Class;
using CommonControl.POCO.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CommonControl.POCO.Response
{
    public class GetConstantCompanyBigDataResponse : IResponse
    {
        private List<LocationAddress> locationAddress;
        private List<UomGroupConverter> uomGroupConverter;

        public List<LocationAddress> LocationAddress { get => locationAddress; set => locationAddress = value; }
        public List<UomGroupConverter> UomGroupConverter { get => uomGroupConverter; set => uomGroupConverter = value; }
    }
}
