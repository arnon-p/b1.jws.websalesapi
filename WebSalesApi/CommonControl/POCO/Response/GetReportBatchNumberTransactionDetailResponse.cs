﻿using CommonControl.Common;
using CommonControl.POCO.Class;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CommonControl.POCO.Response
{
    public class GetReportBatchNumberTransactionDetailResponse : IResponse
    {
        private List<BatchNumberTransactionReportDetail> response;

        public List<BatchNumberTransactionReportDetail> Response { get => response; set => response = value; }
    }
}
