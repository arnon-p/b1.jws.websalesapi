﻿using CommonControl.Common;
using CommonControl.POCO.Class;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CommonControl.POCO.Response
{
    public class GetReportInventoryStatusResponse : IResponse
    {
        private List<InventoryStatus> response;

        public List<InventoryStatus> Response { get => response; set => response = value; }
    }
}
