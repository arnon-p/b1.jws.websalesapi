﻿using CommonControl.Common;
using CommonControl.POCO.Class;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CommonControl.POCO.Response
{
    public class SearchItemMasterForSOResponse : IResponse
    {
        private List<ItemMasterForSO> response;

        public List<ItemMasterForSO> Response { get => response; set => response = value; }
    }
}
