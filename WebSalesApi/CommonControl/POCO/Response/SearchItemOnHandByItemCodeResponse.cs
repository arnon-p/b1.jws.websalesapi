﻿using CommonControl.Common;
using CommonControl.POCO.Class;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CommonControl.POCO.Response
{
    public class SearchItemOnHandByItemCodeResponse : IResponse
    {
        private List<ItemOnHand> response;

        public List<ItemOnHand> Response { get => response; set => response = value; }
    }
}
