﻿using CommonControl.Common;
using CommonControl.POCO.Class;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CommonControl.POCO.Response
{
    public class SearchItemWhsOnHandResponse : IResponse
    {
        private List<ItemWhsOnHand> response;

        public List<ItemWhsOnHand> Response { get => response; set => response = value; }
    }
}
