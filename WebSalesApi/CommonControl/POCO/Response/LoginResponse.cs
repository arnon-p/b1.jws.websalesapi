﻿using CommonControl.Common;
using CommonControl.POCO.Class;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CommonControl.POCO.Response
{
    public class LoginResponse : IResponse
    {
        private WebAdminUserObj response;

        public WebAdminUserObj Response { get => response; set => response = value; }
    }
}
