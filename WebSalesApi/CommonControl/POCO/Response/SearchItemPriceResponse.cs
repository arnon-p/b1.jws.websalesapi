﻿using CommonControl.Common;
using CommonControl.POCO.Class;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CommonControl.POCO.Response
{
    public class SearchItemPriceResponse : IResponse
    {
        private ItemPrice response;

        public ItemPrice Response { get => response; set => response = value; }
    }
}
