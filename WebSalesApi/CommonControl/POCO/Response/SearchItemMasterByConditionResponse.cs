﻿using CommonControl.Common;
using CommonControl.POCO.Class;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CommonControl.POCO.Response
{
    public class SearchItemMasterByConditionResponse : IResponse
    {
        private List<ItemMaster> response;

        public List<ItemMaster> Response { get => response; set => response = value; }
    }
}
