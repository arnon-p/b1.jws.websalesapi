﻿using CommonControl.Common;
using CommonControl.POCO.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CommonControl.POCO.Response
{
    public class SearchSalesPriceHistoryResponse : IResponse
    {
        private List<SalesPriceHistory> response;

        public List<SalesPriceHistory> Response { get => response; set => response = value; }
    }
}
