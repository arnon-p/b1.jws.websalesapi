﻿using CommonControl.Common;
using CommonControl.POCO.Class;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CommonControl.POCO.Response
{
    public class SearchItemMasterCriteriaResponse : IResponse
    {
        private List<ItemMasterCriteria> response;

        public List<ItemMasterCriteria> Response { get => response; set => response = value; }
    }
}
