﻿using CommonControl.Common;
using CommonControl.POCO.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CommonControl.POCO.Response
{
    public class GetBrochureResponse : IResponse
    {
        private List<FileInfo> response;

        public List<FileInfo> Response { get => response; set => response = value; }
    }
}
