﻿using CommonControl.Common;
using CommonControl.POCO.Class;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CommonControl.POCO.Response
{
    public class ApproveSalesOrderResponse : IResponse
    {
        private List<ApproveDocumentInfo> response;

        public List<ApproveDocumentInfo> Response { get => response; set => response = value; }
    }
}
