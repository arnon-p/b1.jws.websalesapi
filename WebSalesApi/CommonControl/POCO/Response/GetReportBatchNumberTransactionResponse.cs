﻿using CommonControl.Common;
using CommonControl.POCO.Class;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CommonControl.POCO.Response
{
    public class GetReportBatchNumberTransactionResponse : IResponse
    {
        private List<BatchNumberTransactionReport> response;

        public List<BatchNumberTransactionReport> Response { get => response; set => response = value; }
    }
}
