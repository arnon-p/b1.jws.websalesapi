﻿using CommonControl.Common;
using CommonControl.POCO.Class;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CommonControl.POCO.Response
{
    public class GetReportCustomerPaymentResponse : IResponse
    {
        private List<CustomerPaymentReport> response;

        public List<CustomerPaymentReport> Response { get => response; set => response = value; }
    }
}
