﻿using CommonControl.Common;
using CommonControl.POCO.Class;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CommonControl.POCO.Response
{
    public class CreateSalesOrderResponse : IResponse
    {
        private List<SalesOrderResponse> response;

        public List<SalesOrderResponse> Response { get => response; set => response = value; }
    }
}
