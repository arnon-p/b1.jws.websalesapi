﻿using CommonControl.Common;
using CommonControl.POCO.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CommonControl.POCO.Response
{
    public class SearchOrderStatusByConditionResponse : IResponse
    {
        private List<OrderStatus> response;

        public List<OrderStatus> Response { get => response; set => response = value; }
    }
}
