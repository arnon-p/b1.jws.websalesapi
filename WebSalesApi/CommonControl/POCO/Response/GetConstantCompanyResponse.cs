﻿using CommonControl.Common;
using CommonControl.POCO.Class;
using CommonControl.POCO.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CommonControl.POCO.Response
{
    public class GetConstantCompanyResponse : IResponse
    {
        private List<SalesEmployee> salesEmployee;
        private List<BusinessPartnerGroup> bpGroup;
        private List<PaymentTerms> paymentTerms;
        private List<ShippingType> shippingType;
        private List<AddressType> addressType;
        private List<ItemGroup> itemGroup;
        private List<string> whse;
        private List<Properties> properties;
        private List<Country> country;
        private List<TaxGroup> taxGroup;
        private List<State> state;
        private List<Dimension> dimension1;
        private List<Dimension> dimension2;
        private List<Dimension> dimension3;
        private List<Dimension> dimension4;
        private List<Dimension> dimension5;
        private List<SeriesDocument> series;
        private List<DocumentStatus> documentStatus;
        private List<PriceList> priceList;
        private List<ExportType> exportType;
        private List<Brand> brand;
        private List<Channel> channel;
        private List<LocationAddress> locationAddress;
        private List<UomGroupConverter> uomGroupConverter;
        private List<ApproveStatus> approveStatus;

        public List<SalesEmployee> SalesEmployee { get => salesEmployee; set => salesEmployee = value; }
        public List<BusinessPartnerGroup> BpGroup { get => bpGroup; set => bpGroup = value; }
        public List<PaymentTerms> PaymentTerms { get => paymentTerms; set => paymentTerms = value; }
        public List<ShippingType> ShippingType { get => shippingType; set => shippingType = value; }
        public List<AddressType> AddressType { get => addressType; set => addressType = value; }
        public List<ItemGroup> ItemGroup { get => itemGroup; set => itemGroup = value; }
        public List<string> Whse { get => whse; set => whse = value; }
        public List<Properties> Properties { get => properties; set => properties = value; }
        public List<Country> Country { get => country; set => country = value; }
        public List<TaxGroup> TaxGroup { get => taxGroup; set => taxGroup = value; }
        public List<State> State { get => state; set => state = value; }
        public List<Dimension> Dimension1 { get => dimension1; set => dimension1 = value; }
        public List<Dimension> Dimension2 { get => dimension2; set => dimension2 = value; }
        public List<Dimension> Dimension3 { get => dimension3; set => dimension3 = value; }
        public List<Dimension> Dimension4 { get => dimension4; set => dimension4 = value; }
        public List<Dimension> Dimension5 { get => dimension5; set => dimension5 = value; }
        public List<SeriesDocument> Series { get => series; set => series = value; }
        public List<DocumentStatus> DocumentStatus { get => documentStatus; set => documentStatus = value; }
        public List<PriceList> PriceList { get => priceList; set => priceList = value; }
        public List<ExportType> ExportType { get => exportType; set => exportType = value; }
        public List<Brand> Brand { get => brand; set => brand = value; }
        public List<Channel> Channel { get => channel; set => channel = value; }
        public List<LocationAddress> LocationAddress { get => locationAddress; set => locationAddress = value; }
        public List<UomGroupConverter> UomGroupConverter { get => uomGroupConverter; set => uomGroupConverter = value; }
        public List<ApproveStatus> ApproveStatus { get => approveStatus; set => approveStatus = value; }
    }
}
