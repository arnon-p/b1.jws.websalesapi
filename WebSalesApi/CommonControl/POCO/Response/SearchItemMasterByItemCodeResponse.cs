﻿using CommonControl.Common;
using CommonControl.POCO.Class;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CommonControl.POCO.Response
{
    public class SearchItemMasterByItemCodeResponse : IResponse
    {
        private ItemMaster response;

        public ItemMaster Response { get => response; set => response = value; }
    }
}
