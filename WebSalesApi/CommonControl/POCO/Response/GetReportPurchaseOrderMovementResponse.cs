﻿using CommonControl.Common;
using CommonControl.POCO.Class;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CommonControl.POCO.Response
{
    public class GetReportPurchaseOrderMovementResponse : IResponse
    {
        private List<PurchaseOrderMovement> response;

        public List<PurchaseOrderMovement> Response { get => response; set => response = value; }
    }
}
