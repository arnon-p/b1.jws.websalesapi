﻿using CommonControl.Common;
using CommonControl.POCO.Class;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CommonControl.POCO.Response
{
    public class SearchSalesOrderByDocEntryResponse : IResponse
    {
        private SalesOrder response;

        public SalesOrder Response { get => response; set => response = value; }
    }
}
