﻿using CommonControl.Common;
using CommonControl.POCO.Class;
using CommonControl.POCO.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CommonControl.POCO.Response
{
    public class SearchSeriesByConditionResponse : IResponse
    {
        private List<SeriesDocument> response;

        public List<SeriesDocument> Response { get => response; set => response = value; }
    }
}
