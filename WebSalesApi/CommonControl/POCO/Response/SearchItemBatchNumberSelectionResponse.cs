﻿using CommonControl.Common;
using CommonControl.POCO.Class;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CommonControl.POCO.Response
{
    public class SearchItemBatchNumberSelectionResponse : IResponse
    {
        private List<BatchNumberDetail> response;

        public List<BatchNumberDetail> Response { get => response; set => response = value; }
    }
}
