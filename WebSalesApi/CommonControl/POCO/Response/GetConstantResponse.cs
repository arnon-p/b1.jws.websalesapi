﻿using CommonControl.Common;
using CommonControl.POCO.Class;
using CommonControl.POCO.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CommonControl.POCO.Response
{
    public class GetConstantResponse : IResponse
    {
        private List<CompanyLogin> company;
        public List<CompanyLogin> Company { get => company; set => company = value; }
    }
}
