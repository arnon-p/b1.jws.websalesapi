﻿using CommonControl.Common;
using CommonControl.POCO.Class;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CommonControl.POCO.Response
{
    public class SearchApprovalSalesOrderByConditionResponse : IResponse
    {
        private List<ApproveSalesOrder> response;

        public List<ApproveSalesOrder> Response { get => response; set => response = value; }
    }
}
