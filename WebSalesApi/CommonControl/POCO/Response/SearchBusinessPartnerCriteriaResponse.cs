﻿using CommonControl.Common;
using CommonControl.POCO.Class;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CommonControl.POCO.Response
{
    public class SearchBusinessPartnerCriteriaResponse : IResponse
    {
        private List<BusinessPartnerCriteria> response;

        public List<BusinessPartnerCriteria> Response { get => response; set => response = value; }
    }
}
