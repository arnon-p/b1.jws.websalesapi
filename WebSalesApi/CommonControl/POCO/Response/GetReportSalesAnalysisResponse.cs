﻿using CommonControl.Common;
using CommonControl.POCO.Class;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CommonControl.POCO.Response
{
    public class GetReportSalesAnalysisResponse : IResponse
    {
        private List<SalesAnalysisReport> response;

        public List<SalesAnalysisReport> Response { get => response; set => response = value; }
    }
}
