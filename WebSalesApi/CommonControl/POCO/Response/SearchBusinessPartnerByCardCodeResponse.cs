﻿using CommonControl.Common;
using CommonControl.POCO.Class;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CommonControl.POCO.Response
{
    public class SearchBusinessPartnerByCardCodeResponse : IResponse
    {
        private BusinessPartner response;

        public BusinessPartner Response { get => response; set => response = value; }
    }
}
