﻿using CommonControl.Common;
using CommonControl.POCO.Class;
using CommonControl.POCO.Condition;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CommonControl.POCO.Request
{
    public class CustomerPaymentReportRequest : IRequest
    {
        private ReportCustomerPaymentCondition condition;

        public ReportCustomerPaymentCondition Condition { get => condition; set => condition = value; }
    }
}
