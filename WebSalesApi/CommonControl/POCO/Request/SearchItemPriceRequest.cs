﻿using CommonControl.Common;
using CommonControl.POCO.Condition;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CommonControl.POCO.Request
{
    public class SearchItemPriceRequest : IRequest
    {
        private ItemPriceCondition condition;

        public ItemPriceCondition Condition { get => condition; set => condition = value; }
    }
}
