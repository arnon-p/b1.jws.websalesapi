﻿using CommonControl.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CommonControl.POCO.Request
{
    public class ConstantCompanyRequest : IRequest
    {
        private string company;

        public string Company { get => company; set => company = value; }
    }
}
