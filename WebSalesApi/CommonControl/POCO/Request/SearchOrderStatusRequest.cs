﻿using CommonControl.Common;
using CommonControl.POCO.Condition;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CommonControl.POCO.Request
{
    public class SearchOrderStatusRequest : IRequest
    {
        private ReportOrderStatusCondition condition;

        public ReportOrderStatusCondition Condition { get => condition; set => condition = value; }
    }
}
