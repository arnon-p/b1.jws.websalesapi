﻿using CommonControl.Common;
using CommonControl.POCO.Class;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CommonControl.POCO.Request
{
    public class BusinessPartnerRequest : IRequest
    {
        private BusinessPartner data;

        public BusinessPartner Data { get => data; set => data = value; }
    }
}
