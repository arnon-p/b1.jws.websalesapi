﻿using CommonControl.Common;
using CommonControl.POCO.Condition;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CommonControl.POCO.Request
{
    public class BatchNumberTransactionReportRequest : IRequest
    {
        private ReportBatchNumberTransactionCondition condition;

        public ReportBatchNumberTransactionCondition Condition { get => condition; set => condition = value; }
    }
}
