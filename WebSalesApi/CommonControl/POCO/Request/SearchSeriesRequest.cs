﻿using CommonControl.Common;
using CommonControl.POCO.Condition;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CommonControl.POCO.Request
{
    public class SearchSeriesRequest : IRequest
    {
        private SeriesCondition condition;

        public SeriesCondition Condition { get => condition; set => condition = value; }
    }
}
