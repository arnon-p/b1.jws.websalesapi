﻿using CommonControl.Common;
using CommonControl.POCO.Condition;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CommonControl.POCO.Request
{
    public class SearchSalesPriceHistoryRequest : IRequest
    {
        private SalesPriceHistoryCondition condition;

        public SalesPriceHistoryCondition Condition { get => condition; set => condition = value; }
    }
}
