﻿using CommonControl.Common;
using CommonControl.POCO.Class;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CommonControl.POCO.Request
{
    public class SalesOrderRequest : IRequest
    {
        private List<SalesOrder> data;

        public List<SalesOrder> Data { get => data; set => data = value; }
    }
}
