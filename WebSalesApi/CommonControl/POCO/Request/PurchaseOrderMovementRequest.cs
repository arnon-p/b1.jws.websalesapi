﻿using CommonControl.Common;
using CommonControl.POCO.Class;
using CommonControl.POCO.Condition;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CommonControl.POCO.Request
{
    public class PurchaseOrderMovementRequest : IRequest
    {
        private ReportPurchaseOrderMovementCondition condition;

        public ReportPurchaseOrderMovementCondition Condition { get => condition; set => condition = value; }
    }
}
