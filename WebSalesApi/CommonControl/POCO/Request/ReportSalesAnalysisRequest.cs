﻿using CommonControl.Common;
using CommonControl.POCO.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CommonControl.POCO.Request
{
    public class ReportSalesAnalysisRequest : IRequest
    {
        private SalesAnalysisCondition condition;

        public SalesAnalysisCondition Condition { get => condition; set => condition = value; }
    }
}
