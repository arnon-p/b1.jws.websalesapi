﻿using CommonControl.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CommonControl.POCO.Request
{
    public class DeleteBrochureRequest : IRequest
    {
        private string fileName;

        public string FileName { get => fileName; set => fileName = value; }
    }
}
