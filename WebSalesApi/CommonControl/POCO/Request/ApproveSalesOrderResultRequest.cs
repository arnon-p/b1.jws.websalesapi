﻿using CommonControl.Common;
using CommonControl.POCO.Class;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CommonControl.POCO.Request
{
    public class ApproveSalesOrderResultRequest : IRequest
    {
        private List<ApproveSalesOrderResult> data;

        public List<ApproveSalesOrderResult> Data { get => data; set => data = value; }
    }
}
