﻿using B1Utility.Utils;
using CommonControl.Common;
using CommonControl.Constants;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CommonControl.Utils
{
    public static class MessageResponseUtils
    {
        public static bool checkMessageSuccess(Message msg)
        {
            bool res = true;

            if (msg.Success)
            {
                if (CollectionUtils.IsAny(msg.ChildMessage))
                {
                    foreach (Message msgChild in msg.ChildMessage)
                    {
                        if (msgChild.Success)
                        {
                            res = checkMessageSuccess(msgChild);
                        }
                        else
                        {
                            res = false;
                            break;
                        }
                    }
                }
            }
            else
            {
                res = false;
            }

            return res;
        }

        public static string getLastMessageError(Message msg)
        {
            string res = String.Empty;

            if (msg.Success)
            {
                if (CollectionUtils.IsAny(msg.ChildMessage))
                {
                    foreach (Message msgChild in msg.ChildMessage)
                    {
                        if (msgChild.Success)
                        {
                            res = getLastMessageError(msgChild);
                        }
                        else
                        {
                            res = String.Format(ServiceConstants.ERROR_MESSAGE_DETAIL, msgChild.RefKey, msgChild.ErrorMessage);
                            break;
                        }
                    }
                }
            }
            else
            {
                res = msg.ErrorMessage;
            }

            return res;
        }

        public static string getLastMessageError(List<Message> msgList)
        {
            string res = String.Empty;

            foreach (Message msg in msgList)
            {
                string msgError = getLastMessageError(msg);

                if (!StringUtils.isBlankOrNull(msgError))
                {
                    res = msgError;
                    break;
                }
            }

            return res;
        }
    }
}
