﻿using CommonControl.POCO.Request;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using System.Web.Http.Cors;
using WebSalesApi.Models.ActionImpl;

namespace WebSalesApi.Controllers
{
    [EnableCors(origins: "*", headers: "Content-Type, Accept, a-token, Authorization", methods: "GET, POST")]
    [RoutePrefix("api/v1/jws/master")]
    public class MasterController : ApiController
    {
        [HttpPost]
        [Route("searchItemMasterByCondition")]
        public HttpResponseMessage searchItemMasterByCondition([FromBody] SearchItemMasterRequest req)
        {
            return new SearchItemMasterByConditionImpl().startService(req, Request);
        }

        [HttpPost]
        [Route("searchItemMasterByItemCode")]
        public HttpResponseMessage searchItemMasterByItemCode([FromBody] SearchItemMasterRequest req)
        {
            return new SearchItemMasterByItemCodeImpl().startService(req, Request);
        }

        [HttpPost]
        [Route("searchItemOnHandByItemCode")]
        public HttpResponseMessage searchItemOnHandByItemCode([FromBody] SearchItemMasterRequest req)
        {
            return new SearchItemOnHandByItemCodeImpl().startService(req, Request);
        }

        [HttpPost]
        [Route("searchBusinessPartnerByCondition")]
        public HttpResponseMessage searchBusinessPartnerByCondition([FromBody] SearchBusinessPartnerRequest req)
        {
            return new SearchBusinessPartnerByConditionImpl().startService(req, Request);
        }

        [HttpPost]
        [Route("searchBusinessPartnerByCardCode")]
        public HttpResponseMessage searchBusinessPartnerByCardCode([FromBody] SearchBusinessPartnerRequest req)
        {
            return new SearchBusinessPartnerByCardCodeImpl().startService(req, Request);
        }

        [HttpPost]
        [Route("searchBusinessPartnerAddressByCardCode")]
        public HttpResponseMessage searchBusinessPartnerAddressByCardCode([FromBody] SearchBusinessPartnerRequest req)
        {
            return new SearchBusinessPartnerAddressByCardCodeImpl().startService(req, Request);
        }

        [HttpPost]
        [Route("searchBusinessPartnerContactByCardCode")]
        public HttpResponseMessage searchBusinessPartnerContactByCardCode([FromBody] SearchBusinessPartnerRequest req)
        {
            return new SearchBusinessPartnerContactByCardCodeImpl().startService(req, Request);
        }

        [HttpPost]
        [Route("createBusinessPartner")]
        public HttpResponseMessage createBusinessPartner([FromBody] BusinessPartnerRequest req)
        {
            return new CreateBusinessPartnerImpl().startService(req, Request);
        }

        [HttpPost]
        [Route("updateBusinessPartner")]
        public HttpResponseMessage updateBusinessPartner([FromBody] BusinessPartnerRequest req)
        {
            return new UpdateBusinessPartnerImpl().startService(req, Request);
        }

        [HttpPost]
        [Route("searchBusinessPartnerCriteria")]
        public HttpResponseMessage searchBusinessPartnerCriteria([FromBody] SearchBusinessPartnerRequest req)
        {
            return new SearchBusinessPartnerCriteriaImpl().startService(req, Request);
        }

        [HttpPost]
        [Route("searchItemMasterCriteria")]
        public HttpResponseMessage searchItemMasterCriteria([FromBody] SearchItemMasterRequest req)
        {
            return new SearchItemMasterCriteriaImpl().startService(req, Request);
        }
    }
}
