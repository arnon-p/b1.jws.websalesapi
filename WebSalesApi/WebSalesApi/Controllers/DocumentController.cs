﻿using CommonControl.POCO.Request;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using System.Web.Http.Cors;
using WebSalesApi.Models.ActionImpl;

namespace ISS_PosWebApi.Controllers
{
    [EnableCors(origins: "*", headers: "Content-Type, Accept, a-token, Authorization", methods: "GET, POST")]
    [RoutePrefix("api/v1/jws/document")]
    public class DocumentController : ApiController
    {
        [HttpPost]
        [Route("searchBusinessPartnerForSO")]
        public HttpResponseMessage searchBusinessPartnerForSO([FromBody] SearchBusinessPartnerRequest req)
        {
            return new SearchBusinessPartnerForSOImpl().startService(req, Request);
        }

        [HttpPost]
        [Route("searchItemMasterForSO")]
        public HttpResponseMessage searchItemMasterForSO([FromBody] SearchItemMasterRequest req)
        {
            return new SearchItemMasterForSOImpl().startService(req, Request);
        }

        [HttpPost]
        [Route("searchItemPrice")]
        public HttpResponseMessage searchItemPrice([FromBody] SearchItemPriceRequest req)
        {
            return new SearchItemPriceImpl().startService(req, Request);
        }

        [HttpPost]
        [Route("searchItemBatchNumberSelection")]
        public HttpResponseMessage searchItemBatchNumberSelection([FromBody] SearchItemMasterRequest req)
        {
            return new SearchItemBatchNumberSelectionImpl().startService(req, Request);
        }

        [HttpPost]
        [Route("searchItemWhsOnHand")]
        public HttpResponseMessage searchItemWhsOnHand([FromBody] SearchItemMasterRequest req)
        {
            return new SearchItemWhsOnHandImpl().startService(req, Request);
        }

        [HttpPost]
        [Route("createSalesOrder")]
        public HttpResponseMessage createSalesOrder([FromBody] SalesOrderRequest req)
        {
            return new CreateSalesOrderImpl().startService(req, Request);
        }

        [HttpPost]
        [Route("updateSalesOrder")]
        public HttpResponseMessage updateSalesOrder([FromBody] SalesOrderRequest req)
        {
            return new UpdateSalesOrderImpl().startService(req, Request);
        }

        [HttpPost]
        [Route("searchSeriesByCondition")]
        public HttpResponseMessage searchSeriesByCondition([FromBody] SearchSeriesRequest req)
        {
            return new SearchSeriesByConditionImpl().startService(req, Request);
        }

        [HttpPost]
        [Route("searchSalesOrderByCondition")]
        public HttpResponseMessage searchSalesOrderByCondition([FromBody] SearchSalesOrderRequest req)
        {
            return new SearchSalesOrderByConditionImpl().startService(req, Request);
        }

        [HttpPost]
        [Route("searchSalesOrderByDocEntry")]
        public HttpResponseMessage searchSalesOrderByDocEntry([FromBody] SearchSalesOrderRequest req)
        {
            return new SearchSalesOrderByDocEntryImpl().startService(req, Request);
        }

        [HttpPost]
        [Route("searchSalesOrderDocumentRelation")]
        public HttpResponseMessage searchSalesOrderDocumentRelation([FromBody] SearchSalesOrderRequest req)
        {
            return new SearchSalesOrderDocumentRelationImpl().startService(req, Request);
        }

        [HttpPost]
        [Route("searchOrderStatusByCondition")]
        public HttpResponseMessage searchOrderStatusByCondition([FromBody] SearchOrderStatusRequest req)
        {
            return new SearchOrderStatusByConditionImpl().startService(req, Request);
        }

        [HttpPost]
        [Route("searchApprovalSalesOrderByCondition")]
        public HttpResponseMessage searchApprovalSalesOrderByCondition([FromBody] ApproveSalesOrderRequest req)
        {
            return new SearchApprovalSalesOrderByConditionImpl().startService(req, Request);
        }

        [HttpPost]
        [Route("approveSalesOrder")]
        public HttpResponseMessage approveSalesOrder([FromBody] ApproveSalesOrderResultRequest req)
        {
            return new ApproveSalesOrderImpl().startService(req, Request);
        }

        [HttpPost]
        [Route("searchSalesPriceHistoryByCondition")]
        public HttpResponseMessage searchSalesPriceHistoryByCondition([FromBody] SearchSalesPriceHistoryRequest req)
        {
            return new SearchSalesPriceHistoryByConditionImpl().startService(req, Request);
        }

        [HttpPost]
        [Route("searchGrossProfitByCondition")]
        public HttpResponseMessage searchGrossProfitByCondition([FromBody] GrossProfitRequest req)
        {
            return new SearchGrossProfitByConditionImpl().startService(req, Request);
        }
    }
}
