﻿using B1Utility.Utils;
using CommonControl.Common;
using CommonControl.POCO.Class;
using CommonControl.POCO.Common;
using CommonControl.POCO.Request;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.Http.Cors;
using WebSalesApi.Models.ActionImpl;
using WebSalesApi.Models.Utils;

namespace WebSalesApi.Controllers
{
    [EnableCors(origins: "*", headers: "Content-Type, Accept, a-token, Authorization", methods: "GET, POST")]
    [RoutePrefix("api/v1/jws/common")]
    public class CommonController : ApiController
    {
        [HttpPost]
        [Route("login")]
        public HttpResponseMessage login([FromBody] LoginRequest req)
        {
            return new LoginImpl().startService(req, Request, true);
        }

        [HttpPost]
        [Route("logout")]
        public HttpResponseMessage logout()
        {
            return new LogoutImpl().startService(null, Request);
        }

        [HttpPost]
        [Route("users")]
        public HttpResponseMessage users()
        {
            return Request.CreateResponse(HttpStatusCode.OK, UserUtils.Users);
        }

        [HttpPost]
        [Route("getConstant")]
        public HttpResponseMessage getConstant([FromBody] IRequest req)
        {
            return new GetConstantImpl().startService(req, Request, true);
        }

        [HttpPost]
        [Route("getConstantCompany")]
        public HttpResponseMessage getConstantCompany([FromBody] ConstantCompanyRequest req)
        {
            return new GetConstantCompanyImpl().startService(req, Request);
            //var response = new GetConstantCompanyImpl().startService(req, Request);
            //return Task.FromResult(response);
        }

        [HttpPost]
        [Route("getConstantCompanyBigData")]
        public HttpResponseMessage getConstantCompanyBigData([FromBody] ConstantCompanyRequest req)
        {
            return new GetConstantCompanyBigDataImpl().startService(req, Request);
            //var response = new GetConstantCompanyBigDataImpl().startService(req, Request);
            //return Task.FromResult(response);
        }

        [HttpGet]
        [Route("getAttachmentFiles/{company}/{fileName}")]
        public HttpResponseMessage getFiles(string company, string fileName)
        {
            return WebServiceUtils.Instance.getFiles(company, fileName, Request);
        }

        [HttpGet]
        [Route("getTempFiles/{tempFileName}")]
        public HttpResponseMessage getTempFiles(string tempFileName)
        {
            return WebServiceUtils.Instance.getTempFiles(tempFileName, Request);
        }

        [HttpGet]
        [Route("getReportFiles/{reportFileName}")]
        public HttpResponseMessage getReportFiles(string reportFileName)
        {
            return WebServiceUtils.Instance.getReportFiles(reportFileName, Request);
        }

        [HttpPost]
        [Route("getBrochure")]
        public HttpResponseMessage getBrochure([FromBody] IRequest req)
        {
            return new GetBrochureImpl().startService(req, Request);
        }

        [HttpGet]
        [Route("getBrochureFiles/{company}/{brochureFileName}")]
        public HttpResponseMessage getBrochureFiles(string company, string brochureFileName)
        {
            return WebServiceUtils.Instance.getBrochureFiles(company, brochureFileName, Request);
        }

        [HttpPost]
        [Route("deleteBrochure")]
        public HttpResponseMessage deleteBrochure([FromBody] DeleteBrochureRequest req)
        {
            return new DeleteBrochureImpl().startService(req, Request);
        }

        [HttpPost]
        [Route("upload")]
        public HttpResponseMessage upload()
        {
            var httpRequest = HttpContext.Current.Request;
            if (!Request.Content.IsMimeMultipartContent())
            {
                return Request.CreateResponse(HttpStatusCode.UnsupportedMediaType);
            }

            string filePath = String.Empty;
            AttachInfo upload = new AttachInfo();
            if (httpRequest.Files.Count > 0)
            {
                string tempKey = StringUtils.randomAlphaNumeric(10);
                var postedFile = httpRequest.Files[0];
                upload.FileName = postedFile.FileName;
                upload.Name = Path.GetFileNameWithoutExtension(upload.FileName);
                upload.Ext = Path.GetExtension(upload.FileName).Substring(1);
                upload.Date = DateTime.Today;
                upload.TempKey = tempKey + Path.GetExtension(upload.FileName);
                upload.NewFileName = String.Format("{0}-{1}", tempKey, upload.FileName);
                upload.NewName = Path.GetFileNameWithoutExtension(upload.NewFileName);
                filePath = Path.Combine(ProjectConstantUtils.TempPath, upload.TempKey);

                if (!Directory.Exists(ProjectConstantUtils.TempPath))
                {
                    Directory.CreateDirectory(ProjectConstantUtils.TempPath);
                }

                postedFile.SaveAs(filePath);
            }

            return Request.CreateResponse(HttpStatusCode.OK, upload);
        }

        [HttpPost]
        [Route("uploadBrochure")]
        public HttpResponseMessage uploadBrochure()
        {
            var httpRequest = HttpContext.Current.Request;
            if (!Request.Content.IsMimeMultipartContent())
            {
                return Request.CreateResponse(HttpStatusCode.UnsupportedMediaType);
            }

            string token = String.Empty;
            var headers = Request.Headers;
            if (headers.Contains("a-token"))
            {
                token = headers.GetValues("a-token").First();
            }

            WebAdminUserObj userObj = UserUtils.validateToken(token);

            string filePath = String.Empty;
            AttachInfo upload = new AttachInfo();
            if (userObj != null)
            {
                string compPath = Path.Combine(ProjectConstantUtils.BrochurePath, userObj.Company);
                if (httpRequest.Files.Count > 0)
                {
                    var postedFile = httpRequest.Files[0];
                    upload.FileName = postedFile.FileName;
                    upload.Name = Path.GetFileNameWithoutExtension(upload.FileName);
                    upload.Ext = Path.GetExtension(upload.FileName).Substring(1);
                    upload.Date = DateTime.Today;
                    filePath = Path.Combine(compPath, postedFile.FileName);

                    if (!Directory.Exists(compPath))
                    {
                        Directory.CreateDirectory(compPath);
                    }

                    postedFile.SaveAs(filePath);
                }
            }

            return Request.CreateResponse(HttpStatusCode.OK, upload);
        }

        [HttpPost]
        [Route("testSendMail")]
        public HttpResponseMessage testSendMail([FromBody] IRequest req)
        {
            EmailMessageInfo mail = new EmailMessageInfo();
            mail.Subject = "Test send by Web Sales";
            mail.Body = "This email send by Web Sales";
            //mail.Mailto = new List<string>();
            mail.Email = ProjectConstantUtils.EmailSenderInfo.EmailUser;
            //mail.Mailto.Add(ProjectConstantUtils.EmailSenderInfo.EmailUser);
            MailUtils.sendEmail(mail);

            return Request.CreateResponse(HttpStatusCode.OK, mail);
        }

        [HttpGet]
        [Route("testMail")]
        public HttpResponseMessage testMail()
        {
            string msg = "send email success.";
            try
            {
                EmailMessageInfo mail = new EmailMessageInfo();
                mail.Subject = "Test send by Web Sales";
                mail.Body = "This email send by Web Sales";
                //mail.Mailto = new List<string>();
                mail.Email = ProjectConstantUtils.EmailSenderInfo.EmailUser;
                //mail.Mailto.Add(ProjectConstantUtils.EmailSenderInfo.EmailUser);
                MailUtils.sendEmail(mail);
            }
            catch (Exception ex)
            {
                msg = ex.Message;
            }

            return Request.CreateResponse(HttpStatusCode.OK, msg);
        }

        [HttpGet]
        [Route("sessionDestroy/{userName}")]
        public HttpResponseMessage sessionDestroy(string userName)
        {
            if (UserUtils.Users.ContainsKey(userName))
            {
                UserUtils.Users.Remove(userName);
                return Request.CreateResponse(HttpStatusCode.OK, String.Format("User {0} session destroyed.", userName));
            }
            else
            {
                return Request.CreateResponse(HttpStatusCode.OK, String.Format("User {0} not found.", userName));
            }

        }
    }
}
