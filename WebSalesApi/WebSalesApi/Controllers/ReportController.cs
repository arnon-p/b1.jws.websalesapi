﻿using B1Utility.DataAccess;
using CommonControl.POCO.Request;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using System.Web.Http.Cors;
using WebSalesApi.Models.ActionImpl;
using WebSalesApi.Models.POCO;
using WebSalesApi.Models.Utils;

namespace WebSalesApi.Controllers
{
    [EnableCors(origins: "*", headers: "Content-Type, Accept, a-token, Authorization", methods: "GET, POST")]
    [RoutePrefix("api/v1/jws/report")]
    public class ReportController : ApiController
    {
        [HttpPost]
        [Route("getReportInventoryStatus")]
        public HttpResponseMessage getReportInventoryStatus([FromBody] InventoryStatusRequest req)
        {
            return new GetReportInventoryStatusImpl().startService(req, Request);
        }

        [HttpPost]
        [Route("getReportInventoryStatusDocument")]
        public HttpResponseMessage getReportInventoryStatusDocument([FromBody] InventoryStatusRequest req)
        {
            return new GetReportInventoryStatusDocumentImpl().startService(req, Request);
        }

        [HttpPost]
        [Route("getReportBathNumberTransaction")]
        public HttpResponseMessage getReportBathNumberTransaction([FromBody] BatchNumberTransactionReportRequest req)
        {
            return new GetReportBathNumberTransactionImpl().startService(req, Request);
        }

        [HttpPost]
        [Route("getReportBathNumberTransactionDetail")]
        public HttpResponseMessage getReportBathNumberTransactionDetail([FromBody] BatchNumberTransactionReportDetailRequest req)
        {
            return new GetReportBathNumberTransactionDetailImpl().startService(req, Request);
        }

        [HttpPost]
        [Route("getReportCustomerPayment")]
        public HttpResponseMessage getReportCustomerPayment([FromBody] CustomerPaymentReportRequest req)
        {
            // Aging
            return new GetReportCustomerPaymentImpl().startService(req, Request);
        }                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                     

        [HttpPost]
        [Route("getReportPurchaseOrderMovement")]
        public HttpResponseMessage getReportPurchaseOrderMovement([FromBody] PurchaseOrderMovementRequest req)
        {
            return new GetReportPurchaseOrderMovementImpl().startService(req, Request);
        }

        [HttpPost]
        [Route("getReportPurchaseOrderMovementCost")]
        public HttpResponseMessage getReportPurchaseOrderMovementCost([FromBody] PurchaseOrderMovementRequest req)
        {
            return new GetReportPurchaseOrderMovementCostImpl().startService(req, Request);
        }

        [HttpPost]
        [Route("getReportSalesAnalysis")]
        public HttpResponseMessage getReportSalesAnalysis([FromBody] ReportSalesAnalysisRequest req)
        {
            return new GetReportSalesAnalysisImpl().startService(req, Request);
        }

        [HttpPost]
        [Route("getReportOnHand")]
        public HttpResponseMessage getReportOnHand([FromBody] ReportOnHandRequest req)
        {
            return new GetReportOnHandImpl().startService(req, Request);
        }

        [HttpPost]
        [Route("getReportOnHandCost")]
        public HttpResponseMessage getReportOnHandCost([FromBody] ReportOnHandRequest req)
        {
            return new GetReportOnHandCostImpl().startService(req, Request);
        }

        [HttpPost]
        [Route("getReportSalesBackorder")]
        public HttpResponseMessage getReportSalesBackorder([FromBody] SalesBackOrderRequest req)
        {
            return new GetReportSalesBackorderImpl().startService(req, Request);
        }

        [HttpPost]
        [Route("getReportSalesOrder")]
        public HttpResponseMessage getReportSalesOrder([FromBody] ReportSalesOrderRequest req)
        {
            return new GetReportSalesOrderImpl().startService(req, Request);
        }

        [HttpPost]
        [Route("getReportTest")]
        public HttpResponseMessage getReportTest([FromBody] ReportSalesOrderRequest req)
        {
            string res = String.Empty;
            try
            {
                CrystalReportOptions rptOption = new CrystalReportOptions();
                rptOption.DbUsername = ServiceDataAccess.Instance.CompanyDic[req.Condition.Company].CompanyInfo.DbUserName;
                rptOption.DbPassword = ServiceDataAccess.Instance.CompanyDic[req.Condition.Company].CompanyInfo.DbPassword;
                rptOption.ExportFormatType = CrystalDecisions.Shared.ExportFormatType.PortableDocFormat;
                rptOption.RptPath = ProjectConstantUtils.getCrystalReportPath(req.Condition.Company, "test", "test.rpt");

                rptOption.ParamValues = new List<ParamValue>();
                rptOption.ParamValues.Add(new ParamValue("DocKey@", req.Condition.DocEntry));

                string fileName = ReportUtils.getFileNameExport("test", rptOption.ExportFormatType);
                rptOption.DestFilePath = Path.Combine(ProjectConstantUtils.ReportPath, fileName);

                ReportUtils.saveToFile(rptOption);

                res = fileName;
            }
            catch(Exception ex)
            {
                res = ex.Message;
            }

            return Request.CreateResponse(HttpStatusCode.OK, res);
        }
    }
}
