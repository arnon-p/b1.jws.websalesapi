﻿using B1Utility.Constants;
using B1Utility.DataAccess;
using B1Utility.Utils;
using Newtonsoft.Json.Converters;
using Newtonsoft.Json.Serialization;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Http;
using WebSalesApi.Models.Converter;
using WebSalesApi.Models.Utils;

namespace WebSalesApi
{
    public static class WebApiConfig
    {
        public static void Register(HttpConfiguration config)
        {
            config.EnableCors();

            config.MapHttpAttributeRoutes();

            config.EnsureInitialized();

            config.Routes.MapHttpRoute(
                name: "DefaultApi",
                routeTemplate: "api/{controller}/{id}",
                defaults: new { id = RouteParameter.Optional }
            );

            // Uncomment the following line of code to enable query support for actions with an IQueryable or IQueryable<T> return type.
            // To avoid processing unexpected or malicious queries, use the validation settings on QueryableAttribute to validate incoming queries.
            // For more information, visit http://go.microsoft.com/fwlink/?LinkId=279712.
            //config.EnableQuerySupport();

            // To disable tracing in your application, please comment out or remove the following line of code
            // For more information, refer to: http://www.asp.net/web-api
            config.EnableSystemDiagnosticsTracing();

            config.Formatters.JsonFormatter.SerializerSettings.Converters.Add(new CustomDateTimeConverter());

            config.Formatters.JsonFormatter.SerializerSettings.Converters.Add(new StringEnumConverter(new CamelCaseNamingStrategy()));
            //config.Formatters.JsonFormatter.SerializerSettings.Converters.Add(new StringEnumConverter { CamelCaseText = false });

            config.Formatters.JsonFormatter.SerializerSettings.ContractResolver = new CamelCasePropertyNamesContractResolver();

            config.Formatters.JsonFormatter.UseDataContractJsonSerializer = false;

            ServiceDataAccess.Instance.loadConfig(HttpContext.Current.Server.MapPath("~/"));

            B1DataAccess.Instance.LogPath = StringUtils.format(ServiceConstants.FILE_PATH_BASE, HttpContext.Current.Server.MapPath("~/"));

            ProjectConstantUtils.initCommon(ConfigurationManager.AppSettings["SecurityTokenApi"]);

            ProjectConstantUtils.CrystalReportPath = HttpContext.Current.Server.MapPath("~/Resources/report/");
            ProjectConstantUtils.TempPath = HttpContext.Current.Server.MapPath("~/Resources/temp/upload/");
            ProjectConstantUtils.ReportPath = HttpContext.Current.Server.MapPath("~/Resources/temp/report/");
            ProjectConstantUtils.AttachPath = HttpContext.Current.Server.MapPath("~/Resources/temp/attach/");
            ProjectConstantUtils.BrochurePath = HttpContext.Current.Server.MapPath("~/Resources/brochure/");
            ProjectConstantUtils.WebAdminAuthenApi = ConfigurationManager.AppSettings["WebAdminAuthenApi"];
            ProjectConstantUtils.WebSalesUrl = ConfigurationManager.AppSettings["WebSalesUrl"];

            // TODO Query from DB
            ProjectConstantUtils.EmailSenderInfo = new CommonControl.POCO.Common.EmailServerInfo(ConfigurationManager.AppSettings["SmtpHost"], Convert.ToInt32(ConfigurationManager.AppSettings["SmtpPort"]), ConfigurationManager.AppSettings["EmailUser"], ConfigurationManager.AppSettings["EmailPass"]);

            //System.Net.ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
        }
    }
}
