﻿using B1Utility.DataAccess;
using B1Utility.Enum;
using B1Utility.Utils;
using CommonControl.Common;
using CommonControl.Constants;
using CommonControl.Enum;
using WebSalesApi.Models.Utils;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Diagnostics;
using System.Linq;
using System.Net.Http;
using System.Web;
using CommonControl.POCO.Class;
using CommonControl.POCO.Common;

namespace WebSalesApi.Models.ServiceBaseImpl
{
    public abstract class WebServiceBase
    {
        protected string randomKey;
        protected string logPath;
        protected WebAdminUserObj userObj;

        public HttpResponseMessage startService(IRequest obj, HttpRequestMessage request, bool skipAuthen = false)
        {
            IResponse response = new IResponse();
            Stopwatch stopWatch = new Stopwatch();
            stopWatch.Start();
            
            try
            {
                logPath = B1DataAccess.Instance.LogPath;
                randomKey = StringUtils.randomAlphaNumeric(6);

                if (StringUtils.isBlankOrNull(ProjectConstantUtils.BasePath))
                {
                    ProjectConstantUtils.BasePath = request.RequestUri.GetLeftPart(UriPartial.Authority);
                }

                ResponseStatus status = authenUser(obj, request, skipAuthen);

                if (ResponseStatus.SUCCESS.Equals(status))
                {
                    // log
                    logImplement(obj, ServiceActionType.REQUEST);

                    object reqToObj = requestToObject(obj);

                    if (validateObject(reqToObj))
                    {
                        response = implement(reqToObj);

                        if (ResponseStatus.NONE.Equals(response.Status))
                        {
                            response.Status = ResponseStatus.SUCCESS;
                        }

                        afterImplement(response);
                    }
                    else
                    {
                        response.Status = ResponseStatus.INVALID;
                    }

                    // log
                    logImplement(response, ServiceActionType.RESPONSE);
                }
                else
                {
                    response.Status = status;
                }
            }
            catch (Exception ex)
            {
                response.Status = ResponseStatus.ERROR;
                response.ErrorMessage = String.Format(ServiceConstants.ERROR_MESSAGE_DETAIL, ex.Message, ex.StackTrace);
                FileUtils.writeLogService(ServiceConstants.INTERNAL_ERROR, logPath, response, ServiceActionType.ERROR, randomKey);
                response.ErrorMessage = ex.Message;

                try
                {
                    EmailMessageInfo email = new EmailMessageInfo();
                    email.Subject = "(Error) Web Sales";
                    email.Body = String.Format(ServiceConstants.ERROR_MESSAGE_DETAIL_DATA, ex.Message, ex.StackTrace, JsonUtils.toJson(obj));
                    email.Email = ServiceConstants.EMAIL_ADMIN;
                    email.Mailto = new List<string>();
                    email.Mailto.Add(ProjectConstantUtils.EmailSenderInfo.EmailUser);
                    MailUtils.sendEmail(email, true);
                } catch { }
            }

            stopWatch.Stop();
            TimeSpan ts = stopWatch.Elapsed;
            response.ResponseTime = ts.Milliseconds;

            return WebServiceUtils.Instance.createResponse(request, response);
        }

        private ResponseStatus authenUser(IRequest obj, HttpRequestMessage request, bool skipAuthen)
        {
            ResponseStatus status = ResponseStatus.AUTH_NOTPASS;

            if (skipAuthen)
            {
                status = ResponseStatus.SUCCESS;
            }
            else
            {
                string token = String.Empty;

                var headers = request.Headers;
                if (headers.Contains("a-token"))
                {
                    token = headers.GetValues("a-token").First();
                }

                if (ProjectConstantUtils.SecurityTokenApi.Equals(token))
                {
                    status = ResponseStatus.SUCCESS;
                }
                else
                {
                    userObj = UserUtils.validateToken(token);

                    if (userObj == null)
                    {
                        status = ResponseStatus.AUTH_NOTPASS;
                    }
                    else
                    {
                        status = ResponseStatus.SUCCESS;
                    }
                }

                //if (ProjectConstantUtils.SecurityTokenApi.Equals(token))
                //{
                //    status = ResponseStatus.SUCCESS;
                //}
            }

            return status;
        }

        protected abstract object requestToObject(IRequest request);
        protected abstract bool validateObject(object request);
        protected abstract IResponse implement(object obj);
        protected abstract void afterImplement(IResponse obj);
        protected abstract void logImplement(object obj, ServiceActionType serviceActionType);
    }
}