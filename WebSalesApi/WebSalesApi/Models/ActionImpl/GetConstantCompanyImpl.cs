﻿using B1Utility.DataAccess;
using B1Utility.Enum;
using B1Utility.POCO;
using B1Utility.Utils;
using CommonControl.Common;
using CommonControl.POCO.Common;
using CommonControl.POCO.Condition;
using CommonControl.POCO.Request;
using CommonControl.POCO.Response;
using FrontControl.Lookup;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WebSalesApi.Models.ServiceBaseImpl;

namespace WebSalesApi.Models.ActionImpl
{
    public class GetConstantCompanyImpl : WebServiceBase
    {
        protected override object requestToObject(IRequest request)
        {
            return request;
        }

        protected override bool validateObject(object request)
        {
            ConstantCompanyRequest req = (ConstantCompanyRequest)request;
            return !StringUtils.isBlankOrNull(req.Company);
        }

        protected override IResponse implement(object obj)
        {
            ConstantCompanyRequest req = (ConstantCompanyRequest)obj;

            GetConstantCompanyResponse response = new GetConstantCompanyResponse();
            // TODO CR Authorize get SalesEmployee from userObj.SapProfile.SalesEmployee
            response.SalesEmployee = userObj.SapProfile.SalesEmployee;
            //response.SalesEmployee = LookupUtils.Instance.getCommonServiceImpl().getSalesEmployee(req.Company);
            response.BpGroup = LookupUtils.Instance.getCommonServiceImpl().getBusinessPartnerGroup(req.Company);
            response.PaymentTerms = LookupUtils.Instance.getCommonServiceImpl().getPaymentTerms(req.Company);
            response.ShippingType = LookupUtils.Instance.getCommonServiceImpl().getShippingType(req.Company);
            response.ItemGroup = LookupUtils.Instance.getCommonServiceImpl().getItemGroup(req.Company);
            response.Whse = LookupUtils.Instance.getCommonServiceImpl().getWhse(req.Company);
            response.Properties = LookupUtils.Instance.getCommonServiceImpl().getProperties(req.Company);
            response.Country = LookupUtils.Instance.getCommonServiceImpl().getCountry(req.Company);
            response.TaxGroup = LookupUtils.Instance.getCommonServiceImpl().getTaxGroup(req.Company);
            response.State = LookupUtils.Instance.getCommonServiceImpl().getState(req.Company);

            List<Dimension> dimList = LookupUtils.Instance.getCommonServiceImpl().getDimension(req.Company);

            if (CollectionUtils.IsAny(dimList))
            {
                response.Dimension1 = dimList.Where(x => x.DimCode.Equals("1")).ToList();
                response.Dimension2 = dimList.Where(x => x.DimCode.Equals("2")).ToList();
                response.Dimension3 = dimList.Where(x => x.DimCode.Equals("3")).ToList();
                response.Dimension4 = dimList.Where(x => x.DimCode.Equals("4")).ToList();
                response.Dimension5 = dimList.Where(x => x.DimCode.Equals("5")).ToList();
            }

            response.AddressType = new List<AddressType>();
            response.AddressType.Add(new AddressType("B", "Bill To"));
            response.AddressType.Add(new AddressType("S", "Ship To"));

            SeriesCondition seriesCond = new SeriesCondition();
            seriesCond.DocDate = DateTimeUtils.getCurrentCompanyDate();
            seriesCond.Company = req.Company;
            response.Series = LookupUtils.Instance.getDocumentServiceImpl().searchSeriesByCondition(seriesCond);

            response.DocumentStatus = new List<DocumentStatus>();
            response.DocumentStatus.Add(new DocumentStatus("O", "Open"));
            response.DocumentStatus.Add(new DocumentStatus("C", "Close"));

            response.PriceList = LookupUtils.Instance.getCommonServiceImpl().getPriceList(req.Company);

            response.ExportType = new List<ExportType>();
            response.ExportType.Add(new ExportType((int)CrystalReportExportFormatType.PDF, CrystalReportExportFormatType.PDF.ToString()));
            response.ExportType.Add(new ExportType((int)CrystalReportExportFormatType.ExcelXLS, CrystalReportExportFormatType.ExcelXLS.ToString()));
            response.ExportType.Add(new ExportType((int)CrystalReportExportFormatType.ExcelXLSX, CrystalReportExportFormatType.ExcelXLSX.ToString()));
            response.ExportType.Add(new ExportType((int)CrystalReportExportFormatType.WordDOC, CrystalReportExportFormatType.WordDOC.ToString()));

            // move to GetConstantCompanyBigData
            //response.UomGroupConverter = LookupUtils.Instance.getCommonServiceImpl().getUomGroupConverter(req.Company);

            response.ApproveStatus = new List<ApproveStatus>();
            response.ApproveStatus.Add(new ApproveStatus("-", "Draft"));
            response.ApproveStatus.Add(new ApproveStatus("W", "Pending"));
            response.ApproveStatus.Add(new ApproveStatus("Y", "Approved"));
            response.ApproveStatus.Add(new ApproveStatus("N", "Rejected"));

            // WGE Constants
            response.Brand = LookupUtils.Instance.getCommonServiceImpl().getBrand(req.Company);

            response.Channel = new List<Channel>();
            response.Channel.Add(new Channel("ID", "ID"));
            response.Channel.Add(new Channel("FS", "FS"));
            response.Channel.Add(new Channel("CS", "CS"));
            response.Channel.Add(new Channel("PE", "PE"));
            response.Channel.Add(new Channel("DL", "DL"));
            response.Channel.Add(new Channel("MK", "MK"));

            // move to GetConstantCompanyBigData
            //response.LocationAddress = LookupUtils.Instance.getCommonServiceImpl().getLocationAddress(req.Company);
            // WGE Constants

            return response;
        }

        protected override void afterImplement(IResponse obj)
        {
        }

        protected override void logImplement(object obj, ServiceActionType serviceActionType)
        {
        }
    }
}