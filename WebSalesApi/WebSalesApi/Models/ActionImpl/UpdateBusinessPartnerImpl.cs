﻿using B1Utility.Enum;
using B1Utility.Utils;
using CommonControl.Common;
using CommonControl.POCO.Class;
using CommonControl.POCO.Request;
using FrontControl.Lookup;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using WebSalesApi.Models.ServiceBaseImpl;
using WebSalesApi.Models.Utils;

namespace WebSalesApi.Models.ActionImpl
{
    public class UpdateBusinessPartnerImpl : WebServiceBase
    {
        protected override object requestToObject(IRequest request)
        {
            BusinessPartnerRequest req = (BusinessPartnerRequest)request;
            return req.Data;
        }

        protected override bool validateObject(object request)
        {
            BusinessPartner condition = (BusinessPartner)request;

            bool res = false;

            if (!StringUtils.isBlankOrNull(condition.Company)
                && !StringUtils.isBlankOrNull(condition.CardCode))
            {
                res = true;
            }

            return res;
        }

        protected override IResponse implement(object obj)
        {
            BusinessPartner bp = (BusinessPartner)obj;

            if (CollectionUtils.IsAny(bp.BpBillList))
            {
                bp.BpBillList.ForEach(x => x.Block = x.Tambon + " " + x.Amphur);
            }

            if (CollectionUtils.IsAny(bp.BpShipList))
            {
                bp.BpShipList.ForEach(x => x.Block = x.Tambon + " " + x.Amphur);
            }

            if (CollectionUtils.IsAny(bp.Attachments))
            {
                foreach (AttachInfo att in bp.Attachments)
                {
                    if (!StringUtils.isBlankOrNull(att.TempKey))
                    {
                        att.SrcPath = ProjectConstantUtils.AttachPath;
                        if (!File.Exists(Path.Combine(ProjectConstantUtils.AttachPath, att.NewFileName)))
                        {
                            File.Copy(Path.Combine(ProjectConstantUtils.TempPath, att.TempKey), Path.Combine(ProjectConstantUtils.AttachPath, att.NewFileName), true);
                        }
                    }
                }
            }

            LookupUtils.Instance.getMasterServiceImpl().updateBusinessPartner(bp);

            return new IResponse();
        }

        protected override void afterImplement(IResponse obj)
        {
        }

        protected override void logImplement(object obj, ServiceActionType serviceActionType)
        {
            FileUtils.writeLogService("UpdateBusinessPartner", logPath, obj, serviceActionType, randomKey);
        }
    }
}