﻿using B1Utility.Enum;
using B1Utility.Utils;
using CommonControl.Common;
using CommonControl.Enum;
using CommonControl.POCO.Class;
using CommonControl.POCO.Common;
using CommonControl.POCO.Condition;
using CommonControl.POCO.Request;
using CommonControl.POCO.Response;
using FrontControl.Lookup;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WebSalesApi.Models.ServiceBaseImpl;

namespace WebSalesApi.Models.ActionImpl
{
    public class SearchSalesOrderByDocEntryImpl : WebServiceBase
    {
        protected override object requestToObject(IRequest request)
        {
            SearchSalesOrderRequest req = (SearchSalesOrderRequest)request;

            return req.Condition;
        }

        protected override bool validateObject(object request)
        {
            SalesOrderCondition condition = (SalesOrderCondition)request;

            bool res = false;

            if (!StringUtils.isBlankOrNull(condition.Company)
                && condition.DocEntry > 0)
            {
                res = true;
            }

            return res;
        }

        protected override IResponse implement(object obj)
        {
            SalesOrderCondition condition = (SalesOrderCondition)obj;

            SalesOrder so = LookupUtils.Instance.getDocumentServiceImpl().searchSalesOrderByDocEntry(condition);

            SearchSalesOrderByDocEntryResponse response = new SearchSalesOrderByDocEntryResponse();
            response.Response = so;

            return response;
        }

        protected override void afterImplement(IResponse obj)
        {
        }

        protected override void logImplement(object obj, ServiceActionType serviceActionType)
        {
        }
    }
}