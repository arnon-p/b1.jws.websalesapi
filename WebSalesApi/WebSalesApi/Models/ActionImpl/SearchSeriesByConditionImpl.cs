﻿using B1Utility.Enum;
using B1Utility.Utils;
using CommonControl.Common;
using CommonControl.Enum;
using CommonControl.POCO.Class;
using CommonControl.POCO.Common;
using CommonControl.POCO.Condition;
using CommonControl.POCO.Request;
using CommonControl.POCO.Response;
using FrontControl.Lookup;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WebSalesApi.Models.ServiceBaseImpl;

namespace WebSalesApi.Models.ActionImpl
{
    public class SearchSeriesByConditionImpl : WebServiceBase
    {
        protected override object requestToObject(IRequest request)
        {
            SearchSeriesRequest req = (SearchSeriesRequest)request;

            return req.Condition;
        }

        protected override bool validateObject(object request)
        {
            SeriesCondition condition = (SeriesCondition)request;

            bool res = false;

            if (!StringUtils.isBlankOrNull(condition.Company))
            {
                res = true;
            }

            return res;
        }

        protected override IResponse implement(object obj)
        {
            SeriesCondition condition = (SeriesCondition)obj;

            List<SeriesDocument> seriesList = LookupUtils.Instance.getDocumentServiceImpl().searchSeriesByCondition(condition);

            SearchSeriesByConditionResponse response = new SearchSeriesByConditionResponse();
            response.Response = seriesList;

            return response;
        }

        protected override void afterImplement(IResponse obj)
        {
        }

        protected override void logImplement(object obj, ServiceActionType serviceActionType)
        {
        }
    }
}