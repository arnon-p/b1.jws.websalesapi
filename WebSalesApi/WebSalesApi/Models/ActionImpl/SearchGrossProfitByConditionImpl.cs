﻿using B1Utility.Enum;
using B1Utility.Utils;
using CommonControl.Common;
using CommonControl.POCO.Class;
using CommonControl.POCO.Condition;
using CommonControl.POCO.Request;
using CommonControl.POCO.Response;
using FrontControl.Lookup;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using WebSalesApi.Models.ServiceBaseImpl;
using WebSalesApi.Models.Utils;

namespace WebSalesApi.Models.ActionImpl
{
    public class SearchGrossProfitByConditionImpl : WebServiceBase
    {
        protected override object requestToObject(IRequest request)
        {
            GrossProfitRequest req = (GrossProfitRequest)request;
            return req.Condition;
        }

        protected override bool validateObject(object request)
        {
            GrossProfitCondition condition = (GrossProfitCondition)request;

            bool res = false;

            if (!StringUtils.isBlankOrNull(condition.Company)
                && condition.DocEntry > 0)
            {
                res = true;
            }

            return res;
        }

        protected override IResponse implement(object obj)
        {
            GrossProfitCondition condition = (GrossProfitCondition)obj;

            List<GrossProfitInfo> gpInfo = LookupUtils.Instance.getDocumentServiceImpl().searchGrossProfitByCondition(condition);

            GrossProfitResponse response = new GrossProfitResponse();
            response.Response = gpInfo;

            return response;
        }

        protected override void afterImplement(IResponse obj)
        {
        }

        protected override void logImplement(object obj, ServiceActionType serviceActionType)
        {
        }
    }
}