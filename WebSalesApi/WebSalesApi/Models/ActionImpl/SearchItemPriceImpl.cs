﻿using B1Utility.Enum;
using B1Utility.Utils;
using CommonControl.Common;
using CommonControl.Enum;
using CommonControl.POCO.Class;
using CommonControl.POCO.Condition;
using CommonControl.POCO.Request;
using CommonControl.POCO.Response;
using FrontControl.Lookup;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WebSalesApi.Models.ServiceBaseImpl;

namespace WebSalesApi.Models.ActionImpl
{
    public class SearchItemPriceImpl : WebServiceBase
    {
        protected override object requestToObject(IRequest request)
        {
            SearchItemPriceRequest req = (SearchItemPriceRequest)request;

            return req.Condition;
        }

        protected override bool validateObject(object request)
        {
            ItemPriceCondition condition = (ItemPriceCondition)request;

            bool res = false;

            if (!StringUtils.isBlankOrNull(condition.Company)
                && !StringUtils.isBlankOrNull(condition.ItemCode)
                && condition.Quantity > 0
                && condition.Date.Year > 1)
            {
                res = true;
            }

            return res;
        }

        protected override IResponse implement(object obj)
        {
            ItemPriceCondition condition = (ItemPriceCondition)obj;

            ItemPrice itemPrice = LookupUtils.Instance.getDocumentServiceImpl().getItemPrice(condition);

            SearchItemPriceResponse response = new SearchItemPriceResponse();
            response.Response = itemPrice;

            return response;
        }

        protected override void afterImplement(IResponse obj)
        {
        }

        protected override void logImplement(object obj, ServiceActionType serviceActionType)
        {
        }
    }
}