﻿using B1Utility.Enum;
using B1Utility.Utils;
using CommonControl.Common;
using CommonControl.POCO.Class;
using CommonControl.POCO.Request;
using CommonControl.POCO.Response;
using FrontControl.Lookup;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using WebSalesApi.Models.ServiceBaseImpl;
using WebSalesApi.Models.Utils;

namespace WebSalesApi.Models.ActionImpl
{
    public class DeleteBrochureImpl : WebServiceBase
    {
        protected override object requestToObject(IRequest request)
        {
            return request;
        }

        protected override bool validateObject(object request)
        {
            return userObj != null;
        }

        protected override IResponse implement(object obj)
        {
            DeleteBrochureRequest req = (DeleteBrochureRequest)obj;

            IResponse response = new IResponse();

            string brochurePath = Path.Combine(ProjectConstantUtils.BrochurePath, userObj.Company, req.FileName);

            if (File.Exists(brochurePath))
            {
                File.Delete(brochurePath);
            }

            return response;
        }

        protected override void afterImplement(IResponse obj)
        {
        }

        protected override void logImplement(object obj, ServiceActionType serviceActionType)
        {
        }
    }
}