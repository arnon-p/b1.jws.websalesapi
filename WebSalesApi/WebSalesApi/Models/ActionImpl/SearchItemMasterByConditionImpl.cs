﻿using B1Utility.Enum;
using B1Utility.Utils;
using CommonControl.Common;
using CommonControl.Enum;
using CommonControl.POCO.Class;
using CommonControl.POCO.Condition;
using CommonControl.POCO.Request;
using CommonControl.POCO.Response;
using FrontControl.Lookup;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WebSalesApi.Models.ServiceBaseImpl;

namespace WebSalesApi.Models.ActionImpl
{
    public class SearchItemMasterByConditionImpl : WebServiceBase
    {
        protected override object requestToObject(IRequest request)
        {
            SearchItemMasterRequest req = (SearchItemMasterRequest)request;

            return req.Condition;
        }

        protected override bool validateObject(object request)
        {
            ItemMasterCondition condition = (ItemMasterCondition)request;

            bool res = false;

            if (!StringUtils.isBlankOrNull(condition.Company))
            {
                res = true;
            }

            return res;
        }

        protected override IResponse implement(object obj)
        {
            ItemMasterCondition condition = (ItemMasterCondition)obj;

            List<ItemMaster> itemList = LookupUtils.Instance.getMasterServiceImpl().searchItemMasterByCondition(ref condition);

            SearchItemMasterByConditionResponse response = new SearchItemMasterByConditionResponse();
            response.Response = itemList;
            response.Paginable = condition.Paginable;

            return response;
        }

        protected override void afterImplement(IResponse obj)
        {
        }

        protected override void logImplement(object obj, ServiceActionType serviceActionType)
        {
        }
    }
}