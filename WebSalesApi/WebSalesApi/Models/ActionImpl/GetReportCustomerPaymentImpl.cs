﻿using B1Utility.Enum;
using B1Utility.Utils;
using CommonControl.Common;
using CommonControl.POCO.Class;
using CommonControl.POCO.Condition;
using CommonControl.POCO.Request;
using CommonControl.POCO.Response;
using FrontControl.Lookup;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WebSalesApi.Models.ServiceBaseImpl;

namespace WebSalesApi.Models.ActionImpl
{
    public class GetReportCustomerPaymentImpl : WebServiceBase
    {
        protected override object requestToObject(IRequest request)
        {
            CustomerPaymentReportRequest req = (CustomerPaymentReportRequest)request;
            return req.Condition;
        }

        protected override bool validateObject(object request)
        {
            ReportCustomerPaymentCondition condition = (ReportCustomerPaymentCondition)request;

            bool res = false;

            if (!StringUtils.isBlankOrNull(condition.Company))
            {
                res = true;
            }

            return res;
        }

        protected override IResponse implement(object obj)
        {
            ReportCustomerPaymentCondition condition = (ReportCustomerPaymentCondition)obj;
            // CR Authorize
            condition.UserSalesEmployee = userObj.SapProfile.SalesEmployee.Select(x => x.SlpCode).ToList();

            List<CustomerPaymentReport> agingList = LookupUtils.Instance.getReportServiceImpl().getReportCustomerPayment(condition);

            GetReportCustomerPaymentResponse response = new GetReportCustomerPaymentResponse();
            response.Response = agingList;

            return response;
        }

        protected override void afterImplement(IResponse obj)
        {
        }

        protected override void logImplement(object obj, ServiceActionType serviceActionType)
        {
        }
    }
}