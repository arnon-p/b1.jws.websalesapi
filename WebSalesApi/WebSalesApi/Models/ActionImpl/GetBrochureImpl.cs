﻿using B1Utility.Enum;
using B1Utility.Utils;
using CommonControl.Common;
using CommonControl.POCO.Class;
using CommonControl.POCO.Request;
using CommonControl.POCO.Response;
using FrontControl.Lookup;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using WebSalesApi.Models.ServiceBaseImpl;
using WebSalesApi.Models.Utils;

namespace WebSalesApi.Models.ActionImpl
{
    public class GetBrochureImpl : WebServiceBase
    {
        protected override object requestToObject(IRequest request)
        {
            return request;
        }

        protected override bool validateObject(object request)
        {
            return true;
        }

        protected override IResponse implement(object obj)
        {
            GetBrochureResponse response = new GetBrochureResponse();

            string compPath = Path.Combine(ProjectConstantUtils.BrochurePath, userObj.Company);

            if (Directory.Exists(compPath))
            {
                var dir = new DirectoryInfo(compPath);
                FileInfo[] files = dir.GetFiles();

                response.Response = files.Select(x => new CommonControl.POCO.Common.FileInfo(x.CreationTime, x.Name)).OrderByDescending(x => x.Date).ToList();
            }

            return response;
        }

        protected override void afterImplement(IResponse obj)
        {
        }

        protected override void logImplement(object obj, ServiceActionType serviceActionType)
        {
        }
    }
}