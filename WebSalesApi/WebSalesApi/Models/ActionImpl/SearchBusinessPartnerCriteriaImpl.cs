﻿using B1Utility.Enum;
using B1Utility.Utils;
using CommonControl.Common;
using CommonControl.Enum;
using CommonControl.POCO.Class;
using CommonControl.POCO.Condition;
using CommonControl.POCO.Request;
using CommonControl.POCO.Response;
using FrontControl.Lookup;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WebSalesApi.Models.ServiceBaseImpl;

namespace WebSalesApi.Models.ActionImpl
{
    public class SearchBusinessPartnerCriteriaImpl : WebServiceBase
    {
        protected override object requestToObject(IRequest request)
        {
            SearchBusinessPartnerRequest req = (SearchBusinessPartnerRequest)request;

            return req.Condition;
        }

        protected override bool validateObject(object request)
        {
            BusinessPartnerCondition condition = (BusinessPartnerCondition)request;

            bool res = false;

            if (!StringUtils.isBlankOrNull(condition.Company))
            {
                res = true;
            }

            return res;
        }

        protected override IResponse implement(object obj)
        {
            BusinessPartnerCondition condition = (BusinessPartnerCondition)obj;
            // CR Authorize
            condition.UserSalesEmployee = userObj.SapProfile.SalesEmployee.Select(x => x.SlpCode).ToList();

            List<BusinessPartnerCriteria> bpList = LookupUtils.Instance.getMasterServiceImpl().searchBusinessPartnerCriteria(condition);

            SearchBusinessPartnerCriteriaResponse response = new SearchBusinessPartnerCriteriaResponse();
            response.Response = bpList;

            return response;
        }

        protected override void afterImplement(IResponse obj)
        {
        }

        protected override void logImplement(object obj, ServiceActionType serviceActionType)
        {
        }
    }
}