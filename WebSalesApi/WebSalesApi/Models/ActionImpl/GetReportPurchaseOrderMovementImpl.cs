﻿using B1Utility.Enum;
using B1Utility.Utils;
using CommonControl.Common;
using CommonControl.POCO.Class;
using CommonControl.POCO.Condition;
using CommonControl.POCO.Request;
using CommonControl.POCO.Response;
using FrontControl.Lookup;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WebSalesApi.Models.ServiceBaseImpl;

namespace WebSalesApi.Models.ActionImpl
{
    public class GetReportPurchaseOrderMovementImpl : WebServiceBase
    {
        protected override object requestToObject(IRequest request)
        {
            PurchaseOrderMovementRequest req = (PurchaseOrderMovementRequest)request;
            return req.Condition;
        }

        protected override bool validateObject(object request)
        {
            ReportPurchaseOrderMovementCondition condition = (ReportPurchaseOrderMovementCondition)request;

            bool res = false;

            if (!StringUtils.isBlankOrNull(condition.Company))
            {
                res = true;
            }

            return res;
        }

        protected override IResponse implement(object obj)
        {
            ReportPurchaseOrderMovementCondition condition = (ReportPurchaseOrderMovementCondition)obj;

            List<PurchaseOrderMovement> poMoveList = LookupUtils.Instance.getReportServiceImpl().getReportPurchaseOrderMovement(condition);

            poMoveList.ForEach(x => { x.Currency = null; x.Price = 0; x.U_WGE_P08 = null; });

            GetReportPurchaseOrderMovementResponse response = new GetReportPurchaseOrderMovementResponse();
            response.Response = poMoveList;

            return response;
        }

        protected override void afterImplement(IResponse obj)
        {
        }

        protected override void logImplement(object obj, ServiceActionType serviceActionType)
        {
        }
    }
}