﻿using B1Utility.Enum;
using B1Utility.Utils;
using CommonControl.Common;
using CommonControl.Enum;
using CommonControl.POCO.Class;
using CommonControl.POCO.Common;
using CommonControl.POCO.Condition;
using CommonControl.POCO.Request;
using CommonControl.POCO.Response;
using FrontControl.Lookup;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WebSalesApi.Models.ServiceBaseImpl;

namespace WebSalesApi.Models.ActionImpl
{
    public class SearchSalesOrderDocumentRelationImpl : WebServiceBase
    {
        protected override object requestToObject(IRequest request)
        {
            SearchSalesOrderRequest req = (SearchSalesOrderRequest)request;

            return req.Condition;
        }

        protected override bool validateObject(object request)
        {
            SalesOrderCondition condition = (SalesOrderCondition)request;

            bool res = false;

            if (!StringUtils.isBlankOrNull(condition.Company)
                && condition.DocEntry > 0
                && !condition.IsDraft)
            {
                res = true;
            }

            return res;
        }

        protected override IResponse implement(object obj)
        {
            SalesOrderCondition condition = (SalesOrderCondition)obj;

            List<DocumentRelation> docRelationList = LookupUtils.Instance.getDocumentServiceImpl().searchSalesOrderDocumentRelation(condition);

            SearchSalesOrderDocumentRelationResponse response = new SearchSalesOrderDocumentRelationResponse();
            response.Response = docRelationList;

            return response;
        }

        protected override void afterImplement(IResponse obj)
        {
        }

        protected override void logImplement(object obj, ServiceActionType serviceActionType)
        {
        }
    }
}