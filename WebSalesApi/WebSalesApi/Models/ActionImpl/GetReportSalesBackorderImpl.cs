﻿using B1Utility.DataAccess;
using B1Utility.Enum;
using B1Utility.Utils;
using CommonControl.Common;
using CommonControl.POCO.Class;
using CommonControl.POCO.Common;
using CommonControl.POCO.Condition;
using CommonControl.POCO.Request;
using CommonControl.POCO.Response;
using CrystalDecisions.Shared;
using FrontControl.Lookup;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using WebSalesApi.Models.POCO;
using WebSalesApi.Models.ServiceBaseImpl;
using WebSalesApi.Models.Utils;
using ReportUtils = WebSalesApi.Models.Utils.ReportUtils;

namespace WebSalesApi.Models.ActionImpl
{
    public class GetReportSalesBackorderImpl : WebServiceBase
    {
        protected override object requestToObject(IRequest request)
        {
            SalesBackOrderRequest req = (SalesBackOrderRequest)request;
            return req.Condition;
        }

        protected override bool validateObject(object request)
        {
            ReportSalesBackorderCondition condition = (ReportSalesBackorderCondition)request;

            bool res = false;

            if (!StringUtils.isBlankOrNull(condition.Company))
            {
                res = true;
            }

            return res;
        }

        protected override IResponse implement(object obj)
        {
            ReportSalesBackorderCondition condition = (ReportSalesBackorderCondition)obj;

            condition.ExportType = CrystalDecisions.Shared.ExportFormatType.PortableDocFormat;

            CrystalReportOptions rptOption = new CrystalReportOptions();
            rptOption.DbUsername = ServiceDataAccess.Instance.CompanyDic[condition.Company].CompanyInfo.DbUserName;
            rptOption.DbPassword = ServiceDataAccess.Instance.CompanyDic[condition.Company].CompanyInfo.DbPassword;
            rptOption.ExportFormatType = condition.ExportType;
            rptOption.RptPath = ProjectConstantUtils.getCrystalReportPath(condition.Company, CommonControl.Constants.ServiceConstants.SALES_BACK_ORDER_REPORT, CommonControl.Constants.ServiceConstants.SALES_BACK_ORDER_REPORT + ".rpt");
            //rptOption.RptPath = ServiceDataAccess.Instance.getReportFilePath(condition.Company, CommonControl.Constants.ServiceConstants.SALES_BACK_ORDER_REPORT, CommonControl.Constants.ServiceConstants.SALES_BACK_ORDER_REPORT);
            //rptOption.RptPath = ServiceDataAccess.Instance.getReportFilePath(condition.Company, CommonControl.Constants.ServiceConstants.SALES_BACK_ORDER_REPORT, condition.ReportName);

            rptOption.ParamValues = new List<ParamValue>();

            // Delivery Date
            ParameterRangeValue pDueDate = new ParameterRangeValue();
            if (condition.DocDueDateFrom.Year > 1 && condition.DocDueDateTo.Year > 1)
            {
                pDueDate.StartValue = condition.DocDueDateFrom;
                pDueDate.EndValue = condition.DocDueDateTo;
                rptOption.ParamValues.Add(new ParamValue("pDueDate", pDueDate));
            }
            else
            {
                pDueDate.StartValue = default(DateTime);
                pDueDate.EndValue = default(DateTime);
                rptOption.ParamValues.Add(new ParamValue("pDueDate", pDueDate, true));
            }

            rptOption.ParamValues.Add(new ParamValue("pSales@Select SlpName,SlpCode from OSLP where SlpName like 's%'", condition.SlpCode.ToArray()));

            //ParameterRangeValue pSales = new ParameterRangeValue();
            //if (condition.SlpCodeFrom > 0 && condition.SlpCodeTo > 0)
            //{
            //    pSales.StartValue = condition.SlpCodeFrom;
            //    pSales.EndValue = condition.SlpCodeTo;
            //    rptOption.ParamValues.Add(new ParamValue("pSales@Select * from OSLP", pSales));
            //}
            //else
            //{
            //    pSales.StartValue = default(int);
            //    pSales.EndValue = default(int);
            //    rptOption.ParamValues.Add(new ParamValue("pSales@Select * from OSLP", pSales, true));
            //}

            // Customer
            ParameterRangeValue pCustomer = new ParameterRangeValue();
            if (!StringUtils.isBlankOrNull(condition.CardCodeFrom) && !StringUtils.isBlankOrNull(condition.CardCodeTo))
            {
                pCustomer.StartValue = condition.CardCodeFrom;
                pCustomer.EndValue = condition.CardCodeTo;
                rptOption.ParamValues.Add(new ParamValue("pCustomer@Select * from OCRD", pCustomer));
            }
            else
            {
                pCustomer.StartValue = String.Empty;
                pCustomer.EndValue = String.Empty;
                rptOption.ParamValues.Add(new ParamValue("pCustomer@Select * from OCRD", pCustomer, true));
            }

            // Item No.
            ParameterRangeValue pItemCode = new ParameterRangeValue();
            if (!StringUtils.isBlankOrNull(condition.ItemCodeFrom) && !StringUtils.isBlankOrNull(condition.ItemCodeTo))
            {
                pItemCode.StartValue = condition.ItemCodeFrom;
                pItemCode.EndValue = condition.ItemCodeTo;
                rptOption.ParamValues.Add(new ParamValue("pItemCode@Select * from OITM", pItemCode));
            }
            else
            {
                pItemCode.StartValue = String.Empty;
                pItemCode.EndValue = String.Empty;
                rptOption.ParamValues.Add(new ParamValue("pItemCode@Select * from OITM", pItemCode, true));
            }

            //if (B1DataAccess.Instance.IsHana)
            //{
            //    rptOption.ParamValues.Add("Schema@", ServiceDataAccess.Instance.getCompanyDB(condition.Company));
            //}

            string fileName = ReportUtils.getFileNameExport(CommonControl.Constants.ServiceConstants.SALES_BACK_ORDER_REPORT, condition.ExportType);
            rptOption.DestFilePath = Path.Combine(ProjectConstantUtils.ReportPath, fileName);

            ReportUtils.saveToFile(rptOption);

            IDownloadResponse response = new IDownloadResponse();
            response.FileName = fileName;

            return response;
        }

        protected override void afterImplement(IResponse obj)
        {
        }

        protected override void logImplement(object obj, ServiceActionType serviceActionType)
        {
        }
    }
}