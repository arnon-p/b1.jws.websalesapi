﻿using B1Utility.Enum;
using B1Utility.Utils;
using CommonControl.Common;
using CommonControl.POCO.Class;
using CommonControl.POCO.Common;
using CommonControl.POCO.Condition;
using CommonControl.POCO.Request;
using CommonControl.POCO.Response;
using FrontControl.Lookup;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WebSalesApi.Models.ServiceBaseImpl;

namespace WebSalesApi.Models.ActionImpl
{
    public class GetReportSalesAnalysisImpl : WebServiceBase
    {
        protected override object requestToObject(IRequest request)
        {
            ReportSalesAnalysisRequest req = (ReportSalesAnalysisRequest)request;
            return req.Condition;
        }

        protected override bool validateObject(object request)
        {
            SalesAnalysisCondition condition = (SalesAnalysisCondition)request;

            bool res = false;

            if (!StringUtils.isBlankOrNull(condition.Company)
                && condition.DocDateFrom.Year > 0
                && condition.DocDateTo.Year > 0)
            {
                res = true;
            }

            return res;
        }

        protected override IResponse implement(object obj)
        {
            SalesAnalysisCondition condition = (SalesAnalysisCondition)obj;
            // CR Authorize
            condition.UserSalesEmployee = userObj.SapProfile.SalesEmployee.Select(x => x.SlpCode).ToList();

            List<SalesAnalysisReport> salesList = LookupUtils.Instance.getReportServiceImpl().getReportSalesAnalysis(condition);

            GetReportSalesAnalysisResponse response = new GetReportSalesAnalysisResponse();
            response.Response = salesList;

            return response;
        }

        protected override void afterImplement(IResponse obj)
        {
        }

        protected override void logImplement(object obj, ServiceActionType serviceActionType)
        {
        }
    }
}