﻿using B1Utility.Enum;
using B1Utility.Utils;
using CommonControl.Common;
using CommonControl.Enum;
using CommonControl.POCO.Class;
using CommonControl.POCO.Common;
using CommonControl.POCO.Condition;
using CommonControl.POCO.Request;
using CommonControl.POCO.Response;
using FrontControl.Lookup;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WebSalesApi.Models.ServiceBaseImpl;

namespace WebSalesApi.Models.ActionImpl
{
    public class SearchOrderStatusByConditionImpl : WebServiceBase
    {
        protected override object requestToObject(IRequest request)
        {
            SearchOrderStatusRequest req = (SearchOrderStatusRequest)request;

            return req.Condition;
        }

        protected override bool validateObject(object request)
        {
            ReportOrderStatusCondition condition = (ReportOrderStatusCondition)request;

            bool res = false;

            if (!StringUtils.isBlankOrNull(condition.Company))
            {
                res = true;
            }

            return res;
        }

        protected override IResponse implement(object obj)
        {
            ReportOrderStatusCondition condition = (ReportOrderStatusCondition)obj;
            // CR Authorize
            condition.UserSalesEmployee = userObj.SapProfile.SalesEmployee.Select(x => x.SlpCode).ToList();

            List<OrderStatus> orderStatusList = LookupUtils.Instance.getDocumentServiceImpl().searchOrderStatusByCondition(ref condition);

            SearchOrderStatusByConditionResponse response = new SearchOrderStatusByConditionResponse();
            response.Response = orderStatusList;
            response.Paginable = condition.Paginable;

            return response;
        }

        protected override void afterImplement(IResponse obj)
        {
        }

        protected override void logImplement(object obj, ServiceActionType serviceActionType)
        {
        }
    }
}