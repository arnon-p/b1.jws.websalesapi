﻿using B1Utility.Enum;
using B1Utility.Utils;
using CommonControl.Common;
using CommonControl.Enum;
using CommonControl.POCO.Class;
using CommonControl.POCO.Condition;
using CommonControl.POCO.Request;
using CommonControl.POCO.Response;
using FrontControl.Lookup;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WebSalesApi.Models.ServiceBaseImpl;

namespace WebSalesApi.Models.ActionImpl
{
    public class SearchItemOnHandByItemCodeImpl : WebServiceBase
    {
        protected override object requestToObject(IRequest request)
        {
            SearchItemMasterRequest req = (SearchItemMasterRequest)request;

            return req.Condition;
        }

        protected override bool validateObject(object request)
        {
            ItemMasterCondition condition = (ItemMasterCondition)request;

            bool res = false;

            if (!StringUtils.isBlankOrNull(condition.Company)
                && !StringUtils.isBlankOrNull(condition.ItemCode))
            {
                res = true;
            }

            return res;
        }

        protected override IResponse implement(object obj)
        {
            ItemMasterCondition condition = (ItemMasterCondition)obj;

            List<ItemOnHand> onHandList = LookupUtils.Instance.getMasterServiceImpl().searchItemOnHandByItemCode(condition);

            SearchItemOnHandByItemCodeResponse response = new SearchItemOnHandByItemCodeResponse();
            response.Response = onHandList;

            return response;
        }

        protected override void afterImplement(IResponse obj)
        {
        }

        protected override void logImplement(object obj, ServiceActionType serviceActionType)
        {
        }
    }
}