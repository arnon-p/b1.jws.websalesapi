﻿using B1Utility.Enum;
using B1Utility.Utils;
using CommonControl.Common;
using CommonControl.POCO.Class;
using CommonControl.POCO.Condition;
using CommonControl.POCO.Request;
using CommonControl.POCO.Response;
using FrontControl.Lookup;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WebSalesApi.Models.ServiceBaseImpl;

namespace WebSalesApi.Models.ActionImpl
{
    public class SearchApprovalSalesOrderByConditionImpl : WebServiceBase
    {
        protected override object requestToObject(IRequest request)
        {
            ApproveSalesOrderRequest req = (ApproveSalesOrderRequest)request;
            return req.Condition;
        }

        protected override bool validateObject(object request)
        {
            ApproveSalesOrderCondition condition = (ApproveSalesOrderCondition)request;

            bool res = true;

            if (StringUtils.isBlankOrNull(condition.Company)
                || userObj == null)
            {
                res = false;
            }

            return res;
        }

        protected override IResponse implement(object obj)
        {
            ApproveSalesOrderCondition condition = (ApproveSalesOrderCondition)obj;
            condition.UserId = userObj.SapProfile.UserId;
            condition.EmpId = userObj.SapProfile.EmpId;
            condition.ExtEmpId = userObj.SapProfile.ExtEmpId;

            List<ApproveSalesOrder> soList = LookupUtils.Instance.getDocumentServiceImpl().searchApprovalSalesOrderByCondition(ref condition);

            SearchApprovalSalesOrderByConditionResponse response = new SearchApprovalSalesOrderByConditionResponse();
            response.Response = soList;
            response.Paginable = condition.Paginable;

            return response;
        }

        protected override void afterImplement(IResponse obj)
        {
        }

        protected override void logImplement(object obj, ServiceActionType serviceActionType)
        {
        }
    }
}