﻿using B1Utility.DataAccess;
using B1Utility.Enum;
using B1Utility.Utils;
using CommonControl.Common;
using CommonControl.Enum;
using CommonControl.POCO.Class;
using CommonControl.POCO.Common;
using CommonControl.POCO.Request;
using CommonControl.POCO.Response;
using FrontControl.Lookup;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using WebSalesApi.Models.ServiceBaseImpl;
using WebSalesApi.Models.Utils;

namespace WebSalesApi.Models.ActionImpl
{
    public class CreateSalesOrderImpl : WebServiceBase
    {
        protected override object requestToObject(IRequest request)
        {
            SalesOrderRequest req = (SalesOrderRequest)request;
            return req.Data;
        }

        protected override bool validateObject(object request)
        {
            List<SalesOrder> req = (List<SalesOrder>)request;

            bool res = true;

            if (userObj != null)
            {
                foreach (SalesOrder so in req)
                {
                    if (StringUtils.isBlankOrNull(so.Company)
                        || StringUtils.isBlankOrNull(so.CardCode)
                        || !CollectionUtils.IsAny(so.Lines))
                    {
                        res = false;
                        break;
                    }
                }
            }
            else
            {
                res = false;
            }

            return res;
        }

        protected override IResponse implement(object obj)
        {
            List<SalesOrder> req = (List<SalesOrder>)obj;

            foreach (SalesOrder so in req)
            {
                so.IsDraft = true;
                so.DocumentsOwner = userObj.SapProfile.EmpId;

                if (CollectionUtils.IsAny(so.Attachments))
                {
                    foreach (AttachInfo att in so.Attachments)
                    {
                        if (!StringUtils.isBlankOrNull(att.TempKey))
                        {
                            att.SrcPath = ProjectConstantUtils.AttachPath;
                            if (!File.Exists(Path.Combine(ProjectConstantUtils.AttachPath, att.NewFileName)))
                            {
                                File.Copy(Path.Combine(ProjectConstantUtils.TempPath, att.TempKey), Path.Combine(ProjectConstantUtils.AttachPath, att.NewFileName), true);
                            }
                        }
                    }
                }
            }

            List<SalesOrderResponse> soResList = LookupUtils.Instance.getDocumentServiceImpl().createSalesOrderList(req);

            CreateSalesOrderResponse response = new CreateSalesOrderResponse();
            response.Response = soResList;

            return response;
        }

        protected override void afterImplement(IResponse obj)
        {
            CreateSalesOrderResponse res = (CreateSalesOrderResponse)obj;

            if (ResponseStatus.SUCCESS.Equals(res.Status))
            {
                SalesOrderResponse soResponse = res.Response.ElementAt(0);

                List<EmailSenderInfo> mailto = LookupUtils.Instance.getDocumentServiceImpl().getApproverEmail(soResponse.Company, soResponse.DocEntry);
                if (CollectionUtils.IsAny(mailto))
                {
                    // email to approver
                    foreach (EmailSenderInfo email in mailto)
                    {
                        EmailMessageInfo mail = new EmailMessageInfo();
                        mail.Subject = String.Format(CommonControl.Constants.ServiceConstants.EMAIL_SUBJECT_PENDING, soResponse.Company, soResponse.DocEntry);
                        mail.Body = StringUtils.format(CommonControl.Constants.ServiceConstants.EMAIL_BODY_PENDING, email.FullName, soResponse.DocEntry, ProjectConstantUtils.WebSalesUrl);
                        mail.Email = email.Email;

                        MailUtils.sendEmail(mail);
                    }

                    // email to originator
                    EmailSenderInfo originator = LookupUtils.Instance.getDocumentServiceImpl().getOriginatorEmail(soResponse.Company, soResponse.DocEntry);
                    if (originator != null)
                    {
                        EmailMessageInfo mail = new EmailMessageInfo();
                        mail.Subject = String.Format(CommonControl.Constants.ServiceConstants.EMAIL_SUBJECT_ORIGINATOR_CREATE, soResponse.Company, soResponse.DocEntry);
                        mail.Body = StringUtils.format(CommonControl.Constants.ServiceConstants.EMAIL_BODY_ORIGINATOR_CREATE, originator.FullName, soResponse.DocEntry, ProjectConstantUtils.WebSalesUrl);
                        mail.Email = originator.Email;

                        MailUtils.sendEmail(mail);
                    }
                }
                else
                {
                    int chkApprove = LookupUtils.Instance.getDocumentServiceImpl().checkApprove(soResponse.Company, soResponse.DocEntry);
                    if (chkApprove > 0)
                    {
                        ApproveDocumentInfo soConvertResponse = new ApproveDocumentInfo();
                        soConvertResponse.Company = soResponse.Company;
                        soConvertResponse.ObjType = soResponse.ObjType;
                        soConvertResponse.DocEntry = soResponse.DocEntry;
                        soConvertResponse.DocNum = soResponse.DocNum;
                        soConvertResponse.DraftEntry = soResponse.DraftEntry;

                        try
                        {
                            soConvertResponse = LookupUtils.Instance.getDocumentServiceImpl().approveSalesOrder(soConvertResponse);

                            if (soConvertResponse != null)
                            {
                                // TODO Change
                                soResponse.ObjType = soConvertResponse.ObjType;
                                soResponse.DocEntry = soConvertResponse.DocEntry;
                                soResponse.DocNum = soConvertResponse.DocNum;
                                soResponse.DraftEntry = soConvertResponse.DraftEntry;
                                // TODO Change

                                res.Response = new List<SalesOrderResponse>();
                                res.Response.Add(soResponse);

                                EmailSenderInfo originator = LookupUtils.Instance.getDocumentServiceImpl().getOriginatorEmail(soConvertResponse.Company, soConvertResponse.DraftEntry);
                                if (originator != null)
                                {
                                    EmailMessageInfo mail = new EmailMessageInfo();
                                    mail.Subject = String.Format(CommonControl.Constants.ServiceConstants.EMAIL_SUBJECT_CREATE, soConvertResponse.Company, soConvertResponse.DocNum);
                                    mail.Body = StringUtils.format(CommonControl.Constants.ServiceConstants.EMAIL_BODY_CREATE, originator.FullName, soConvertResponse.DraftEntry, soConvertResponse.DocNum, ProjectConstantUtils.WebSalesUrl);
                                    mail.Email = originator.Email;

                                    MailUtils.sendEmail(mail);
                                }
                                else
                                {
                                    // not found originator
                                    FileUtils.writeLogService("NotFoundEmailOriginator", logPath, res, ServiceActionType.ERROR, randomKey);
                                }
                            }
                            else
                            {
                                // not found response
                                FileUtils.writeLogService("NotFoundApproveResponse", logPath, res, ServiceActionType.ERROR, randomKey);
                            }
                        }
                        catch (Exception apprEx)
                        {
                            EmailSenderInfo originator = LookupUtils.Instance.getDocumentServiceImpl().getOriginatorEmail(soResponse.Company, soResponse.DocEntry);
                            if (originator != null)
                            {
                                EmailMessageInfo mail = new EmailMessageInfo();
                                mail.Subject = String.Format(CommonControl.Constants.ServiceConstants.EMAIL_SUBJECT_ERROR_CONVERT, soResponse.Company, soResponse.DocEntry);
                                mail.Body = StringUtils.format(CommonControl.Constants.ServiceConstants.EMAIL_BODY_ERROR_CONVERT, originator.FullName, apprEx.Message, ProjectConstantUtils.WebSalesUrl);
                                mail.Email = originator.Email;

                                mail.Mailto = new List<string>();
                                mail.Mailto.Add(ProjectConstantUtils.EmailSenderInfo.EmailUser);

                                if (!StringUtils.isBlankOrNull(originator.EmailCenter))
                                {
                                    mail.Mailto.Add(originator.EmailCenter);
                                }

                                if (!StringUtils.isBlankOrNull(originator.EmailSupport))
                                {
                                    mail.Mailto.Add(originator.EmailSupport);
                                }

                                MailUtils.sendEmail(mail, true);
                            }
                            else
                            {
                                EmailMessageInfo mail = new EmailMessageInfo();
                                mail.Subject = String.Format(CommonControl.Constants.ServiceConstants.EMAIL_SUBJECT_ERROR_CONVERT, soResponse.Company, soResponse.DocEntry);
                                mail.Body = StringUtils.format(CommonControl.Constants.ServiceConstants.EMAIL_BODY_ERROR_CONVERT, ProjectConstantUtils.EmailSenderInfo.EmailUser, apprEx.Message, ProjectConstantUtils.WebSalesUrl);
                                mail.Email = ProjectConstantUtils.EmailSenderInfo.EmailUser;
                                MailUtils.sendEmail(mail);
                            }
                        }
                    }
                    else
                    {
                        // not found approve
                        FileUtils.writeLogService("NotFoundCheckApprove", logPath, res, ServiceActionType.ERROR, randomKey);
                    }
                }
            }
        }

        protected override void logImplement(object obj, ServiceActionType serviceActionType)
        {
            FileUtils.writeLogService("CreateSalesOrder", logPath, obj, serviceActionType, randomKey);
        }
    }
}