﻿using B1Utility.Enum;
using B1Utility.Utils;
using CommonControl.Common;
using CommonControl.Enum;
using CommonControl.POCO.Class;
using CommonControl.POCO.Common;
using CommonControl.POCO.Condition;
using CommonControl.POCO.Request;
using CommonControl.POCO.Response;
using FrontControl.Lookup;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WebSalesApi.Models.ServiceBaseImpl;

namespace WebSalesApi.Models.ActionImpl
{
    public class SearchSalesOrderByConditionImpl : WebServiceBase
    {
        protected override object requestToObject(IRequest request)
        {
            SearchSalesOrderRequest req = (SearchSalesOrderRequest)request;

            return req.Condition;
        }

        protected override bool validateObject(object request)
        {
            SalesOrderCondition condition = (SalesOrderCondition)request;

            bool res = false;

            if (!StringUtils.isBlankOrNull(condition.Company))
            {
                res = true;
            }

            return res;
        }

        protected override IResponse implement(object obj)
        {
            SalesOrderCondition condition = (SalesOrderCondition)obj;
            // CR Authorize
            condition.UserSalesEmployee = userObj.SapProfile.SalesEmployee.Select(x => x.SlpCode).ToList();

            List<SalesOrder> soList = LookupUtils.Instance.getDocumentServiceImpl().searchSalesOrderByCondition(ref condition);

            SearchSalesOrderByConditionResponse response = new SearchSalesOrderByConditionResponse();
            response.Response = soList;
            response.Paginable = condition.Paginable;

            return response;
        }

        protected override void afterImplement(IResponse obj)
        {
        }

        protected override void logImplement(object obj, ServiceActionType serviceActionType)
        {
        }
    }
}