﻿using B1Utility.Enum;
using B1Utility.Utils;
using CommonControl.Common;
using CommonControl.POCO.Class;
using CommonControl.POCO.Request;
using CommonControl.POCO.Response;
using FrontControl.Lookup;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using WebSalesApi.Models.ServiceBaseImpl;
using WebSalesApi.Models.Utils;

namespace WebSalesApi.Models.ActionImpl
{
    public class UpdateSalesOrderImpl : WebServiceBase
    {
        protected override object requestToObject(IRequest request)
        {
            SalesOrderRequest req = (SalesOrderRequest)request;
            return req.Data;
        }

        protected override bool validateObject(object request)
        {
            List<SalesOrder> req = (List<SalesOrder>)request;

            bool res = true;

            foreach(SalesOrder so in req)
            {
                if (StringUtils.isBlankOrNull(so.Company)
                    || StringUtils.isBlankOrNull(so.CardCode)
                    || !CollectionUtils.IsAny(so.Lines))
                {
                    res = false;
                    break;
                }
            }

            return res;
        }

        protected override IResponse implement(object obj)
        {
            List<SalesOrder> req = (List<SalesOrder>)obj;

            foreach (SalesOrder so in req)
            {
                //so.IsDraft = true;

                if (CollectionUtils.IsAny(so.Attachments))
                {
                    foreach (AttachInfo att in so.Attachments)
                    {
                        if (!StringUtils.isBlankOrNull(att.TempKey))
                        {
                            att.SrcPath = ProjectConstantUtils.AttachPath;
                            if (!File.Exists(Path.Combine(ProjectConstantUtils.AttachPath, att.NewFileName)))
                            {
                                File.Copy(Path.Combine(ProjectConstantUtils.TempPath, att.TempKey), Path.Combine(ProjectConstantUtils.AttachPath, att.NewFileName), true);
                            }
                        }
                    }
                }
            }

            List<SalesOrderResponse> soResList = LookupUtils.Instance.getDocumentServiceImpl().updateSalesOrderList(req);

            UpdateSalesOrderResponse response = new UpdateSalesOrderResponse();
            response.Response = soResList;

            return response;
        }

        protected override void afterImplement(IResponse obj)
        {
        }

        protected override void logImplement(object obj, ServiceActionType serviceActionType)
        {
            FileUtils.writeLogService("UpdateSalesOrder", logPath, obj, serviceActionType, randomKey);
        }
    }
}