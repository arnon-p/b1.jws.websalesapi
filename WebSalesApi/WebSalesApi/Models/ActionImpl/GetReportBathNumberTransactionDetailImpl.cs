﻿using B1Utility.Enum;
using B1Utility.Utils;
using CommonControl.Common;
using CommonControl.POCO.Class;
using CommonControl.POCO.Condition;
using CommonControl.POCO.Request;
using CommonControl.POCO.Response;
using FrontControl.Lookup;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WebSalesApi.Models.ServiceBaseImpl;

namespace WebSalesApi.Models.ActionImpl
{
    public class GetReportBathNumberTransactionDetailImpl : WebServiceBase
    {
        protected override object requestToObject(IRequest request)
        {
            BatchNumberTransactionReportDetailRequest req = (BatchNumberTransactionReportDetailRequest)request;
            return req.Condition;
        }

        protected override bool validateObject(object request)
        {
            ReportBatchNumberTransactionDetailCondition condition = (ReportBatchNumberTransactionDetailCondition)request;
            bool res = false;

            if (!StringUtils.isBlankOrNull(condition.ItemCode)
                && !StringUtils.isBlankOrNull(condition.BatchNum)
                && !StringUtils.isBlankOrNull(condition.WhsCode)
                && !StringUtils.isBlankOrNull(condition.Company))
            {
                res = true;
            }

            return res;
        }

        protected override IResponse implement(object obj)
        {
            ReportBatchNumberTransactionDetailCondition condition = (ReportBatchNumberTransactionDetailCondition)obj;

            List<BatchNumberTransactionReportDetail> batchList = LookupUtils.Instance.getReportServiceImpl().getReportBatchNumberTransactionDetail(condition);

            GetReportBatchNumberTransactionDetailResponse response = new GetReportBatchNumberTransactionDetailResponse();
            response.Response = batchList;

            return response;
        }

        protected override void afterImplement(IResponse obj)
        {
        }

        protected override void logImplement(object obj, ServiceActionType serviceActionType)
        {
        }
    }
}