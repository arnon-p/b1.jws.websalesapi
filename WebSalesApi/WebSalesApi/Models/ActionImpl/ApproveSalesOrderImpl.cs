﻿using B1Utility.DataAccess;
using B1Utility.Enum;
using B1Utility.Utils;
using CommonControl.Common;
using CommonControl.Constants;
using CommonControl.Enum;
using CommonControl.POCO.Class;
using CommonControl.POCO.Common;
using CommonControl.POCO.Request;
using CommonControl.POCO.Response;
using FrontControl.Lookup;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using WebSalesApi.Models.ServiceBaseImpl;
using WebSalesApi.Models.Utils;

namespace WebSalesApi.Models.ActionImpl
{
    public class ApproveSalesOrderImpl : WebServiceBase
    {
        protected override object requestToObject(IRequest request)
        {
            ApproveSalesOrderResultRequest req = (ApproveSalesOrderResultRequest)request;
            return req.Data;
        }

        protected override bool validateObject(object request)
        {
            List<ApproveSalesOrderResult> req = (List<ApproveSalesOrderResult>)request;

            bool res = true;

            foreach (ApproveSalesOrderResult so in req)
            {
                if (StringUtils.isBlankOrNull(so.Company)
                    || so.DocEntry == 0
                    || StringUtils.isBlankOrNull(so.WddStatus)
                    || userObj == null)
                {
                    res = false;
                    break;
                }
            }

            return res;
        }

        protected override IResponse implement(object obj)
        {
            List<ApproveSalesOrderResult> req = (List<ApproveSalesOrderResult>)obj;

            //req.ForEach(x => { x.UserId = userObj.SapProfile.UserId; x.EmpId = userObj.SapProfile.EmpId; x.ExtEmpId = userObj.SapProfile.ExtEmpId; });
            
            foreach (ApproveSalesOrderResult so in req)
            {
                if (userObj.SapProfile != null)
                {
                    so.UserId = userObj.SapProfile.UserId;
                    so.EmpId = userObj.SapProfile.EmpId;

                    if (!StringUtils.isBlankOrNull(userObj.SapProfile.ExtEmpId))
                    {
                        so.ExtEmpId = userObj.SapProfile.ExtEmpId;
                    }
                }
            }

            List<ApproveDocumentInfo> soResList = LookupUtils.Instance.getDocumentServiceImpl().approveDocument(req);

            ApproveSalesOrderResponse response = new ApproveSalesOrderResponse();
            response.Response = soResList;

            return response;
        }

        protected override void afterImplement(IResponse obj)
        {
            ApproveSalesOrderResponse res = (ApproveSalesOrderResponse)obj;

            if (ResponseStatus.SUCCESS.Equals(res.Status))
            {
                foreach (ApproveDocumentInfo so in res.Response)
                {
                    if (so.IsError)
                    {
                        EmailSenderInfo originator = LookupUtils.Instance.getDocumentServiceImpl().getOriginatorEmail(so.Company, so.DraftEntry);
                        if (originator != null)
                        {
                            EmailMessageInfo mail = new EmailMessageInfo();
                            mail.Subject = String.Format(CommonControl.Constants.ServiceConstants.EMAIL_SUBJECT_ERROR_CONVERT, so.Company, so.DraftEntry);
                            mail.Body = StringUtils.format(CommonControl.Constants.ServiceConstants.EMAIL_BODY_ERROR_CONVERT, originator.FullName, so.ErrorMessage, ProjectConstantUtils.WebSalesUrl);
                            mail.Email = originator.Email;

                            mail.Mailto = new List<string>();
                            mail.Mailto.Add(ProjectConstantUtils.EmailSenderInfo.EmailUser);

                            if (!StringUtils.isBlankOrNull(originator.EmailCenter))
                            {
                                mail.Mailto.Add(originator.EmailCenter);
                            }

                            if (!StringUtils.isBlankOrNull(originator.EmailSupport))
                            {
                                mail.Mailto.Add(originator.EmailSupport);
                            }

                            MailUtils.sendEmail(mail, true);
                        }
                        else
                        {
                            EmailMessageInfo mail = new EmailMessageInfo();
                            mail.Subject = String.Format(CommonControl.Constants.ServiceConstants.EMAIL_SUBJECT_ERROR_CONVERT, so.Company, so.DocEntry);
                            mail.Body = StringUtils.format(CommonControl.Constants.ServiceConstants.EMAIL_BODY_ERROR_CONVERT, ProjectConstantUtils.EmailSenderInfo.EmailUser, so.ErrorMessage, ProjectConstantUtils.WebSalesUrl);
                            mail.Email = ProjectConstantUtils.EmailSenderInfo.EmailUser;
                            MailUtils.sendEmail(mail);
                            FileUtils.writeLogService("NotFoundEmailOriginator", logPath, res, ServiceActionType.ERROR, randomKey);
                        }
                    }
                    else if (ServiceConstants.STATUS_APPROVE.Equals(so.WddStatus) || ServiceConstants.STATUS_REJECT.Equals(so.WddStatus))
                    {
                        EmailSenderInfo mailto = LookupUtils.Instance.getDocumentServiceImpl().getOriginatorEmail(so.Company, so.DraftEntry);

                        if (mailto != null)
                        {
                            EmailMessageInfo mail = new EmailMessageInfo();
                            if (ServiceConstants.STATUS_APPROVE.Equals(so.WddStatus))   // approve
                            {
                                mail.Subject = String.Format(CommonControl.Constants.ServiceConstants.EMAIL_SUBJECT_CREATE, so.Company, so.DocNum);
                                mail.Body = StringUtils.format(CommonControl.Constants.ServiceConstants.EMAIL_BODY_CREATE, mailto.FullName, so.DraftEntry, so.DocNum, ProjectConstantUtils.WebSalesUrl);
                                mail.Email = mailto.Email;
                            }
                            else if (ServiceConstants.STATUS_REJECT.Equals(so.WddStatus))  // reject
                            {
                                mail.Subject = String.Format(CommonControl.Constants.ServiceConstants.EMAIL_SUBJECT_REJECT, so.Company, so.DocNum);
                                mail.Body = StringUtils.format(CommonControl.Constants.ServiceConstants.EMAIL_BODY_REJECT, mailto.FullName, so.DraftEntry, so.DocNum, ProjectConstantUtils.WebSalesUrl);
                                mail.Email = mailto.Email;
                            }

                            MailUtils.sendEmail(mail);
                        }
                    }
                    else if (ServiceConstants.STATUS_PENDING.Equals(so.WddStatus) && so.IsNextStage)
                    {
                        List<EmailSenderInfo> mailto = LookupUtils.Instance.getDocumentServiceImpl().getApproverEmail(so.Company, so.DocEntry);

                        // email to approver
                        foreach (EmailSenderInfo email in mailto)
                        {
                            EmailMessageInfo mail = new EmailMessageInfo();
                            mail.Subject = String.Format(CommonControl.Constants.ServiceConstants.EMAIL_SUBJECT_PENDING, so.Company, so.DocEntry);
                            mail.Body = StringUtils.format(CommonControl.Constants.ServiceConstants.EMAIL_BODY_PENDING, email.FullName, so.DocEntry, ProjectConstantUtils.WebSalesUrl);
                            mail.Email = email.Email;

                            MailUtils.sendEmail(mail);
                        }
                    }
                }
            }
        }

        protected override void logImplement(object obj, ServiceActionType serviceActionType)
        {
            FileUtils.writeLogService("ApproveSalesOrder", logPath, obj, serviceActionType, randomKey);
        }
    }
}