﻿using B1Utility.Enum;
using B1Utility.Utils;
using CommonControl.Common;
using CommonControl.POCO.Class;
using CommonControl.POCO.Condition;
using CommonControl.POCO.Request;
using CommonControl.POCO.Response;
using FrontControl.Lookup;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WebSalesApi.Models.ServiceBaseImpl;

namespace WebSalesApi.Models.ActionImpl
{
    public class GetReportInventoryStatusDocumentImpl : WebServiceBase
    {
        protected override object requestToObject(IRequest request)
        {
            InventoryStatusRequest req = (InventoryStatusRequest)request;
            return req.Condition;
        }

        protected override bool validateObject(object request)
        {
            ReportInventoryStatusCondition condition = (ReportInventoryStatusCondition)request;

            bool res = false;

            if (!StringUtils.isBlankOrNull(condition.ItemCode)
                && !StringUtils.isBlankOrNull(condition.Company))
            {
                res = true;
            }

            return res;
        }

        protected override IResponse implement(object obj)
        {
            ReportInventoryStatusCondition condition = (ReportInventoryStatusCondition)obj;

            List<InventoryStatusDocument> invStatusList = LookupUtils.Instance.getReportServiceImpl().getReportInventoryStatusDocument(condition);

            GetReportInventoryStatusDocumentResponse response = new GetReportInventoryStatusDocumentResponse();
            response.Response = invStatusList;

            return response;
        }

        protected override void afterImplement(IResponse obj)
        {
        }

        protected override void logImplement(object obj, ServiceActionType serviceActionType)
        {
        }
    }
}