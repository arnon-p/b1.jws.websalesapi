﻿using B1Utility.Enum;
using B1Utility.Utils;
using CommonControl.Common;
using CommonControl.POCO.Class;
using CommonControl.POCO.Condition;
using CommonControl.POCO.Request;
using CommonControl.POCO.Response;
using FrontControl.Lookup;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WebSalesApi.Models.ServiceBaseImpl;

namespace WebSalesApi.Models.ActionImpl
{
    public class GetReportBathNumberTransactionImpl : WebServiceBase
    {
        protected override object requestToObject(IRequest request)
        {
            BatchNumberTransactionReportRequest req = (BatchNumberTransactionReportRequest)request;
            return req.Condition;
        }

        protected override bool validateObject(object request)
        {
            ReportBatchNumberTransactionCondition condition = (ReportBatchNumberTransactionCondition)request;
            bool res = false;

            if ((!StringUtils.isBlankOrNull(condition.ItemCodeFrom)
                || !StringUtils.isBlankOrNull(condition.ItemCodeTo)
                || condition.ItmsGrpCod > 0)
                && !StringUtils.isBlankOrNull(condition.Company))
            {
                res = true;
            }

            return res;
        }

        protected override IResponse implement(object obj)
        {
            ReportBatchNumberTransactionCondition condition = (ReportBatchNumberTransactionCondition)obj;

            List<BatchNumberTransactionReport> batchList = LookupUtils.Instance.getReportServiceImpl().getReportBatchNumberTransaction(condition);

            GetReportBatchNumberTransactionResponse response = new GetReportBatchNumberTransactionResponse();
            response.Response = batchList;

            return response;
        }

        protected override void afterImplement(IResponse obj)
        {
        }

        protected override void logImplement(object obj, ServiceActionType serviceActionType)
        {
        }
    }
}