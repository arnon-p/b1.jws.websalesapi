﻿using B1Utility.DataAccess;
using B1Utility.Enum;
using B1Utility.POCO;
using B1Utility.Utils;
using CommonControl.Common;
using CommonControl.POCO.Common;
using CommonControl.POCO.Condition;
using CommonControl.POCO.Request;
using CommonControl.POCO.Response;
using FrontControl.Lookup;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WebSalesApi.Models.ServiceBaseImpl;

namespace WebSalesApi.Models.ActionImpl
{
    public class GetConstantCompanyBigDataImpl : WebServiceBase
    {
        protected override object requestToObject(IRequest request)
        {
            return request;
        }

        protected override bool validateObject(object request)
        {
            ConstantCompanyRequest req = (ConstantCompanyRequest)request;
            return !StringUtils.isBlankOrNull(req.Company);
        }

        protected override IResponse implement(object obj)
        {
            ConstantCompanyRequest req = (ConstantCompanyRequest)obj;

            GetConstantCompanyBigDataResponse response = new GetConstantCompanyBigDataResponse();

            response.UomGroupConverter = LookupUtils.Instance.getCommonServiceImpl().getUomGroupConverter(req.Company);
            // WGE Constants
            response.LocationAddress = LookupUtils.Instance.getCommonServiceImpl().getLocationAddress(req.Company);
            // WGE Constants

            return response;
        }

        protected override void afterImplement(IResponse obj)
        {
        }

        protected override void logImplement(object obj, ServiceActionType serviceActionType)
        {
        }
    }
}