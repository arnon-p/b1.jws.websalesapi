﻿using B1Utility.DataAccess;
using B1Utility.Enum;
using B1Utility.Utils;
using CommonControl.Common;
using CommonControl.POCO.Class;
using CommonControl.POCO.Common;
using CommonControl.POCO.Condition;
using CommonControl.POCO.Request;
using CommonControl.POCO.Response;
using FrontControl.Lookup;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using WebSalesApi.Models.POCO;
using WebSalesApi.Models.ServiceBaseImpl;
using WebSalesApi.Models.Utils;
using ReportUtils = WebSalesApi.Models.Utils.ReportUtils;

namespace WebSalesApi.Models.ActionImpl
{
    public class GetReportOnHandCostImpl : WebServiceBase
    {
        protected override object requestToObject(IRequest request)
        {
            ReportOnHandRequest req = (ReportOnHandRequest)request;
            return req.Condition;
        }

        protected override bool validateObject(object request)
        {
            ReportOnHandCondition condition = (ReportOnHandCondition)request;

            bool res = false;

            if (!StringUtils.isBlankOrNull(condition.Company)
                && CollectionUtils.IsAny(condition.Channel)
                && CollectionUtils.IsAny(condition.Brand)
                && !StringUtils.isBlankOrNull(condition.ItemCodeFrom)
                && !StringUtils.isBlankOrNull(condition.ItemCodeTo)
                && !StringUtils.isBlankOrNull(condition.ReportName)
                && CollectionUtils.IsAny(condition.WhsCode))
            {
                res = true;
            }

            return res;
        }

        protected override IResponse implement(object obj)
        {
            ReportOnHandCondition condition = (ReportOnHandCondition)obj;

            condition.ExportType = CrystalDecisions.Shared.ExportFormatType.Excel;

            CrystalReportOptions rptOption = new CrystalReportOptions();
            rptOption.DbUsername = ServiceDataAccess.Instance.CompanyDic[condition.Company].CompanyInfo.DbUserName;
            rptOption.DbPassword = ServiceDataAccess.Instance.CompanyDic[condition.Company].CompanyInfo.DbPassword;
            rptOption.ExportFormatType = condition.ExportType;
            rptOption.RptPath = ProjectConstantUtils.getCrystalReportPath(condition.Company, CommonControl.Constants.ServiceConstants.ONHAND_COST_REPORT, CommonControl.Constants.ServiceConstants.ONHAND_COST_REPORT + ".rpt");
            //rptOption.RptPath = ServiceDataAccess.Instance.getReportFilePath(condition.Company, CommonControl.Constants.ServiceConstants.ONHAND_COST_REPORT, CommonControl.Constants.ServiceConstants.ONHAND_COST_REPORT);
            //rptOption.RptPath = ServiceDataAccess.Instance.getReportFilePath(condition.Company, CommonControl.Constants.ServiceConstants.ONHAND_COST_REPORT, condition.ReportName);

            rptOption.ParamValues = new List<ParamValue>();
            rptOption.ParamValues.Add(new ParamValue("Channel", condition.Channel.ToArray()));
            rptOption.ParamValues.Add(new ParamValue("Brand@with brand(Value, Brand) as (select distinct u_wge_brand, u_wge_brand from oitm) select * from brand where Brand is not null and Brand not like '-' order by Brand", condition.Brand.ToArray()));
            rptOption.ParamValues.Add(new ParamValue("ItemCodeFrom@Select * From OITM", condition.ItemCodeFrom));
            rptOption.ParamValues.Add(new ParamValue("ItemCodeTo@Select * From OITM", condition.ItemCodeTo));
            rptOption.ParamValues.Add(new ParamValue("Whse@Select * From OWHS", condition.WhsCode.ToArray()));

            //if (B1DataAccess.Instance.IsHana)
            //{
            //    rptOption.ParamValues.Add("Schema@", ServiceDataAccess.Instance.getCompanyDB(condition.Company));
            //}

            string fileName = ReportUtils.getFileNameExport(CommonControl.Constants.ServiceConstants.ONHAND_COST_REPORT, condition.ExportType);
            rptOption.DestFilePath = Path.Combine(ProjectConstantUtils.ReportPath, fileName);

            ReportUtils.saveToFile(rptOption);

            IDownloadResponse response = new IDownloadResponse();
            response.FileName = fileName;

            return response;
        }

        protected override void afterImplement(IResponse obj)
        {
        }

        protected override void logImplement(object obj, ServiceActionType serviceActionType)
        {
        }
    }
}