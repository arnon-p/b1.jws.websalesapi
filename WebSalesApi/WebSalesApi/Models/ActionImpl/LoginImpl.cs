﻿using B1Utility.Enum;
using B1Utility.Utils;
using CommonControl.Common;
using CommonControl.Enum;
using CommonControl.POCO.Class;
using CommonControl.POCO.Request;
using CommonControl.POCO.Response;
using FrontControl.Lookup;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using WebSalesApi.Models.ServiceBaseImpl;
using WebSalesApi.Models.Utils;

namespace WebSalesApi.Models.ActionImpl
{
    public class LoginImpl : WebServiceBase
    {
        protected override object requestToObject(IRequest request)
        {
            return request;
        }

        protected override bool validateObject(object request)
        {
            LoginRequest req = (LoginRequest)request;

            bool res = false;

            if (!StringUtils.isBlankOrNull(req.Company)
                && !StringUtils.isBlankOrNull(req.UserName)
                && !StringUtils.isBlankOrNull(req.UserPassword))
            {
                res = true;
            }

            return res;
        }

        protected override IResponse implement(object obj)
        {
            LoginRequest req = (LoginRequest)obj;

            LoginResponse response = new LoginResponse();

            string webAdminRes = WebServiceUtils.Instance.sendData(ProjectConstantUtils.WebAdminAuthenApi, JsonUtils.toJson(req, true));

            if (StringUtils.isBlankOrNull(webAdminRes))
            {
                response.Status = ResponseStatus.AUTH_NOTPASS;
            }
            else
            {
                WebAdminUserObj userObj = JsonUtils.fromJson<WebAdminUserObj>(webAdminRes, true);

                if (ResponseStatus.SUCCESS.Equals(userObj.Status))
                {
                    if (CollectionUtils.IsAny(userObj.Bean.ProfileBean.CompanyList))
                    {
                        if (userObj.Bean.ProfileBean.CompanyList.Where(x => req.Company.ToLower().Equals(x.CompanyCode.ToLower())).Count() > 0)
                        {
                            userObj.SapProfile = LookupUtils.Instance.getCommonServiceImpl().getSapProfile(req.Company, userObj.Bean.ProfileBean);

                            if (userObj.SapProfile != null)
                            {
                                if (UserUtils.Users.ContainsKey(req.UserName))
                                {
                                    WebAdminUserObj user = UserUtils.Users[req.UserName];

                                    if (user.Company.Equals(req.Company))
                                    {
                                        response.Response = user;
                                    }
                                    else
                                    {
                                        userObj.Company = req.Company;
                                        userObj.Token = EncryptionUtils.createToken(req.UserName);
                                        UserUtils.login(userObj);
                                        response.Response = userObj;
                                    }
                                }
                                else
                                {
                                    userObj.Company = req.Company;
                                    userObj.Token = EncryptionUtils.createToken(req.UserName);
                                    UserUtils.login(userObj);
                                    response.Response = userObj;
                                }
                            }
                            else
                            {
                                // sap profile not found
                                response.Status = ResponseStatus.AU_NP;
                            }
                        }
                        else
                        {
                            // access company not pass
                            response.Status = ResponseStatus.AU_NP;
                        }
                    }
                    else
                    {
                        // company not found
                        response.Status = ResponseStatus.AU_NP;
                    }
                }
                else
                {
                    response.Status = userObj.Status;
                }
            }

            return response;
        }

        protected override void afterImplement(IResponse obj)
        {
        }

        protected override void logImplement(object obj, ServiceActionType serviceActionType)
        {
        }
    }
}