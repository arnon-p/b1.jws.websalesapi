﻿using B1Utility.DataAccess;
using B1Utility.Enum;
using B1Utility.POCO;
using B1Utility.Utils;
using CommonControl.Common;
using CommonControl.POCO.Common;
using CommonControl.POCO.Condition;
using CommonControl.POCO.Request;
using CommonControl.POCO.Response;
using FrontControl.Lookup;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WebSalesApi.Models.ServiceBaseImpl;

namespace WebSalesApi.Models.ActionImpl
{
    public class GetConstantImpl : WebServiceBase
    {
        protected override object requestToObject(IRequest request)
        {
            return request;
        }

        protected override bool validateObject(object request)
        {
            return true;
        }

        protected override IResponse implement(object obj)
        {
            GetConstantResponse response = new GetConstantResponse();

            response.Company = new List<CompanyLogin>();
            foreach(KeyValuePair<string, SapCompany> data in ServiceDataAccess.Instance.CompanyDic)
            {
                response.Company.Add(new CompanyLogin(data.Key, data.Value.CompanyInfo.CompanyDb));
            }

            return response;
        }

        protected override void afterImplement(IResponse obj)
        {
        }

        protected override void logImplement(object obj, ServiceActionType serviceActionType)
        {
        }
    }
}