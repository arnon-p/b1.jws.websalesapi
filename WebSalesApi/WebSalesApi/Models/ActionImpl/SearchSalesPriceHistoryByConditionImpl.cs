﻿using B1Utility.Enum;
using B1Utility.Utils;
using CommonControl.Common;
using CommonControl.Enum;
using CommonControl.POCO.Class;
using CommonControl.POCO.Common;
using CommonControl.POCO.Condition;
using CommonControl.POCO.Request;
using CommonControl.POCO.Response;
using FrontControl.Lookup;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WebSalesApi.Models.ServiceBaseImpl;

namespace WebSalesApi.Models.ActionImpl
{
    public class SearchSalesPriceHistoryByConditionImpl : WebServiceBase
    {
        protected override object requestToObject(IRequest request)
        {
            SearchSalesPriceHistoryRequest req = (SearchSalesPriceHistoryRequest)request;

            return req.Condition;
        }

        protected override bool validateObject(object request)
        {
            SalesPriceHistoryCondition condition = (SalesPriceHistoryCondition)request;

            bool res = false;

            if (!StringUtils.isBlankOrNull(condition.Company)
                && !StringUtils.isBlankOrNull(condition.ItemCode)
                && !StringUtils.isBlankOrNull(condition.CardCode))
            {
                res = true;
            }

            return res;
        }

        protected override IResponse implement(object obj)
        {
            SalesPriceHistoryCondition condition = (SalesPriceHistoryCondition)obj;
            // CR Authorize
            condition.UserSalesEmployee = userObj.SapProfile.SalesEmployee.Select(x => x.SlpCode).ToList();

            List<SalesPriceHistory> salesPriceHistoryList = LookupUtils.Instance.getDocumentServiceImpl().searchSalesPriceHistoryByCondition(condition);

            SearchSalesPriceHistoryResponse response = new SearchSalesPriceHistoryResponse();
            response.Response = salesPriceHistoryList;

            return response;
        }

        protected override void afterImplement(IResponse obj)
        {
        }

        protected override void logImplement(object obj, ServiceActionType serviceActionType)
        {
        }
    }
}