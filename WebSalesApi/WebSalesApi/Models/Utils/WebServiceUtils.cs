﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Web;
using System.IO;
using System.Web.Mvc;
using CommonControl.Common;
using CommonControl.Enum;
using System.Net.Http.Formatting;
using System.Text;
using B1Utility.DataAccess;

namespace WebSalesApi.Models.Utils
{
    public class WebServiceUtils
    {
        private static WebServiceUtils instance;
        private WebServiceUtils() { }

        public static WebServiceUtils Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new WebServiceUtils();
                }
                return instance;
            }
        }

        public HttpResponseMessage createResponse(HttpRequestMessage request, IResponse res)
        {
            HttpResponseMessage response = null;

            if (ResponseStatus.ERROR.Equals(res.Status))
            {
                response = request.CreateResponse(HttpStatusCode.InternalServerError, res);
            }
            else
            {
                response = request.CreateResponse(HttpStatusCode.OK, res);
            }

            return response;
        }

        public HttpResponseMessage createResponse(HttpRequestMessage request, IResponse res, bool isDownload)
        {
            HttpResponseMessage response = null;

            if (ResponseStatus.ERROR.Equals(res.Status))
            {
                response = request.CreateResponse(HttpStatusCode.InternalServerError, res);
            }
            else
            {
                //if (isDownload)
                //{
                //    IDownloadResponse download = (IDownloadResponse)res;
                //    response = request.CreateResponse(HttpStatusCode.OK);
                //    response.Content = new StreamContent(download.StreamContent);
                //    response.Content.Headers.ContentType = new System.Net.Http.Headers.MediaTypeHeaderValue(MimeMapping.GetMimeMapping(download.FileName));
                //    //response.Content.Headers.ContentType = new System.Net.Http.Headers.MediaTypeHeaderValue("application/octet-stream");  
                //    response.Content.Headers.ContentDisposition = new System.Net.Http.Headers.ContentDispositionHeaderValue("attachment");
                //    response.Content.Headers.ContentDisposition.FileName = download.FileName;
                //}
                //else
                //{
                //    response = request.CreateResponse(HttpStatusCode.OK, res);
                //}
            }

            return response;
        }

        public byte[] readFully(Stream input)
        {
            byte[] buffer = new byte[16 * 1024];
            using (MemoryStream ms = new MemoryStream())
            {
                int read;
                while ((read = input.Read(buffer, 0, buffer.Length)) > 0)
                {
                    ms.Write(buffer, 0, read);
                }
                return ms.ToArray();
            }
        }

        public string sendData(string url, string jsonData)
        {
            string response = null;

            try
            {
                var client = new HttpClient();
                client.BaseAddress = new Uri(url);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                //client.DefaultRequestHeaders.Add(ConfigurationManager.AppSettings["SapSecurityHeader"], ConfigurationManager.AppSettings["SapSecurityToken"]);
                HttpContent content = new StringContent(jsonData, Encoding.UTF8, "application/json");
                //content.Headers.Add("uuid", ConfigurationManager.AppSettings["RangyApiKey"]);
                HttpResponseMessage res = client.PostAsync(url, content).Result;

                if (!res.IsSuccessStatusCode)
                {
                    var _error = res.Content.ReadAsStringAsync().Result;
                    throw new Exception(_error);
                }
                else
                {
                    response = res.Content.ReadAsStringAsync().Result;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return response;
        }

        public string getData(string url)
        {
            string response = null;

            try
            {
                var client = new HttpClient();
                client.BaseAddress = new Uri(url);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                //client.DefaultRequestHeaders.Add(ConfigurationManager.AppSettings["SapSecurityHeader"], ConfigurationManager.AppSettings["SapSecurityToken"]);
                HttpResponseMessage res = client.GetAsync(url).Result;

                if (!res.IsSuccessStatusCode)
                {
                    var _error = res.Content.ReadAsStringAsync().Result;
                    throw new Exception(_error);
                }
                else
                {
                    response = res.Content.ReadAsStringAsync().Result;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return response;
        }

        public HttpResponseMessage getFiles(string company, string fileName, HttpRequestMessage request)
        {
            string pathFile = ServiceDataAccess.Instance.getCompanyAttachPath(company) + fileName;

            return responseFiles(fileName, pathFile, request);
        }

        public HttpResponseMessage getTempFiles(string tempFileName, HttpRequestMessage request)
        {
            string pathFile = Path.Combine(ProjectConstantUtils.TempPath, tempFileName);

            return responseFiles(tempFileName, pathFile, request);
        }

        public HttpResponseMessage getReportFiles(string reportFileName, HttpRequestMessage request)
        {
            string pathFile = Path.Combine(ProjectConstantUtils.ReportPath, reportFileName);

            return responseFiles(reportFileName, pathFile, request);
        }

        public HttpResponseMessage getBrochureFiles(string company, string brochureFileName, HttpRequestMessage request)
        {
            string pathFile = Path.Combine(ProjectConstantUtils.BrochurePath, company, brochureFileName);

            return responseFiles(brochureFileName, pathFile, request);
        }

        private static HttpResponseMessage responseFiles(string fileName, string pathFile, HttpRequestMessage request)
        {
            HttpResponseMessage response = null;

            if (File.Exists(pathFile))
            {
                //converting file into bytes array  
                var dataBytes = File.ReadAllBytes(pathFile);
                //adding bytes to memory stream   
                var dataStream = new MemoryStream(dataBytes);

                //var dataStream = new FileStream(pathFile, FileMode.Open, FileAccess.Read);

                response = request.CreateResponse(HttpStatusCode.OK);
                response.Content = new StreamContent(dataStream);
                response.Content.Headers.ContentType = new System.Net.Http.Headers.MediaTypeHeaderValue(MimeMapping.GetMimeMapping(fileName));
                //response.Content.Headers.ContentType = new System.Net.Http.Headers.MediaTypeHeaderValue("application/octet-stream");  

                if (ProjectConstantUtils.ShowInlineExt.Contains(Path.GetExtension(fileName).Substring(1).ToLower()))
                {
                    response.Content.Headers.ContentDisposition = new System.Net.Http.Headers.ContentDispositionHeaderValue("inline");
                }
                else
                {
                    response.Content.Headers.ContentDisposition = new System.Net.Http.Headers.ContentDispositionHeaderValue("attachment");
                }

                response.Content.Headers.ContentDisposition.FileName = fileName;
            }
            else
            {
                response = request.CreateResponse(HttpStatusCode.InternalServerError, "File not found : " + pathFile);
            }

            return response;
        }
    }
}