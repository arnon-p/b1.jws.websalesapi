﻿using B1Utility.Utils;
using CommonControl.POCO.Class;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebSalesApi.Models.Utils
{
    public static class UserUtils
    {
        private static Dictionary<string, WebAdminUserObj> users;

        public static Dictionary<string, WebAdminUserObj> Users { get => users; set => users = value; }

        static UserUtils()
        {
            Users = new Dictionary<string, WebAdminUserObj>();
        }

        public static WebAdminUserObj validateToken(string token)
        {
            WebAdminUserObj res = null;

            try
            {
                if (users.Where(x => x.Value.Token.Equals(token)).Count() == 1)
                {
                    res = users[users.Where(x => x.Value.Token.Equals(token)).Select(x => x.Key).FirstOrDefault()];
                }
            }
            catch (Exception ex)
            {
                ExceptionUtils.handleException(ex);
            }

            return res;
        }

        public static void login(WebAdminUserObj userObj)
        {
            try
            {
                if (users.ContainsKey(userObj.Bean.UserName))
                {
                    users.Remove(userObj.Bean.UserName);
                }
                users.Add(userObj.Bean.UserName, userObj);
            }
            catch(Exception ex)
            {
                ExceptionUtils.handleException(ex);
            }
        }

        public static void logout(string token)
        {
            try
            {
                WebAdminUserObj user = validateToken(token);

                if (user != null)
                {
                    users.Remove(user.Bean.UserName);
                }
            }
            catch (Exception ex)
            {
                ExceptionUtils.handleException(ex);
            }
        }
    }
}