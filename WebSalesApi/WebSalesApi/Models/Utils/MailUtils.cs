﻿using B1Utility.Utils;
using CommonControl.POCO.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Web;

namespace WebSalesApi.Models.Utils
{
    public static class MailUtils
    {
        //private static SmtpClient smtpServer;

        static MailUtils()
        {
            //smtpServer = new SmtpClient();
            //smtpServer.Host = ProjectConstantUtils.EmailSenderInfo.SmtpHost;
            //smtpServer.Port = ProjectConstantUtils.EmailSenderInfo.SmtpPort;
            //smtpServer.Credentials = new NetworkCredential(ProjectConstantUtils.EmailSenderInfo.EmailUser, ProjectConstantUtils.EmailSenderInfo.EmailPass);
            //smtpServer.EnableSsl = true;
            //smtpServer.UseDefaultCredentials = false;
        }
        public static void sendEmail(EmailMessageInfo mailMessage, bool isError = false)
        {
            try
            {
                //MailMessage mail = new MailMessage();
                //mail.From = new MailAddress(ProjectConstantUtils.EmailSenderInfo.EmailUser);

                //foreach(string addr in mailMessage.Mailto)
                //{
                //    mail.To.Add(addr);
                //}

                //mail.Subject = mailMessage.Subject;
                //mail.Body = mailMessage.Body;

                //smtpServer.Send(mail);
                //smtpServer.SendAsync(mail, DateTimeUtils.getCurrentTimeStamp());

                using (MailMessage mail = new MailMessage())
                {
                    mail.From = new MailAddress(ProjectConstantUtils.EmailSenderInfo.EmailUser);
                    mail.To.Add(mailMessage.Email);

                    if (isError)
                    {
                        if (CollectionUtils.IsAny(mailMessage.Mailto))
                        {
                            foreach (string mailCC in mailMessage.Mailto)
                            {
                                mail.CC.Add(mailCC);
                            }
                        }
                    }
                    //foreach (string addr in mailMessage.Mailto)
                    //{
                    //    mail.To.Add(addr);
                    //}

                    mail.Subject = mailMessage.Subject;
                    mail.Body = mailMessage.Body;
                    mail.IsBodyHtml = true;

                    using (SmtpClient smtp = new SmtpClient(ProjectConstantUtils.EmailSenderInfo.SmtpHost, ProjectConstantUtils.EmailSenderInfo.SmtpPort))
                    {
                        //smtp.UseDefaultCredentials = false;
                        //smtp.DeliveryMethod = SmtpDeliveryMethod.Network;
                        smtp.EnableSsl = true;
                        smtp.Credentials = new NetworkCredential(ProjectConstantUtils.EmailSenderInfo.EmailUser, ProjectConstantUtils.EmailSenderInfo.EmailPass);
                        smtp.Send(mail);
                    }
                }
            }
            catch(Exception ex)
            {
                ExceptionUtils.handleException(ex);
            }
        }
    }
}