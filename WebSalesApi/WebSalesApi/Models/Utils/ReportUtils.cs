﻿using B1Utility.DataAccess;
using B1Utility.Enum;
using B1Utility.Utils;
using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.ReportAppServer.ClientDoc;
using CrystalDecisions.ReportAppServer.Controllers;
using CrystalDecisions.Shared;
using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing.Printing;
using System.IO;
using System.Linq;
using System.Text;
using WebSalesApi.Models.POCO;

namespace WebSalesApi.Models.Utils
{
    public static class ReportUtils
    {
        public static void saveToFile(CrystalReportOptions options)
        {
            try
            {
                ReportDocument reportDocument = initReportDocument(options);
                reportDocument.ExportToDisk(options.ExportFormatType, options.DestFilePath);
                reportDocument.Dispose();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static ReportDocument initReportDocument(CrystalReportOptions options)
        {
            ReportDocument reportDocument = null;

            try
            {
                if (String.IsNullOrEmpty(options.RptPath))
                {
                    throw new Exception("Crystals Report file is null");
                }

                reportDocument = new ReportDocument();

                reportDocument.Load(options.RptPath);

                if (!StringUtils.isBlankOrNull(options.DbUsername) && !StringUtils.isBlankOrNull(options.DbPassword))
                {
                    if (InterfaceDBDataAccess.Instance.IsB1Hana)
                    {
                        reportDocument.SetDatabaseLogon(options.DbUsername, options.DbPassword);
                    }
                    else
                    {
                        reportDocument.SetDatabaseLogon(options.DbUsername, options.DbPassword);
                    }
                }
                else
                {
                    reportDocument.SetDataSource(options.DataTable);
                }

                if (CollectionUtils.IsAny(options.ParamValues))
                {
                    foreach (ParamValue param in options.ParamValues)
                    {
                        reportDocument.SetParameterValue(param.Key, param.Value);

                        if (param.IsNonValue)
                        {
                            reportDocument.ParameterFields[param.Key].CurrentValues.IsNoValue = true;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return reportDocument;
        }

        public static string getExtensionByExportType(ExportFormatType exportType)
        {
            string ext = String.Empty;
            try
            {
                switch(exportType)
                {
                    case ExportFormatType.PortableDocFormat:
                        ext = "pdf";
                        break;
                    case ExportFormatType.Excel:
                        ext = "xls";
                        break;
                    case ExportFormatType.ExcelWorkbook:
                        ext = "xlsx";
                        break;
                    case ExportFormatType.WordForWindows:
                        ext = "doc";
                        break;
                }
            }
            catch (Exception ex)
            {
                ExceptionUtils.handleException(ex);
            }

            return ext;
        }

        public static string getFileNameExport(string reportName, CrystalDecisions.Shared.ExportFormatType exportType)
        {
            return StringUtils.format("{0}_{1}.{2}", reportName, StringUtils.randomAlphaNumeric(16), getExtensionByExportType(exportType));
        }
    }
}