﻿using B1Utility.Enum;
using B1Utility.Utils;
using CommonControl.POCO.Common;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;

namespace WebSalesApi.Models.Utils
{
    public static class ProjectConstantUtils
    {
        private static string securityTokenApi;
        private static string attachPath;
        private static string basePath;
        private static string tempPath;
        private static string reportPath;
        private static string crystalReportPath;
        private static string brochurePath;
        private static string webAdminAuthenApi;
        private static List<string> showInlineExt;
        private static EmailServerInfo emailSenderInfo;
        private static string webSalesUrl;

        static ProjectConstantUtils()
        {
            showInlineExt = new List<string>();
            showInlineExt.Add("pdf");
            showInlineExt.Add("png");
            showInlineExt.Add("jpeg");
            showInlineExt.Add("jpg");
        }

        public static void initCommon(string pSecurityTokenApi)
        {
            SecurityTokenApi = pSecurityTokenApi;
        }

        public static string SecurityTokenApi { get => securityTokenApi; set => securityTokenApi = value; }
        public static string AttachPath { get => attachPath; set => attachPath = value; }
        public static string BasePath { get => basePath; set => basePath = value; }
        public static string TempPath { get => tempPath; set => tempPath = value; }
        public static string ReportPath { get => reportPath; set => reportPath = value; }
        public static string CrystalReportPath { get => crystalReportPath; set => crystalReportPath = value; }
        public static string BrochurePath { get => brochurePath; set => brochurePath = value; }
        public static string WebAdminAuthenApi { get => webAdminAuthenApi; set => webAdminAuthenApi = value; }
        public static List<string> ShowInlineExt { get => showInlineExt; set => showInlineExt = value; }
        public static EmailServerInfo EmailSenderInfo { get => emailSenderInfo; set => emailSenderInfo = value; }
        public static string WebSalesUrl { get => webSalesUrl; set => webSalesUrl = value; }

        public static string getCrystalReportPath(string company, string reportType, string fileName)
        {
            return Path.Combine(crystalReportPath, company, reportType, fileName);
        }
    }
}