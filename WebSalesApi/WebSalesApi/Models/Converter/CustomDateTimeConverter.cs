﻿using B1Utility.Utils;
using Newtonsoft.Json.Converters;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;

namespace WebSalesApi.Models.Converter
{
    public class CustomDateTimeConverter : IsoDateTimeConverter
    {
        private CultureInfo en = new CultureInfo("en-US");
        private CultureInfo th = new CultureInfo("th-TH");
        //private string formatDate = "yyyy-MM-dd HH:mm:ss";

        //IsoDateTimeConverter
        public CustomDateTimeConverter()
        {
            //base.DateTimeFormat = formatDate;
            base.Culture = en;
        }

        //DateTimeConverterBase
        public override object ReadJson(Newtonsoft.Json.JsonReader reader, Type objectType, object existingValue, Newtonsoft.Json.JsonSerializer serializer)
        {
            return DateTimeUtils.unixTimeToDateTime((long)reader.Value);
        }

        public override void WriteJson(Newtonsoft.Json.JsonWriter writer, object value, Newtonsoft.Json.JsonSerializer serializer)
        {
            writer.WriteValue(DateTimeUtils.convertToUnixTime((DateTime)value));
        }
    }
}