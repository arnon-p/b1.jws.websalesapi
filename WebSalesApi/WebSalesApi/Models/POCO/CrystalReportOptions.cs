﻿using CrystalDecisions.Shared;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;

namespace WebSalesApi.Models.POCO
{
    public class CrystalReportOptions
    {
        private string rptPath;
        private string destFilePath;
        private DataTable dataTable;
        private PrinterOptions printerOptions;
        private ExportFormatType exportFormatType;
        private string dbUsername;
        private string dbPassword;
        private List<ParamValue> paramValues;
        private bool isReplacePrintQueuePrinterName;

        public CrystalReportOptions()
        {
            exportFormatType = ExportFormatType.PortableDocFormat;
        }

        public string RptPath
        {
            get { return rptPath; }
            set { rptPath = value; }
        }

        public string DestFilePath
        {
            get { return destFilePath; }
            set { destFilePath = value; }
        }

        public DataTable DataTable
        {
            get { return dataTable; }
            set { dataTable = value; }
        }

        public PrinterOptions PrinterOptions
        {
            get { return printerOptions; }
            set { printerOptions = value; }
        }

        public ExportFormatType ExportFormatType
        {
            get { return exportFormatType; }
            set { exportFormatType = value; }
        }

        public string DbUsername
        {
            get { return dbUsername; }
            set { dbUsername = value; }
        }

        public string DbPassword
        {
            get { return dbPassword; }
            set { dbPassword = value; }
        }

        public List<ParamValue> ParamValues
        {
            get { return paramValues; }
            set { paramValues = value; }
        }

        //public Dictionary<string, object> ParamValues
        //{
        //    get { return ParamValues1; }
        //    set { ParamValues1 = value; }
        //}

        public bool IsReplacePrintQueuePrinterName
        {
            get { return isReplacePrintQueuePrinterName; }
            set { isReplacePrintQueuePrinterName = value; }
        }
    }
}
