﻿using CrystalDecisions.Shared;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace WebSalesApi.Models.POCO
{
    public class PrinterOptions
    {
        private string printerName;
        private PaperOrientation paperOrientation;
        private PaperSize paperSize;
        private int nCopies;
        private int startPageN;
        private int endPageN;

        public PrinterOptions()
        {
            paperOrientation = PaperOrientation.Portrait;
            paperSize = PaperSize.PaperA4;
            nCopies = 1;
            startPageN = 1;
            endPageN = 1000;
        }

        public string PrinterName
        {
            get { return printerName; }
            set { printerName = value; }
        }

        public PaperOrientation PaperOrientation
        {
            get { return paperOrientation; }
            set { paperOrientation = value; }
        }

        public PaperSize PaperSize
        {
            get { return paperSize; }
            set { paperSize = value; }
        }

        public int NCopies
        {
            get { return nCopies; }
            set { nCopies = value; }
        }

        public int StartPageN
        {
            get { return startPageN; }
            set { startPageN = value; }
        }

        public int EndPageN
        {
            get { return endPageN; }
            set { endPageN = value; }
        }
    }
}
