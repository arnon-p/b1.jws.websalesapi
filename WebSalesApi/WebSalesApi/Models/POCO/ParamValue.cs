﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebSalesApi.Models.POCO
{
    public class ParamValue
    {
        private string key;
        private object value;
        private bool isNonValue;

        public ParamValue()
        {
        }

        public ParamValue(string pKey, object pValue, bool pNonValue = false)
        {
            this.key = pKey;
            this.value = pValue;
            this.isNonValue = pNonValue;
        }

        public string Key { get => key; set => key = value; }
        public object Value { get => value; set => this.value = value; }
        public bool IsNonValue { get => isNonValue; set => isNonValue = value; }
    }
}